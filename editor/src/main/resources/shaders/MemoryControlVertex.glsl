attribute vec2 position0;

uniform mat4 matProjection;

void main() {
    gl_Position = matProjection * vec4(position0, 0.0, 1.0);
}
