#version 150

in vec2 texCoord;

uniform sampler2D skyTexture;

void main() {
    gl_FragColor = texture(skyTexture, texCoord);
}
