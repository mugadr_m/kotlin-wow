#version 150

in vec3 position0;

out float depth;

uniform mat4 matProjection;
uniform mat4 matView;
uniform vec3 cameraPosition;

void main() {
    vec4 position = matProjection * matView * vec4(position0, 1.0f);
    gl_Position = position;
    depth = length(position0 - cameraPosition);
}