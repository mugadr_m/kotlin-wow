#version 150

in vec3 position0;
in vec3 normal0;
in vec2 texCoord0;
in vec2 texCoord1;
in vec4 color0;

out vec3 normal;
out vec2 texCoord;
out vec2 alphaCoord;
out vec4 vertexColor;
out float depth;
out vec3 worldPosition;

uniform mat4 matView;
uniform mat4 matProjection;
uniform vec3 cameraPosition;

void main() {
    normal = normal0;
    texCoord = texCoord0;
    alphaCoord = texCoord1;
    vertexColor = color0;
    depth = length(cameraPosition - position0);
    worldPosition = position0;

    gl_Position = matProjection * matView * vec4(position0, 1.0f);
}
