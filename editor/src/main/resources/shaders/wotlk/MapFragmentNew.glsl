#version 150

in vec3 normal;
in vec2 texCoord;
in vec2 alphaCoord;
in vec4 vertexColor;
in float depth;
in vec3 worldPosition;

out vec4 targetColor;

const vec3 sunDirection = vec3(1.0, 1.0, -1.0f);
uniform vec4 ambientColor;
uniform vec4 diffuseColor;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;

uniform sampler2D texture0_s;
uniform sampler2D texture1_s;
uniform sampler2D texture2_s;
uniform sampler2D texture3_s;

uniform sampler2D baseTexture;

uniform vec3 clipParameters;
uniform vec4 fogColor;

uniform vec4 specularFactors;
uniform vec3 cameraPosition;

float getSpecularLight() {
    vec3 v = normalize(cameraPosition.xyz - worldPosition.xyz);
    vec3 h = normalize(-normalize(sunDirection) + v);
    return max(0, dot(normal, h));
}

void main() {
    vec4 baseColor = texture(baseTexture, alphaCoord);
    if(baseColor.r < 0.3f) {
        discard;
    }

    vec4 color0 = texture(texture0, texCoord);
    vec4 color1 = texture(texture1, texCoord);
    vec4 color2 = texture(texture2, texCoord);
    vec4 color3 = texture(texture3, texCoord);

    vec4 c0_s = texture(texture0_s, texCoord);
    vec4 c1_s = texture(texture1_s, texCoord);
    vec4 c2_s = texture(texture2_s, texCoord);
    vec4 c3_s = texture(texture3_s, texCoord);

    float specularLight = getSpecularLight();
    vec3 lightDir = normalize(sunDirection);
    vec3 norm = normalize(normal);
    float light = dot(norm, lightDir);
    if (light < 0.0)
        light = 0.0;
    if (light > 0.5)
        light = 0.5 + (light - 0.5) * 0.65;

    vec3 lightColor = (ambientColor + light * diffuseColor).rgb;

    vec3 spc0 = c0_s.a * c0_s.rgb * pow(specularLight, 8) * specularFactors.x;
    vec3 spc1 = c1_s.a * c1_s.rgb * pow(specularLight, 8) * specularFactors.y;
    vec3 spc2 = c2_s.a * c2_s.rgb * pow(specularLight, 8) * specularFactors.z;
    vec3 spc3 = c3_s.a * c3_s.rgb * pow(specularLight, 8) * specularFactors.w;

    spc0 += (1.0f - specularFactors.x) * pow(specularLight, 8);
    spc1 += (1.0f - specularFactors.y) * pow(specularLight, 8);
    spc2 += (1.0f - specularFactors.z) * pow(specularLight, 8);
    spc3 += (1.0f - specularFactors.w) * pow(specularLight, 8);

    color0.rgb *= lightColor + spc0;
    color1.rgb *= lightColor + spc1;
    color2.rgb *= lightColor + spc2;
    color3.rgb *= lightColor + spc3;

    color0.rgb = clamp(color0.rgb, 0.0f, 1.0f);
    color1.rgb = clamp(color1.rgb, 0.0f, 1.0f);
    color2.rgb = clamp(color2.rgb, 0.0f, 1.0f);
    color3.rgb = clamp(color3.rgb, 0.0f, 1.0f);

    vec4 color = (1.0 - baseColor.g - baseColor.b - baseColor.a) * color0;
    color += baseColor.g * color1;
    color += baseColor.b * color2;
    color += baseColor.a * color3;

	color.rgb *= lightColor;
	color.rgb *= vertexColor.bgr * 2.0;
	color.rgb *= baseColor.r;

    float fogFactor = (depth - clipParameters.x) / (clipParameters.y - clipParameters.x);
    fogFactor = clamp(fogFactor, 0.0f, 1.0f);
    fogFactor = pow(fogFactor, 1.5f);

    targetColor = vec4(color.rgb, 1.0);
    targetColor.rgb = fogFactor * fogColor.rgb + (1.0f - fogFactor) * targetColor.rgb;
}
