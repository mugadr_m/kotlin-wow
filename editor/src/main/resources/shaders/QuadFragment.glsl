uniform vec4 quadColor;

void main() {
    gl_FragColor = quadColor;
}
