in vec2 position0;
in vec2 texCoord0;

uniform mat4 matProjection;
uniform mat4 matTransform;
uniform mat4 matTranslate;
uniform mat4 globalTransform;

varying vec2 textureCoord;

void main() {
    gl_Position = matProjection * globalTransform * matTranslate * matTransform * vec4(position0, 0, 1);
    textureCoord = texCoord0;
}