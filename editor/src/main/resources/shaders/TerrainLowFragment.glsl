#version 150

in float depth;

uniform vec4 fogColor;
uniform vec2 clipParameters;

void main() {
    if((depth / clipParameters.y) < (clipParameters.x / clipParameters.y)) {
        discard;
    }

    gl_FragColor = vec4(fogColor.rgb, 1.0f);
}