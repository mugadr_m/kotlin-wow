#version 150

in vec3 position0;
in vec2 texCoord0;

out vec2 texCoord;

uniform mat4 matView;
uniform mat4 matProjection;
uniform mat4 matTransform;

void main() {
    gl_Position = matProjection * matView * matTransform * vec4(position0, 1.0);
    texCoord = texCoord0;
}
