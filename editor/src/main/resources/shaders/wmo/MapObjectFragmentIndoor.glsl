#version 150

in vec2 texCoord;
in vec3 normal;
in vec4 color;
in float depth;

out vec4 fragColor;

uniform sampler2D batchTexture;
uniform vec3 clipParameters;
uniform vec4 fogColor;

uniform float alphaMin;

const vec3 sunDirection = vec3(1.0, 1.0, -1.0f);
uniform vec4 ambientColor;
uniform vec4 diffuseColor;

vec3 getLightColor() {
    vec3 lightDir = normalize(sunDirection);
    vec3 norm = normalize(normal);
    float light = dot(norm, lightDir);
    if(light < 0.0)
        light = 0.0;
    if (light > 0.5)
    	light = 0.5 + (light - 0.5) * 0.65;

    vec3 lightColor = clamp(ambientColor + light * diffuseColor, 0.0, 1.0).rgb;
    return lightColor;
}

void main() {
    vec4 texColor = texture(batchTexture, texCoord);
    if(texColor.a < alphaMin) {
        discard;
    }

    vec3 lightColor = getLightColor();
    vec3 groupColor = color.bgr;
    float w = color.a;
    vec3 finalColor = w * lightColor + (1.0f - w) * groupColor;
    finalColor = clamp(finalColor, 0.0f, 1.0f);
    texColor.rgb *= finalColor;

    float fogFactor = (depth - clipParameters.x) / (clipParameters.y - clipParameters.x);
    fogFactor = clamp(fogFactor, 0.0f, 1.0f);
    fogFactor = pow(fogFactor, 1.5f);

    fragColor.rgb = fogFactor * fogColor.rgb + (1.0f - fogFactor) * texColor.rgb;
    fragColor.a = texColor.a;
}