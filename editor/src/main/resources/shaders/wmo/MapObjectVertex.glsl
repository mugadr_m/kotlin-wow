#version 150

in vec3 position0;
in vec2 texCoord0;
in vec3 normal0;
in vec4 color0;

uniform mat4 matView;
uniform mat4 matProjection;
uniform mat4 matTransform;
uniform mat4 matTransformTransposedInverted;
uniform vec3 cameraPosition;

out vec2 texCoord;
out vec3 normal;
out vec4 color;
out float depth;

void main() {
    vec4 posTransformed = matTransform * vec4(position0, 1.0f);
    vec3 wmoPosition = posTransformed.xyz / posTransformed.w;
    depth = length(wmoPosition - cameraPosition);

    gl_Position = matProjection * matView * posTransformed;

    texCoord = texCoord0;
    normal = normalize(mat3(matTransformTransposedInverted) * normal0);
    color = color0;
}

