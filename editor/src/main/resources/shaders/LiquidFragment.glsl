#version 150

in float depth;
in vec2 texCoord;
in float waterDepth;

out vec4 fragColor;

uniform vec3 clipParameters;
uniform vec4 fogColor;
uniform vec4 waterColorLight;
uniform vec4 waterColorDark;

uniform sampler2D waterTexture;

void main() {
    float fogFactor = (depth - clipParameters.x) / (clipParameters.y - clipParameters.x);
    fogFactor = clamp(fogFactor, 0.0f, 1.0f);
    fogFactor = pow(fogFactor, 1.5f);

    vec4 texColor = texture(waterTexture, texCoord);
    vec4 waterColorMix = mix(waterColorLight, waterColorDark, waterDepth);
    vec4 targetColor = clamp(2 * texColor + waterColorMix, 0.0, 1.0);
    targetColor.a = waterColorMix.a;
    targetColor.rgb = fogFactor * fogColor.rgb + (1.0f - fogFactor) * targetColor.rgb;
    fragColor = targetColor;
}