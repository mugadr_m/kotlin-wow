attribute vec2 position0;

uniform mat4 matTransform;
uniform mat4 matProjection;
uniform mat4 globalTransform;

void main() {
    gl_Position = matProjection * globalTransform * matTransform * vec4(position0, 0.0, 1.0);
}
