#version 150

in vec3 position0;
in vec2 texCoord0;
in float waterDepth0;

uniform mat4 matTransform;
uniform mat4 matView;
uniform mat4 matProjection;
uniform vec3 cameraPosition;

out float depth;
out float waterDepth;
out vec2 texCoord;

void main() {
    vec4 actualPosition = matTransform * vec4(position0, 1.0f);
    depth = length(cameraPosition - actualPosition.xyz);
    texCoord = texCoord0;
    waterDepth = waterDepth0;

    gl_Position = matProjection * matView * actualPosition;
}