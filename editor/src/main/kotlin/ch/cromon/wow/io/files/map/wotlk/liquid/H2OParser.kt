package ch.cromon.wow.io.files.map.wotlk.liquid

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.utils.CHUNK_SIZE


class H2OParser(private val liquidManager: TileLiquidManager, private val chunkData: LittleEndianStream) {
    private val liquidHeaders: Array<LiquidChunkHeader> = (0 until 256).map {
        LiquidChunkHeader(chunkData.readInt(), chunkData.readInt(), chunkData.readInt())
    }.toTypedArray()

    init {
        loadChunks()
    }

    private fun loadChunks() {
        for ((chunkIndex, header) in liquidHeaders.withIndex()) {
            if(header.numLayers <= 0) {
                continue
            }

            chunkData.position = header.offsetInstances
            val instances = (0 until header.numLayers).map {
                LiquidChunkInstance.read(chunkData)
            }

            val cx = chunkIndex % 16
            val cy = chunkIndex / 16

            val startX = cx * CHUNK_SIZE
            val startY = cy * CHUNK_SIZE

            for(instance in instances) {
                liquidManager.handleLiquidInstance(chunkData, Vector3(startX, startY, 0.0f), instance)
            }

        }
    }
}