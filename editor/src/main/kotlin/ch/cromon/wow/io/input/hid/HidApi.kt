@file:Suppress("FunctionName")

package ch.cromon.wow.io.input.hid

import com.sun.jna.Library
import com.sun.jna.Native
import com.sun.jna.Pointer


interface HidApi : Library {
    companion object {
        private lateinit var instance: HidApi

        fun load(path: String) {
            System.load(path)
            instance = Native.loadLibrary(path, HidApi::class.java)
            if(!init()) {
                throw IllegalArgumentException("Call to hid_init() in $path failed")
            }
        }

        fun init() = instance.hid_init() == 0
        fun enumerate(): HidDeviceInfo? = instance.hid_enumerate(0, 0)
        fun enumerate(vendorId: Int, productId: Int) = instance.hid_enumerate(vendorId.toShort(), productId.toShort())
        fun freeEnum(deviceList: HidDeviceInfo?) {
            if(deviceList != null) {
                instance.hid_free_enumeration(deviceList)
            }
        }

        fun openByPath(path: String) = instance.hid_open_path(path)
        fun close(device: Pointer) = instance.hid_close(device)
        fun setNonBlocking(device: Pointer, nonBlocking: Boolean) = instance.hid_set_nonblocking(device, if(nonBlocking) 1 else 0)

        fun sendFeatureReport(device: Pointer, data: ByteArray, size: Int = -1): Int {
            val actualSize = if(size >= 0) size else data.size
            return instance.hid_send_feature_report(device, data, actualSize)
        }

        fun getFeatureReport(device: Pointer, buffer: ByteArray, size: Int = -1): Int {
            val actualSize = if(size >= 0) size else buffer.size
            return instance.hid_get_feature_report(device, buffer, actualSize)
        }

        fun read(device: Pointer, buffer: ByteArray, size: Int = -1): Int {
            val actualSize = if(size >= 0) size else buffer.size
            return instance.hid_read(device, buffer, actualSize)
        }

        fun write(device: Pointer, data: ByteArray, size: Int = -1): Int {
            val actualSize = if(size >= 0) size else data.size
            return instance.hid_write(device, data, actualSize)
        }
    }

    fun hid_init(): Int
    fun hid_enumerate(vendorId: Short, productId: Short): HidDeviceInfo?
    fun hid_free_enumeration(list: HidDeviceInfo?)
    fun hid_open_path(path: String): Pointer
    fun hid_close(device: Pointer)
    fun hid_set_nonblocking(device: Pointer, nonBlocking: Int)
    fun hid_send_feature_report(device: Pointer, data: ByteArray, size: Int): Int
    fun hid_get_feature_report(device: Pointer, data: ByteArray, size: Int): Int
    fun hid_read(device: Pointer, data: ByteArray, size: Int): Int
    fun hid_write(device: Pointer, data: ByteArray, size: Int): Int
}