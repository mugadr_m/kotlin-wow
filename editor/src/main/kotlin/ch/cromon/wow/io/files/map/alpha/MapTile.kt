package ch.cromon.wow.io.files.map.alpha

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.files.map.single.AbstractSingleMapTile
import ch.cromon.wow.io.files.map.wotlk.liquid.TileLiquidManager
import ch.cromon.wow.math.BoundingBox
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.memory.MapVertexBufferCache
import ch.cromon.wow.utils.TILE_SIZE
import java.nio.FloatBuffer
import java.nio.charset.StandardCharsets


class AlphaMapTile(indexX: Int, indexY: Int) : AbstractSingleMapTile(indexX, indexY) {
    override val boundingBox: BoundingBox = BoundingBox()

    private val chunkOffsets = IntArray(256)
    private val chunkSizes = IntArray(256)
    private val chunkMap = ArrayList<AlphaMapChunk>()
    private lateinit var vertexData: FloatBuffer
    private lateinit var tileLiquidManager: TileLiquidManager

    override val chunks = chunkMap
    override val fullVertices get() = vertexData

    override val liquidData: TileLiquidManager
        get() = tileLiquidManager

    val positionX = TILE_SIZE * indexX
    val positionY = TILE_SIZE * indexY

    fun load(content: LittleEndianStream, offset: Int): Boolean {
        val basePosition = offset + 8
        content.position = basePosition
        val ofsMcin = content.readInt()
        val ofsMtex = content.readInt()
        val mtexSize = content.readInt()
        val ofsMddf = content.readInt()
        var mddfSize = content.readInt()
        var ofsModf = content.readInt()
        var sizeModf = content.readInt()

        loadChunkOffsets(ofsMcin + basePosition, content)
        loadTextures(ofsMtex + basePosition, mtexSize, content)

        val chunkMin = Vector3(Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE)
        val chunkMax = Vector3(-Float.MAX_VALUE, -Float.MAX_VALUE, -Float.MAX_VALUE)

        vertexData = MapVertexBufferCache.getBuffer()
        for(i in 0 until 256) {
            val chunkOffset = chunkOffsets[i]
            val size = chunkSizes[i]
            val chunk = AlphaMapChunk()
            chunk.load(this, chunkOffset, content)
            chunkMap.add(chunk)
            chunkMin.takeMin(chunk.boundingBox.min)
            chunkMax.takeMax(chunk.boundingBox.max)

            chunk.vertices.forEach {
                vertexData.put(it.position.x)
                        .put(it.position.y)
                        .put(it.position.z)
                        .put(it.texCoord.x)
                        .put(it.texCoord.y)
                        .put(it.texCoordAlpha.x)
                        .put(it.texCoordAlpha.y)
                        .put(it.normal.x)
                        .put(it.normal.y)
                        .put(it.normal.z)
                        .put(Float.fromBits(it.vertexColor))
            }
        }

        boundingBox.min.set(chunkMin)
        boundingBox.max.set(chunkMax)


        vertexData.flip()
        return true
    }

    override fun getChunk(x: Int, y: Int) = chunkMap[y * 16 + x]

    override fun unload() {

    }

    private fun loadChunkOffsets(ofsMcin: Int, content: LittleEndianStream) {
        content.position = ofsMcin + 8
        for(i in 0 until 256) {
            chunkOffsets[i] = content.readInt()
            chunkSizes[i] = content.readInt()
            content.position += 8
        }
    }

    private fun loadTextures(ofsMtex: Int, texSize: Int, content: LittleEndianStream) {
        if(texSize <= 8) {
            return
        }

        content.position = ofsMtex + 8
        val bytes = content.readBytes(texSize - 8)
        textures.addAll(String(bytes, StandardCharsets.US_ASCII)
                .split('\u0000')
                .filter({ it.isNotBlank() }))
    }
}