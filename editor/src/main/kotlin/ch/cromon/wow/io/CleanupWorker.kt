package ch.cromon.wow.io

import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.util.concurrent.*
import javax.annotation.PreDestroy


@Component
@Scope("singleton")
object CleanupWorker {
    private val workQueue = LinkedBlockingQueue<Runnable>()

    private val executor = ThreadPoolExecutor(1, 5, 30, TimeUnit.SECONDS, workQueue)

    init {
        executor.rejectedExecutionHandler = RejectedExecutionHandler{
            _, pool ->
            if(!pool.isTerminated && !pool.isTerminating && !pool.isShutdown) {
                throw RejectedExecutionException("Rejected execution of task")
            }
        }
    }

    fun cleanup(action: () -> Unit) {
        executor.execute(action)
    }

    @PreDestroy
    fun shutdown() {
        executor.shutdown()
    }
}