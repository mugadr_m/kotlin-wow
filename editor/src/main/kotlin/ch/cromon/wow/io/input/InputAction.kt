package ch.cromon.wow.io.input


enum class InputAction {
    CAMERA_LEFT,
    CAMERA_RIGHT,
    CAMERA_FRONT,
    CAMERA_BACK,
    CAMERA_UP,
    CAMERA_DOWN,
    CAMERA_ROTATE_H,
    CAMERA_ROTATE_V
}