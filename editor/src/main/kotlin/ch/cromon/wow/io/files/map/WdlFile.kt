package ch.cromon.wow.io.files.map

import ch.cromon.wow.io.LittleEndianStream
import java.util.*

data class MareEntry(val outer: ShortArray = ShortArray(17 * 17), val inner: ShortArray = ShortArray(16 * 16)) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MareEntry

        if (!Arrays.equals(outer, other.outer)) return false
        if (!Arrays.equals(inner, other.inner)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = Arrays.hashCode(outer)
        result = 31 * result + Arrays.hashCode(inner)
        return result
    }
}

class WdlFile(private val data: LittleEndianStream) {
    companion object {
        private const val MAOF = 0x4D414F46
    }

    private val chunkMap = ArrayList<Int>()
    private val mareChunks = HashMap<Int, MareEntry>()

    private val offsets = IntArray(64 * 64)

    fun getMareChunk(ix: Int, iy: Int) = mareChunks[iy * 64 + ix]

    fun parse() {
        while(data.position + 8 < data.size) {
            val signature = data.readInt()
            val size = data.readInt()
            val curPos = data.position
            chunkMap.add(signature)
            when(signature) {
                MAOF -> {
                    for(i in 0 until 4096) {
                        offsets[i] = data.readInt()
                    }
                }
            }
            data.position = curPos + size
        }

        if(!chunkMap.contains(MAOF)) {
            throw IllegalStateException("Failed to read WDL file, missing MAOF chunk")
        }

        parseHeightMap()
    }

    private fun parseHeightMap() {
        for(i in 0 until 4096) {
            val offset = offsets[i]
            if(offset <= 0) {
                continue
            }

            data.position = offset + 8
            val mareData = MareEntry()
            for(j in 0 until 17 * 17) {
                mareData.outer[j] = data.readShort()
            }
            for(j in 0 until 16 * 16) {
                mareData.inner[j] = data.readShort()
            }

            mareChunks[i] = mareData
        }
    }
}