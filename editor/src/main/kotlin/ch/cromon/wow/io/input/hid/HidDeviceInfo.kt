package ch.cromon.wow.io.input.hid

import com.sun.jna.Structure
import com.sun.jna.WString


data class HidDeviceInfo(
        @JvmField
        var path: String = "",
        @JvmField
        var vendorId: Short = 0,
        @JvmField
        var productId: Short = 0,
        @JvmField
        var serialNumber: WString = WString(""),
        @JvmField
        var releaseNumber: Short = 0,
        @JvmField
        var manufacturerString: WString = WString(""),
        @JvmField
        var productString: WString = WString(""),
        @JvmField
        var usagePage: Short = 0,
        @JvmField
        var usage: Short = 0,
        @JvmField
        var interfaceNumber: Int = 0,
        @JvmField
        var next: HidDeviceInfo? = null
) : Structure(), Structure.ByReference {
    override fun getFieldOrder(): MutableList<String> {
        return arrayListOf(
                "path",
                "vendorId",
                "productId",
                "serialNumber",
                "releaseNumber",
                "manufacturerString",
                "productString",
                "usagePage",
                "usage",
                "interfaceNumber",
                "next"
        )
    }
}