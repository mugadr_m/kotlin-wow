package ch.cromon.wow.io.data

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.charset.StandardCharsets


object JenkinsHash {
    fun calculate(data: String): Long {
        val hashStr = data.toUpperCase().replace('/', '\\')
        var bytes = hashStr.toByteArray(StandardCharsets.ISO_8859_1)
        var length = bytes.size
        var a = (0xdeadbeef + length) and 0xFFFFFFFF
        var b = a
        var c = a

        if (length == 0) {
            return ((c shl 32) or b)
        }

        val newLen = length + (12 - length % 12) % 12
        if (newLen != length) {
            val tmp = bytes
            bytes = ByteArray(newLen)
            System.arraycopy(tmp, 0, bytes, 0, tmp.size)
            length = newLen
        }

        val buffer = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN)
        for (i in 0 until (length - 12) step 12) {
            a += buffer.getInt(i).toLong() and 0xFFFFFFFF
            b += buffer.getInt(i + 4).toLong() and 0xFFFFFFFF
            c += buffer.getInt(i + 8).toLong() and 0xFFFFFFFF

            a = a and 0xFFFFFFFF
            b = b and 0xFFFFFFFF
            c = c and 0xFFFFFFFF

            a -= c
            a = a and 0xFFFFFFFF
            a = a xor rot(c, 4)
            c += b
            c = c and 0xFFFFFFFF

            b -= a
            b = b and 0xFFFFFFFF
            b = b xor rot(a, 6)
            a += c
            a = a and 0xFFFFFFFF

            c -= b
            c = c and 0xFFFFFFFF
            c = c xor rot(b, 8)
            b += a
            b = b and 0xFFFFFFFF

            a -= c
            a = a and 0xFFFFFFFF
            a = a xor rot(c, 16)
            c += b
            c = c and 0xFFFFFFFF

            b -= a
            b = b and 0xFFFFFFFF
            b = b xor rot(a, 19)
            a += c
            a = a and 0xFFFFFFFF

            c -= b
            c = c and 0xFFFFFFFF
            c = c xor rot(b, 4)
            b += a
            b = b and 0xFFFFFFFF
        }

        val i = length - 12
        a += buffer.getInt(i).toLong() and 0xFFFFFFFF
        b += buffer.getInt(i + 4).toLong() and 0xFFFFFFFF
        c += buffer.getInt(i + 8).toLong() and 0xFFFFFFFF

        a = a and 0xFFFFFFFF
        b = b and 0xFFFFFFFF
        c = c and 0xFFFFFFFF

        c = ((c xor b) - rot(b, 14)) and 0xFFFFFFFF
        a = ((a xor c) - rot(c, 11)) and 0xFFFFFFFF
        b = ((b xor a) - rot(a, 25)) and 0xFFFFFFFF
        c = ((c xor b) - rot(b, 16)) and 0xFFFFFFFF
        a = ((a xor c) - rot(c, 4)) and 0xFFFFFFFF
        b = ((b xor a) - rot(a, 14)) and 0xFFFFFFFF
        c = ((c xor b) - rot(b, 24)) and 0xFFFFFFFF

        return (c shl 32) or b
    }

    private fun rot(x: Long, k: Int): Long = ((x shl k) or (x shr (32 - k))) and 0xFFFFFFFF
}