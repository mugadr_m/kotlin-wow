package ch.cromon.wow.io.files.map.alpha

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.files.map.single.AbstractSingleMapChunk
import ch.cromon.wow.io.files.map.single.MapVertex
import ch.cromon.wow.io.files.map.single.TextureLayer
import ch.cromon.wow.math.BoundingBox
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.memory.MapAlphaCache
import ch.cromon.wow.memory.MapVertexCache
import ch.cromon.wow.utils.CHUNK_SIZE
import ch.cromon.wow.utils.UNIT_SIZE
import ch.cromon.wow.utils.toUInt

data class Header(
        val flags: Int,
        val indexX: Int,
        val indexY: Int,
        val radius: Float,
        val layers: Int,
        val numDoodads: Int,
        val ofsHeight: Int,
        val ofsNormal: Int,
        val ofsLayers: Int,
        val ofsReferences: Int,
        val ofsAlpha: Int,
        val sizeAlpha: Int,
        val ofsShadow: Int,
        val sizeShadow: Int,
        val areaId: Int,
        val numWmoRefs: Int,
        val holes: Short,
        val unk1: Short,
        val detailMap1: Long,
        val detailMap2: Long,
        val noEffectDoodad: Long,
        val ofsSoundEmitters: Int,
        val numSoundEmitters: Int,
        val ofsLiquid: Int,
        val sizeLiquid: Int)

class AlphaMapChunk : AbstractSingleMapChunk() {
    companion object {
        private val innerHeights = FloatArray(8 * 8)
        private val outerHeights = FloatArray(9 * 9)
        private val innerNormals = ByteArray(8 * 8 * 3)
        private val outerNormals = ByteArray(9 * 9 * 3)

        private val alphaWorkObject = ByteArray(4096 * 4)
    }

    override val boundingBox = BoundingBox()

    private lateinit var header: Header
    private lateinit var parent: AlphaMapTile

    lateinit var vertices: Array<MapVertex>
    private lateinit var alphaBuffer: IntArray
    private val layers = ArrayList<TextureLayer>()

    override val textureLayers = layers
    override val alphaData get() = alphaBuffer

    override val zoneId: Int
        get() = header.areaId

    fun load(parent: AlphaMapTile, offset: Int, content: LittleEndianStream): Boolean {
        vertices = MapVertexCache.getVertices()
        alphaBuffer = MapAlphaCache.getData()
        this.parent = parent
        val chunkRelPos = offset + 8 + 128
        content.position = offset + 8
        readHeader(content)

        loadHeights(chunkRelPos + header.ofsHeight, content)
        loadNormals(chunkRelPos + header.ofsNormal, content)
        loadLayers(chunkRelPos + header.ofsLayers, content)
        loadAlpha(chunkRelPos + header.ofsAlpha, content)
        return true
    }

    private fun loadHeights(offset: Int, content: LittleEndianStream) {
        content.position = offset

        val baseX = header.indexX * CHUNK_SIZE + parent.positionX
        val baseY = header.indexY * CHUNK_SIZE + parent.positionY
        for (i in 0 until (9 * 9)) {
            outerHeights[i] = content.readFloat()
        }

        for (i in 0 until (8 * 8)) {
            innerHeights[i] = content.readFloat()
        }

        val minPos = Vector3(Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE)
        val maxPos = Vector3(Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE)
        var counter = 0
        for (i in 0 until 17) {
            val innerRow = i % 2 != 0
            val columns = if (innerRow) 8 else 9
            for (j in 0 until columns) {
                val y = baseY + i * UNIT_SIZE * 0.5f
                var x = baseX + j * UNIT_SIZE
                val z = if (innerRow) {
                    x += 0.5f * UNIT_SIZE
                    innerHeights[j + ((i - 1) / 2) * 8]
                } else {
                    outerHeights[j + (i / 2) * 9]
                }

                val pos = vertices[counter].position
                pos.x = x
                pos.y = y
                pos.z = z
                maxPos.takeMax(pos)
                minPos.takeMin(pos)

                val ax = j / 8.0f + (if (innerRow) (0.5f / 8.0f) else 0.0f)
                val ay = i / 16.0f
                val tx = j + (if (innerRow) 0.5f else 0.0f)
                val ty = i * 0.5f
                val tc = vertices[counter].texCoord
                tc.x = tx
                tc.y = ty
                val ta = vertices[counter].texCoordAlpha
                ta.x = ax
                ta.y = ay

                ++counter
            }
        }

        boundingBox.min.set(minPos)
        boundingBox.max.set(maxPos)
    }

    private fun loadNormals(offset: Int, content: LittleEndianStream) {
        content.position = offset
        for (i in 0 until (9 * 9)) {
            outerNormals[i * 3] = content.readByte()
            outerNormals[i * 3 + 1] = content.readByte()
            outerNormals[i * 3 + 2] = content.readByte()
        }

        for (i in 0 until (8 * 8)) {
            innerNormals[i * 3] = content.readByte()
            innerNormals[i * 3 + 1] = content.readByte()
            innerNormals[i * 3 + 2] = content.readByte()
        }

        var counter = 0
        for (i in 0 until 17) {
            val innerRow = i % 2 != 0
            val columns = if (innerRow) 8 else 9
            for (j in 0 until columns) {
                val nx = if(innerRow) innerNormals[(j + ((i - 1) / 2) * 8) * 3] else outerNormals[(j + (i / 2) * 9) * 3]
                val ny = if(innerRow) innerNormals[(j + ((i - 1) / 2) * 8) * 3 + 1] else outerNormals[(j + (i / 2) * 9) * 3 + 1]
                val nz = if(innerRow) innerNormals[(j + ((i - 1) / 2) * 8) * 3 + 2] else outerNormals[(j + (i / 2) * 9) * 3 + 2]

                val normal = vertices[counter++].normal
                normal.x = -nx / 127.0f
                normal.y = -ny / 127.0f
                normal.z = -nz / 127.0f
            }
        }
    }

    private fun loadLayers(offset: Int, data: LittleEndianStream) {
        data.position = offset + 4
        val size = data.readInt()

        val numLayers = size / 16
        if(numLayers <= 0) {
            return
        }

        for(i in 0 until numLayers) {
            val texture = data.readInt()
            val flags = data.readInt()
            val alphaOffset = data.readInt()
            val groundEffect = data.readInt()
            layers.add(TextureLayer(texture, flags, alphaOffset, groundEffect))
        }
    }

    private fun loadAlpha(offset: Int, data: LittleEndianStream) {
        if(layers.isEmpty()) {
            return
        }

        if(header.sizeAlpha > 0) {
            data.position = offset
            data.readBytes(alphaWorkObject, 0, header.sizeAlpha)
        }

        for(i in 0 until layers.size) {
            val layer = layers[i]
            if((layer.flags and 0x100) != 0) {
                loadCompressed(alphaWorkObject, layer, i)
            } else {
                loadBaseLayer(i)
            }
        }
    }

    private fun loadBaseLayer(index: Int) {
        for(i in 0 until 4096) {
            var old = alphaBuffer[i]
            old = old or (0xFF shl (8 * index))
            alphaBuffer[i] = old
        }
    }

    private fun loadCompressed(data: ByteArray, layer: TextureLayer, index: Int) {
        var curPos = layer.alphaOffset
        var numOut = 0
        for(k in 0 until 63) {
            for(j in 0 until 32) {
                val alpha = data[curPos++].toUInt()
                var val1 = alpha and 0x0F
                var val2 = (alpha shr 4) and 0x0F
                val2 = if(j == 31) val1 else val2
                val1 = ((val1 / 15.0f) * 255.0f).toInt()
                val2 = ((val2 / 15.0f) * 255.0f).toInt()

                var old = alphaBuffer[numOut]
                old = old or (val1 shl (8 * index))
                alphaBuffer[numOut++] = old

                old = alphaBuffer[numOut]
                old = old or (val2 shl (8 * index))
                alphaBuffer[numOut++] = old
            }
        }

        for(j in 0 until 64) {
            var old = alphaBuffer[63 * 64 + j]
            old = old or (alphaBuffer[62 * 64 + j] and (0xFF shl (index * 8)))
            alphaBuffer[63 * 64 + j] = old
        }
    }

    private fun readHeader(data: LittleEndianStream) {
        header = Header(
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readFloat(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readShort(),
                data.readShort(),
                data.readLong(),
                data.readLong(),
                data.readLong(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt()
        )
    }
}