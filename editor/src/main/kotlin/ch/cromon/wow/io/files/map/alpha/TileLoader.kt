package ch.cromon.wow.io.files.map.alpha

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.data.DataManager
import ch.cromon.wow.io.files.map.MapTileLoader
import ch.cromon.wow.io.files.map.MareEntry
import ch.cromon.wow.io.files.map.Tile
import ch.cromon.wow.io.files.map.WdlFile
import ch.cromon.wow.utils.BuildVersion
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component


@Component
@Scope("singleton")
@BuildVersion(minBuild = 0, maxBuild = 3494)
class AlphaMapLoader : MapTileLoader {
    companion object {
        private const val MAIN = 0x4D41494E
    }

    @Autowired
    private lateinit var dataManager: DataManager

    private var activeMap = ""
    private lateinit var mapStream: LittleEndianStream

    private var activeWdl: WdlFile? = null

    private var mapOffsets = IntArray(4096)

    override fun load(mapName: String, indexX: Int, indexY: Int): Tile? {
        if(activeMap != mapName) {
            mapStream = dataManager.openFile("$mapName.wdt") ?: return null
            if(!initWdtFile()) {
                return null
            }

            activeWdl = initWdlFile(mapName)
            activeMap = mapName
        }

        if(indexX !in 0..63 || indexY !in 0..63) {
            return null
        }

        val offset = mapOffsets[indexX + indexY * 64]
        if(offset <= 0) {
            return null
        }

        val ret = AlphaMapTile(indexX, indexY)
        ret.load(mapStream, offset)
        return ret
    }

    override fun activate() {

    }

    override fun loadLowDetail(x: Int, y: Int): MareEntry? {
        val wdlFile = activeWdl ?: return null
        return wdlFile.getMareChunk(x, y)
    }

    private fun initWdtFile(): Boolean {
        var mainFound = false
        while(mapStream.position + 8 <= mapStream.size) {
            val signature = mapStream.readInt()
            val size = mapStream.readInt()
            if(signature == MAIN) {
                for(i in 0 until 4096) {
                    mapOffsets[i] = mapStream.readInt()
                    mapStream.position += 12
                }
                mainFound = true
                break
            }

            mapStream.position += size
        }

        return mainFound
    }

    private fun initWdlFile(mapName: String): WdlFile? {
        val path = "$mapName.wdl"
        val content = dataManager.openFile(path) ?: return null
        val wdl = WdlFile(content)
        wdl.parse()
        return wdl
    }

}