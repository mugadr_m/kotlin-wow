package ch.cromon.wow.io.input

import ch.cromon.wow.io.input.InputAction.*
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.awt.event.KeyEvent

@Component
@Scope("singleton")
class Keyboard {
    private val keyStates = BooleanArray(Short.MAX_VALUE.toInt())

    fun setKeyState(virtualKey: Short, state: Boolean) {
        keyStates[virtualKey.toInt()] = state
    }

    fun getKeyState(virtualKey: Int): Boolean = keyStates[virtualKey]

    fun getInputActionIntensity(action: InputAction): Float {
        return when (action) {
            CAMERA_LEFT -> getKeyAction(KeyEvent.VK_A)
            CAMERA_RIGHT -> getKeyAction(KeyEvent.VK_D)
            CAMERA_BACK -> getKeyAction(KeyEvent.VK_S)
            CAMERA_FRONT -> getKeyAction(KeyEvent.VK_W)
            CAMERA_UP -> getKeyAction(KeyEvent.VK_Q)
            CAMERA_DOWN -> getKeyAction(KeyEvent.VK_E)
            else -> 0.0f
        }
    }

    private fun getKeyAction(virtualKey: Int) =
            if (getKeyState(virtualKey)) {
                1.0f
            } else {
                0.0f
            }
}