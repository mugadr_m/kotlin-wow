package ch.cromon.wow.io

import ch.cromon.wow.io.data.DataManager
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.util.concurrent.*
import java.util.concurrent.atomic.AtomicInteger
import java.util.function.Supplier
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy


@Component
@Scope("singleton")
class AsyncImageLoader {
    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var dataManager: DataManager

    private val workQueue = LinkedBlockingQueue<Runnable>()

    private val executor = ThreadPoolExecutor(4, 4, 0, TimeUnit.SECONDS, workQueue)

    private lateinit var fallbackSource: ImageSource

    @PreDestroy
    fun shutdown() {
        executor.shutdown()
    }

    @PostConstruct
    fun initialize() {
        val data = byteArrayOf(
                0xFF.toByte(), 0x00, 0x00, 0xFF.toByte(),
                0x00, 0xFF.toByte(), 0x00, 0xFF.toByte(),
                0x00, 0x00, 0xFF.toByte(), 0xFF.toByte(),
                0x3F, 0x7F, 0xFF.toByte(), 0xFF.toByte()
        )

        fallbackSource = ImageSource(data, 2, 2, ImageSourceFormat.ARGB8)

        val factoryDelegate = Executors.defaultThreadFactory()
        executor.threadFactory = object : ThreadFactory {
            val counter = AtomicInteger()

            override fun newThread(r: Runnable?): Thread {
                val thread = factoryDelegate.newThread(r)
                thread.name = "ImageLoader-thread-" + counter.getAndIncrement()
                return thread
            }
        }
    }

    fun loadFromImageResource(resource: String): CompletableFuture<ImageSource> {
        return CompletableFuture.supplyAsync(Supplier{
            try {
                ImageSource.fromImageResource(resource)
            } catch (e: IllegalArgumentException) {
                fallbackSource
            }
        }, executor)
    }

    fun loadImageToSize(image: LittleEndianStream?, width: Int, height: Int): CompletableFuture<ImageSource> {
        return CompletableFuture.supplyAsync(Supplier{
            if(image == null) {
                return@Supplier fallbackSource
            }

            try {
                ImageSource.fromTextureToSize(image, width, height)
            } catch (e: Exception) {
                logger.error("Error loading texture $image", e)
                fallbackSource
            }
        }, executor)
    }

    fun loadImage(image: LittleEndianStream?): CompletableFuture<ImageSource> {
        return CompletableFuture.supplyAsync(Supplier{
            if(image == null) {
                return@Supplier fallbackSource
            }

            try {
                ImageSource.fromTexture(image)
            } catch (e: Exception) {
                logger.error("Error loading texture $image", e)
                fallbackSource
            }
        }, executor)
    }

    fun loadImage(image: String): CompletableFuture<ImageSource> {
        return CompletableFuture.supplyAsync(Supplier{
            val content = dataManager.openFile(image) ?: return@Supplier fallbackSource
            try {
                ImageSource.fromTexture(content)
            } catch (e: Exception) {
                logger.error("Error loading texture $image", e)
                fallbackSource
            }
        }, executor)
    }

    fun loadOneOf(vararg images: String): CompletableFuture<ImageSource> {
        return CompletableFuture.supplyAsync(Supplier{
            for(image in images) {
                val content = dataManager.openFile(image) ?: continue
                return@Supplier try {
                    ImageSource.fromTexture(content)
                } catch (e: Exception) {
                    logger.error("Error loading texture $image", e)
                    continue
                }
            }

            fallbackSource
        }, executor)
    }
}