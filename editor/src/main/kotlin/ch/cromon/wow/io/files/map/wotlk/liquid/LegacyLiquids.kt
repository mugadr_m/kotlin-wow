package ch.cromon.wow.io.files.map.wotlk.liquid

import ch.cromon.wow.io.files.storage.LiquidType


val RIVER_LIQUID = LiquidType(
        1, "River", arrayListOf("XTextures\\river\\lake_a.%d.blp")
)

val OCEAN_LIQUID = LiquidType(
        2, "Ocean", arrayListOf("XTextures\\ocean\\ocean_h.%d.blp")
)

val MAGMA_LIQUID = LiquidType(
        3, "Magma", arrayListOf("XTextures\\lava\\lava.%d.blp")
)

val SLIME_LIQUID = LiquidType(
        4, "Slime", arrayListOf("XTextures\\slime\\slime.%d.blp")
)