package ch.cromon.wow.io.files.sky.alpha

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.math.Vector4
import ch.cromon.wow.scene.sky.InterpolatedColor
import ch.cromon.wow.scene.sky.InterpolatedFloat
import java.nio.charset.StandardCharsets
import java.util.*

data class LightEntry(val cx: Int, val cy: Int, val chunkRadius: Int, val position: Vector3, val innerRadius: Float, val outerRadius: Float, val name: String)

class LitFile(data: LittleEndianStream) {
    companion object {
        val FLOAT_TIMES = (0 until 32).map {
            (2880 / 32) * it
        }
    }

    val version = data.readInt()
    private val numLights = data.readInt()
    val lights = ArrayList<LightEntry>()
    val colorData = ArrayList<ArrayList<InterpolatedColor>>()
    val floatData = ArrayList<ArrayList<InterpolatedFloat>>()

    init {
        for(i in 0 until numLights) {
            lights.add(
                    LightEntry(
                            data.readInt(),
                            data.readInt(),
                            data.readInt(),
                            Vector3(data.readFloat(), data.readFloat(), data.readFloat()),
                            data.readFloat(),
                            data.readFloat(),
                            String(data.readBytes(32), StandardCharsets.US_ASCII).takeWhile { it != '\u0000'}
                    )
            )
        }

        for(i in 0 until numLights) {
            val curPosition = data.position
            val counts = (0 until 18).map { data.readInt() }
            val lightColors = ArrayList<InterpolatedColor>()
            val lightFloats = ArrayList<InterpolatedFloat>()
            for(j in 0 until 18) {
                val numEntries = counts[j]
                val pairs = (0 until 32).map({ data.readInt() to data.readInt() }).take(numEntries)
                val times = pairs.map { it.first }
                val colors = pairs.map { it.second }.map { color ->
                    val b = (color and 0xFF)
                    val g = ((color shr 8) and 0xFF)
                    val r = ((color shr 16) and 0xFF)
                    val a = ((color shr 24) and 0xFF)
                    Vector4(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f)
                }

                lightColors.add(InterpolatedColor(times, colors))
            }

            val fogEnd = (0 until 32).map { data.readFloat() }
            val fogDensity = (0 until 32).map { data.readFloat() }
            lightFloats.add(InterpolatedFloat(FLOAT_TIMES, fogEnd))
            lightFloats.add(InterpolatedFloat(FLOAT_TIMES, fogDensity))
            colorData.add(lightColors)
            floatData.add(lightFloats)
            data.position = curPosition + 4 * 0x1550
        }
    }
}