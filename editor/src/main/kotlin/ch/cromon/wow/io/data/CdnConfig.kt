package ch.cromon.wow.io.data

import org.slf4j.LoggerFactory
import java.io.InputStream
import java.util.*


private fun forEachLine(input: InputStream, callback: (String, Int) -> Unit) {
    try {
        Scanner(input).use {
            var line: String? = it.nextLine()
            var lineIndex = 0
            while (line != null) {
                ++lineIndex
                if (line.isBlank() || line.startsWith("#")) {
                    line = it.nextLine()
                    continue
                }

                callback(line, lineIndex)
                line = it.nextLine()
            }
        }
    } catch (ignored: NoSuchElementException) {

    }
}


class TokenConfig(input: InputStream) : Iterable<Map<String, String>> {
    override fun iterator(): Iterator<Map<String, String>> {
        return elements.iterator()
    }

    private val elements = ArrayList<Map<String, String>>()

    init {
        readValues(input)
    }

    operator fun get(index: Int) = elements[index]

    val size get() = elements.size

    private fun readValues(input: InputStream) {
        var hasHeader = false
        var fields: Array<String> = arrayOf()
        forEachLine(input, { line, _ ->
            val tokens = line.split("|")
            if (!hasHeader) {
                hasHeader = true
                fields = Array(tokens.size, { tokens[it].split("!")[0].replace(" ", "") })
            } else {
                val row = HashMap<String, String>()
                for ((index, value) in tokens.withIndex()) {
                    row[fields[index]] = value
                }
                elements.add(row)
            }
        })
    }
}

class KeyValueConfig(input: InputStream) {
    companion object {
        private val LOG = LoggerFactory.getLogger(KeyValueConfig::class.java)
    }

    private val elements = HashMap<String, List<String>>()

    init {
        readValues(input)
    }

    operator fun get(index: String) = elements[index]

    private fun readValues(input: InputStream) {
        forEachLine(input, { line, lineIndex ->
            val tokens = line.split("=")
            if (tokens.size != 2) {
                LOG.error("Invalid key value config on line $lineIndex: Expected format key=value but got $line")
                throw IllegalArgumentException("Invalid key value config supplied")
            }

            elements[tokens[0].trim()] = tokens[1].trim().split(" ")
        })
    }
}