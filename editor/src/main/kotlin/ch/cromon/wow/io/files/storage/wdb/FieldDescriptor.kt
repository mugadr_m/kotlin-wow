package ch.cromon.wow.io.files.storage.wdb


data class FieldDescriptor(val bits: Int, val offset: Int, val dataType: Int = 0xFF, var length: Int = 1) {
    val bitCount: Int get() {
        var bitSize = 32 - bits
        if(bitSize < 0) {
            bitSize = bitSize * -1 + 32
        }
        return bitSize
    }

    val byteCount: Int get() {
        val ret = (32 - bits) shr 3
        return if(ret < 0) Math.abs(ret) + 4 else ret
    }

    val isCommonColumn get() = dataType != 0xFF

    fun calcLength(nextField: FieldDescriptor) {
        length = Math.max(1.0, Math.floor((nextField.offset - offset) / byteCount.toDouble())).toInt()
    }
}