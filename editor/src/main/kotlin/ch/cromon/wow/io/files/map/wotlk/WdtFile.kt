package ch.cromon.wow.io.files.map.wotlk

import ch.cromon.wow.io.LittleEndianStream


class WdtFile(data: LittleEndianStream) {
    private var flagValue = 0

    val flags get() = flagValue

    init {
        while(data.position + 8 <= data.size) {
            val id = data.readInt()
            val size = data.readInt()
            if(id == 0x4D504844) {
                flagValue = data.readInt()
                break
            }

            data.position += size
        }
    }
}