package ch.cromon.wow.io.files.storage

import ch.cromon.wow.io.data.DataManager
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

interface StorageRow {
    fun getInt(offset: Int): Int
    fun getFloat(offset: Int): Float
    fun getString(offset: Int): String
    fun tryGetString(offset: Int): String?
}

interface StorageFile : Iterable<StorageRow> {
    fun getRowById(id: Int): StorageRow?
    fun getRowByIndex(index: Int): StorageRow

    fun getFieldCount(): Int
    fun getRowCount(): Int
}

enum class MatchType {
    NO_MATCH,
    INVALID,
    MATCH
}

@Component
@Scope("singleton")
class DataStorage {
    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var dataManager: DataManager

    private lateinit var mapStorage: StorageFile
    private lateinit var areaStorage: StorageFile
    private var loadingScreen: StorageFile? = null
    private var fileData: StorageFile? = null
    private lateinit var worldSafeLocs: StorageFile
    private lateinit var taxiNode: StorageFile
    private lateinit var liquidType: StorageFile

    fun getMapName(id: Int) = MapFormatGuesser.getMapName(id)
    fun listMapRows(callback: (StorageRow, MapDescriptor) -> Unit) = MapFormatGuesser.listMapRows(callback)
    fun getWideLoadingScreen(id: Int) = MapFormatGuesser.getWideLoadingScreen(id)
    fun getLoadingScreen(id: Int) = MapFormatGuesser.getLoadingScreenFile(id)
    fun getFallbackLoadingScreen() = dataManager.openFile(MapFormatGuesser.getFallbackLoadingScreen())
    fun getLoadingScreenIdForMap(mapId: Int) = MapFormatGuesser.getLoadingScreenIdForMap(mapId)
    fun getSafeLocationsForMap(mapId: Int) = MapFormatGuesser.getSafeLocationsForMap(mapId)
    fun getTaxiNodesForMap(mapId: Int) = MapFormatGuesser.getTaxiNodesForMap(mapId)
    fun getAreaName(areaId: Int) = MapFormatGuesser.getAreaName(areaId)

    fun getLiquidType(liquidTypeId: Int) = LiquidDataLoader.getLiquidTypeById(liquidTypeId)

    fun loadStorageFiles() {
        loadMapFiles()

        MapFormatGuesser.guess(dataManager.build, mapStorage, areaStorage, loadingScreen, worldSafeLocs, taxiNode, fileData, dataManager)
        LiquidDataLoader.initialize(dataManager.build, liquidType)
    }

    private fun loadMapFiles() {
        mapStorage = loadStorage("Map") ?: throw IllegalStateException("No Map.dbc or Map.db2 found")
        areaStorage = loadStorage("AreaTable") ?: throw IllegalStateException("No AreaTable.dbc or AreaTable.db2 found")
        loadingScreen = loadStorage("LoadingScreens")
        worldSafeLocs = loadStorage("WorldSafeLocs") ?: throw IllegalStateException("No WorldSafeLocs.dbc or WorldSafeLocs.db2 found")
        taxiNode = loadStorage("TaxiNodes") ?: throw IllegalStateException("No TaxiNodes.dbc or TaxiNodes.db2 found")
        fileData = loadStorage("FileData")
        liquidType = loadStorage("LiquidType") ?: throw IllegalStateException("No LiquidType.dbc or LiquidType.db2 found")
    }

    fun loadStorage(name: String): StorageFile? {
        val dbcPath = "DBFilesClient\\$name.dbc"
        val db2Path = "DBFilesClient\\$name.db2"

        val db2Data = dataManager.openFile(db2Path)
        if (db2Data != null) {
            logger.info("Loading {}", db2Path)
            return Db2File(db2Data, dataManager.build)
        }

        val dbcData = dataManager.openFile(dbcPath) ?: return null
        logger.info("Loading {}", dbcPath)
        return DbcFile(dbcData)
    }
}