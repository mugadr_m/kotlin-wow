package ch.cromon.wow.io.files.storage.wdb

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.files.storage.Db2FormatParser
import ch.cromon.wow.io.files.storage.Db2Row
import java.nio.ByteBuffer
import java.nio.ByteOrder


class Db5Parser(content: LittleEndianStream) : Db2FormatParser() {
    val numRecords = content.readInt()
    var numFields = content.readInt()
    val recordSize = content.readInt()
    val stringBlockSize = content.readInt()
    val offsetTableStart = stringBlockSize
    val tableHash = content.readInt()
    val layoutHash = content.readInt()
    val minId = content.readInt()
    val maxId = content.readInt()
    val copyTableSize = content.readInt()
    private val flags = content.readShort().toInt()
    private var idIndex = content.readShort().toInt()

    private val fieldStructure = ArrayList<FieldDescriptor>()
    private var copyTablePos = 0
    private var indexTablePos = 0
    private var hasIndexTable = HeaderFlags.HAS_INDEX_MAP setIn flags
    private var hasOffsetTable = HeaderFlags.HAS_OFFSET_MAP setIn flags
    private val stringBlock = HashMap<Int, String>()
    private val offsetDuplicates = HashMap<Int, Int>()
    private val recordTable = HashMap<Int, LittleEndianStream>()
    private val recordList = ArrayList<LittleEndianStream>()

    init {
        var indexOffset = 0
        if(hasIndexTable) {
            idIndex = 0
            indexOffset = 4
            hasIndexTable = true
        }

        for(i in 0 until numFields) {
            fieldStructure.add(FieldDescriptor(content.readShort().toInt(), content.readShort().toInt() + indexOffset))
            if(i > 0) {
                fieldStructure[i - 1].calcLength(fieldStructure[i])
            }
        }

        if(hasIndexTable) {
            fieldStructure.add(0, FieldDescriptor(0, 0))
            ++numFields
            if(numFields > 1) {
                fieldStructure[1].calcLength(fieldStructure[0])
            }
        }

        val headerEnd = content.position
        copyTablePos = content.size - copyTableSize
        indexTablePos = copyTablePos - if(hasIndexTable) 4 * numRecords else 0

        if(!hasOffsetTable) {
            content.position = indexTablePos - stringBlockSize
            parseStringBlock(stringBlock, content.readBytes(stringBlockSize))
        }

        content.position = headerEnd
        val copyTable = populateCopyTable(content)
        parseCopyTable(content, copyTable)
        copyTable.map({
            it.key to LittleEndianStream(it.value)
        }).associateTo(recordTable, {
            it.first to it.second
        })
        recordList.addAll(recordTable.values)
    }

    private fun populateCopyTable(content: LittleEndianStream): MutableMap<Int, ByteArray> {
        val copyTable = HashMap<Int, ByteArray>()
        val offsetMap = ArrayList<Pair<Int, Int>>()
        val firstIndex = HashMap<Int, OffsetDuplicate>()
        var indexTable: Map<Int, Int> = HashMap()
        if(hasOffsetTable) {
            parseOffsetTable(content, offsetMap, firstIndex)
        }

        if(hasIndexTable) {
            indexTable = parseIndexTable(content)
        }

        extractRecords(content, copyTable, offsetMap, firstIndex, indexTable)
        return copyTable
    }

    private fun extractRecords(content: LittleEndianStream,
                               copyTable: MutableMap<Int, ByteArray>,
                               offsetMap: ArrayList<Pair<Int, Int>>,
                               firstIndex: MutableMap<Int, OffsetDuplicate>,
                               indexMap: Map<Int, Int>) {
        val numElements = Math.max(numRecords, offsetMap.size)
        val curPos = content.position
        for(i in 0 until numElements) {
            if(hasOffsetTable) {
                val id = indexMap[copyTable.size]!!
                val map = offsetMap[i]
                if(copyTableSize == 0 && firstIndex[map.first]?.hidden != i) {
                    continue
                }

                content.position = map.first
                val bytes = ByteArray(4 + map.second)
                ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).putInt(id).put(content.readBytes(map.second))
                copyTable[id] = bytes
            } else {
                content.position = curPos + i * recordSize
                val recordBytes = content.readBytes(recordSize)
                if(hasIndexTable) {
                    val bytes = ByteArray(4 + recordSize)
                    ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).putInt(indexMap[i]!!).put(recordBytes)
                    copyTable[indexMap[i]!!] = bytes
                } else {
                    val byteCount = fieldStructure[idIndex].byteCount
                    val offset = fieldStructure[idIndex].offset
                    var id = 0
                    for(j in 0 until byteCount) {
                        id = id or (recordBytes[offset + j].toInt() shl (j * 8))
                    }
                    copyTable[id] = recordBytes
                }
            }
        }
    }

    private fun parseOffsetTable(content: LittleEndianStream, offsetMap: ArrayList<Pair<Int, Int>>, firstIndex: MutableMap<Int, OffsetDuplicate>) {
        content.position = offsetTableStart
        for(i in 0 until (maxId - minId + 1)) {
            val offset = content.readInt()
            val length = content.readShort().toInt()
            if(offset == 0 || length == 0) {
                continue
            }

            if(copyTableSize == 0) {
                if(!firstIndex.containsKey(offset)) {
                    firstIndex[offset] = OffsetDuplicate(offsetMap.size, firstIndex.size)
                } else {
                    offsetDuplicates[minId + i] = firstIndex[offset]!!.visible
                }
            }

            offsetMap.add(offset to length)
        }
    }

    private fun parseIndexTable(content: LittleEndianStream): Map<Int, Int> {
        val ret = HashMap<Int, Int>()
        content.position = indexTablePos
        for(i in 0 until numRecords) {
            ret[i] = content.readInt()
        }

        return ret
    }

    private fun parseCopyTable(content: LittleEndianStream, copyTable: MutableMap<Int, ByteArray>) {
        if(copyTableSize <= 0) {
            return
        }

        content.position = copyTablePos
        while(content.position < content.size) {
            val id = content.readInt()
            val idCopy = content.readInt()
            val oldData = copyTable[idCopy] ?: continue
            val newData = ByteArray(oldData.size)
            ByteBuffer.wrap(newData).order(ByteOrder.LITTLE_ENDIAN).putInt(id).put(oldData, 4, oldData.size - 4)
            copyTable[id] = newData
        }
    }

    override fun getRowByIndex(row: Int): Db2Row {
        return Db2Row(recordList[row], stringBlock)
    }

    override fun getRowById(row: Int): Db2Row? {
        val data = recordTable[row] ?: return null
        return Db2Row(data, stringBlock)
    }

    override fun getRecordCount(): Int {
        return numRecords
    }

    override fun getFieldCount(): Int {
        return numFields
    }
}