package ch.cromon.wow.io.files.storage

import ch.cromon.wow.io.LittleEndianStream
import org.slf4j.LoggerFactory
import java.nio.charset.StandardCharsets
import java.util.*

class DbcRowIterator(private var curIndex: Int, val file: StorageFile) : Iterator<StorageRow> {
    override fun hasNext() = curIndex < file.getRowCount()

    override fun next() = file.getRowByIndex(curIndex++)

}

class DbcRow(private val content: LittleEndianStream, private val strings: HashMap<Int, String>) : StorageRow {
    override fun getInt(offset: Int) = withOffset(offset, 4).readInt()
    override fun getFloat(offset: Int) = withOffset(offset, 4).readFloat()
    override fun getString(offset: Int) = strings[getInt(offset)] ?: throw IllegalArgumentException("No string found at offset")
    override fun tryGetString(offset: Int) = strings[getInt(offset)]

    private fun withOffset(offset: Int, size: Int): LittleEndianStream {
        if(offset + size > content.size) {
            throw IllegalArgumentException("Attempted to read past end of DBC record")
        }

        content.position = offset
        return content
    }
}

class DbcFile(content: LittleEndianStream) : StorageFile {
    companion object {
        const val SIGNATURE = 0x43424457
    }

    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val magic: Int = content.readInt()
    private val numRecords: Int = content.readInt()
    private val numFields: Int = content.readInt()
    private val recordSize: Int = content.readInt()
    private val stringBlockSize: Int = content.readInt()

    private val stringBlock = HashMap<Int, String>()

    private val rows = ArrayList<DbcRow>()
    private val rowMap = HashMap<Int, DbcRow>()

    init {
        if(magic != SIGNATURE) {
            logger.error("Magic in DBC does not match. Got ${magic.toString(16)} but expected ${SIGNATURE.toString(16)}")
            throw IllegalArgumentException("Invalid DBC. Magic $magic does not match $SIGNATURE")
        }

        loadStringBlock(content)
        loadRows(content)
    }

    override fun getRowById(id: Int) = rowMap[id]
    override fun getRowByIndex(index: Int) = rows[index]
    override fun getFieldCount() = numFields
    override fun getRowCount() = numRecords

    override fun iterator(): Iterator<StorageRow> {
        return DbcRowIterator(0, this)
    }

    override fun spliterator(): Spliterator<StorageRow> {
        return Spliterators.spliterator(iterator(), getRowCount().toLong(), Spliterator.SIZED)
    }

    private fun loadStringBlock(content: LittleEndianStream) {
        content.position = 20 + numRecords * recordSize
        val stringBytes = content.readBytes(stringBlockSize)
        val curString = ArrayList<Byte>()
        var curStringOffset = 0
        for(i in 0 until stringBlockSize) {
            val byte = stringBytes[i]
            if(byte == 0.toByte()) {
                stringBlock[curStringOffset] = String(curString.toByteArray(), StandardCharsets.UTF_8)
                curStringOffset = i + 1
                curString.clear()
            } else {
                curString.add(byte)
            }
        }
    }

    private fun loadRows(content: LittleEndianStream) {
        content.position = 20

        for(i in 0 until numRecords) {
            val record = content.readBytes(recordSize)
            val row = DbcRow(LittleEndianStream(record), stringBlock)
            val id = row.getInt(0)
            rows.add(row)
            rowMap[id] = row
        }
    }
}