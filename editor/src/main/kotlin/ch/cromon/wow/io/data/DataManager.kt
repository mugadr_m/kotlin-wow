package ch.cromon.wow.io.data

import ch.cromon.wow.io.files.storage.definition.DefinitionLoader
import org.slf4j.Logger
import org.springframework.beans.factory.NoSuchBeanDefinitionException
import org.springframework.beans.factory.NoUniqueBeanDefinitionException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.io.File
import java.util.*
import javax.annotation.PostConstruct

data class DataLoadEvent(val success: Boolean, val exception: Throwable?, val build: Int, private val obj: Any) : ApplicationEvent(obj)

@Component
@Scope("singleton")
class DataManager {
    private lateinit var fileProvider: FileProvider

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    @Autowired
    private lateinit var context: ApplicationContext

    @Autowired
    private lateinit var logger: Logger

    val build get() = fileProvider.build

    @PostConstruct
    fun onInitialize() {
        DefinitionLoader.initialize()
    }

    fun openFile(name: String) = fileProvider.openFile(name)
    fun hasFile(name: String) = fileProvider.hasFile(name)
    fun openFromFileDataId(id: Int) = fileProvider.openFromFileDataId(id)
    fun canOpenByFileDataId(): Boolean = fileProvider.canOpenByFileDataId()

    fun initFromCdn(region: String) {
        if(!loadProvider("CDN")) {
            return
        }

        val properties = Properties()
        properties[REGION_PROPERTY] = region
        initialize(properties)
    }

    fun initFromLocal(path: String) {
        if(isMpqBased(path)) {
            if(!loadProvider("MPQ")) {
                return
            }
        } else {
            if (!loadProvider("LOCAL")) {
                return
            }
        }

        val properties = Properties()
        properties[GAME_FOLDER_PROPERTY] = path
        initialize(properties)
    }

    fun initFromRemote(url: String) {
        if(!loadProvider("REMOTE")) {
            return
        }

        val properties = Properties()
        properties[REMOTE_SERVER_PROPERTY] = url
        initialize(properties)
    }

    private fun initialize(properties: Properties) {
        fileProvider.initialize(properties).handle {
            success, error ->
            if(error != null) {
                publisher.publishEvent(DataLoadEvent(false, error, 0, this))
            } else {
                try {
                    publisher.publishEvent(DataLoadEvent(success, null, fileProvider.build, this))
                } catch (e: Exception) {
                    logger.error("Initialization failed", e)
                    publisher.publishEvent(DataLoadEvent(false, e, 0, this))
                }
            }
        }
    }

    private fun loadProvider(qualifier: String): Boolean {
        try {
            fileProvider = BeanFactoryAnnotationUtils.qualifiedBeanOfType(context.autowireCapableBeanFactory, FileProvider::class.java, qualifier)
            return true
        } catch (e: NoSuchBeanDefinitionException) {
            logger.error("No file provider found for $qualifier", e)
            publisher.publishEvent(DataLoadEvent(false, e, 0, this))
        } catch (e: NoUniqueBeanDefinitionException) {
            logger.error("Multiple file providers found for $qualifier", e)
            publisher.publishEvent(DataLoadEvent(false, e, 0, this))
        }

        return false
    }

    private fun isMpqBased(path: String): Boolean {
        val hasBuildInfo = File(path, ".build.info").exists()
        if(hasBuildInfo) {
            return false
        }

        val dataFolder = File(path, "Data")
        val children = dataFolder.listFiles()
        return children != null && children.find { it.extension.toUpperCase() == "MPQ" } != null
    }
}