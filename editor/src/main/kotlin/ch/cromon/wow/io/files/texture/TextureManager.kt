package ch.cromon.wow.io.files.texture

import ch.cromon.wow.gx.Texture
import ch.cromon.wow.io.data.DataManager
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.*
import java.util.concurrent.atomic.AtomicInteger
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy


@Component
@Scope("singleton")
class TextureManager {
    @Autowired
    private lateinit var dataManager: DataManager

    @Autowired
    private lateinit var logger: Logger

    private val workQueue = LinkedBlockingQueue<Runnable>()

    private val executor = ThreadPoolExecutor(2, 8, 20, TimeUnit.SECONDS, workQueue)

    private val cache = HashMap<String, Texture>()

    private val workItems = LinkedList<() -> Unit>()

    private val unloadList = LinkedList<Texture>()

    private val cleanupTextures = LinkedList<Texture>()

    @PostConstruct
    fun initialize() {
        val factoryDelegate = Executors.defaultThreadFactory()
        executor.threadFactory = object : ThreadFactory {
            val counter = AtomicInteger()

            override fun newThread(r: Runnable?): Thread {
                val thread = factoryDelegate.newThread(r)
                thread.name = "TextureManagerPool-thread-" + counter.getAndIncrement()
                return thread
            }
        }

        // if a texture cannot be queued that's not a problem it just stays the default texture
        executor.rejectedExecutionHandler = RejectedExecutionHandler{ _, _ -> }
    }

    @PreDestroy
    fun shutdown() {
        executor.shutdown()
    }

    fun release(texture: Texture) {
        texture.tag ?: return

        synchronized(cache) {
            if(!texture.release()) {
                return
            }

            unloadList.add(texture)
        }
    }

    fun compact() {
        synchronized(cache) {
            // avoid creating an iterator object that needs GC on an empty collection
            if(unloadList.isNotEmpty()) {
                for(tex in unloadList) {
                    if(tex.isDestroyed()) {
                        cleanupTextures.add(tex)
                        cache.remove(tex.tag)
                    }
                }

                unloadList.clear()
            }
        }
    }

    fun onFrame() {
        var numLoaded = 0
        while(numLoaded < 2) {
            val workItem: () -> Unit = workItems.pollFirst() ?: break
            workItem()
            ++numLoaded
        }

        var numUnloaded = 0
        while(numUnloaded < 5) {
            val workItem = cleanupTextures.pollFirst() ?: break
            workItem.delete()
            ++numUnloaded
        }
    }

    fun loadTexture(file: String): Texture {
        val name = file.toUpperCase()
        val texture = synchronized(cache) {
            val entry = cache[name]
            if(entry != null) {
                entry.addRef()
                return entry
            }

            val ret = Texture()
            ret.addRef()
            ret.tag = name
            cache[name] = ret
            ret
        }

        executor.submit({ onLoadTexture(texture, file) })
        return texture
    }

    private fun onLoadTexture(texture: Texture, file: String) {
        val content = dataManager.openFile(file)
        if(content == null) {
            logger.warn("Texture not found: $file")
            return
        }

        val data = BlpFile.loadAllLayers(content)
        if(data.width < 1 || data.height < 1 || data.data.isEmpty() || data.format == TextureFormat.UNKNOWN) {
            logger.warn("Failed to load texture $file because its data is invalid: $data")
            return
        }

        workItems.add({
            texture.loadFromBlp(data)
        })
    }
}