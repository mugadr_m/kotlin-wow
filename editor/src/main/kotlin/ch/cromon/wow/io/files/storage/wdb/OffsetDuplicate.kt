package ch.cromon.wow.io.files.storage.wdb


data class OffsetDuplicate(val hidden: Int, val visible: Int)