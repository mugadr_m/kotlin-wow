@file:Suppress("FunctionName")

package ch.cromon.wow.io.data.mpq

import ch.cromon.wow.utils.toULong
import com.sun.jna.Library
import com.sun.jna.Native
import com.sun.jna.Pointer
import com.sun.jna.ptr.IntByReference
import com.sun.jna.ptr.PointerByReference
import java.nio.ByteBuffer


interface Stormlib : Library {
    companion object {
        private lateinit var instance: Stormlib
        private val unusedNumRead = IntByReference()
        private val nullPtr = Pointer(0)

        fun loadFromPath(path: String) {
            System.load(path)
            instance = Native.loadLibrary(path, Stormlib::class.java)
        }

        fun SFileOpenArchive(mpqName: String, priority: Int, flags: Int): Pointer? {
            val ref = PointerByReference()
            val ret = instance.SFileOpenArchive(mpqName, priority, flags, ref)
            if(ret == 0.toByte()) {
                return null
            }

            return ref.value
        }

        fun SFileOpenFileEx(mpq: Pointer, file: String): Pointer? {
            val ref = PointerByReference()
            val ret = instance.SFileOpenFileEx(mpq, file, 0, ref)
            if(ret == 0.toByte()) {
                return null
            }

            return ref.value
        }

        fun SFileHasFile(mpq: Pointer, file: String): Boolean {
            return instance.SFileHasFile(mpq, file) != 0.toByte()
        }

        fun SFileGetFileSize(file: Pointer): Long {
            val ref = IntByReference()
            val low = instance.SFileGetFileSize(file, ref).toULong()
            val high = ref.value.toULong()
            return (low or (high shl 32))
        }

        fun SFileReadFile(file: Pointer, buffer: ByteArray) {
            instance.SFileReadFile(file, buffer, buffer.size, unusedNumRead, nullPtr)
        }

        fun SFileReadFile(file: Pointer, buffer: ByteBuffer, toRead: Int) {
            instance.SFileReadFile(file, buffer, toRead, unusedNumRead, nullPtr)
        }

        fun SFileCloseFile(file: Pointer) {
            instance.SFileCloseFile(file)
        }

        fun SFileSetFilePointer(file: Pointer, position: Int, method: Int = 0): Int {
            return instance.SFileSetFilePointer(file, position, 0, method)
        }
    }

    fun SFileOpenArchive(mpqName: String, priority: Int, flags: Int, phMpq: PointerByReference): Byte
    fun SFileHasFile(mpq: Pointer, file: String): Byte
    fun SFileOpenFileEx(mpq: Pointer, file: String, scope: Int, phFile: PointerByReference): Byte
    fun SFileGetFileSize(file: Pointer, highSize: IntByReference): Int
    fun SFileReadFile(file: Pointer, buffer: ByteArray, toRead: Int, numRead: IntByReference, overlapped: Pointer)
    fun SFileReadFile(file: Pointer, buffer: ByteBuffer, toRead: Int, numRead: IntByReference, overlapped: Pointer)
    fun SFileCloseFile(file: Pointer)
    fun SFileSetFilePointer(file: Pointer, lowPos: Int, hiPos: Int, method: Int): Int
}