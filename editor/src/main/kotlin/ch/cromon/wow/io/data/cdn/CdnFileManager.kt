package ch.cromon.wow.io.data.cdn

import ch.cromon.wow.io.LittleEndianFileStream
import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.data.*
import ch.cromon.wow.io.http.HttpClient
import ch.cromon.wow.utils.I18N
import ch.cromon.wow.utils.I18N.CDN_DOWNLOAD_PROGRESS
import ch.cromon.wow.utils.I18N.CDN_GET_SERVER
import ch.cromon.wow.utils.I18N.CDN_GET_SERVER_ERROR_REGION
import ch.cromon.wow.utils.I18N.CDN_LOAD_ADDRESSES
import ch.cromon.wow.utils.I18N.CDN_LOAD_ADDRESSES_DONE
import ch.cromon.wow.utils.I18N.CDN_LOAD_ADDRESSES_ERROR
import ch.cromon.wow.utils.I18N.CDN_LOAD_VERSION
import ch.cromon.wow.utils.I18N.ENCODING_DECOMPRESS
import ch.cromon.wow.utils.I18N.ENCODING_DONE
import ch.cromon.wow.utils.I18N.ENCODING_PARSE
import ch.cromon.wow.utils.I18N.ROOT_DECOMPRESS
import ch.cromon.wow.utils.I18N.ROOT_DONE
import ch.cromon.wow.utils.I18N.ROOT_PARSE
import ch.cromon.wow.utils.isAllZero
import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import org.springframework.util.MultiValueMap
import org.springframework.util.StopWatch
import java.io.ByteArrayInputStream
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.stream.Collectors

@Component
@Qualifier("CDN")
@Scope("prototype")
class CdnFileManager : FileProvider() {
    companion object {
        private val logger = LoggerFactory.getLogger(CdnFileManager::class.java)
        private const val CDN_URL_PATTERN = "http://%s.patch.battle.net:1119/wow/cdns"
        private const val VERSION_URL_PATTERN = "http://%s.patch.battle.net:1119/wow/versions"
    }

    private val indexData = TreeMap<FileKeyMD5, IndexEntry>()

    private lateinit var cdnConfig: TokenConfig
    private lateinit var versionConfig: TokenConfig
    private lateinit var cdnConfigFile: String
    private lateinit var buildConfig: String

    private lateinit var buildInfoConfig: KeyValueConfig
    private lateinit var cdnInfoConfig: KeyValueConfig

    private lateinit var cdnBaseUrl: String

    private lateinit var encodingMap: Map<FileKeyMD5, EncodingEntry>
    private lateinit var rootMap: MultiValueMap<Long, FileKeyMD5>
    private lateinit var fileRootMap: MultiValueMap<Int, FileKeyMD5>

    private val archives = ArrayList<String>()

    override fun canOpenByFileDataId() = true

    override fun containsFileDataId(id: Int): Boolean {
        try {
            val rootInfo = fileRootMap.getFirst(id) ?: return false
            val encodingInfo = encodingMap[rootInfo] ?: return false
            indexData[encodingInfo.key] ?: return false
            return true
        } catch (e: NoSuchElementException) {
            return false
        }
    }

    override fun openFromFileDataId(fileDataId: Int): LittleEndianStream? {
        try {
            val rootInfo = fileRootMap.getFirst(fileDataId) ?: return null
            val encodingInfo = encodingMap[rootInfo] ?: return null
            val indexInfo = indexData[encodingInfo.key] ?: return downloadDataDirect(encodingInfo.key, {})
            return BlteHandler(downloadFile(indexInfo)).decompressedData
        } catch (e: NoSuchElementException) {
            return null
        }
    }

    override fun hasFile(name: String): Boolean {
        val hash = JenkinsHash.calculate(name)
        try {
            val rootInfo = rootMap.getFirst(hash) ?: return false
            val encodingInfo = encodingMap[rootInfo] ?: return false

            indexData[encodingInfo.key] ?: return dataFileExists(encodingInfo.key)
            return true
        } catch (e: NoSuchElementException) {
            return false
        }
    }

    override fun openFile(name: String): LittleEndianStream? {
        val hash = JenkinsHash.calculate(name)
        try {
            val rootInfo = rootMap.getFirst(hash) ?: return null
            val encodingInfo = encodingMap[rootInfo] ?: return null

            val indexInfo = indexData[encodingInfo.key] ?: return downloadDataDirect(encodingInfo.key, {})
            return BlteHandler(downloadFile(indexInfo)).decompressedData
        } catch (e: NoSuchElementException) {
            return null
        }

    }

    override fun openFileAsStream(name: String): LittleEndianFileStream? {
        throw NotImplementedError("This function is not available for CASC")
    }

    override fun initialize(properties: Properties): CompletableFuture<Boolean> {
        return CompletableFuture.supplyAsync({
            val region = properties.getProperty(REGION_PROPERTY)
            val watch = StopWatch("CDN Loading")
            if (!(loadCdnAddresses(region, watch) &&
                    selectCdnServer(region, watch) &&
                    loadVersionConfig(region, watch) &&
                    selectBuildVersion(region, watch))) {
                logger.info(watch.prettyPrint())
                return@supplyAsync false
            }
            loadBuildConfig(watch)
            loadIndexFiles(watch)
            val ret = loadEncodingFile(watch) &&
                    loadRootFile(watch)
            logger.info(watch.prettyPrint())
            ret
        }).handle({ success, error ->
            if (error != null) {
                logger.error("Failed to load CDN", error)
            }
            success
        })
    }

    private fun downloadFile(entry: IndexEntry): LittleEndianStream {
        val archive = archives[entry.index]
        val url = "$cdnBaseUrl/data/${archive.substring(0, 2)}/${archive.substring(2, 4)}/$archive"
        return LittleEndianStream(HttpClient.download(url, "Range" to "bytes=${entry.offset}-${entry.offset + entry.size}"))
    }

    private fun loadCdnAddresses(region: String, watch: StopWatch): Boolean {
        return try {
            handleProgress(CDN_LOAD_ADDRESSES)
            watch.start("Load CDN Addresses")
            val url = String.format(CDN_URL_PATTERN, region)
            handleProgress(CDN_LOAD_ADDRESSES_DONE, url)
            logger.info("Loading CDN configuration from $url")
            val content = HttpClient.download(url)
            cdnConfig = TokenConfig(ByteArrayInputStream(content))
            watch.stop()
            true
        } catch (e: Exception) {
            handleProgress(CDN_LOAD_ADDRESSES_ERROR, e.toString())
            logger.error("Failed to fetch CDN addresses", e)
            false
        }
    }

    private fun selectCdnServer(targetRegion: String, watch: StopWatch): Boolean {
        handleProgress(CDN_GET_SERVER)
        watch.start("Selecting CDN Server")
        var selectedIndex = -1
        for ((index, elements) in cdnConfig.withIndex()) {
            val region = elements["Name"]
            if (selectedIndex < 0) {
                selectedIndex = index
            }

            if (targetRegion == region) {
                selectedIndex = index
                break
            }
        }

        if (selectedIndex < 0) {
            handleProgress(CDN_GET_SERVER_ERROR_REGION)
            logger.error("Region not found in CDN configuration")
            return false
        }

        val infoElems = cdnConfig[selectedIndex]
        val cdnPath = infoElems["Path"]
        val hosts = infoElems["Hosts"]?.split(' ')
        if (cdnPath == null || hosts == null || hosts.isEmpty()) {
            logger.error("Provided CDN config is invalid. Required information is missing")
            return false
        }

        cdnBaseUrl = "http://${hosts[0]}/$cdnPath"
        logger.info("CDN Base URL is: $cdnBaseUrl")
        watch.stop()
        return true
    }

    private fun loadVersionConfig(region: String, watch: StopWatch): Boolean {
        return try {
            handleProgress(CDN_LOAD_VERSION)
            watch.start("Loading Version Info")
            val url = String.format(VERSION_URL_PATTERN, region)
            logger.info("Loading version configuration from $url")
            val content = HttpClient.download(url)
            versionConfig = TokenConfig(ByteArrayInputStream(content))
            watch.stop()
            true
        } catch (e: Exception) {
            logger.error("Failed to fetch CDN versions", e)
            false
        }
    }

    private fun selectBuildVersion(targetRegion: String, watch: StopWatch): Boolean {
        watch.start("Selecting Build Version")
        var selectedIndex = -1
        for ((index, elements) in versionConfig.withIndex()) {
            if (selectedIndex < 0) {
                selectedIndex = index
            }

            val region = elements["Region"]
            if (targetRegion == region) {
                selectedIndex = index
                break
            }
        }

        if (selectedIndex < 0) {
            logger.error("No version configuration found for region")
            return false
        }

        val buildInfo = versionConfig[selectedIndex]
        val buildId = buildInfo["BuildId"]
        if(buildId == null) {
            logger.error("No build id found in version config")
            return false
        }

        val versionName = buildInfo["VersionsName"]
        logger.info("Build Information - Build ID: $buildId, Version: $versionName")
        buildVersion = buildId.toInt()

        val config = buildInfo["CDNConfig"]
        val buildCfg = buildInfo["BuildConfig"]
        if (config == null || config.isBlank()) {
            logger.error("No CDN Config file found in build configuration")
            return false
        }

        if (buildCfg == null || buildCfg.isBlank()) {
            logger.error("No build configuration found in CDN configuration")
            return false
        }

        cdnConfigFile = config
        buildConfig = buildCfg
        logger.info("Loading CDN info from $cdnConfigFile")
        watch.stop()
        return true
    }

    private fun loadBuildConfig(watch: StopWatch) {
        watch.start("Loading Build Config")
        val rawBuildConfig = downloadFileDirect(buildConfig)
        buildInfoConfig = KeyValueConfig(ByteArrayInputStream(rawBuildConfig))

        val rawCdnConfig = downloadFileDirect(cdnConfigFile)
        cdnInfoConfig = KeyValueConfig(ByteArrayInputStream(rawCdnConfig))
        watch.stop()
    }


    private fun loadEncodingFile(watch: StopWatch): Boolean {
        val encodingKey = buildInfoConfig["encoding"]?.get(1) ?: return false
        logger.info("Loading encoding file from $encodingKey")

        watch.start("Download Encoding File")
        val encodingFile = downloadDataDirect(FileKeyMD5(HexBin.decode(encodingKey)), { pct -> onProgress("Encoding File", pct * 100) })
        watch.stop()
        watch.start("Decode Encoding File")
        handleProgress(ENCODING_DECOMPRESS)
        val blteParser = BlteHandler(encodingFile)
        watch.stop()
        handleProgress(ENCODING_PARSE)
        watch.start("Parse Encoding File")
        try {
            encodingMap = EncodingParser(blteParser.decompressedData).encodingData
        } catch (e: Exception) {
            logger.error("Error parsing Encoding file", e)
        }
        watch.stop()
        logger.info("Finished loading encoding file")
        handleProgress(ENCODING_DONE)

        return true
    }

    private fun loadIndexFiles(watch: StopWatch) {
        try {
            watch.start("Parse Indices")
            val archives = cdnInfoConfig["archives"] ?: ArrayList()
            this.archives.clear()
            this.archives.addAll(archives)
            archives.withIndex().mapIndexed({ _, elem ->
                object {
                    val index = elem.index
                    val value = elem.value
                }
            }).parallelStream()
                    .map {
                        object {
                            val key = it.value
                            val content = downloadIndexFile(it.value)
                            val index = it.index
                        }
                    }
                    .collect(Collectors.toList())
                    .forEach {
                        handleProgress(I18N.CDN_LOAD_INDEX, it.key)
                        parseIndexFile(it.content, it.index)
                        logger.debug("Loaded file: ${it.key}")
                    }
            watch.stop()

        } catch (e: Exception) {
            logger.error("Error loading index files", e)
        }
    }

    private fun loadRootFile(watch: StopWatch): Boolean {
        val rootKey = buildInfoConfig["root"]?.get(0) ?: return false
        logger.info("Loading root file from $rootKey")

        val encodingEntry = encodingMap[FileKeyMD5(rootKey)] ?: return false

        watch.start("Download Root File")
        val rootDataRaw = downloadDataDirect(encodingEntry.key, { pct ->
            onProgress("Root File", pct * 100)
        })
        watch.stop()
        handleProgress(ROOT_DECOMPRESS)
        watch.start("Decode Root File")
        val rootData = BlteHandler(rootDataRaw).decompressedData
        watch.stop()
        watch.start("Parse Root File")
        handleProgress(ROOT_PARSE)
        val rootParser = RootParser(rootData)
        rootMap = rootParser.rootData
        fileRootMap = rootParser.fileDataIds
        watch.stop()
        logger.info("Root file loaded")
        handleProgress(ROOT_DONE)
        return true
    }

    private fun downloadFileDirect(fileKey: String): ByteArray {
        val url = "$cdnBaseUrl/config/${fileKey.substring(0, 2)}/${fileKey.substring(2, 4)}/$fileKey"
        return HttpClient.download(url)
    }

    private fun downloadIndexFile(key: String): LittleEndianStream {
        val file = "data/${key.substring(0, 2)}/${key.substring(2, 4)}/$key.index"
        val url = "$cdnBaseUrl/$file"
        val content = HttpClient.download(url)
        return LittleEndianStream(content)
    }

    private fun parseIndexFile(content: LittleEndianStream, index: Int): Boolean {
        content.position = content.size - 12
        val numElements = content.readInt()
        content.position = 0

        for (i in 0 until numElements) {
            var hash = content.readMd5()
            if (hash.isAllZero()) {
                hash = content.readMd5()
                if (hash.isAllZero()) {
                    logger.error("Found zero key with zero backup")
                    return false
                }
            }

            val size = content.readIntBE()
            val offset = content.readIntBE()

            indexData[FileKeyMD5(hash)] = IndexEntry(index, offset, size)
        }

        return true
    }

    private fun downloadDataDirect(key: FileKeyMD5, progress: (Float) -> Unit): LittleEndianStream {
        val hash = key.toHexString()
        val url = "$cdnBaseUrl/data/${hash.substring(0, 2)}/${hash.substring(2, 4)}/$hash"
        return LittleEndianStream(HttpClient.downloadWithProgress(url, { downloaded, total ->
            progress(downloaded.toFloat() / total.toFloat())
        }))
    }

    private fun dataFileExists(key: FileKeyMD5): Boolean {
        val hash = key.toHexString()
        val url = "$cdnBaseUrl/data/${hash.substring(0, 2)}/${hash.substring(2, 4)}/$hash"
        return HttpClient.exists(url)
    }

    private fun onProgress(message: String, pct: Float) {
        handleProgress(CDN_DOWNLOAD_PROGRESS, message, pct)
    }
}