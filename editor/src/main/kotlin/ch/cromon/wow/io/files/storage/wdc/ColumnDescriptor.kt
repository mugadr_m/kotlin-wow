package ch.cromon.wow.io.files.storage.wdc

enum class CompressionType {
    NONE,
    IMMEDIATE,
    SPARSE,
    PALLET,
    PALLET_ARRAY
}

data class ColumnDescriptor(
        val recordOffset: Int,
        val size: Int,
        val additionalDataSize: Int,
        val compressionType: CompressionType,
        val bitOffset: Int,
        val bitWidth: Int,
        val cardinality: Int,
        val palletValues: ArrayList<ByteArray> = ArrayList(),
        val sparseValues: HashMap<Int, ByteArray> = HashMap(),
        var arraySize: Int = 1
        )