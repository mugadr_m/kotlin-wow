package ch.cromon.wow.io.files.storage

import org.slf4j.LoggerFactory

data class LiquidType(
        val id: Int,
        val name: String,
        val textures: List<String>
)

object LiquidDataLoader : AbstractFormatGuesser() {
    private val logger = LoggerFactory.getLogger(LiquidDataLoader.javaClass)

    private lateinit var liquidDb: StorageFile
    private var build: Int = 0

    private var textureIndex = -1
    private var nameIndex = -1

    fun getLiquidTypeById(id: Int): LiquidType? {
        val row = liquidDb.getRowById(id) ?: return null
        return LiquidType(
                row.getInt(0),
                if(nameIndex < 0) "<unknown>" else row.getString(nameIndex * 4),
                (0 until 6).map { row.getString((textureIndex + it) * 4)}
        )
    }

    fun initialize(build: Int, liquidDb: StorageFile) {
        this.liquidDb = liquidDb
        this.build = build

        loadTextures()
    }

    private fun loadTextures() {
        if(this.build < 8634 || this.build > 12340) {
            return
        }

        textureIndex = getBestMatchingStringField(liquidDb, { it.endsWith(".blp", true) }, 0)
        if(textureIndex < 0) {
            logger.error("Unable to find texture offset in LiquidType storage")
            throw IllegalArgumentException("No texture offset found in LiquidType")
        }


        nameIndex = getBestMatchingStringField(liquidDb, { true }, 0, *((0 until 6).map { it + textureIndex }.toIntArray()))
        if(nameIndex < 0) {
            logger.warn("No name index found in LiquidType storage")
        }
    }
}