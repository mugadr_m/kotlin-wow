package ch.cromon.wow.io.data.remote

import java.net.URI


data class ServerDefinition(val name: String, val type: ServerType, val url: URI)

data class Servers(private val servers: List<ServerDefinition>) : Iterable<ServerDefinition> {
    override fun iterator(): Iterator<ServerDefinition> = servers.iterator()
}