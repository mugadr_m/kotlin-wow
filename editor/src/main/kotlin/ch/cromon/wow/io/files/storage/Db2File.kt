package ch.cromon.wow.io.files.storage

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.files.storage.wdb.Db5Parser
import ch.cromon.wow.io.files.storage.wdb.Db6Parser
import ch.cromon.wow.io.files.storage.wdc.Wdc1Parser
import java.nio.charset.StandardCharsets
import java.util.*

class Db2Row(private val content: LittleEndianStream, private val stringBlock: Map<Int, String>) : StorageRow {
    override fun getInt(offset: Int) = withOffset(offset, 4).readInt()
    override fun getFloat(offset: Int) = withOffset(offset, 4).readFloat()
    override fun getString(offset: Int) = stringBlock[getInt(offset)] ?: throw IllegalArgumentException("No string found at offset")
    override fun tryGetString(offset: Int) = stringBlock[getInt(offset)]

    private fun withOffset(offset: Int, size: Int): LittleEndianStream {
        if(offset + size > content.size) {
            throw IllegalArgumentException("Attempted to read past end of DBC record")
        }

        content.position = offset
        return content
    }
}

abstract class Db2FormatParser {
    abstract fun getRowByIndex(row: Int): Db2Row
    abstract fun getRowById(row: Int): Db2Row?
    abstract fun getRecordCount(): Int
    abstract fun getFieldCount(): Int

    protected fun parseStringBlock(outBlock: HashMap<Int, String>, content: ByteArray) {
        val curString = ArrayList<Byte>()
        var curStringOffset = 0
        for((index, byte) in content.withIndex()) {
            if(byte == 0.toByte()) {
                outBlock[curStringOffset] = String(curString.toByteArray(), StandardCharsets.UTF_8)
                curStringOffset = index + 1
                curString.clear()
            } else {
                curString.add(byte)
            }
        }
    }
}

class Db2Iterator(private var curIndex: Int, private val file: Db2File) : Iterator<StorageRow> {
    override fun hasNext() = curIndex < file.getRowCount()
    override fun next() = file.getRowByIndex(curIndex++)
}

class Db2File(content: LittleEndianStream, build: Int) : StorageFile {
    private val magic = String(content.readBytes(4), StandardCharsets.ISO_8859_1)
    private val parser: Db2FormatParser

    init {
        parser = when(magic) {
            "WDB5" -> Db5Parser(content)
            "WDB6" -> Db6Parser(content, build)
            "WDC1" -> Wdc1Parser(content)
            else -> throw IllegalArgumentException("Parser not found for file signature '$magic'")
        }
    }

    override fun iterator() = Db2Iterator(0, this)

    override fun spliterator() = Spliterators.spliterator(iterator(), getRowCount().toLong(), Spliterator.SIZED)!!

    override fun getRowById(id: Int) = parser.getRowById(id)
    override fun getRowByIndex(index: Int) = parser.getRowByIndex(index)
    override fun getFieldCount() = parser.getFieldCount()
    override fun getRowCount() = parser.getRecordCount()
}