package ch.cromon.wow.io.files.storage

import ch.cromon.wow.io.data.DataManager


abstract class AbstractFormatGuesser {
    protected var fileData: StorageFile? = null
    protected lateinit var dataManager: DataManager

    protected fun getBestMatchingIntField(file: StorageFile, filter: (Int) -> Boolean, vararg excludes: Int): Int {
        return getBestMatchingField(file, { row, offset ->
            if (filter(row.getInt(offset))) MatchType.MATCH to row.getInt(offset) else MatchType.NO_MATCH to null
        }, null, *excludes)
    }

    protected fun getBestMatchingIntFieldWithVariance(file: StorageFile, filter: (Int) -> Boolean, vararg excludes: Int): Int {
        return getBestMatchingField(file, { row, offset ->
            if (filter(row.getInt(offset))) MatchType.MATCH to row.getInt(offset) else MatchType.NO_MATCH to null
        }, { fields, values ->
            var maxIndex = -1
            var maxValue = -1
            for (i in 0 until fields.size) {
                val rowValues = values[i]
                val distinct = rowValues.distinct().size
                if (distinct > maxValue) {
                    maxIndex = i
                    maxValue = distinct
                }
            }

            (0 until fields.size)
                    .filter { it != maxIndex }
                    .forEach { fields[it] = false }
        }, *excludes)
    }

    protected fun getBestMatchingStringField(file: StorageFile, filter: (String) -> Boolean, vararg excludes: Int): Int {
        return getBestMatchingField(file, { row, offset ->
            val maybeString = row.tryGetString(offset)

            if (maybeString == null || maybeString.isEmpty()) {
                MatchType.INVALID to null
            } else {
                if (filter(maybeString)) MatchType.MATCH to maybeString else MatchType.NO_MATCH to null
            }
        }, null, *excludes)
    }

    protected fun getBestMatchingVector3(file: StorageFile, vararg excludes: Int): Int {
        return getBestMatchingField(file, { row, offset ->
            if (offset > (file.getFieldCount() * 4 - 12)) {
                MatchType.INVALID to null
            } else {
                if (isLocationFloat(row.getFloat(offset)) &&
                        isLocationFloat(row.getFloat(offset + 4)) &&
                        isLocationFloat(row.getFloat(offset + 8)))
                    MatchType.MATCH to row.getFloat(offset)
                else
                    MatchType.INVALID to null
            }
        }, null, *excludes)
    }

    protected fun getBestMatchingBoolean(file: StorageFile, vararg excludes: Int): Int {
        return getBestMatchingField(file, { row, offset ->
            val value = row.getInt(offset)
            if (value != 0 && value != 1) {
                MatchType.INVALID to null
            } else {
                MatchType.MATCH to (value != 0)
            }
        }, { fields, values ->
            for ((index, row) in values.withIndex()) {
                if (row.distinct().size == 1) {
                    fields[index] = false
                }
            }
        }, *excludes)
    }

    private fun <T> getBestMatchingField(
            file: StorageFile,
            filter: (StorageRow, Int) -> Pair<MatchType, T?>,
            valueFilter: ((MutableList<Boolean>, List<ArrayList<T>>) -> Unit)?,
            vararg excludes: Int): Int {
        val numFields = file.getFieldCount()
        val potentialIndices = (0 until numFields).map { true }.toMutableList()
        val matchCounts = (0 until numFields).map { 0 }.toMutableList()
        val values = (0 until numFields).map { ArrayList<T>() }
        excludes.forEach { potentialIndices[it] = false }
        for (i in 0 until file.getRowCount()) {
            val row = file.getRowByIndex(i)
            for (j in 0 until numFields) {
                if (!potentialIndices[j]) {
                    continue
                }

                val ret = filter(row, j * 4)
                when (ret.first) {
                    MatchType.INVALID -> {
                        if (matchCounts[j] <= 0) {
                            potentialIndices[j] = false
                            matchCounts[j] = 0
                        }
                    }
                    MatchType.MATCH -> {
                        matchCounts[j]++
                        val value = ret.second
                        if (value != null) {
                            values[j].add(value)
                        }
                    }
                    MatchType.NO_MATCH -> {
                    }
                }
            }
        }

        if (valueFilter != null) {
            valueFilter(potentialIndices, values)
        }

        return potentialIndices
                .withIndex()
                .zip(matchCounts)
                .filter { it.first.value }
                .sortedByDescending { it.second }
                .map { it.first.index }
                .firstOrNull() ?: -1
    }

    private fun isLocationFloat(value: Float): Boolean {
        return !value.isInfinite() && !value.isNaN() &&
                value < 1e9 && value > -1e9
    }
}