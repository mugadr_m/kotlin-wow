package ch.cromon.wow.io.files.storage.wdb

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.files.storage.Db2FormatParser
import ch.cromon.wow.io.files.storage.Db2Row
import java.nio.ByteBuffer
import java.nio.ByteOrder


class Db6Parser(content: LittleEndianStream, private val build: Int) : Db2FormatParser() {
    companion object {
        private val COMMON_DATA_BITS = arrayListOf(0, 16, 24, 0, 0)
    }

    private val recordCount = content.readInt()
    private var fieldCount = content.readInt()
    private val recordSize = content.readInt()
    private val stringBlockSize = content.readInt()
    private val offsetTableStart = stringBlockSize
    private val tableHash = content.readInt()
    private val layoutHash = content.readInt()
    private val minId = content.readInt()
    private val maxId = content.readInt()
    private val copyTableSize = content.readInt()
    private val flags = content.readShort().toInt()
    private var idIndex = content.readShort().toInt()
    private val totalFieldSize = content.readInt()
    private val commonDataTableSize = content.readInt()

    private val hasIndexTable = HeaderFlags.HAS_INDEX_MAP setIn flags
    private val hasOffsetTable = HeaderFlags.HAS_OFFSET_MAP setIn flags

    private val fieldStructure = ArrayList<FieldDescriptor>()
    private var internalRecordSize = recordSize
    private var copyTablePos = 0
    private var indexTablePos = 0
    private var commonDataTablePos = 0
    private val stringBlock = HashMap<Int, String>()
    private val offsetDuplicates = HashMap<Int, Int>()

    init {
        var indexOffset = 0
        if(hasIndexTable) {
            idIndex = 0
            indexOffset = 4
        }

        for(i in 0 until fieldCount) {
            fieldStructure.add(FieldDescriptor(content.readShort().toInt(), content.readShort().toInt() + indexOffset))
            if(i > 0) {
                fieldStructure[i - 1].calcLength(fieldStructure[i])
            }
        }

        if(hasIndexTable) {
            fieldStructure.add(0, FieldDescriptor(0, 0))
            ++fieldCount
            internalRecordSize += 4
            if(fieldCount > 1) {
                fieldStructure[1].calcLength(fieldStructure[0])
            }
        }

        val headerEnd = content.position
        commonDataTablePos = content.size - commonDataTableSize
        copyTablePos = commonDataTablePos - copyTableSize
        indexTablePos = copyTablePos - if(hasIndexTable) 4 * recordCount else 0

        if(!hasOffsetTable) {
            content.position = indexTablePos - stringBlockSize
            parseStringBlock(stringBlock, content.readBytes(stringBlockSize))
        }

        content.position = headerEnd
        val copyTable = populateCopyTable(content)
        parseCopyTable(content, copyTable)
        parseCommonDataTable(content, copyTable)
    }

    private fun parseCommonDataTable(content: LittleEndianStream, copyTable: MutableMap<Int, ByteArray>) {
        if(commonDataTableSize <= 0) {
            return
        }

        content.position = commonDataTablePos
        val numColumns = content.readInt()
        val lookup = Array<HashMap<Int, ByteArray>>(numColumns, { HashMap() })
        for(i in 0 until numColumns) {
            val count = content.readInt()
            val type = content.readByte().toInt()
            val bits = COMMON_DATA_BITS[type]
            val size = (32 - bits) shr 3
            if(i >= fieldStructure.size) {
                val offset = if(fieldStructure.isEmpty()) 0 else (fieldStructure[i - 1].offset + fieldStructure[i - 1].byteCount)
                fieldStructure.add(FieldDescriptor(bits, offset, type))
                if(i > 0) {
                    fieldStructure[i - 1].calcLength(fieldStructure[i])
                }
            }

            for(j in 0 until count) {
                lookup[i][content.readInt()] = content.readBytes(size)
                if(build >= 24492) {
                    content.position += 4 - size
                }
            }
        }

        extendCopyTable(lookup, copyTable)
    }

    private fun extendCopyTable(lookup: Array<HashMap<Int, ByteArray>>, copyTable: MutableMap<Int, ByteArray>) {
        for(id in copyTable.keys) {
            for(i in 0 until lookup.size) {
                if(!fieldStructure[i].isCommonColumn) {
                    continue
                }

                val column = lookup[i]
                val zeroData = ByteArray(fieldStructure[i].byteCount)
                val currentData = copyTable[id]!!
                val data = column[id] ?: zeroData
                val newData = ByteArray(currentData.size + data.size)
                ByteBuffer.wrap(newData).order(ByteOrder.LITTLE_ENDIAN).put(currentData).put(data)
                copyTable[id] = newData
            }
        }

        internalRecordSize = copyTable.values.firstOrNull()?.size ?: 0
    }

    private fun populateCopyTable(content: LittleEndianStream): MutableMap<Int, ByteArray> {
        val copyTable = HashMap<Int, ByteArray>()
        val offsetMap = ArrayList<Pair<Int, Int>>()
        val firstIndex = HashMap<Int, OffsetDuplicate>()
        var indexTable: Map<Int, Int> = HashMap()
        if(hasOffsetTable) {
            parseOffsetTable(content, offsetMap, firstIndex)
        }

        if(hasIndexTable) {
            indexTable = parseIndexTable(content)
        }

        extractRecords(content, copyTable, offsetMap, firstIndex, indexTable)
        return copyTable
    }

    private fun extractRecords(content: LittleEndianStream,
                               copyTable: MutableMap<Int, ByteArray>,
                               offsetMap: ArrayList<Pair<Int, Int>>,
                               firstIndex: MutableMap<Int, OffsetDuplicate>,
                               indexMap: Map<Int, Int>) {
        val numElements = Math.max(recordCount, offsetMap.size)
        val curPos = content.position
        for(i in 0 until numElements) {
            if(hasOffsetTable) {
                val id = indexMap[copyTable.size]!!
                val map = offsetMap[i]
                if(copyTableSize == 0 && firstIndex[map.first]?.hidden != i) {
                    continue
                }

                content.position = map.first
                val bytes = ByteArray(4 + map.second)
                ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).putInt(id).put(content.readBytes(map.second))
                copyTable[id] = bytes
            } else {
                content.position = curPos + i * recordSize
                val recordBytes = content.readBytes(recordSize)
                if(hasIndexTable) {
                    val bytes = ByteArray(4 + recordSize)
                    ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).putInt(indexMap[i]!!).put(recordBytes)
                    copyTable[indexMap[i]!!] = bytes
                } else {
                    val byteCount = fieldStructure[idIndex].byteCount
                    val offset = fieldStructure[idIndex].offset
                    var id = 0
                    for(j in 0 until byteCount) {
                        id = id or (recordBytes[offset + j].toInt() shl (j * 8))
                    }
                    copyTable[id] = recordBytes
                }
            }
        }
    }

    private fun parseOffsetTable(content: LittleEndianStream, offsetMap: ArrayList<Pair<Int, Int>>, firstIndex: MutableMap<Int, OffsetDuplicate>) {
        content.position = offsetTableStart
        for(i in 0 until (maxId - minId + 1)) {
            val offset = content.readInt()
            val length = content.readShort().toInt()
            if(offset == 0 || length == 0) {
                continue
            }

            if(copyTableSize == 0) {
                if(!firstIndex.containsKey(offset)) {
                    firstIndex[offset] = OffsetDuplicate(offsetMap.size, firstIndex.size)
                } else {
                    offsetDuplicates[minId + i] = firstIndex[offset]!!.visible
                }
            }

            offsetMap.add(offset to length)
        }
    }

    private fun parseIndexTable(content: LittleEndianStream): Map<Int, Int> {
        val ret = HashMap<Int, Int>()
        content.position = indexTablePos
        for(i in 0 until recordCount) {
            ret[i] = content.readInt()
        }

        return ret
    }

    private fun parseCopyTable(content: LittleEndianStream, copyTable: MutableMap<Int, ByteArray>) {
        if(copyTableSize <= 0) {
            return
        }

        content.position = copyTablePos
        while(content.position < content.size) {
            val id = content.readInt()
            val idCopy = content.readInt()
            val oldData = copyTable[idCopy] ?: continue
            val newData = ByteArray(oldData.size)
            ByteBuffer.wrap(newData).order(ByteOrder.LITTLE_ENDIAN).putInt(id).put(oldData, 4, oldData.size - 4)
            copyTable[id] = newData
        }
    }

    override fun getRowByIndex(row: Int): Db2Row {
        TODO("not implemented")
    }

    override fun getRowById(row: Int): Db2Row? {
        TODO("not implemented")
    }

    override fun getRecordCount(): Int {
        TODO("not implemented")
    }

    override fun getFieldCount(): Int {
        TODO("not implemented")
    }
}