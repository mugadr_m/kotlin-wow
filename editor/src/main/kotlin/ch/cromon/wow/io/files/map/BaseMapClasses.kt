package ch.cromon.wow.io.files.map

import ch.cromon.wow.math.BoundingBox


abstract class Tile {
    abstract val indexX: Int
    abstract val indexY: Int
    abstract fun getChunk(x: Int, y: Int): Chunk
    abstract fun unload()

    abstract val boundingBox: BoundingBox

    val mapObjectDefinitions = ArrayList<MapObjectDefinition>()
}

abstract class Chunk {
    abstract val boundingBox: BoundingBox
    abstract val zoneId: Int

    val mapObjectReferences = ArrayList<Int>()
    val mapDoodadReferences = ArrayList<Int>()
}