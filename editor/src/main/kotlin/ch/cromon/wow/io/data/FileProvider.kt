package ch.cromon.wow.io.data

import ch.cromon.wow.io.LittleEndianFileStream
import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.utils.I18N
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationEventPublisher
import java.util.*
import java.util.concurrent.CompletableFuture

data class DataLoadProgressEvent(val message: String, private val trigger: Any): ApplicationEvent(trigger)

abstract class FileProvider {
    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    protected var buildVersion: Int = 0

    val build get() = buildVersion

    abstract fun initialize(properties: Properties): CompletableFuture<Boolean>
    abstract fun openFromFileDataId(fileDataId: Int): LittleEndianStream?
    abstract fun openFile(name: String): LittleEndianStream?
    abstract fun openFileAsStream(name: String): LittleEndianFileStream?
    abstract fun hasFile(name: String): Boolean
    abstract fun canOpenByFileDataId(): Boolean
    abstract fun containsFileDataId(id: Int): Boolean

    protected fun handleProgress(messageKey: String, vararg parameters: Any) {
        val baseMessage = I18N.getString(messageKey)
        val message = if(parameters.isNotEmpty()) {
            String.format(baseMessage, *parameters)
        } else {
            baseMessage
        }

        publisher.publishEvent(DataLoadProgressEvent(message, this))
    }
}