package ch.cromon.wow.io.files.wmo

import ch.cromon.wow.math.BoundingBox
import java.nio.FloatBuffer
import java.nio.ShortBuffer


abstract class AbstractMapObject {
    lateinit var vertexData: FloatBuffer
        protected set

    lateinit var indexData: ShortBuffer
        protected set

    val groups = ArrayList<AbstractMapObjectGroup>()
    val materials = ArrayList<MapObjectMaterial>()

    lateinit var boundingBox: BoundingBox
        protected set

    abstract fun unload()
}