package ch.cromon.wow.io.data

const val REGION_PROPERTY = "data.region"
const val GAME_FOLDER_PROPERTY = "game.folder"
const val REMOTE_SERVER_PROPERTY = "remote.server.url"