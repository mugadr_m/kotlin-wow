package ch.cromon.wow.io.data

class IndexEntry(val index: Int, val offset: Int, val size: Int)