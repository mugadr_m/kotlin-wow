package ch.cromon.wow.io.data.local

import ch.cromon.wow.io.LittleEndianFileStream
import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.data.*
import ch.cromon.wow.utils.I18N.ENCODING_DECOMPRESS
import ch.cromon.wow.utils.I18N.ENCODING_DONE
import ch.cromon.wow.utils.I18N.ENCODING_PARSE
import ch.cromon.wow.utils.I18N.LOCAL_BUILD_INFO_MISSING
import ch.cromon.wow.utils.I18N.LOCAL_LOAD_BUILD
import ch.cromon.wow.utils.I18N.LOCAL_LOAD_BUILD_CONFIG
import ch.cromon.wow.utils.I18N.LOCAL_LOAD_BUILD_CONFIG_LOCATION
import ch.cromon.wow.utils.I18N.LOCAL_LOAD_BUILD_DONE
import ch.cromon.wow.utils.I18N.LOCAL_LOAD_BUILD_ERROR
import ch.cromon.wow.utils.I18N.LOCAL_LOAD_BUILD_INFO
import ch.cromon.wow.utils.I18N.LOCAL_LOAD_ENCODING_FILE
import ch.cromon.wow.utils.I18N.LOCAL_LOAD_INDEX
import ch.cromon.wow.utils.I18N.LOCAL_LOAD_ROOT_FILE
import ch.cromon.wow.utils.I18N.ROOT_DECOMPRESS
import ch.cromon.wow.utils.I18N.ROOT_DONE
import ch.cromon.wow.utils.I18N.ROOT_PARSE
import ch.cromon.wow.utils.format
import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin
import org.apache.commons.io.IOUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import org.springframework.util.MultiValueMap
import org.springframework.util.StopWatch
import java.io.File
import java.io.FileInputStream
import java.io.RandomAccessFile
import java.util.*
import java.util.concurrent.CompletableFuture
import javax.annotation.PreDestroy

@Component
@Qualifier("LOCAL")
@Scope("prototype")
class LocalFileManager : FileProvider() {
    companion object {
        private val logger = LoggerFactory.getLogger(LocalFileManager::class.java)
    }

    private lateinit var buildInfo: TokenConfig
    private lateinit var activeBuild: Map<String, String>
    private lateinit var buildConfig: KeyValueConfig

    private lateinit var basePath: String

    private val indexEntries = TreeMap<FileKeyMD5, IndexEntry>()
    private lateinit var encodingEntries: TreeMap<FileKeyMD5, EncodingEntry>
    private lateinit var rootEntries: MultiValueMap<Long, FileKeyMD5>
    private lateinit var fileRootMap: MultiValueMap<Int, FileKeyMD5>

    private val dataStreams = HashMap<Int, RandomAccessFile>()

    @PreDestroy
    fun cleanup() {
        dataStreams.forEach {
            try {
                it.value.close()
            } catch (e: Exception) {
                logger.error("Failed to close data stream", e)
            }
        }
    }

    override fun initialize(properties: Properties): CompletableFuture<Boolean> {
        val folder = properties.getProperty(GAME_FOLDER_PROPERTY)
        if (folder == null || folder.isBlank()) {
            logger.error("Missing property $GAME_FOLDER_PROPERTY")
            return CompletableFuture.completedFuture(false)
        }

        basePath = folder

        return CompletableFuture.supplyAsync {
            val watch = StopWatch("Local File Loading")
            val ret = loadBuildInfo(watch) &&
                    loadActiveBuild(watch) &&
                    loadBuildConfig(watch) &&
                    loadIndexFiles(watch) &&
                    loadEncodingFile(watch) &&
                    loadRootFile(watch)
            logger.info(watch.prettyPrint())
            ret
        }.handle {
            success, error ->
            if(error != null) {
                logger.error("Failed to init local index handler", error)
                false
            } else {
                success
            }
        }
    }

    override fun hasFile(name: String): Boolean {
        val hash = JenkinsHash.calculate(name)
        try {
            val rootInfo = rootEntries.getFirst(hash) ?: return false
            val encodingInfo = encodingEntries[rootInfo] ?: return false

            getIndexEntry(encodingInfo.key) ?: return false
            return true
        } catch (e: NoSuchElementException) {
            return false
        }
    }

    override fun containsFileDataId(id: Int): Boolean {
        try {
            val rootInfos = fileRootMap[id] ?: return false
            for(rootInfo in rootInfos) {
                val encodingInfo = encodingEntries[rootInfo] ?: continue

                getIndexEntry(encodingInfo.key) ?: continue
                return true
            }
            return false
        } catch (e: NoSuchElementException) {
            return false
        }
    }

    override fun canOpenByFileDataId(): Boolean {
        return true
    }

    override fun openFromFileDataId(fileDataId: Int): LittleEndianStream? {
        try {
            val rootInfos = fileRootMap[fileDataId] ?: return null
            for(rootInfo in rootInfos) {
                val encodingInfo = encodingEntries[rootInfo] ?: continue

                val indexInfo = getIndexEntry(encodingInfo.key) ?: continue
                return BlteHandler(loadFile(indexInfo)).decompressedData
            }
            return null
        } catch (e: NoSuchElementException) {
            return null
        }
    }

    override fun openFile(name: String): LittleEndianStream? {
        val hash = JenkinsHash.calculate(name)
        try {
            val rootInfos = rootEntries[hash] ?: return null
            for(rootInfo in rootInfos) {
                val encodingInfo = encodingEntries[rootInfo] ?: continue

                val indexInfo = getIndexEntry(encodingInfo.key) ?: continue
                return BlteHandler(loadFile(indexInfo)).decompressedData
            }
            return null
        } catch (e: NoSuchElementException) {
            return null
        }
    }

    override fun openFileAsStream(name: String): LittleEndianFileStream? {
        throw NotImplementedError("This function is not available for CASC")
    }

    private fun loadBuildInfo(watch: StopWatch): Boolean {
        handleProgress(LOCAL_LOAD_BUILD_INFO)
        watch.start("Load Build Info")
        val buildFile = File(basePath, ".build.info")
        logger.info("Loading .build.info from ${buildFile.absolutePath}")
        if (!buildFile.exists()) {
            watch.stop()
            handleProgress(LOCAL_BUILD_INFO_MISSING)
            logger.error("Missing file '.build.info' in folder $basePath")
            return false
        }

        FileInputStream(buildFile).use {
            buildInfo = TokenConfig(it)
            watch.stop()
            return true
        }
    }

    private fun loadActiveBuild(watch: StopWatch): Boolean {
        handleProgress(LOCAL_LOAD_BUILD)
        watch.start("Find Active Build")
        for (i in 0 until buildInfo.size) {
            val entry = buildInfo[i]
            val version = entry["Version"]
            val active = entry["Active"] == "1"
            logger.info("Found build $version which is ${if (active) "active" else "inactive"}")
            if (active) {
                if(version == null) {
                    logger.error("No version found for active build")
                    return false
                }

                logger.info("Using build $version with tags ${entry["Tags"]}")
                val parts = version.split(".")
                if(parts.size != 4) {
                    logger.error("Unable to parse version $version")
                    return false
                }
                buildVersion = parts[3].toInt()
                activeBuild = entry
                watch.stop()
                handleProgress(LOCAL_LOAD_BUILD_DONE, version)
                return true
            }
        }

        logger.error("No active build found in build config")
        watch.stop()
        handleProgress(LOCAL_LOAD_BUILD_ERROR)
        return false
    }

    private fun loadBuildConfig(watch: StopWatch): Boolean {
        handleProgress(LOCAL_LOAD_BUILD_CONFIG)
        watch.start("Load Build Config")
        val buildKey = activeBuild["BuildKey"]
        if (buildKey == null || buildKey.isBlank()) {
            logger.error("No build key found in build info")
            watch.stop()
            return false
        }

        handleProgress(LOCAL_LOAD_BUILD_CONFIG_LOCATION, buildKey)
        logger.info("Loading build config from $buildKey")
        val configPath = File(basePath, "data/config/${buildKey.substring(0, 2)}/${buildKey.substring(2, 4)}/$buildKey")
        if (!configPath.exists()) {
            logger.error("Build config not found at $configPath")
            watch.stop()
            return false
        }

        FileInputStream(configPath).use {
            buildConfig = KeyValueConfig(it)
        }

        watch.stop()
        return true
    }

    private fun loadIndexFiles(watch: StopWatch): Boolean {
        watch.start("Load Index Files")
        val indexFolder = File(basePath, "data/data")
        val indexFiles = indexFolder.listFiles() ?: return false
        indexFiles
                .filter { it.extension == "idx" }
                .sortedBy { it.name }
                .groupBy { it.name.substring(0, 2).toInt(16) }
                .filter { it.key < 0x10 }
                .map { it.value.lastOrNull() }
                .filterNotNull()
                .forEach { parseIndexFile(it) }

        watch.stop()
        return true
    }

    private fun parseIndexFile(file: File) {
        handleProgress(LOCAL_LOAD_INDEX, file.nameWithoutExtension)
        FileInputStream(file).use {
            val content = IOUtils.toByteArray(it)
            val stream = LittleEndianStream(content)

            val len = stream.readInt()
            stream.position = ((8 + len + 0x0F) and 0xFFFFFFF0.toInt())
            val dataLen = stream.readInt()
            stream.position += 4

            val numBlocks = dataLen / 18
            for (i in 0 until numBlocks) {
                val keyBytes = ByteArray(16)
                stream.readBytes(keyBytes, 0, 9)
                val key = FileKeyMD5(keyBytes)

                val indexHigh = stream.readByte().toInt()
                val indexLow = stream.readIntBE().toLong() and 0xFFFFFFFF

                val size = stream.readInt()
                val index = (indexHigh shl 2) or ((indexLow and 0xC0000000) shr 30).toInt()
                val offset = indexLow and 0x3FFFFFFF

                indexEntries.putIfAbsent(key, IndexEntry(index, offset.toInt(), size))
            }

            logger.info("Loaded index ${file.name}")
        }
    }

    private fun loadEncodingFile(watch: StopWatch): Boolean {
        handleProgress(LOCAL_LOAD_ENCODING_FILE)
        watch.start("Load Encoding File")
        val encodingKey = buildConfig["encoding"]
        if(encodingKey == null || encodingKey.size != 2) {
            logger.error("No valid encoding file key found")
            watch.stop()
            return false
        }

        logger.info("Loading encoding file from ${encodingKey[1]}")

        val encoding = FileKeyMD5(HexBin.decode(encodingKey[1]))
        val indexInfo = getIndexEntry(encoding)
        if(indexInfo == null) {
            logger.error("Missing index info for encoding file")
            watch.stop()
            return false
        }

        val encodingDataRaw = loadFile(indexInfo)
        watch.stop()
        handleProgress(ENCODING_DECOMPRESS)
        watch.start("Decode Encoding File")
        val encodingData = BlteHandler(encodingDataRaw).decompressedData
        watch.stop()
        handleProgress(ENCODING_PARSE)
        watch.start("Parse Encoding File")
        encodingEntries = EncodingParser(encodingData).encodingData
        watch.stop()
        handleProgress(ENCODING_DONE)
        return true
    }

    private fun loadRootFile(watch: StopWatch): Boolean {
        handleProgress(LOCAL_LOAD_ROOT_FILE)
        watch.start("Load Root File")
        val rootKey = buildConfig["root"]
        if(rootKey == null || rootKey.isEmpty()) {
            logger.error("No valid root file key found")
            watch.stop()
            return false
        }

        logger.info("Loading root file from ${rootKey.first()}")

        val root = FileKeyMD5(HexBin.decode(rootKey.first()))
        val encodingEntry = encodingEntries[root]
        if(encodingEntry == null) {
            logger.error("No encoding entry found for root file")
            watch.stop()
            return false
        }

        val indexEntry = getIndexEntry(encodingEntry.key)
        if(indexEntry == null) {
            logger.error("No index entry found for root file")
            watch.stop()
            return false
        }

        val rootContent = loadFile(indexEntry)
        watch.stop()
        handleProgress(ROOT_DECOMPRESS)
        watch.start("Decode Root File")
        val rootData = BlteHandler(rootContent).decompressedData
        watch.stop()
        handleProgress(ROOT_PARSE)
        watch.start("Parse Root File")
        val rootParser = RootParser(rootData)
        rootEntries = rootParser.rootData
        fileRootMap = rootParser.fileDataIds
        watch.stop()
        handleProgress(ROOT_DONE)
        return true
    }

    private fun loadFile(indexEntry: IndexEntry): LittleEndianStream {
        val stream = getDataStream(indexEntry.index)
        stream.seek((indexEntry.offset + 16 + 4 + 10).toLong())
        val content = ByteArray(indexEntry.size - 30)
        stream.readFully(content)
        return LittleEndianStream(content)
    }

    private fun getDataStream(index: Int): RandomAccessFile {
        val existing = dataStreams[index]
        if(existing != null) {
            return existing
        }

        val streamFile = "$basePath/Data/data/data.${index.format(3)}"
        val ret = RandomAccessFile(streamFile, "r")
        dataStreams[index] = ret
        return ret
    }

    private fun getIndexEntry(key: FileKeyMD5): IndexEntry? {
        val data = key.binary
        val shortData = ByteArray(16)
        System.arraycopy(data, 0, shortData, 0, 9)
        return indexEntries[FileKeyMD5(shortData)]
    }
}