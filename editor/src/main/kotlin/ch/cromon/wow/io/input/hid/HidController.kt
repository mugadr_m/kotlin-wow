package ch.cromon.wow.io.input.hid

import ch.cromon.wow.io.input.InputAction
import ch.cromon.wow.utils.NativeLoader
import com.sun.jna.Pointer
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.nio.ByteBuffer
import javax.annotation.PostConstruct
import kotlin.experimental.and


@Component
@Scope("singleton")
class HidController {
    @Autowired
    private lateinit var logger: Logger

    private lateinit var loader: NativeLoader

    private lateinit var connectedDevice: Pointer
    private var isDeviceConnected = false

    private val hidBuffer = ByteArray(65)
    private val hidReader = ByteBuffer.wrap(hidBuffer)

    private val descriptor = Ps4Descriptor()

    val isValid get() = isDeviceConnected

    @PostConstruct
    fun initialize() {
        loader = NativeLoader("hidapi", "/hidapi", HidApi.Companion::load)
        loader.extract()

        val deviceList = HidApi.enumerate(0x54c, 0x5c4)
        if (deviceList != null) {
            try {
                val manufacturer = deviceList.manufacturerString
                val product = deviceList.productId
                val path = deviceList.path
                val device = HidApi.openByPath(path)
                if (device != Pointer.NULL) {
                    try {
                        logger.info("Found Controller{product=$product manufacturer=$manufacturer path=$path}")
                    } finally {
                        HidApi.close(device)
                    }
                }
            } finally {
                HidApi.freeEnum(deviceList)
            }
        }
    }

    fun getInputActionIntensity(action: InputAction): Float {
        return when (action) {
            InputAction.CAMERA_LEFT -> if(!descriptor.cross) Math.max(-descriptor.leftStick.x, 0.0f) else 0.0f
            InputAction.CAMERA_RIGHT -> if(!descriptor.cross) Math.max(descriptor.leftStick.x, 0.0f) else 0.0f
            InputAction.CAMERA_BACK -> if(!descriptor.cross) Math.max(descriptor.leftStick.y, 0.0f) else 0.0f
            InputAction.CAMERA_FRONT -> if(!descriptor.cross) Math.max(-descriptor.leftStick.y, 0.0f) else 0.0f
            InputAction.CAMERA_UP -> {
                if (!descriptor.l1 || descriptor.cross) {
                    0.0f
                } else {
                    Math.max(descriptor.l2Intensity, 0.0f)
                }
            }
            InputAction.CAMERA_DOWN -> {
                if (descriptor.l1 || descriptor.cross) {
                    0.0f
                } else {
                    Math.max(descriptor.l2Intensity, 0.0f)
                }
            }
            InputAction.CAMERA_ROTATE_H -> {
                if(!descriptor.cross) {
                    0.0f
                } else {
                    -descriptor.leftStick.x * 5
                }
            }
            InputAction.CAMERA_ROTATE_V -> {
                if(!descriptor.cross) {
                    0.0f
                } else {
                    descriptor.leftStick.y * 5
                }
            }
            else -> 0.0f
        }
    }

    @Scheduled(fixedRate = 1000)
    private fun checkForDevices() {
        if (isDeviceConnected) {
            return
        }

        val deviceList = HidApi.enumerate(0x54c, 0x5c4) ?: return
        try {
            val manufacturer = deviceList.manufacturerString
            val product = deviceList.productId
            val path = deviceList.path
            val device = HidApi.openByPath(path)
            if (device != Pointer.NULL) {
                connectedDevice = device
                initDevice()

                isDeviceConnected = true
                logger.info("Found Controller{product=$product manufacturer=$manufacturer path=$path}")
            }
        } finally {
            HidApi.freeEnum(deviceList)
        }
    }

    fun onFrame() {
        if (!isDeviceConnected) {
            return
        }

        var res = HidApi.read(connectedDevice, hidBuffer)
        if(res < 0) {
            return
        }

        while(res > 0) {
            res = HidApi.read(connectedDevice, hidBuffer)
        }

        readDpad()
        readButtons()
        readSticks()
    }

    private fun initDevice() {
        val buffer = ByteArray(256)
        buffer[0] = 2
        buffer[1] = 0xf2.toByte()
        buffer[2] = 1
        buffer[3] = 0
        HidApi.sendFeatureReport(connectedDevice, buffer, 17)

        buffer[0] = 2
        HidApi.getFeatureReport(connectedDevice, buffer)

        HidApi.setNonBlocking(connectedDevice, true)

        HidApi.write(connectedDevice, byteArrayOf(
                0xA3.toByte(), 0xF2.toByte(), 1, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0D, 0x6A,
                0x3C, 0xEF.toByte()
        ))
    }

    private fun readDpad() {
        val statusBit = hidReader.get(5)
        val dpad = (statusBit and 0x0F).toInt()
        descriptor.dpadTop = dpad == 0 || dpad == 1 || dpad == 7
        descriptor.dpadBottom = dpad == 3 || dpad == 4 || dpad == 5
        descriptor.dpadLeft = dpad == 7 || dpad == 6 || dpad == 5
        descriptor.dpadRight = dpad == 1 || dpad == 2 || dpad == 3
    }

    private fun readButtons() {
        handleRightButtons()
        handleLRButtons()
    }

    private fun handleRightButtons() {
        val statusBit = hidReader.get(5).toInt()
        val sqOld = descriptor.square
        val crOld = descriptor.cross
        val ciOld = descriptor.circle
        val trOld = descriptor.triangle

        descriptor.square = (statusBit and 16) != 0
        descriptor.cross = (statusBit and 32) != 0
        descriptor.circle = (statusBit and 64) != 0
        descriptor.triangle = (statusBit and 128) != 0

        if (sqOld != descriptor.square) {
            logger.info("Square was ${if (sqOld) "released" else "pressed"}")
        }

        if (crOld != descriptor.cross) {
            logger.info("Cross was ${if (crOld) "released" else "pressed"}")
        }

        if (ciOld != descriptor.circle) {
            logger.info("Circle was ${if (ciOld) "released" else "pressed"}")
        }

        if (trOld != descriptor.triangle) {
            logger.info("Triangle was ${if (trOld) "released" else "pressed"}")
        }
    }

    private fun handleLRButtons() {
        descriptor.l2Intensity = (hidReader.get(8).toInt() and 0xFF) / 255.0f
        descriptor.r2Intensity = (hidReader.get(9).toInt() and 0xFF) / 255.0f
        val statusBit = hidReader.get(6).toInt()
        val l1Old = descriptor.l1
        val l2Old = descriptor.l2
        val l3Old = descriptor.l3
        val r1Old = descriptor.r1
        val r2Old = descriptor.r2
        val r3Old = descriptor.r3

        descriptor.l1 = (statusBit and 1) != 0
        descriptor.r1 = (statusBit and 2) != 0
        descriptor.l2 = (statusBit and 4) != 0
        descriptor.r2 = (statusBit and 8) != 0
        descriptor.l3 = (statusBit and 64) != 0
        descriptor.r3 = (statusBit and 128) != 0

        if (l1Old != descriptor.l1) {
            logger.info("L1 was ${if (l1Old) "release" else "pressed"}")
        }

        if (r1Old != descriptor.r1) {
            logger.info("R1 was ${if (r1Old) "release" else "pressed"}")
        }

        if (l2Old != descriptor.l2) {
            logger.info("L2 was ${if (l2Old) "release" else "pressed"}")
        }

        if (r2Old != descriptor.r2) {
            logger.info("R2 was ${if (r2Old) "release" else "pressed"}")
        }

        if (l3Old != descriptor.l3) {
            logger.info("L3 was ${if (l3Old) "release" else "pressed"}")
        }

        if (r3Old != descriptor.r3) {
            logger.info("R3 was ${if (r3Old) "release" else "pressed"}")
        }
    }

    private fun readSticks() {
        descriptor.leftStick.x = ((hidReader[1].toInt() and 0xFF) - 130.0f) / 130.0f
        descriptor.leftStick.y = ((hidReader[2].toInt() and 0xFF) - 125.0f) / 130.0f
        if(Math.abs(descriptor.leftStick.x) < 5.0f / 125.0f) {
            descriptor.leftStick.x = 0.0f
        }
        if(Math.abs(descriptor.leftStick.y) < 5.0f / 125.0f) {
            descriptor.leftStick.y = 0.0f
        }
    }
}