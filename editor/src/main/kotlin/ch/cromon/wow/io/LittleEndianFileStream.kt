package ch.cromon.wow.io

import java.nio.ByteBuffer


interface LittleEndianFileStream {
    var position: Int
    val size: Int

    fun close()

    fun readFloat() = Float.fromBits(readInt())
    fun readLong(): Long
    fun readInt(): Int
    fun readIntBE(): Int
    fun readShort(): Short
    fun readByte(): Byte
    fun readBytes(numBytes: Int): ByteArray
    fun readBytes(buffer: ByteBuffer, numBytes: Int)
}