package ch.cromon.wow.io.http

import org.apache.commons.io.IOUtils
import org.slf4j.LoggerFactory
import java.net.HttpURLConnection
import java.net.URL

class HttpClient private constructor(url: String) {
    companion object {
        private val logger = LoggerFactory.getLogger(HttpClient::class.java)

        fun downloadWithProgress(url: String, callback: (Int, Int) -> Unit): ByteArray {
            return HttpClient(url).download(callback)
        }

        fun download(url: String): ByteArray {
            return HttpClient(url).download()
        }

        fun download(url: String, vararg headers: Pair<String, String>): ByteArray {
            val client = HttpClient(url)
            headers.forEach {
                client.addHeader(it.first, it.second)
            }

            return client.download()
        }

        fun exists(url: String) = HttpClient(url).exists()
    }

    private val connection: HttpURLConnection = URL(url).openConnection() as HttpURLConnection

    private fun exists(): Boolean {
        connection.doOutput = false
        connection.doInput = true
        connection.connect()
        val responseCode = connection.responseCode
        connection.disconnect()
        return responseCode in 200..399
    }

    private fun addHeader(name: String, value: String) {
        connection.setRequestProperty(name, value)
    }

    private fun download(callback: ((Int, Int) -> Unit)? = null): ByteArray {
        connection.doOutput = false
        connection.doInput = true
        connection.connect()
        val contentLength = connection.getHeaderField("Content-Length")
        var totalSize = 0
        if(contentLength?.isBlank() == false) {
            try {
                totalSize = contentLength.toInt()
            } catch (e: NumberFormatException) {
                logger.warn("Failed to get content length from request, no progress possible")
            }
        }

        var totalRead = 0
        connection.inputStream.use {
            if(totalSize == 0) {
                return IOUtils.toByteArray(it)
            }

            val ret = ByteArray(totalSize)
            while(totalRead < totalSize) {
                val numRead = it.read(ret, totalRead, Math.min(0x1000, totalSize - totalRead))
                totalRead += numRead
                callback?.let { it(totalRead, totalSize) }
            }

            return ret
        }
    }

}