package ch.cromon.wow.io.files.map.wotlk

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.files.map.single.AbstractSingleMapChunk
import ch.cromon.wow.io.files.map.single.MapVertex
import ch.cromon.wow.io.files.map.single.TextureLayer
import ch.cromon.wow.io.files.map.wotlk.liquid.TileLiquidManager
import ch.cromon.wow.math.BoundingBox
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.memory.HeightDataCache
import ch.cromon.wow.memory.MapAlphaCache
import ch.cromon.wow.memory.MapVertexCache
import ch.cromon.wow.utils.CHUNK_SIZE
import ch.cromon.wow.utils.MAP_MID_POINT
import ch.cromon.wow.utils.UNIT_SIZE
import ch.cromon.wow.utils.toUInt
import org.slf4j.LoggerFactory

data class Header(
        val flags: Int,
        val indexX: Int,
        val indexY: Int,
        val layers: Int,
        val numDoodads: Int,
        val ofsHeight: Int,
        val ofsNormal: Int,
        val ofsLayers: Int,
        val ofsReferences: Int,
        val ofsAlpha: Int,
        val sizeAlpha: Int,
        val ofsShadow: Int,
        val sizeShadow: Int,
        val areaId: Int,
        val numWmoRefs: Int,
        val holes: Short,
        val unk1: Short,
        val detailMap1: Long,
        val detailMap2: Long,
        val noEffectDoodad: Long,
        val ofsSoundEmitters: Int,
        val numSoundEmitters: Int,
        val ofsLiquid: Int,
        val sizeLiquid: Int,
        val position: Vector3,
        val ofsVertexColors: Int,
        val ofsVertexLights: Int,
        val reserved: Int)

class MapChunk(private val wdtFile: WdtFile) : AbstractSingleMapChunk() {
    companion object {
        val logger = LoggerFactory.getLogger(MapChunk::class.java)!!

        private val alphaWorkObject = ByteArray(4096 * 4)
    }

    private lateinit var header: Header

    private val layers = ArrayList<TextureLayer>()

    lateinit var vertices: Array<MapVertex>

    private lateinit var alphaBuffer: IntArray
    private lateinit var visibleBox: BoundingBox

    override val textureLayers = layers
    override val alphaData get() = alphaBuffer

    val x get() = header.indexX
    val y get() = header.indexY

    override val zoneId: Int
        get() = header.areaId

    override val boundingBox: BoundingBox
        get() = visibleBox

    fun unload() {
        MapVertexCache.release(vertices)
        MapAlphaCache.release(alphaBuffer)
    }

    fun load(data: LittleEndianStream, tileLiquidManager: TileLiquidManager): Boolean {
        vertices = MapVertexCache.getVertices()
        alphaBuffer = MapAlphaCache.getData()
        readHeader(data)
        loadHeights(data)
        loadNormals(data)
        loadVertexColors(data)
        loadTextures(data)
        loadAlpha(data)
        loadShadows(data)
        loadLiquid(data, tileLiquidManager)
        loadReferences(data)

        applyHoles()
        return true
    }

    private fun applyHoles() {
        for(y in 0 until 4) {
            for(x in 0 until 4) {
                val bitOffset = y * 4 + x
                val isHole = (header.holes.toInt() and (1 shl bitOffset)) != 0
                if(!isHole) {
                    continue
                }

                for(k in 0 until 16) {
                    for(l in 0 until 16) {
                        val ax = x * 16 + l
                        val ay = y * 16 + k
                        val newAlpha = alphaBuffer[ay * 64 + ax] and 0xFFFFFF00.toInt()
                        alphaBuffer[ay * 64 + ax] = newAlpha
                    }
                }
            }
        }
    }

    private fun loadHeights(data: LittleEndianStream) {
        data.position = header.ofsHeight
        val baseX = MAP_MID_POINT - header.position.y
        val baseY = MAP_MID_POINT - header.position.x
        val baseZ = header.position.z

        val heights = HeightDataCache.getBuffer()
        for(i in 0 until 145) {
            heights[i] = data.readFloat() + baseZ
        }

        val minPos = Vector3(Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE)
        val maxPos = Vector3(Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE)

        var counter = 0
        for(i in 0 until 17) {
            val innerRow = i % 2 != 0
            val columns = if(innerRow) 8 else 9
            for(j in 0 until columns) {
                val height = heights[counter]
                var x = baseX + j * UNIT_SIZE
                if(innerRow) {
                    x += 0.5f * UNIT_SIZE
                }
                val y = baseY + i * UNIT_SIZE * 0.5f
                val pos = vertices[counter].position
                pos.x = x
                pos.y = y
                pos.z = height
                maxPos.takeMax(pos)
                minPos.takeMin(pos)

                val ax = j / 8.0f + (if(innerRow) (0.5f / 8.0f) else 0.0f)
                val ay = i / 16.0f
                val tx = j + (if(innerRow) 0.5f else 0.0f)
                val ty = i * 0.5f
                val tc = vertices[counter].texCoord
                tc.x = tx
                tc.y = ty
                val ta = vertices[counter].texCoordAlpha
                ta.x = ax
                ta.y = ay

                ++counter
            }
        }

        HeightDataCache.release(heights)

        visibleBox = BoundingBox(minPos, maxPos)
    }

    private fun loadNormals(data: LittleEndianStream) {
        data.position = header.ofsNormal

        for(i in 0 until 145) {
            val nx = data.readByte()
            val ny = data.readByte()
            val nz = data.readByte()

            val normal = vertices[i].normal
            normal.x = -nx / 127.0f
            normal.y = -ny / 127.0f
            normal.z = -nz / 127.0f
        }
    }

    private fun loadVertexColors(data: LittleEndianStream) {
        if(header.ofsVertexColors <= 0) {
            return
        }

        data.position = header.ofsVertexColors
        for(i in 0 until 145) {
            vertices[i].vertexColor = data.readInt()
        }
    }

    private fun loadShadows(data: LittleEndianStream) {
        if(header.ofsShadow <= 0 || header.sizeShadow <= 8) {
            return
        }

        data.position = header.ofsShadow
        for(i in 0 until 64) {
            val shadowVal = data.readLong()
            for(j in 0 until 64) {
                val shadow = if(((shadowVal shr j) and 1) != 0L) 0x80 else 0xFF
                var old = alphaData[i * 64 + j]
                old = old and 0xFFFFFF00.toInt()
                old = old or shadow
                alphaData[i * 64 + j] = old
            }
        }
    }

    private fun loadTextures(data: LittleEndianStream) {
        if(header.ofsLayers <= 0) {
            return
        }

        data.position = header.ofsLayers - 4
        val size = data.readInt()
        val numLayers = size / 16
        if(numLayers <= 0) {
            return
        }

        for(i in 0 until numLayers) {
            val texture = data.readInt()
            val flags = data.readInt()
            val alphaOffset = data.readInt()
            val groundEffect = data.readInt()
            layers.add(TextureLayer(texture, flags, alphaOffset, groundEffect))
        }
    }

    private fun loadAlpha(data: LittleEndianStream) {
        alphaBuffer.fill(0, 0, alphaBuffer.size)

        if(layers.isEmpty() || header.sizeAlpha <= 8) {
            return
        }

        data.position = header.ofsAlpha
        data.readBytes(alphaWorkObject, 0, header.sizeAlpha - 8)
        for(i in 0 until layers.size) {
            val layer = layers[i]
            if((layer.flags and 0x200) != 0) {
                loadLayerRle(alphaWorkObject, layer, i)
            } else if((layer.flags and 0x100) != 0) {
                if((wdtFile.flags and 0x84) != 0) {
                    loadUncompressed(alphaWorkObject, layer, i)
                } else {
                    loadCompressed(alphaWorkObject, layer, i)
                }
            } else {
                loadBaseLayer(i)
            }
        }
    }

    private fun loadBaseLayer(index: Int) {
        for(i in 0 until 4096) {
            var old = alphaBuffer[i]
            old = old or (0xFF shl (8 * index))
            alphaBuffer[i] = old
        }
    }

    private fun loadCompressed(data: ByteArray, layer: TextureLayer, index: Int) {
        var curPos = layer.alphaOffset
        var numOut = 0
        for(k in 0 until 63) {
            for(j in 0 until 32) {
                val alpha = data[curPos++].toUInt()
                var val1 = alpha and 0x0F
                var val2 = (alpha shr 4) and 0x0F
                val2 = if(j == 31) val1 else val2
                val1 = ((val1 / 15.0f) * 255.0f).toInt()
                val2 = ((val2 / 15.0f) * 255.0f).toInt()

                var old = alphaBuffer[numOut]
                old = old or (val1 shl (8 * index))
                alphaBuffer[numOut++] = old

                old = alphaBuffer[numOut]
                old = old or (val2 shl (8 * index))
                alphaBuffer[numOut++] = old
            }
        }

        for(j in 0 until 64) {
            var old = alphaBuffer[63 * 64 + j]
            old = old or (alphaBuffer[62 * 64 + j] and (0xFF shl (index * 8)))
            alphaBuffer[63 * 64 + j] = old
        }
    }

    private fun loadUncompressed(data: ByteArray, layer: TextureLayer, index: Int) {
        var curPos = layer.alphaOffset
        for(i in 0 until 4096) {
            var old = alphaBuffer[i]
            old = old or (data[curPos++].toUInt() shl (8 * index))
            alphaBuffer[i] = old
        }
    }

    private fun loadLayerRle(data: ByteArray, layer: TextureLayer, index: Int) {
        var numOut = 0
        var curPos = layer.alphaOffset
        while(numOut < 4096) {
            val indicator = data[curPos++].toUInt()
            val repeat = indicator and 0x7F
            if((indicator and 0x80) != 0) {
                val value = data[curPos++].toUInt()
                for(k in 0 until repeat) {
                    if(numOut >= 4096) {
                        break
                    }

                    var old = alphaBuffer[numOut]
                    old = old or (value shl (8 * index))
                    alphaBuffer[numOut++] = old
                }
            } else {
                for(k in 0 until repeat) {
                    if(numOut >= 4096) {
                        break
                    }

                    var old = alphaBuffer[numOut]
                    old = old or (data[curPos++].toUInt() shl (8 * index))
                    alphaBuffer[numOut++] = old
                }
            }
        }
    }

    private fun loadLiquid(data: LittleEndianStream, tileLiquidManager: TileLiquidManager) {
        if(header.sizeLiquid <= 8) {
            return
        }

        val liquidType = when {
            (header.flags and 4) != 0 -> 1
            (header.flags and 8) != 0 -> 2
            (header.flags and 16) != 0 -> 3
            (header.flags and 32) != 0 -> 4
            else -> -1
        }

        data.position = header.ofsLiquid
        tileLiquidManager.handleChunkLiquidOld(Vector3(x * CHUNK_SIZE, y * CHUNK_SIZE, 0.0f), liquidType, data)
    }

    private fun loadReferences(data: LittleEndianStream) {
        if(header.numDoodads + header.numWmoRefs == 0) {
            return
        }

        data.position = header.ofsReferences

        for(i in 0 until header.numDoodads) {
            mapDoodadReferences.add(data.readInt())
        }

        for(i in 0 until header.numWmoRefs) {
            mapObjectReferences.add(data.readInt())
        }
    }

    private fun readHeader(data: LittleEndianStream) {
        header = Header(
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readShort(),
                data.readShort(),
                data.readLong(),
                data.readLong(),
                data.readLong(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                data.readInt(),
                Vector3(data.readFloat(),data.readFloat(), data.readFloat()),
                data.readInt(),
                data.readInt(),
                data.readInt()
        )
    }
}