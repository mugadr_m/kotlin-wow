package ch.cromon.wow.io.files.storage.definition

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.slf4j.LoggerFactory
import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import org.springframework.util.StopWatch
import org.w3c.dom.Element
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStream
import java.util.concurrent.ConcurrentHashMap
import java.util.regex.Pattern
import javax.xml.parsers.DocumentBuilderFactory

data class Field(val name: String, val type: String, @JsonProperty(required = false, value = "index") val isIndex: Boolean = false, @JsonProperty(required = false) val arraySize: Int = 1)

data class Table(val name: String, val build: Int, val fields: ArrayList<Field>) {
    fun getField(name: String) = fields.firstOrNull { it.name.toLowerCase() == name }
    fun getFieldIndex(name: String): Int {
        var offset = 0
        for(field in fields) {
            if(field.name.equals(name, true)) {
                return offset
            }

            offset += field.arraySize
        }

        return -1
    }
}

object DefinitionLoader {
    private val NAME_PATTERN = Pattern.compile("^([^.]+)\\.([0-9]+)\\.json$", Pattern.MULTILINE)
    private val logger = LoggerFactory.getLogger(DefinitionLoader::class.java)
    private val MAPPER = ObjectMapper().registerKotlinModule()

    private val definitionMap = HashMap<String, List<Table>>()

    fun findDefinition(tableName: String, build: Int): Table? {
        val definitions = definitionMap[tableName.toUpperCase()] ?: return null
        if (definitions.isEmpty()) {
            return null
        }

        if (definitions.size == 1) {
            return definitions.first()
        }

        for (i in 0 until (definitions.size - 1)) {
            if (definitions[i].build <= build && definitions[i + 1].build >= build) {
                return definitions[i]
            }
        }

        return definitions.last()
    }

    fun initialize() {
        val definitions = ConcurrentHashMap<String, ArrayList<Table>>()
        val stopWatch = StopWatch("Definition Loader")
        stopWatch.start("Parsing")
        PathMatchingResourcePatternResolver().getResources("classpath*:definitions/*.json").toList().parallelStream().forEach {
            val fileName = it.filename
            val matcher = NAME_PATTERN.matcher(fileName)
            if (!matcher.matches()) {
                return@forEach
            }

            val tableName = matcher.group(1)
            try {
                val table = MAPPER.readValue(it.inputStream, Table::class.java)
                definitions.putIfAbsent(tableName.toUpperCase(), ArrayList())
                definitions[tableName.toUpperCase()]?.add(table)
            } catch (e: Exception) {
                logger.error("Error loading resource $it", e)
            }
        }
        stopWatch.stop()

        stopWatch.start("Sorting")
        definitions.forEach {
            definitionMap[it.key] = it.value.sortedBy { it.build }
        }
        stopWatch.stop()
        logger.info(stopWatch.prettyPrint())
    }
}

fun parseDefinition(data: InputStream) {
    val mapper = jacksonObjectMapper()
    mapper.enable(SerializationFeature.INDENT_OUTPUT)
    val document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(data)
    val tableNodes = document.getElementsByTagName("Table")
    for (i in 0 until tableNodes.length) {
        val table = tableNodes.item(i)
        val tableName = table.attributes.getNamedItem("Name").textContent
        if (tableName.toLowerCase() == "map") {
            continue
        }

        val build = table.attributes.getNamedItem("Build").textContent.toInt()
        val fieldList = ArrayList<Field>()
        val fieldNodes = (table as Element).getElementsByTagName("Field")
        for (j in 0 until fieldNodes.length) {
            val fieldNode = fieldNodes.item(j)
            val fieldName = fieldNode.attributes.getNamedItem("Name").textContent
            val type = fieldNode.attributes.getNamedItem("Type").textContent
            val isIndex = fieldNode.attributes.getNamedItem("IsIndex")?.textContent?.toBoolean() ?: false
            val arrayIndex = fieldNode.attributes.getNamedItem("ArrayIndex")?.textContent?.toInt() ?: 1
            fieldList.add(Field(fieldName, type, isIndex, arrayIndex))
        }

        val tbl = Table(tableName, build, fieldList)
        FileOutputStream("./editor/src/main/resources/definitions/$tableName.$build.json").use {
            mapper.writeValue(it, tbl)
        }
    }
}

fun parseDefinitions(basePath: String) {
    val definitions = File(basePath).listFiles { _, name ->
        name.toLowerCase().endsWith(".xml")
    }

    definitions.toList().parallelStream().forEach { FileInputStream(it).use { parseDefinition(it) } }
}