package ch.cromon.wow.io

import ch.cromon.wow.io.files.texture.BlpFile
import ch.cromon.wow.io.files.texture.TextureFormat
import java.awt.image.BufferedImage
import java.awt.image.DataBuffer
import java.awt.image.DataBufferByte
import java.awt.image.DataBufferInt
import javax.imageio.ImageIO

enum class ImageSourceFormat(val textureFormat: TextureFormat) {
    ARGB8(TextureFormat.ARGB8),
    DXT1(TextureFormat.DXT1),
    DXT3(TextureFormat.DXT3),
    DXT5(TextureFormat.DXT5);

    companion object {
        val MAP = ImageSourceFormat.values().associateBy { it.textureFormat }
    }
}

class ImageSource(val buffer: ByteArray, val width: Int, val height: Int, val format: ImageSourceFormat) {
    val isCompressed = format != ImageSourceFormat.ARGB8

    companion object {
        fun fromTextureToSize(content: LittleEndianStream, width: Int, height: Int): ImageSource {
            val loadInfo = BlpFile.loadBestMatchingLayer(content, width, height)
            val format = ImageSourceFormat.MAP[loadInfo.format] ?: throw IllegalArgumentException("Texture format ${loadInfo.format} is not supported")
            return ImageSource(loadInfo.data, loadInfo.width, loadInfo.height, format)
        }

        fun fromTexture(content: LittleEndianStream): ImageSource {
            val loadInfo = BlpFile.loadFirstLayer(content)
            val format = ImageSourceFormat.MAP[loadInfo.format] ?: throw IllegalArgumentException("Texture format ${loadInfo.format} is not supported")
            return ImageSource(loadInfo.data, loadInfo.width, loadInfo.height, format)
        }

        fun fromImageResource(name: String): ImageSource {
            return ImageSource::class.java.getResourceAsStream("/images/$name")?.use {
                var img = ImageIO.read(it)
                if(img.type != BufferedImage.TYPE_INT_ARGB) {
                    val tmp = BufferedImage(img.width, img.height, BufferedImage.TYPE_INT_ARGB)
                    val graphics = tmp.createGraphics()
                    graphics.drawImage(img, 0, 0, img.width, img.height, null)
                    img = tmp
                }
                fromBufferedImage(img)
            } ?: throw IllegalArgumentException("Unable to find resource images/$name")
        }

        fun fromBufferedImage(image: BufferedImage): ImageSource {
            val dataBuffer = image.raster.dataBuffer
            when {
                dataBuffer.dataType == DataBuffer.TYPE_BYTE -> return ImageSource((dataBuffer as DataBufferByte).data, image.width, image.height, ImageSourceFormat.ARGB8)
                dataBuffer.dataType == DataBuffer.TYPE_INT -> {
                    val colorModel = image.colorModel
                    val intBuffer = dataBuffer as DataBufferInt
                    val raw = intBuffer.data
                    val data = ByteArray(image.width * image.height * 4)
                    for(i in 0 until raw.size) {
                        val color = raw[i]
                        data[i * 4] = colorModel.getRed(color).toByte()
                        data[i * 4 + 1] = colorModel.getGreen(color).toByte()
                        data[i * 4 + 2] = colorModel.getBlue(color).toByte()
                        data[i * 4 + 3] = colorModel.getAlpha(color).toByte()
                    }

                    return ImageSource(data, image.width, image.height, ImageSourceFormat.ARGB8)
                }
                else -> throw IllegalArgumentException("BufferedImage must be backed by an IntBuffer or ByteBuffer")
            }
        }
    }
}