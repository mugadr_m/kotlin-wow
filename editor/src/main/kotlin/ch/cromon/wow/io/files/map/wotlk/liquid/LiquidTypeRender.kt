package ch.cromon.wow.io.files.map.wotlk.liquid

import ch.cromon.wow.io.files.storage.LiquidType
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.math.Vector3
import org.lwjgl.BufferUtils
import java.nio.FloatBuffer
import java.nio.IntBuffer

const val FALLBACK_TEXTURE = "XTextures\\river\\lake_a.%d.blp"

val FALLBACK_LIQUID = LiquidType(
        -1,
        "fallback_liquid_type",
        arrayListOf(FALLBACK_TEXTURE)
)

class LiquidTypeRender(liquidType: LiquidType) {
    private val liquidTexturePattern = liquidType.textures.firstOrNull() ?: FALLBACK_TEXTURE

    val liquidTextureNames = (1 until 30).map { String.format(liquidTexturePattern, it) }
    val indexCount get() = indices.size
    val isOcean = liquidType.id == 2

    val id = liquidType.id

    private val vertices = ArrayList<Vector3>()
    private val texCoords = ArrayList<Vector2>()
    private val depths = ArrayList<Float>()
    private val indices = ArrayList<Int>()

    val baseIndex get() = vertices.size

    fun pushChunk(vertices: ArrayList<Vector3>, texCoords: ArrayList<Vector2>, depths: ArrayList<Float>, indices: ArrayList<Int>) {
        this.vertices.addAll(vertices)
        this.texCoords.addAll(texCoords)
        this.indices.addAll(indices)
        this.depths.addAll(depths)
    }

    fun fillBuffer(): FloatBuffer {
        if(vertices.size != texCoords.size) {
            throw IllegalStateException("Received unbalanced number of vertices and texture coords")
        }

        val buffer = BufferUtils.createFloatBuffer(vertices.size * (3 + 2 + 1))
        for(i in 0 until vertices.size) {
            val v = vertices[i]
            val t = texCoords[i]
            val d = depths[i]
            buffer.put(v.x).put(v.y).put(v.z)
                    .put(t.x).put(t.y)
                    .put(d)
        }

        return buffer.flip() as FloatBuffer
    }

    fun fillIndexBuffer(): IntBuffer {
        val buffer = BufferUtils.createIntBuffer(indices.size)
        buffer.put(indices.toIntArray())
        buffer.flip()
        return buffer
    }
}