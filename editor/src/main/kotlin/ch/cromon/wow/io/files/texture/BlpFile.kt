package ch.cromon.wow.io.files.texture

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.utils.toUInt
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.lwjgl.opengl.EXTTextureCompressionS3TC.*
import org.lwjgl.opengl.GL11.GL_RGBA

private val alphaLookup1 = byteArrayOf(0x00, 0xFF.toByte())
private val alphaLookup4 = byteArrayOf(
        0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
        0x88.toByte(), 0x99.toByte(), 0xAA.toByte(),
        0xBB.toByte(), 0xCC.toByte(), 0xDD.toByte(),
        0xEE.toByte(), 0xFF.toByte()
)

enum class TextureFormat(val native: Int) {
    ARGB8(GL_RGBA),
    DXT1(GL_COMPRESSED_RGB_S3TC_DXT1_EXT),
    DXT3(GL_COMPRESSED_RGBA_S3TC_DXT3_EXT),
    DXT5(GL_COMPRESSED_RGBA_S3TC_DXT5_EXT),
    UNKNOWN(0)
}

data class LayeredTextureInfo(
        val format: TextureFormat,
        val width: Int,
        val height: Int,
        val data: List<ByteArray>
)

data class TextureLoadInfo(
        val format: TextureFormat,
        val width: Int,
        val height: Int,
        val data: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TextureLoadInfo

        return EqualsBuilder()
                .append(format, other.format)
                .append(width, other.width)
                .append(height, other.height)
                .append(data, other.data)
                .build()!!
    }

    override fun hashCode() = HashCodeBuilder(37, 0)
            .append(format)
            .append(width)
            .append(height)
            .append(data)
            .build()!!
}


class BlpFile(private val content: LittleEndianStream) {
    companion object {
        const val MAGIC = 0x32504C42

        fun loadBestMatchingLayer(content: LittleEndianStream, targetWidth: Int, targetHeight: Int): TextureLoadInfo {
            val file = BlpFile(content)
            if(!file.hasMips || file.numLayers <= 1) {
                return loadLayer(0, file)
            }

            var index = 0
            while(index < (file.numLayers - 1)) {
                val w = Math.max(1, file.width shr index)
                val h = Math.max(1, file.height shr index)
                val nw = Math.max(1, file.width shr (index + 1))
                val nh = Math.max(1, file.height shr (index + 1))
                if(w >= targetWidth && h >= targetHeight && nw <= targetWidth && nh <= targetHeight) {
                    return loadLayer(index, file)
                }
                ++index
            }

            return loadLayer(index, file)
        }

        fun loadAllLayers(content: LittleEndianStream): LayeredTextureInfo {
            val file = BlpFile(content)
            val layerData = ArrayList<ByteArray>()
            var width = 0
            var height = 0
            var format = TextureFormat.UNKNOWN

            for(i in 0 until 16) {
                if(file.sizes[i] == 0) {
                    break
                }

                val layer = loadLayer(i, file)
                if(i == 0) {
                    width = layer.width
                    height = layer.height
                    format = layer.format
                }

                layerData.add(layer.data)
            }

            return LayeredTextureInfo(format, width, height, layerData)
        }

        fun loadFirstLayer(content: LittleEndianStream): TextureLoadInfo {
            val file = BlpFile(content)
            return loadLayer(0, file)
        }

        private fun loadLayer(layer: Int, file: BlpFile): TextureLoadInfo {
            if(file.sizes[layer] == 0) {
                return TextureLoadInfo(file.format, 0, 0, ByteArray(0))
            }

            val w = Math.max(file.width shr layer, 1)
            val h = Math.max(file.height shr layer, 1)

            file.content.position = file.offsets[layer]
            val data = file.content.readBytes(file.sizes[layer])
            return if(file.format != TextureFormat.ARGB8) {
                TextureLoadInfo(file.format, w, h, data)
            } else {
                val palette = intArrayOf().toMutableList()
                parseUncompressedLayer(layer, palette, file, data)
            }
        }

        private fun parseUncompressedLayer(layer: Int, palette: MutableList<Int>, file: BlpFile, data: ByteArray): TextureLoadInfo {
            if(file.compression == 3) {
                return TextureLoadInfo(TextureFormat.ARGB8, file.width, file.height, data)
            }

            if(palette.size == 0) {
                file.content.position = 148
                for(i in 0 until 256) {
                    palette.add(file.content.readInt())
                }
            }

            val w = Math.max(file.width shr layer, 1)
            val h = Math.max(file.height shr layer, 1)
            val colors = ByteArray(w * h * 4)
            if(file.alphaChannel == 8) {
                decompressPaletteFast(palette, data, colors)
            } else {
                decompressPalette(file.alphaChannel, palette, data, colors)
            }

            return TextureLoadInfo(TextureFormat.ARGB8, w, h, colors)
        }

        private fun decompressPaletteFast(palette: MutableList<Int>, indices: ByteArray, colors: ByteArray) {
            val numEntries = colors.size / 4
            for(i in 0 until numEntries) {
                val index = indices[i]
                val alpha = indices[i + numEntries].toInt()
                var color = palette[index.toUInt()]
                color = color and 0x00FFFFFF
                color = color or (alpha shl 24)
                colors[i * 4 + 2] = (color and 0xFF).toByte()
                colors[i * 4 + 1] = ((color shr 8) and 0xFF).toByte()
                colors[i * 4 + 0] = ((color shr 16) and 0xFF).toByte()
                colors[i * 4 + 3] = ((color shr 24) and 0xFF).toByte()
            }
        }

        private fun decompressPalette(alphaSize: Int, palette: MutableList<Int>, indices: ByteArray, colors: ByteArray) {
            val numEntries = colors.size / 4
            for(i in 0 until numEntries) {
                val index = indices[i]
                var color = palette[index.toInt()]
                color = color and 0x00FFFFFF
                color = color or 0xFF000000.toInt()
                colors[i * 4] = (color and 0xFF).toByte()
                colors[i * 4 + 1] = ((color shr 8) and 0xFF).toByte()
                colors[i * 4 + 2] = ((color shr 16) and 0xFF).toByte()
                colors[i * 4 + 3] = ((color shr 24) and 0xFF).toByte()
            }

            when(alphaSize) {
                1 -> {
                    var colorIndex = 0
                    for(i in 0 until (numEntries / 8)) {
                        val value = indices[i + numEntries].toInt()
                        for(j in 0 until 8) {
                            val alpha = alphaLookup1[value and (1 shl j)]
                            colors[colorIndex++ * 4 + 3] = if(alpha.toInt() != 0) 0xFF.toByte() else 0x00
                        }
                    }

                    if((numEntries % 8) != 0) {
                        val value = indices[numEntries + numEntries / 8].toInt()
                        for(j in 0 until (numEntries % 8)) {
                            val alpha = alphaLookup1[value and (1 shl j)]
                            colors[colorIndex++ * 4 + 3] = if(alpha.toInt() != 0) 0xFF.toByte() else 0x00
                        }
                    }
                }

                4 -> {
                    var colorIndex = 0
                    for(i in 0 until (numEntries / 2)) {
                        val value = indices[i + numEntries].toInt()
                        val alpha0 = alphaLookup4[value and 0x0F]
                        val alpha1 = alphaLookup4[(value shr 4) and 0x0F]
                        colors[colorIndex++ * 4 + 3] = alpha0
                        colors[colorIndex++ * 4 + 3] = alpha1
                    }

                    if((numEntries % 2) != 0) {
                        val value = indices[numEntries + numEntries / 2].toInt()
                        val alpha0 = alphaLookup4[value and 0x0F]
                        colors[colorIndex * 4 + 3] = alpha0
                    }
                }

                8 -> decompressPaletteFast(palette, indices, colors)
                else -> throw IllegalArgumentException("Only alpha compression 1, 4 and 8 are supported, got $alphaSize")
            }
        }
    }

    private val signature = content.readInt()
    private val version = content.readInt()
    private val compression = content.readByte().toInt()
    private val alphaChannel = content.readByte().toInt()
    private val alphaCompression = content.readByte().toInt()
    private val hasMips = content.readByte().toInt() != 0
    private val width = content.readInt()
    private val height = content.readInt()
    private val offsets = (0 until 16).map { content.readInt() }
    private val sizes = (0 until 16).map { content.readInt() }
    private val numLayers = Math.min(sizes.filterNot { it == 0 }.size, offsets.filterNot { it == 0 }.size)

    val format = when(compression) {
        2 -> when(alphaCompression) {
            0 -> TextureFormat.DXT1
            1 -> TextureFormat.DXT3
            7 -> TextureFormat.DXT5
            else -> TextureFormat.UNKNOWN
        }
        else -> TextureFormat.ARGB8
    }

    init {
        if(signature != MAGIC) {
            throw IllegalArgumentException("File signature is not matching. Expected $MAGIC but got $signature")
        }

        if(version > 2) {
            throw IllegalArgumentException("BLP version $version not supported")
        }
    }
}