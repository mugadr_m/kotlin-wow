package ch.cromon.wow.io.data.mpq

import java.util.regex.Pattern

internal object AlphaPatterns {
    val DBC = Pattern.compile("DBC.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val FONTS = Pattern.compile("FONTS.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val INTERFACE = Pattern.compile("INTERFACE.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val MISC = Pattern.compile("MISC.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val MODEL = Pattern.compile("MODEL.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val SOUND = Pattern.compile("SOUND.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val SPEECH = Pattern.compile("SPEECH.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val TEXTURE = Pattern.compile("TEXTURE.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
}

internal object Patterns {
    val COMMON = Pattern.compile("COMMON(-[0-9a-zA-Z]+)?\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val PATCH = Pattern.compile("PATCH\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val CUSTOM_PATCH = Pattern.compile("PATCH-[0-9a-zA-Z]+\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val EXPANSION = Pattern.compile("EXPANSION\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val LICHKING = Pattern.compile("LICHKING\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
}

internal class LocalePatterns(locale: String) {
    val backup = Pattern.compile("backup-$locale\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val base = Pattern.compile("base-$locale\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val locale = Pattern.compile("locale-$locale\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val patch = Pattern.compile("patch-$locale\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val expansionLocale = Pattern.compile("expansion-locale-$locale\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val lichkingLocale = Pattern.compile("lichking-locale-$locale\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val customPatches = Pattern.compile("patch-$locale-[0-9a-zA-Z]+\\.MPQ", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val speech = Pattern.compile("speech-$locale\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val expansionSpeech = Pattern.compile("expansion-speech-$locale\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
    val lichkingSpeech = Pattern.compile("lichking-speech-$locale\\.MPQ$", Pattern.CASE_INSENSITIVE or Pattern.MULTILINE)!!
}