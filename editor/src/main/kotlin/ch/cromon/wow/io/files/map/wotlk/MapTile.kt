package ch.cromon.wow.io.files.map.wotlk

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.files.map.Chunk
import ch.cromon.wow.io.files.map.MapObjectDefinition
import ch.cromon.wow.io.files.map.single.AbstractSingleMapTile
import ch.cromon.wow.io.files.map.wotlk.liquid.H2OParser
import ch.cromon.wow.io.files.map.wotlk.liquid.TileLiquidManager
import ch.cromon.wow.io.files.storage.DataStorage
import ch.cromon.wow.math.BoundingBox
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.memory.MapVertexBufferCache
import ch.cromon.wow.utils.toFourCC
import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import java.nio.FloatBuffer
import java.nio.charset.StandardCharsets


class MapTile(x: Int, y: Int, storage: DataStorage, private val wdtFile: WdtFile) : AbstractSingleMapTile(x, y) {
    companion object {
        private val logger = LoggerFactory.getLogger(MapTile::class.java)
    }

    private val dataChunkMap = HashMap<Int, LittleEndianStream>()
    private val mandatoryChunks = arrayListOf("MHDR", "MCIN", "MTEX", "MMID", "MWMO", "MWID", "MDDF", "MODF")

    private val mapChunkMap = HashMap<Int, MapChunk>()

    private lateinit var vertexData: FloatBuffer

    private lateinit var visibleBox: BoundingBox

    private var tileLiquid = TileLiquidManager(storage)

    override val chunks get() = mapChunkMap.values.toList()
    override val fullVertices: FloatBuffer get() = vertexData

    override val boundingBox: BoundingBox
        get() = visibleBox

    override val liquidData
        get() = tileLiquid

    override fun unload() {
        MapVertexBufferCache.release(vertexData)
        chunks.forEach {
            it.unload()
        }
    }

    fun load(content: LittleEndianStream): Boolean {
        parseAllChunks(content)
        if (!validateMandatoryChunks()) {
            logger.warn("Missing mandatory chunks: ${mandatoryChunks.joinToString(",")}")
            return false
        }

        loadTextures()
        loadLiquid()
        loadMapObjects()
        loadChunks(content)

        tileLiquid.updateRenderers()

        vertexData = MapVertexBufferCache.getBuffer()
        chunks.forEach {
            it.vertices.forEach {
                vertexData.put(it.position.x)
                        .put(it.position.y)
                        .put(it.position.z)
                        .put(it.texCoord.x)
                        .put(it.texCoord.y)
                        .put(it.texCoordAlpha.x)
                        .put(it.texCoordAlpha.y)
                        .put(it.normal.x)
                        .put(it.normal.y)
                        .put(it.normal.z)
                        .put(Float.fromBits(it.vertexColor))
            }
        }

        vertexData.flip()

        return true
    }

    override fun getChunk(x: Int, y: Int): Chunk {
        if (x < 0 || x > 15 || y < 0 || y > 15) {
            logger.warn("Attempted to read chunk at index $x/$y")
            throw IllegalArgumentException("Out of bounds chunk index")
        }

        return mapChunkMap[y * 16 + x] ?: throw IllegalArgumentException("No chunk found")
    }

    private fun validateMandatoryChunks(): Boolean {
        val chunkList = ArrayList(mandatoryChunks)
        chunkList
                .filter { dataChunkMap.containsKey(it.toFourCC()) }
                .forEach { mandatoryChunks.remove(it) }

        return mandatoryChunks.isEmpty()
    }

    private fun parseAllChunks(content: LittleEndianStream) {
        while (content.position + 8 <= content.size) {
            val signature = content.readInt()
            val size = content.readInt()
            val data = if (size > 0) content.readBytes(size) else ByteArray(0)
            dataChunkMap[signature] = LittleEndianStream(data)
        }
    }

    private fun loadTextures() {
        val textureData = dataChunkMap["MTEX".toFourCC()] ?: throw IllegalStateException("No MTEX chunk found")
        textures.addAll(String(textureData.data, StandardCharsets.US_ASCII)
                .split('\u0000')
                .filter({ it.isNotBlank() }))
    }

    private fun loadChunks(content: LittleEndianStream): Boolean {
        val mcinData = dataChunkMap["MCIN".toFourCC()] ?: throw IllegalStateException("No MCIN chunk found")
        val chunkOffsets = (0 until 256).map {
            val offset = mcinData.readInt()
            val size = mcinData.readInt()
            mcinData.position += 8
            offset to size
        }

        val chunkMin = Vector3(Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE)
        val chunkMax = Vector3(-Float.MAX_VALUE, -Float.MAX_VALUE, -Float.MAX_VALUE)

        for (chunkOffset in chunkOffsets) {
            val offset = chunkOffset.first
            val size = chunkOffset.second
            content.position = offset + 8
            val mcnkData = content.readBytes(size)
            val chunk = MapChunk(wdtFile)
            if (!chunk.load(LittleEndianStream(mcnkData), tileLiquid)) {
                return false
            }

            chunkMin.takeMin(chunk.boundingBox.min)
            chunkMax.takeMax(chunk.boundingBox.max)

            mapChunkMap[chunk.y * 16 + chunk.x] = chunk
        }

        visibleBox = BoundingBox(chunkMin, chunkMax)

        return true
    }

    private fun loadLiquid() {
        val mh20 = dataChunkMap["MH2O".toFourCC()] ?: return
        if (mh20.size > 0) {
            H2OParser(tileLiquid, mh20)
        }
    }

    private fun loadMapObjects() {
        val mwmo = dataChunkMap["MWMO".toFourCC()] ?: return
        val mwid = dataChunkMap["MWID".toFourCC()] ?: return
        val modf = dataChunkMap["MODF".toFourCC()] ?: return

        if(mwmo.size == 0 || mwid.size == 0 || modf.size == 0) {
            return
        }

        val numNames = mwid.size / 4
        val nameList = ArrayList<String>()
        for (i in 0 until numNames) {
            val offset = mwid.readInt()
            if (offset >= mwmo.size) {
                logger.warn("Invalid entry in MWID outside of MWMO: $offset >= ${mwmo.size}")
                nameList.add("")
                continue
            }

            mwmo.position = offset
            nameList.add(mwmo.readString())
        }

        loadMapObjectDefinitions(modf, nameList)
    }

    private fun loadMapObjectDefinitions(modf: LittleEndianStream, nameList: List<String>) {
        val matrix = Matrix4()
        val matrixRotation = Matrix4()
        val mathMatrix = Matrix4()
        val numMapObjects = modf.size / 64

        for (i in 0 until numMapObjects) {
            modf.position = i * 64
            val nameId = modf.readInt()
            if (nameId >= nameList.size) {
                logger.warn("Invalid MODF entry is ignored. Name id $nameId is outside of the bounds of MWID: [0, ${nameList.size})")
                continue
            }

            val name = nameList[nameId]
            if (StringUtils.isEmpty(name)) {
                continue
            }

            val uuid = modf.readInt()
            val position = modf.readVector3()
            val rotation = modf.readVector3()
            rotation.x = Math.toRadians(rotation.x.toDouble()).toFloat()
            rotation.y = Math.toRadians(90 - rotation.y.toDouble()).toFloat()
            rotation.z = Math.toRadians(rotation.z.toDouble()).toFloat()

            Matrix4.translation(matrix, position.x, position.z, position.y)
            Matrix4.rotation(matrixRotation, rotation.z, Vector3(1, 0, 0))// -rotation.x, 90 - rotation.y)
            val instanceMatrix = Matrix4()
            matrix.times(matrixRotation, instanceMatrix)
            Matrix4.rotation(matrixRotation, -rotation.x, Vector3(0, 1, 0))
            instanceMatrix.times(matrixRotation, mathMatrix)
            Matrix4.rotation(matrixRotation, rotation.y, Vector3(0, 0, 1))
            mathMatrix.times(matrixRotation, instanceMatrix)

            mapObjectDefinitions.add(MapObjectDefinition(
                    name,
                    uuid,
                    instanceMatrix
            ))
        }
    }
}