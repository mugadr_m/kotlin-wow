package ch.cromon.wow.io.files.map.wotlk

import ch.cromon.wow.io.data.DataManager
import ch.cromon.wow.io.files.map.MapTileLoader
import ch.cromon.wow.io.files.map.MareEntry
import ch.cromon.wow.io.files.map.Tile
import ch.cromon.wow.io.files.map.WdlFile
import ch.cromon.wow.io.files.storage.DataStorage
import ch.cromon.wow.utils.BuildVersion
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component
@Scope("singleton")
@BuildVersion(8303, 26365)
class WotlkMapLoader : MapTileLoader {
    @Autowired
    private lateinit var dataLoader: DataManager

    @Autowired
    private lateinit var storageManager: DataStorage

    private var activeWdl: WdlFile? = null
    private var activeWdt: WdtFile? = null
    private var activeContinent: String? = null

    override fun load(mapName: String, indexX: Int, indexY: Int): Tile? {
        val wdtFile = if(activeWdt == null || mapName != activeContinent) {
            initWdtFile(mapName)
        } else {
            activeWdt!!
        }

        if(activeWdl == null) {
            activeWdl = initWdlFile(mapName)
        }

        activeContinent = mapName
        val path = "World\\Maps\\$mapName\\${mapName}_${indexX}_$indexY.adt"
        val file = dataLoader.openFile(path) ?: return null
        val retTile = MapTile(indexX, indexY, storageManager, wdtFile)
        if(!retTile.load(file)) {
            return null
        }

        return retTile
    }

    override fun activate() {

    }

    override fun loadLowDetail(x: Int, y: Int): MareEntry? {
        val wdlFile = activeWdl ?: throw IllegalArgumentException("Missing WDL for continent")
        return wdlFile.getMareChunk(x, y)
    }

    private fun initWdtFile(mapName: String): WdtFile {
        val path = "World\\Maps\\$mapName\\$mapName.wdt"
        val content = dataLoader.openFile(path) ?: throw IllegalArgumentException("Unable to load map, missing WDT: $path")
        return WdtFile(content)
    }

    private fun initWdlFile(mapName: String): WdlFile {
        val path = "World\\Maps\\$mapName\\$mapName.wdl"
        val content = dataLoader.openFile(path) ?: throw IllegalArgumentException("Unable to load map, missing WDL: $path")
        val wdl = WdlFile(content)
        wdl.parse()
        return wdl
    }

}