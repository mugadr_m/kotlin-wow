package ch.cromon.wow.io.data.remote


enum class ServerType(val displayName: String) {
    CLASSIC("Vanilla"),
    BC("Burning Crusade"),
    WOTLK("Wrath of the Lichking"),
    CATA("Cataclysm"),
    MOP("Mists of Pandaria"),
    WOD("Warlords of Draenor"),
    LEGION("Legion"),
    BFA("Battle for Azeroth")
}