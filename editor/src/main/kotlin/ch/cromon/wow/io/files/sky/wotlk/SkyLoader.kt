package ch.cromon.wow.io.files.sky.wotlk

import ch.cromon.wow.io.files.storage.DataStorage
import ch.cromon.wow.scene.sky.LightEntry
import ch.cromon.wow.scene.sky.SkyProvider
import ch.cromon.wow.utils.BuildVersion
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component


@Component
@Scope("singleton")
@BuildVersion(minBuild = 8303, maxBuild = 30000)
class WotlkSkyLoader : SkyProvider {
    private var activeLights: List<LightEntry> = ArrayList()

    override fun initialize(storage: DataStorage) {
        SkyFormatParser.load(storage)
        activeLights = SkyFormatParser.lights
    }

    override fun getLightsForMap(mapId: Int, mapName: String): List<LightEntry> {
        val mapLights = activeLights.filter {
            it.mapId == mapId
        }

        if(mapLights.isNotEmpty()) {
            return mapLights
        }

        val defaultLight = activeLights.filter { it.id == 1 }
        if(defaultLight.isNotEmpty()) {
            return defaultLight
        }

        if(activeLights.isNotEmpty()) {
            return activeLights.take(1)
        }

        return emptyList()
    }
}