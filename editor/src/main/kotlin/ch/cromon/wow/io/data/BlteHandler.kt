package ch.cromon.wow.io.data

import ch.cromon.wow.io.LittleEndianStream
import java.io.ByteArrayOutputStream
import java.util.zip.Inflater

class DataBlock(val compressedSize: Int, val decompressedSize: Int)

class BlteHandler(content: LittleEndianStream) {
    companion object {
        private const val MAGIC = 0x45544C42
    }

    val decompressedData: LittleEndianStream

    init {
        if(content.size < 8) {
            throw IllegalArgumentException("Cannot parse BLTE stream: Not enough data")
        }

        val magic = content.readInt()
        if(magic != MAGIC) {
            throw IllegalArgumentException("Cannot parse BLTE stream: Invalid magic, expected $MAGIC, got $magic")
        }

        val headerSize = content.readIntBE()
        var blockCount = 1
        if(headerSize > 0) {
            if(content.readByte() != 0x0F.toByte()) {
                throw IllegalArgumentException("Cannot parse BLTE stream: Invalid header")
            }

            blockCount = content.readByte().toInt() shl 16
            blockCount = blockCount or (content.readByte().toInt() shl 8)
            blockCount = blockCount or content.readByte().toInt()
            if(blockCount == 0) {
                throw IllegalArgumentException("Cannot parse BLTE stream: Invalid header")
            }

            val frameHeaderSize = 24 * blockCount + 12
            if(frameHeaderSize != headerSize) {
                throw IllegalArgumentException("Cannot parse BLTE stream: Invalid header sizes")
            }
        }

        val dataBlocks = ArrayList<DataBlock>()
        for(i in 0 until blockCount) {
            if(headerSize == 0) {
                dataBlocks.add(DataBlock(content.size - 8, content.size - 9))
            } else {
                dataBlocks.add(DataBlock(content.readIntBE(), content.readIntBE()))
                content.position += 16
            }
        }

        val outputStream = ByteArrayOutputStream()
        for(i in 0 until blockCount) {
            handleBlock(dataBlocks[i], content, outputStream)
        }

        decompressedData = LittleEndianStream(outputStream.toByteArray())
    }

    private fun handleBlock(dataBlock: DataBlock, content: LittleEndianStream, outputStream: ByteArrayOutputStream) {
        val data = content.readBytes(dataBlock.compressedSize)
        val indicator = data[0].toInt()
        when(indicator) {
            0x45 -> throw IllegalArgumentException("Encrypted files not supported")
            0x46 -> throw IllegalArgumentException("Recursive frames not supported")
            0x4E -> outputStream.write(data, 1, data.size - 1)
            0x5A -> {
                val inflater = Inflater()
                inflater.setInput(data, 1, data.size - 1)
                val outData = ByteArray(dataBlock.decompressedSize)
                while(!inflater.finished()) {
                    val numRead = inflater.inflate(outData, 0, dataBlock.decompressedSize)
                    outputStream.write(outData, 0, numRead)
                }
            }
            else -> throw IllegalArgumentException("Unknown block format")
        }
    }
}