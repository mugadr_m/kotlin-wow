package ch.cromon.wow.io.files.sky.wotlk

import ch.cromon.wow.io.files.storage.DataStorage
import ch.cromon.wow.io.files.storage.StorageFile
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.math.Vector4
import ch.cromon.wow.scene.sky.*


object SkyFormatParser {
    private lateinit var lightStorage: StorageFile
    private lateinit var lightParams: StorageFile
    private lateinit var lightFloatBand: StorageFile
    private lateinit var lightIntBand: StorageFile

    private val lightMap = HashMap<Int, LightEntry>()

    val lights get() = lightMap.values.toList()

    fun load(storage: DataStorage) {
        lightStorage = storage.loadStorage("Light") ?: throw IllegalArgumentException("Unable to find Light.dbc or Light.db2")
        lightParams = storage.loadStorage("LightParams") ?: throw IllegalArgumentException("Unable to find LightParams.dbc or LightParams.db2")
        lightFloatBand = storage.loadStorage("LightFloatBand") ?: throw IllegalArgumentException("Unable to find LightFloatBand.dbc or LightParams.db2")
        lightIntBand = storage.loadStorage("LightIntBand") ?: throw IllegalArgumentException("Unable to find LightIntBand.dbc or LightIntBand.db2")

        loadAllLights()
    }

    private fun loadAllLights() {
        for (row in lightStorage) {
            val id = row.getInt(0)
            val mapId = row.getInt(4)
            val position = Vector3(row.getFloat(8), row.getFloat(16), row.getFloat(12)) / 36.0f
            val innerRadius = row.getFloat(20) / 36.0f
            val outerRadius = row.getFloat(24) / 36.0f
            val clearParams = row.getInt(28)
            val intBandId = clearParams * 18 - 17
            val floatBandId = clearParams * 6 - 5
            val colors = ArrayList<InterpolatedColor>()
            val floats = ArrayList<InterpolatedFloat>()
            (0 .. MAX_LIGHT_COLOR).mapTo(colors, { getInterpolatedColor(intBandId + it) })
            (0 .. MAX_LIGHT_FLOAT).mapTo(floats, { getInterpolatedFloat(floatBandId + it) })
            lightMap[id] = LightEntry(
                    id,
                    mapId,
                    position,
                    innerRadius,
                    outerRadius,
                    colors,
                    floats)
        }
    }

    private fun getInterpolatedFloat(id: Int): InterpolatedFloat {
        val row = lightFloatBand.getRowById(id) ?: throw IllegalArgumentException("Row $id not found in LightFloatBand")

        val numEntries = row.getInt(4)
        val timeValues = ArrayList<Int>()
        val floatValues = ArrayList<Float>()
        for(i in 0 until numEntries) {
            val time = row.getInt(8 + i * 4)
            val value = row.getFloat(72 + i * 4)
            timeValues.add(time)
            floatValues.add(value)
        }

        return InterpolatedFloat(timeValues, floatValues)
    }

    private fun getInterpolatedColor(id: Int): InterpolatedColor {
        val row = lightIntBand.getRowById(id) ?: throw IllegalArgumentException("Row $id not found in LightIntBand")

        val numEntries = row.getInt(4)
        val timeValues = ArrayList<Int>()
        val colorValues = ArrayList<Vector4>()
        for (i in 0 until numEntries) {
            val time = row.getInt(8 + i * 4)
            val color = row.getInt(72 + i * 4)
            val b = (color and 0xFF)
            val g = ((color shr 8) and 0xFF)
            val r = ((color shr 16) and 0xFF)
            val a = ((color shr 24) and 0xFF)
            timeValues.add(time)
            colorValues.add(Vector4(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f))
        }

        return InterpolatedColor(timeValues, colorValues)
    }
}