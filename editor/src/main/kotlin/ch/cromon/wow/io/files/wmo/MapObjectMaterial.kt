package ch.cromon.wow.io.files.wmo


data class MapObjectMaterial(
        val flags: Int,
        val textureName: String,
        val blendMode: Int
)