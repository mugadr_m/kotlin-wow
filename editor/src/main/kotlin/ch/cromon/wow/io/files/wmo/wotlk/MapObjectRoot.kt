package ch.cromon.wow.io.files.wmo.wotlk

import ch.cromon.wow.io.CleanupWorker
import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.files.wmo.AbstractMapObject
import ch.cromon.wow.io.files.wmo.MapObjectMaterial
import ch.cromon.wow.math.BoundingBox
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.utils.changeExtension
import ch.cromon.wow.utils.format
import ch.cromon.wow.utils.toFourCC
import org.lwjgl.BufferUtils
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.FloatBuffer
import java.nio.ShortBuffer
import java.nio.charset.StandardCharsets


class MapObjectRoot(
        private val fileName: String,
        private val data: LittleEndianStream,
        private val fileLoadFunction: (String) -> LittleEndianStream?) : AbstractMapObject() {
    companion object {
        private val logger = LoggerFactory.getLogger(MapObjectRoot::class.java)
    }

    private lateinit var header: WmoRootHeader

    val ambientColor get() = header.ambientColor
    val useParentAmbient get() = (header.flags and 2) == 0

    val flags get() = header.flags

    init {
        val chunks = loadChunks()
        loadHeader(chunks["MOHD".toFourCC()])
        loadTextures(chunks["MOTX".toFourCC()], chunks["MOMT".toFourCC()])

        loadGroups(chunks["MOGN".toFourCC()], chunks["MOGI".toFourCC()])

        val wmoGroups = groups.map { it as MapObjectGroup }
        val vertexCount = wmoGroups.sumBy { it.vertices.size }
        val indexCount = wmoGroups.sumBy { it.indices.size }

        vertexData = BufferUtils.createFloatBuffer(vertexCount * 9)
        indexData = BufferUtils.createShortBuffer(indexCount)

        var baseVertex = 0
        var baseIndex = 0

        for(group in wmoGroups) {
            val vertices = group.vertices
            val indices = group.indices

            group.baseVertex = baseVertex
            group.baseIndex = baseIndex

            baseVertex += vertices.size
            baseIndex += indices.size

            for(vertex in vertices) {
                vertexData.put(vertex.position.x).put(vertex.position.y).put(vertex.position.z)
                vertexData.put(vertex.texCoord.x).put(vertex.texCoord.y)
                vertexData.put(vertex.normal.x).put(vertex.normal.y).put(vertex.normal.z)
                vertexData.put(Float.fromBits(vertex.color))
            }

            for(index in indices) {
                indexData.put(index)
            }
        }

        vertexData.flip()
        indexData.flip()

        boundingBox = BoundingBox(header.bboxMin, header.bboxMax)
    }

    override fun unload() {
        CleanupWorker.cleanup {
            for(group in groups) {
                val grp = group as MapObjectGroup
                grp.unload()
                vertexData = FloatBuffer.allocate(0)
                indexData = ShortBuffer.allocate(0)
            }
        }
    }

    private fun loadGroups(mogn: LittleEndianStream?, mogi: LittleEndianStream?) {
        mogn ?: throw IllegalStateException("Missing MOGN chunk in WMO")
        mogi ?: throw IllegalStateException("Missing MOGI chunk in WMO")

        val groupNames = ArrayList<String>()
        val numGroupInfos = mogi.size / 32
        for(i in 0 until numGroupInfos) {
            mogi.position = i * 32 + 28
            val nameOffset = mogi.readInt()
            if(nameOffset < 0) {
                groupNames.add("Unknown")
            } else {
                mogn.position = nameOffset
                groupNames.add(mogn.readString())
            }
        }

        val filePath = File(fileName)
        var basePath = filePath.changeExtension("").toRelativeString(File(".").absoluteFile)
        basePath = basePath.substring(0, basePath.length - 1)
        for(i in 0 until header.nGroups) {
            val groupName = "${basePath}_${i.format(3)}.wmo"
            val groupFile = fileLoadFunction(groupName)
            if(groupFile == null) {
                logger.warn("Group not found: $groupName")
                continue
            }

            groups.add(MapObjectGroup(groupFile, this, mogn))
        }
    }

    private fun loadHeader(mohd: LittleEndianStream?) {
        mohd ?: throw IllegalArgumentException("No MOHD in wmo found")

        header = WmoRootHeader(
                mohd.readInt(),
                mohd.readInt(),
                mohd.readInt(),
                mohd.readInt(),
                mohd.readInt(),
                mohd.readInt(),
                mohd.readInt(),
                mohd.readInt(),
                mohd.readInt(),
                Vector3(mohd.readFloat(), mohd.readFloat(), mohd.readFloat()),
                Vector3(mohd.readFloat(), mohd.readFloat(), mohd.readFloat()),
                mohd.readShort().toInt() and 0xFFFF,
                mohd.readShort().toInt() and 0xFFFF
        )
    }

    private fun loadTextures(motx: LittleEndianStream?, momt: LittleEndianStream?) {
        motx ?: return
        momt ?: return

        val fullString = String(motx.data, StandardCharsets.ISO_8859_1)
        for (i in 0 until header.nTextures) {
            momt.position = i * 64
            val flags = momt.readInt()
            momt.readInt()
            val blendMode = momt.readInt()
            val texture = getTexture(momt.readInt(), fullString)

            materials.add(MapObjectMaterial(flags, texture, blendMode))
        }
    }

    private fun getTexture(offset: Int, fullString: String): String {
        if(offset < 0 || offset >= fullString.length) {
            return ""
        }

        val strEnd = fullString.indexOf('\u0000', offset)
        return fullString.substring(offset, if (strEnd >= 0) strEnd else fullString.length)
    }

    private fun loadChunks(): Map<Int, LittleEndianStream> {
        val ret = HashMap<Int, LittleEndianStream>()
        while (data.size - data.position >= 8) {
            val signature = data.readInt()
            val size = data.readInt()
            val content = LittleEndianStream(data.readBytes(size))
            ret[signature] = content
        }
        return ret
    }
}