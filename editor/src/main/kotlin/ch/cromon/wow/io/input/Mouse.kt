package ch.cromon.wow.io.input

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.messaging.MouseButton
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component
@Scope("singleton")
class Mouse {
    private val buttonState = HashMap<MouseButton, Boolean>()

    private var lastPosition = Vector2()

    val state get() = buttonState

    var position = Vector2()

    fun endFrame() {
        lastPosition = position
    }

    fun setButton(button: MouseButton, state: Boolean) {
        buttonState[button] = state
    }

    fun isPressed(button: MouseButton): Boolean = buttonState[button] ?: false

    fun getInputActionIntensity(action: InputAction): Float {
        return when(action) {
            InputAction.CAMERA_ROTATE_V -> if(isPressed(MouseButton.RIGHT)) (position.y - lastPosition.y) else 0.0f
            InputAction.CAMERA_ROTATE_H -> if(isPressed(MouseButton.RIGHT)) (position.x - lastPosition.x) else 0.0f
            else -> 0.0f
        }
    }
}