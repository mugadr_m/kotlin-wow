package ch.cromon.wow.io

import org.apache.commons.lang3.StringUtils
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.io.File
import java.util.*
import javax.annotation.PostConstruct


@Component
@Scope("singleton")
class TemporaryStorage {
    private lateinit var baseFolder: File

    @PostConstruct
    fun initialize() {
        return
        baseFolder = File(getBaseDirectory(), UUID.randomUUID().toString())
        if(!baseFolder.exists()) {
            baseFolder.mkdirs()
        }

        if(baseFolder.exists()) {
            baseFolder.deleteOnExit()
        }
    }

    fun getTemporaryFile(fileName: String): File {
        return File(baseFolder, fileName)
    }

    private fun getBaseDirectory(): String {
        val userDir = System.getProperty("user.dir")
        if(!StringUtils.isBlank(userDir)) {
            return userDir
        }

        val userHome = System.getProperty("user.home")
        if(!StringUtils.isBlank(userHome)) {
            return userHome
        }

        return File(".").absolutePath
    }
}