package ch.cromon.wow.io.files.wmo

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.math.Vector3


data class MapObjVertex(
        val position: Vector3 = Vector3(),
        val texCoord: Vector2 = Vector2(),
        val normal: Vector3 = Vector3(),
        var color: Int = 0
)