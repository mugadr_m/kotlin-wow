package ch.cromon.wow.io.files.wmo.wotlk

import ch.cromon.wow.io.data.DataManager
import ch.cromon.wow.io.files.wmo.AbstractMapObject
import ch.cromon.wow.io.files.wmo.WmoProvider
import ch.cromon.wow.utils.BuildVersion
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
@BuildVersion(minBuild = 0, maxBuild = 12340)
class WotlkWmoProvider : WmoProvider {
    @Autowired
    private lateinit var dataLoader: DataManager

    override fun load(file: String): AbstractMapObject? {
        val content = dataLoader.openFile(file) ?: return null
        return MapObjectRoot(file, content, dataLoader::openFile)
    }
}