package ch.cromon.wow.io.data.remote

import ch.cromon.wow.io.LittleEndianFileStream
import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.data.FileProvider
import ch.cromon.wow.io.data.REMOTE_SERVER_PROPERTY
import ch.cromon.wow.io.http.HttpClient
import org.apache.http.client.utils.URIBuilder
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.net.URI
import java.util.*
import java.util.concurrent.CompletableFuture


@Component
@Qualifier("REMOTE")
@Scope("prototype")
class RemoteFileHandler : FileProvider() {
    private lateinit var baseUrl: URI

    override fun initialize(properties: Properties): CompletableFuture<Boolean> {
        val remoteUrl = properties.getProperty(REMOTE_SERVER_PROPERTY) ?: return CompletableFuture.completedFuture(false)
        baseUrl = URI(remoteUrl)
        buildVersion = 12340
        return CompletableFuture.completedFuture(true)
    }

    override fun canOpenByFileDataId() = false
    override fun containsFileDataId(id: Int) = false

    override fun openFromFileDataId(fileDataId: Int): LittleEndianStream? {
        TODO("not implemented")
    }

    override fun openFile(name: String): LittleEndianStream? {
        return try {
            val content = HttpClient.download(getUrl(name))
            LittleEndianStream(content)
        } catch (e: Exception) {
            null
        }
    }

    override fun openFileAsStream(name: String): LittleEndianFileStream? {
        throw NotImplementedError("Remote files cannot be opened as file stream")
    }

    override fun hasFile(name: String): Boolean {
        return HttpClient.exists(getUrl(name))
    }

    private fun getUrl(name: String): String {
        val builder = URIBuilder(baseUrl)
        builder.path += "/files/raw"
        builder.setParameter("file", name.toUpperCase())
        return builder.toString()
    }

}