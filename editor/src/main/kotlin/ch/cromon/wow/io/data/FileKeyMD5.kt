package ch.cromon.wow.io.data

import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin


class FileKeyMD5(val binary: ByteArray) : Comparable<FileKeyMD5?> {
    constructor(data: String) : this(HexBin.decode(data))

    override fun compareTo(other: FileKeyMD5?): Int {
        other ?: return 1
        for(i in 0 until 16) {
            val diff = binary[i] - other.binary[i]
            if(diff != 0) {
                return diff
            }
        }

        return 0
    }

    fun toHexString(): String {
        return HexBin.encode(binary).toLowerCase()
    }

}