package ch.cromon.wow.io.files.map

import ch.cromon.wow.io.data.DataLoadEvent
import ch.cromon.wow.math.BoundingBox
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.memory.MapLowVertexBufferCache
import ch.cromon.wow.utils.CdiUtils
import ch.cromon.wow.utils.TILE_SIZE
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

class MapAreaLow(val indexX: Int, val indexY: Int, data: MareEntry) {
    companion object {
        private const val STEP_X = TILE_SIZE / 16.0f
        private const val STEP_Y = TILE_SIZE / 32.0f
    }

    val vertices = MapLowVertexBufferCache.get()
    val boundingBox = BoundingBox()

    init {
        initFromData(data)
    }

    private fun initFromData(data: MareEntry) {
        val posMin = Vector3(Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE)
        val posMax = Vector3(-Float.MAX_VALUE, -Float.MAX_VALUE, -Float.MAX_VALUE)

        for(y in 0 until 33) {
            val inner = (y % 2) != 0
            for(x in 0 until (if(inner) 16 else 17)) {
                val height = if(inner) data.inner[(y / 2) * 16 + x] else data.outer[(y / 2) * 17 + x]
                val v = Vector3(x * STEP_X, y * STEP_Y, height.toFloat())
                v.x += indexX * TILE_SIZE
                v.y += indexY * TILE_SIZE

                if(inner) {
                    v.x += 0.5f * STEP_X
                }

                posMin.takeMin(v)
                posMax.takeMax(v)
                vertices.put(v.x).put(v.y).put(v.z)
            }
        }

        vertices.flip()
        boundingBox.min = posMin
        boundingBox.max = posMax
    }
}

@Component
@Scope("singleton")
class MapAreaLowLoader {
    private lateinit var tileLoader: MapTileLoader

    @EventListener
    fun onDataLoaded(event: DataLoadEvent) {
        tileLoader = CdiUtils.getAllVersionBeans(MapTileLoader::class.java).filter {
            it.minBuild <= event.build && it.maxBuild >= event.build
        }.map { it.bean }.firstOrNull() ?: throw IllegalArgumentException("No MapTileLoader found for build ${event.build}")
    }

    fun loadTile(x: Int, y: Int): MapAreaLow? {
        val lowData = tileLoader.loadLowDetail(x, y) ?: return null
        return MapAreaLow(x, y, lowData)
    }
}