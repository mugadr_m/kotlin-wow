package ch.cromon.wow.io.files.storage.wdb


enum class HeaderFlags {
    HAS_OFFSET_MAP,
    HAS_SECOND_INDEX,
    HAS_INDEX_MAP,
    UNKNOWN,
    COMPRESSED;

    val value get() = 1 shl ordinal

    infix fun setIn(value: Int) = (value and this.value) != 0
}