package ch.cromon.wow.io.data

import ch.cromon.wow.io.LittleEndianStream
import org.springframework.util.LinkedMultiValueMap

class RootParser(data: LittleEndianStream) {
    val rootData = LinkedMultiValueMap<Long, FileKeyMD5>()
    val fileDataIds = LinkedMultiValueMap<Int, FileKeyMD5>()

    init {
        while(data.position < data.size) {
            val count = data.readInt()
            data.position += 8
            val fileDataIdValues = IntArray(count)
            for(i in 0 until count) {
                val value = data.readInt()
                if(i == 0) {
                    fileDataIdValues[i] = value
                } else {
                    fileDataIdValues[i] = fileDataIdValues[i - 1] + 1 + value
                }
            }

            for(i in 0 until count) {
                val key = data.readMd5()
                val hash = data.readLong()
                rootData.add(hash, FileKeyMD5(key))
                fileDataIds.add(fileDataIdValues[i], FileKeyMD5(key))
            }
        }
    }
}