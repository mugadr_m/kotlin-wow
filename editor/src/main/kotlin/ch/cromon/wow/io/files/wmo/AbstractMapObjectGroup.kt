package ch.cromon.wow.io.files.wmo


abstract class AbstractMapObjectGroup {
    var baseVertex: Int = 0
        internal set

    var baseIndex: Int = 0
        internal set

    val batches = ArrayList<MapObjectBatch>()

    abstract val isIndoor: Boolean
}