package ch.cromon.wow.io.files.wmo

import ch.cromon.wow.math.Vector3


data class MapObjectBatch(
        val materialId: Int,
        val hasBoundingBox: Boolean,
        val bboxMin: Vector3,
        val bboxMax: Vector3,
        val startIndex: Int,
        val numIndices: Int,
        val lastVertex: Int
)