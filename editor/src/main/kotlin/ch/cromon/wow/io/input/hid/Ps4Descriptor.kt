package ch.cromon.wow.io.input.hid

import ch.cromon.wow.math.Vector2


data class Ps4Descriptor(
        var leftStick: Vector2 = Vector2(),
        var rightStick: Vector2 = Vector2(),
        var triangle: Boolean = false,
        var circle: Boolean = false,
        var cross: Boolean = false,
        var square: Boolean = false,
        var dpadTop: Boolean = false,
        var dpadRight: Boolean = false,
        var dpadBottom: Boolean = false,
        var dpadLeft: Boolean = false,
        var r3: Boolean = false,
        var l3: Boolean = false,
        var options: Boolean = false,
        var share: Boolean = false,
        var r2: Boolean = false,
        var l2: Boolean = false,
        var r1: Boolean = false,
        var l1: Boolean = false,
        var tpad: Boolean = false,
        var ps: Boolean = false,
        var l2Intensity: Float = 0.0f,
        var r2Intensity: Float = 0.0f
)