package ch.cromon.wow.io.files.sky.alpha

import ch.cromon.wow.io.data.DataManager
import ch.cromon.wow.io.files.storage.DataStorage
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.math.Vector4
import ch.cromon.wow.scene.sky.InterpolatedColor
import ch.cromon.wow.scene.sky.InterpolatedFloat
import ch.cromon.wow.scene.sky.LightEntry
import ch.cromon.wow.scene.sky.SkyProvider
import ch.cromon.wow.utils.BuildVersion
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component


@Component
@Scope("singleton")
@BuildVersion(minBuild = 0, maxBuild = 3494)
class AlphaSkyLoader : SkyProvider {
    @Autowired
    private lateinit var dataManager: DataManager

    override fun initialize(storage: DataStorage) {

    }

    override fun getLightsForMap(mapId: Int, mapName: String): List<LightEntry> {
        val litFilePath = "World\\Maps\\$mapName\\lights.lit"
        val litContent = dataManager.openFile(litFilePath) ?: return buildDefaultLight(mapId)

        val lights = ArrayList<LightEntry>()
        val litFile = LitFile(litContent)
        for((index, light) in litFile.lights.withIndex()) {
            val colors = litFile.colorData[index]
            val floats = litFile.floatData[index]
            lights.add(LightEntry(
                    light.name.hashCode(),
                    mapId,
                    Vector3(light.position.x / 36.0f, light.position.y / 36.0f, light.position.z / 36.0f),
                    light.innerRadius / 36.0f,
                    light.outerRadius / 36.0f,
                    colors,
                    floats,
                    light.chunkRadius < 0
            ))
        }

        return lights
    }

    private fun buildDefaultLight(mapId: Int): List<LightEntry> {
        val interpolatedColor = InterpolatedColor(
                arrayListOf(0, 2880),
                arrayListOf(Vector4(1, 1, 1, 1), Vector4(1, 1, 1, 1))
        )

        val fogEnd = InterpolatedFloat(arrayListOf(0, 2880), arrayListOf(899.0f, 899.0f))
        val fogScale = InterpolatedFloat(arrayListOf(0, 2880), arrayListOf(1.0f, 1.0f))

        val ret = LightEntry(
                0,
                mapId,
                Vector3(0, 0, 0),
                0.0f,
                0.0f,
                (0..17).map { interpolatedColor },
                arrayListOf(fogEnd, fogScale)
        )
        return listOf(ret)
    }

}