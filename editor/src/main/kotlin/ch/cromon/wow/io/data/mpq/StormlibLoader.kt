package ch.cromon.wow.io.data.mpq

import ch.cromon.wow.utils.NativeLoader
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct


@Component
@Scope("singleton")
class StormlibLoader {
    private lateinit var nativeLoader: NativeLoader

    fun extract() {
        nativeLoader.extract()
    }

    @PostConstruct
    fun init() {
        nativeLoader = NativeLoader("storm", "/stormlib", Stormlib.Companion::loadFromPath)
    }
}