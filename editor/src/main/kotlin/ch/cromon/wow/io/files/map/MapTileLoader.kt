package ch.cromon.wow.io.files.map

interface MapTileLoader {
    fun load(mapName: String, indexX: Int, indexY: Int): Tile?
    fun activate()
    fun loadLowDetail(x: Int, y: Int): MareEntry?
}