package ch.cromon.wow.io

import ch.cromon.wow.math.Vector3
import ch.cromon.wow.utils.toUInt
import java.io.EOFException
import java.io.InputStream
import java.nio.charset.StandardCharsets


class LittleEndianStream(val data: ByteArray) : InputStream() {
    var position = 0
    val size = data.size

    private var markedPosition = 0

    override fun read(): Int {
        if(position >= size) {
            throw EOFException("Cannot read past the end of the stream")
        }

        return this.data[position++].toInt()
    }

    override fun available(): Int {
        return size - position
    }

    override fun read(b: ByteArray): Int {
        val canRead = Math.min(available(), b.size)
        if(canRead == 0) {
            return 0
        }

        System.arraycopy(data, position, b, 0, canRead)
        position += canRead
        return canRead
    }

    override fun read(b: ByteArray, off: Int, len: Int): Int {
        if(off + len > b.size) {
            throw IndexOutOfBoundsException("Tried to read past the end of the output buffer")
        }

        val canRead = Math.min(available(), len)
        System.arraycopy(data, position, b, off, canRead)
        position += canRead
        return canRead
    }

    override fun skip(n: Long): Long {
        this.position += n.toInt()
        return this.position.toLong()
    }

    override fun mark(readlimit: Int) {
        markedPosition = position
    }

    override fun reset() {
        position = markedPosition
    }

    override fun markSupported(): Boolean {
        return true
    }

    fun readMd5(): ByteArray {
        return readBytes(16)
    }

    fun readString(): String {
        val ret = ArrayList<Byte>()
        while(available() > 0) {
            val b = readByte()
            if(b == 0.toByte()) {
                break
            }

            ret.add(b)
        }

        return String(ret.toByteArray(), StandardCharsets.US_ASCII)
    }

    fun readVector3() = Vector3(readFloat(), readFloat(), readFloat())

    fun readFloat() = Float.fromBits(readInt())

    fun readLong(): Long {
        val b0 = data[position++].toLong() and 0xFF
        val b1 = data[position++].toLong() and 0xFF
        val b2 = data[position++].toLong() and 0xFF
        val b3 = data[position++].toLong() and 0xFF
        val b4 = data[position++].toLong() and 0xFF
        val b5 = data[position++].toLong() and 0xFF
        val b6 = data[position++].toLong() and 0xFF
        val b7 = data[position++].toLong() and 0xFF

        return b0 or (b1 shl 8) or (b2 shl 16) or (b3 shl 24) or (b4 shl 32) or (b5 shl 40) or (b6 shl 48) or (b7 shl 56)
    }

    fun readInt(): Int {
        val b0 = data[position++].toUInt() and 0xFF
        val b1 = data[position++].toUInt() and 0xFF
        val b2 = data[position++].toUInt() and 0xFF
        val b3 = data[position++].toUInt() and 0xFF

        return b0 or (b1 shl 8) or (b2 shl 16) or (b3 shl 24)
    }

    fun readIntBE(): Int {
        val b0 = data[position++].toInt() and 0xFF
        val b1 = data[position++].toInt() and 0xFF
        val b2 = data[position++].toInt() and 0xFF
        val b3 = data[position++].toInt() and 0xFF

        return b3 or (b2 shl 8) or (b1 shl 16) or (b0 shl 24)
    }

    fun readShort(): Short {
        val b0 = data[position++].toUInt()
        val b1 = data[position++].toUInt()

        return (b0 or (b1 shl 8)).toShort()
    }

    fun readByte() = data[position++]

    fun readBytes(numBytes: Int): ByteArray {
        val ret = data.copyOfRange(position, position + numBytes)
        position += numBytes
        return ret
    }

    fun readBytes(dest: ByteArray, offset: Int, length: Int) {
        System.arraycopy(data, position, dest, offset, length)
        position += length
    }
}