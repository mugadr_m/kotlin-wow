package ch.cromon.wow.io.data.mpq

import ch.cromon.wow.io.LittleEndianFileStream
import ch.cromon.wow.utils.toUInt
import com.sun.jna.Pointer
import java.nio.ByteBuffer


class MpqFileStream(private val file: Pointer) : LittleEndianFileStream {
    override var position: Int
        get() = Stormlib.SFileSetFilePointer(file, 0, 1)
        set(value) {
            Stormlib.SFileSetFilePointer(file, value, 0)
        }

    override val size: Int = Stormlib.SFileGetFileSize(file).toInt()

    override fun close() {
        Stormlib.SFileCloseFile(file)
    }

    override fun readLong(): Long {
        val bytes = readBytes(8)

        val b0 = bytes[0].toLong() and 0xFF
        val b1 = bytes[1].toLong() and 0xFF
        val b2 = bytes[2].toLong() and 0xFF
        val b3 = bytes[3].toLong() and 0xFF
        val b4 = bytes[4].toLong() and 0xFF
        val b5 = bytes[5].toLong() and 0xFF
        val b6 = bytes[6].toLong() and 0xFF
        val b7 = bytes[7].toLong() and 0xFF

        return b0 or (b1 shl 8) or (b2 shl 16) or (b3 shl 24) or (b4 shl 32) or (b5 shl 40) or (b6 shl 48) or (b7 shl 56)
    }

    override fun readInt(): Int {
        val bytes = readBytes(4)
        val b0 = bytes[0].toInt() and 0xFF
        val b1 = bytes[1].toInt() and 0xFF
        val b2 = bytes[2].toInt() and 0xFF
        val b3 = bytes[3].toInt() and 0xFF

        return b0 or (b1 shl 8) or (b2 shl 16) or (b3 shl 24)
    }

    override fun readIntBE(): Int {
        val bytes = readBytes(4)
        val b0 = bytes[0].toInt() and 0xFF
        val b1 = bytes[1].toInt() and 0xFF
        val b2 = bytes[2].toInt() and 0xFF
        val b3 = bytes[3].toInt() and 0xFF

        return b3 or (b2 shl 8) or (b1 shl 16) or (b0 shl 24)
    }

    override fun readShort(): Short {
        val bytes = readBytes(2)
        val b0 = bytes[0].toUInt()
        val b1 = bytes[1].toUInt()

        return (b0 or (b1 shl 8)).toShort()
    }

    override fun readByte(): Byte = readBytes(1)[0]

    override fun readBytes(numBytes: Int): ByteArray {
        val ret = ByteArray(numBytes)
        Stormlib.SFileReadFile(file, ret)
        return ret
    }

    override fun readBytes(buffer: ByteBuffer, numBytes: Int) {
        Stormlib.SFileReadFile(file, buffer, numBytes)
    }
}