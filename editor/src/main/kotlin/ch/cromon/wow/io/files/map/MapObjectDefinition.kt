package ch.cromon.wow.io.files.map

import ch.cromon.wow.math.Matrix4


data class MapObjectDefinition(
        val modelName: String,
        val uniqueId: Int,
        val transform: Matrix4
)