package ch.cromon.wow.io.files.storage.wdb

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.files.storage.Db2FormatParser
import ch.cromon.wow.io.files.storage.Db2Row
import java.util.*


internal class Db2Parser(content: LittleEndianStream) : Db2FormatParser() {
    private val numRecords = content.readInt()
    private val numFields = content.readInt()
    private val recordSize = content.readInt()
    private val stringTableSize = content.readInt()
    private val tableHash = content.readInt()
    private val build = content.readInt()
    private val timeStamp = content.readInt()
    private val minId = content.readInt()
    private val maxId = content.readInt()
    private val locale = content.readInt()
    private val copyTableSize = content.readInt()

    private val records = ArrayList<Db2Row>()
    private val idMap = HashMap<Int, Int>()
    private val stringLengthMap = HashMap<Int, Short>()
    private val stringBlock = HashMap<Int, String>()

    init {
        if(maxId != 0 && build > 12880) {
            loadIdMap(content)
        }
        loadStringBlock(content)
        loadRecords(content)
    }

    override fun getRowByIndex(row: Int) = records[row]

    override fun getRowById(row: Int): Db2Row {
        val entry = idMap[row] ?: throw IllegalArgumentException("Id $row not found in DB2")
        return getRowByIndex(entry)
    }

    private fun loadIdMap(content: LittleEndianStream) {
        if(maxId != 0) {
            val indexCount = maxId - minId + 1
            for(i in 0 until indexCount) {
                val id = minId + i
                idMap[id] = content.readInt()
            }

            for(i in 0 until indexCount) {
                val id = minId + i
                stringLengthMap[id] = content.readShort()
            }
        }
    }

    private fun loadStringBlock(content: LittleEndianStream) {
        val extendedSize = if(maxId != 0) 6 * (maxId - minId + 1) else 0
        content.position = 48 + extendedSize + numRecords * recordSize
        parseStringBlock(stringBlock, content.readBytes(stringTableSize))
    }

    private fun loadRecords(content: LittleEndianStream) {
        val extendedSize = if(maxId != 0) 6 * (maxId - minId + 1) else 0
        content.position = 48 + extendedSize
        for(i in 0 until numRecords) {
            val bytes = content.readBytes(recordSize)
            val row = Db2Row(LittleEndianStream(bytes), stringBlock)
            if(idMap.isEmpty()) {
                idMap[row.getInt(0)] = i
            }
            records.add(row)
        }
    }

    override fun getRecordCount(): Int {
        return numRecords
    }

    override fun getFieldCount(): Int {
        return numFields
    }
}