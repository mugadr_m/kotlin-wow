package ch.cromon.wow.io.files.wmo.wotlk

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.files.wmo.AbstractMapObjectGroup
import ch.cromon.wow.io.files.wmo.MapObjVertex
import ch.cromon.wow.io.files.wmo.MapObjectBatch
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.memory.MapObjectDataCache
import ch.cromon.wow.utils.toFourCC
import org.slf4j.LoggerFactory


class MapObjectGroup(content: LittleEndianStream, private val parent: MapObjectRoot, mogn: LittleEndianStream) : AbstractMapObjectGroup() {
    companion object {
        private val logger = LoggerFactory.getLogger(MapObjectGroup::class.java)
    }

    private lateinit var name: String
    private lateinit var description: String
    private lateinit var header: WmoGroupHeader
    lateinit var vertices: ArrayList<MapObjVertex>
    lateinit var indices: ArrayList<Short>

    private val minPos = Vector3(Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE)
    private val maxPos = Vector3(-Float.MAX_VALUE, -Float.MAX_VALUE, -Float.MAX_VALUE)

    private var isLoaded = false

    override val isIndoor get() = (header.flags and 0x2000) != 0 && (header.flags and 8) == 0

    init {
        val chunks = loadChunks(content)
        val mogp = chunks["MOGP".toFourCC()]
        loadHeader(mogp)
        mogn.position = header.groupName
        name = mogn.readString()
        mogn.position = header.longName
        description = mogn.readString()
        val subChunks = loadSubChunks(mogp)
        loadBatches(subChunks)
        loadVertices(subChunks)
        isLoaded = true
    }

    override fun toString(): String {
        return "WmoGroup{name=$name description=$description}"
    }

    fun unload() {
        if(!isLoaded) {
            return
        }

        MapObjectDataCache.freeVertices(vertices)
        MapObjectDataCache.freeIndices(indices)
    }

    private fun loadBatches(chunks: Map<Int, LittleEndianStream>) {
        val moba = chunks["MOBA".toFourCC()] ?: throw IllegalArgumentException("File does not contain a MOBA chunk")
        val numBatches = moba.size / 24
        if (numBatches != (header.numExteriorBatches + header.numInteriorBatches + header.numTransparentBatches)) {
            logger.warn("Batch count mismatch: MOBA-Chunk: $numBatches vs Header: ${header.numExteriorBatches + header.numInteriorBatches + header.numTransparentBatches}")
            throw IllegalArgumentException("Mismatch in batch counts")
        }

        val wmoBatches = ArrayList<WmoBatch>()
        for (i in 0 until numBatches) {
            moba.position = i * 24
            wmoBatches.add(WmoBatch(
                    Vector3(moba.readShort().toFloat(), moba.readShort().toFloat(), moba.readShort().toFloat()),
                    Vector3(moba.readShort().toFloat(), moba.readShort().toFloat(), moba.readShort().toFloat()),
                    moba.readInt(),
                    moba.readShort().toInt() and 0xFFFF,
                    moba.readShort().toInt() and 0xFFFF,
                    moba.readShort().toInt() and 0xFFFF,
                    moba.readByte().toInt() and 0xFF,
                    moba.readByte().toInt() and 0xFF
            ))
        }

        wmoBatches.mapTo(batches, {
            MapObjectBatch(it.materialId, true, it.bboxMin, it.bboxMax, it.startIndex, it.indexCount, it.lastVertex)
        })
    }

    private fun loadVertices(chunks: Map<Int, LittleEndianStream>) {
        val movi = chunks["MOVI".toFourCC()] ?: throw IllegalArgumentException("File does not contain a MOVI chunk")
        val movt = chunks["MOVT".toFourCC()] ?: throw IllegalArgumentException("File does not contain a MOVT chunk")
        val monr = chunks["MONR".toFourCC()] ?: throw IllegalArgumentException("File does not contain a MONR chunk")
        val motv = chunks["MOTV".toFourCC()] ?: throw IllegalArgumentException("File does not contain a MOTV chunk")
        val mocv = chunks["MOCV".toFourCC()]

        val colors = readColors(mocv)
        val numVertices = movt.size / 12
        vertices = MapObjectDataCache.getVertices(numVertices)

        val indoor = isIndoor
        for (i in 0 until numVertices) {
            val color = if(colors != null) {
              colors[i]
            } else {
                if(indoor) {
                    0xFF7F7F7F.toInt()
                } else {
                    0
                }
            }

            val v = vertices[i]
            v.position.set(movt.readFloat(), -movt.readFloat(), movt.readFloat())
            v.texCoord.set(motv.readFloat(), motv.readFloat())
            v.normal.set(monr.readFloat(), -monr.readFloat(), -monr.readFloat())
            v.color = color

            minPos.takeMin(v.position)
            maxPos.takeMax(v.position)
        }

        val numIndices = movi.size / 2
        indices = MapObjectDataCache.getIndices(numIndices)

        for (i in 0 until numIndices) {
            indices[i] = movi.readShort()
        }
    }

    private fun readColors(mocv: LittleEndianStream?): ArrayList<Int>? {
        mocv ?: return null
        val colors = ArrayList<Int>()
        for(i in 0 until mocv.size / 4) {
            colors.add(mocv.readInt())
        }

        val firstInteriorVertex = if(header.numTransparentBatches > 0) batches[header.numTransparentBatches - 1].lastVertex + 1 else 0
        if((parent.flags and 0x08) != 0) {
            for(i in firstInteriorVertex until colors.size) {
                val w = if((header.flags and 8) != 0) 1 else 0
                var color = colors[i]
                color = color and 0x00FFFFFF
                color = color or (w shl 24)
                colors[i] = color
            }
        } else {
            val baseColor = (if(parent.useParentAmbient) parent.ambientColor else 0) and 0x00FFFFFF
            val cx = baseColor and 0xFF
            val cy = (baseColor shr 8) and 0xFF
            val cz = (baseColor shr 16) and 0xFF

            for(i in 0 until firstInteriorVertex) {
                var color = colors[i]
                val w = (color shr 24) and 0xFF
                val alphaPct = w / 255.0f

                var tmpX = color and 0xFF
                var tmpY = (color shr 8) and 0xFF
                var tmpZ = (color shr 16) and 0xFF
                var tmpW = (color shr 24) and 0xFF

                tmpX -= cz
                tmpY -= cy
                tmpX -= cx

                tmpX = (tmpX - alphaPct * tmpX).toInt() / 2
                tmpY = (tmpY - alphaPct * tmpY).toInt() / 2
                tmpZ = (tmpZ - alphaPct * tmpZ).toInt() / 2

                color = tmpX or (tmpY shl 8) or (tmpZ shl 16) or (tmpW shl 24)
                colors[i] = color
            }

            for(i in firstInteriorVertex until colors.size) {
                var color = colors[i]
                val tmpX = color and 0xFF
                val tmpY = (color shr 8) and 0xFF
                val tmpZ = (color shr 16) and 0xFF
                val tmpW = (color shr 24) and 0xFF

                val x = Math.min(Math.max(((tmpW * tmpX) / 64 + tmpX - cz) / 2, 0), 255)
                val y = Math.min(Math.max(((tmpW * tmpY) / 64 + tmpY - cy) / 2, 0), 255)
                val z = Math.min(Math.max(((tmpW * tmpZ) / 64 + tmpZ - cx) / 2, 0), 255)
                val w = if((header.flags and 8) != 0) 1 else 0

                color = x or (y shl 8) or (z shl 16) or (w shl 24)
                colors[i] = color
            }
        }

        handleAttenuation(colors)
        return colors
    }

    private fun handleAttenuation(colors: ArrayList<Int>) {

    }

    private fun loadSubChunks(content: LittleEndianStream?): Map<Int, LittleEndianStream> {
        content ?: throw IllegalArgumentException("File does not contain a MOGP header")
        return loadChunks(content)
    }

    private fun loadChunks(content: LittleEndianStream): Map<Int, LittleEndianStream> {
        val ret = HashMap<Int, LittleEndianStream>()
        while (content.size - content.position >= 8) {
            val signature = content.readInt()
            val size = content.readInt()
            if(ret.containsKey(signature)) {
                content.position += size
                continue
            }

            ret[signature] = if (size > 0) {
                LittleEndianStream(content.readBytes(size))
            } else {
                LittleEndianStream(ByteArray(0))
            }
        }

        return ret
    }

    private fun loadHeader(content: LittleEndianStream?) {
        content ?: throw IllegalArgumentException("File does not contain a MOGP header")

        header = WmoGroupHeader(
                content.readInt(),
                content.readInt(),
                content.readInt(),
                Vector3(content.readFloat(), content.readFloat(), content.readFloat()),
                Vector3(content.readFloat(), content.readFloat(), content.readFloat()),
                content.readShort().toInt(),
                content.readShort().toInt(),
                content.readShort().toInt(),
                content.readShort().toInt(),
                content.readShort().toInt(),
                content.readShort().toInt(),
                content.readByte().toInt(),
                content.readByte().toInt(),
                content.readByte().toInt(),
                content.readByte().toInt(),
                content.readInt(),
                content.readInt(),
                content.readInt(),
                content.readInt()
        )
    }
}