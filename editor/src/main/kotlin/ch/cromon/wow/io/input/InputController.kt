package ch.cromon.wow.io.input

import ch.cromon.wow.io.input.hid.HidController
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component
@Scope("singleton")
class InputController {
    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var hidController: HidController

    @Autowired
    private lateinit var mouse: Mouse

    @Autowired
    private lateinit var keyboard: Keyboard

    fun onFrame() {
        hidController.onFrame()
    }

    fun endFrame() {
        mouse.endFrame()
    }

    fun getInputActionToogle(action: InputAction): Boolean {
        return false
    }

    fun getInputActionIntensity(action: InputAction): Float {
        val hidValue = hidController.getInputActionIntensity(action)
        val keyboardValue = keyboard.getInputActionIntensity(action)
        val mouseValue = mouse.getInputActionIntensity(action)
        return if(Math.abs(hidValue) > Math.abs(keyboardValue) && Math.abs(hidValue) > Math.abs(mouseValue)) {
            hidValue
        } else if(Math.abs(keyboardValue) > Math.abs(hidValue) && Math.abs(keyboardValue) > Math.abs(mouseValue)) {
            keyboardValue
        } else {
            mouseValue
        }
    }
}