package ch.cromon.wow.io.files.storage

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.data.DataManager
import ch.cromon.wow.io.files.storage.definition.DefinitionLoader
import ch.cromon.wow.math.Vector3
import org.slf4j.LoggerFactory

class MapDescriptor {
    var loadScreenIndex: Int = -1
    var displayNameIndex: Int = -1
    var continentIndex: Int = -1
}

class WorldSafeLocDescriptor {
    var nameIndex: Int = -1
    var mapIdIndex: Int = -1
    var positionIndex: Int = -1
}

class TaxiNodeDescriptor {
    var nameIndex: Int = -1
    var mapIdIndex: Int = -1
    var positionIndex: Int = -1
}

class AreaTableDescriptor {
    var mapIdIndex: Int = -1
    var nameIndex: Int = -1
}

data class WorldSafeLoc(val mapId: Int, val name: String, val position: Vector3)
data class TaxiNode(val mapId: Int, val name: String, val position: Vector3)

internal object MapFormatGuesser : AbstractFormatGuesser() {
    private const val MIN_LEGION_BUILD = 22248
    private val logger = LoggerFactory.getLogger(MapFormatGuesser.javaClass)

    private lateinit var mapDescriptor: MapDescriptor
    private val worldSafeLocationsDescriptor = WorldSafeLocDescriptor()
    private val taxiNodeDescriptor = TaxiNodeDescriptor()
    private val areaTableDescriptor = AreaTableDescriptor()

    private var loadingScreenIndex: Int = 0
    private var wideLoadingScreenIndex: Int = 0

    private lateinit var mapStorage: StorageFile
    private lateinit var areaStorage: StorageFile
    private var loadingScreen: StorageFile? = null
    private lateinit var worldSafeLocations: StorageFile
    private lateinit var taxiNode: StorageFile

    private var buildId: Int = 0
    private var hasLoadingScreenPaths = false
    private var useDefinitions = false

    fun guess(build: Int,
              mapStorage: StorageFile, areaStorage: StorageFile, loadingScreen: StorageFile?,
              worldSafeLocations: StorageFile, taxiNode: StorageFile,
              fileData: StorageFile?,
              dataManager: DataManager) {
        buildId = build
        useDefinitions = build >= MIN_LEGION_BUILD

        this.mapStorage = mapStorage
        this.areaStorage = areaStorage
        this.loadingScreen = loadingScreen
        this.worldSafeLocations = worldSafeLocations
        this.taxiNode = taxiNode
        this.dataManager = dataManager
        this.fileData = fileData

        findContinentIndex()
        findLoadingScreensLink()
        findLoadingScreenPath()
        findMapName()
        findWorldSafeLocFields()
        findTaxiNodeIndices()
        findAreaTable()
    }

    fun listMapRows(callback: (StorageRow, MapDescriptor) -> Unit) {
        for (i in 0 until mapStorage.getRowCount()) {
            callback(mapStorage.getRowByIndex(i), mapDescriptor)
        }
    }

    fun getMapName(id: Int): String? {
        val row = mapStorage.getRowById(id) ?: return null
        return row.getString(mapDescriptor.displayNameIndex * 4)
    }

    fun getLoadingScreenFile(id: Int): LittleEndianStream? {
        if (!dataManager.canOpenByFileDataId()) {
            val lsPath = getLoadingScreen(id) ?: return null
            return dataManager.openFile(lsPath)
        } else {
            val fileId = loadingScreen?.getRowById(id)?.getInt(loadingScreenIndex * 4) ?: return null
            return dataManager.openFromFileDataId(fileId)
        }
    }

    fun hasWideLoadingScreen(id: Int) = loadingScreen?.getRowById(id)?.getInt(wideLoadingScreenIndex * 4) != 0
    fun getLoadingScreen(id: Int) = loadingScreen?.getRowById(id)?.getString(loadingScreenIndex * 4)
    fun getFallbackLoadingScreen() = "Interface\\Glues\\loading.blp"
    fun getLoadingScreenIdForMap(mapId: Int): Int {
        if (mapDescriptor.loadScreenIndex < 0) {
            return -1
        }

        val mapRow = mapStorage.getRowById(mapId) ?: return -1
        val loadingScreenId = mapRow.getInt(mapDescriptor.loadScreenIndex * 4)
        loadingScreen?.getRowById(loadingScreenId) ?: return -1
        return loadingScreenId
    }

    fun getSafeLocationsForMap(mapId: Int): List<WorldSafeLoc> {
        return worldSafeLocations.map {
            worldSafeLocationsDescriptor.run {
                val pos = Vector3(it.getFloat(positionIndex * 4), it.getFloat(positionIndex * 4 + 4), it.getFloat(positionIndex * 4 + 8))
                val name = it.getString(nameIndex * 4)
                val map = it.getInt(mapIdIndex * 4)
                WorldSafeLoc(map, name, pos)
            }
        }.filter {
            it.mapId == mapId
        }
    }

    fun getTaxiNodesForMap(mapId: Int): List<TaxiNode> {
        return taxiNode.map {
            taxiNodeDescriptor.run {
                val pos = Vector3(it.getFloat(positionIndex * 4), it.getFloat(positionIndex * 4 + 4), it.getFloat(positionIndex * 4 + 8))
                val name = it.getString(nameIndex * 4)
                val map = it.getInt(mapIdIndex * 4)
                TaxiNode(map, name, pos)
            }
        }.filter {
            it.mapId == mapId
        }
    }

    fun getAreaName(id: Int): String? = areaStorage.getRowById(id)?.getString(areaTableDescriptor.nameIndex * 4)

    fun getWideLoadingScreen(id: Int): LittleEndianStream? {
        if (wideLoadingScreenIndex < 0) {
            return null
        }

        if (hasLoadingScreenPaths) {
            val lsPath = loadingScreen?.getRowById(id)?.getString(wideLoadingScreenIndex * 4) ?: return null
            return dataManager.openFile(lsPath)
        } else {
            val lsFileId = loadingScreen?.getRowById(id)?.getInt(wideLoadingScreenIndex * 4) ?: return null
            return dataManager.openFromFileDataId(lsFileId)
        }
    }

    private fun findLoadingScreensLink() {
        val lsd = loadingScreen ?: return
        if (!useDefinitions) {
            val loadIndex = getBestMatchingIntField(mapStorage, {
                it != 0 && lsd.getRowById(it) != null
            })

            if (loadIndex < 0) {
                logger.error("Unable to find link between LoadingScreens and Map")
                throw IllegalArgumentException("Unable to parse Map storage file")
            }

            mapDescriptor.loadScreenIndex = loadIndex
        } else {
            val mapFormat = DefinitionLoader.findDefinition("Map", buildId)
                    ?: throw IllegalArgumentException("No map definition found for build $buildId")
            mapDescriptor.loadScreenIndex = mapFormat.getFieldIndex("loadingScreenId")
        }
    }

    private fun findContinentIndex() {
        mapDescriptor = MapDescriptor()
        if (!useDefinitions) {
            val mapIndex = getBestMatchingStringField(mapStorage, {
                dataManager.hasFile("World\\Maps\\$it\\$it.wdt")
            })

            if (mapIndex < 0) {
                logger.error("Unable to find index in Map storage file for continent name")
                throw IllegalArgumentException("Unable to parse Map storage file")
            }

            mapDescriptor.continentIndex = mapIndex
        } else {
            val mapFormat = DefinitionLoader.findDefinition("Map", buildId)
                    ?: throw IllegalArgumentException("No map definition found for build $buildId")
            mapDescriptor.continentIndex = mapFormat.getFieldIndex("directory")
        }
    }

    private fun findMapName() {
        if (!useDefinitions) {
            val nameIndex = getBestMatchingStringField(mapStorage, { !it.contains("\n") && it.length < 100 }, mapDescriptor.continentIndex)
            if (nameIndex < 0) {
                logger.error("Unable to find localized map name column in Map storage")
                throw IllegalArgumentException("Unable to parse Map storage file")
            }

            mapDescriptor.displayNameIndex = nameIndex
        } else {
            val mapFormat = DefinitionLoader.findDefinition("Map", buildId)
                    ?: throw IllegalArgumentException("No map definition found for build $buildId")
            mapDescriptor.displayNameIndex = mapFormat.getFieldIndex("mapName")
        }
    }

    private fun findLoadingScreenPath() {
        val lsd = loadingScreen ?: return
        if (!useDefinitions) {
            val lsIndex = getBestMatchingStringField(lsd, { dataManager.hasFile(it) })

            if (lsIndex < 0) {
                logger.error("Unable to find loading screen path in LoadingScreens storage")
                throw IllegalArgumentException("Unable to parse LoadingScreens storage file")
            }

            loadingScreenIndex = lsIndex
            hasLoadingScreenPaths = true
            val wideIndex = getBestMatchingBoolean(lsd, 0, lsIndex)
            if (wideIndex < 0) {
                logger.info("Unable to find wide indicator in LoadingScreens storage. Assuming all loading screens are non-wide")
            }

            wideLoadingScreenIndex = wideIndex
        } else {
            hasLoadingScreenPaths = false
            val loadScreenFormat = DefinitionLoader.findDefinition("LoadingScreens", buildId) ?: throw IllegalStateException("No Loading Screen format definition for build $buildId found")
            loadingScreenIndex = loadScreenFormat.getFieldIndex("NarrowScreenFileDataId")
            wideLoadingScreenIndex = loadScreenFormat.getFieldIndex("WideScreen169FileDataId")
        }
    }

    private fun findWorldSafeLocFields() {
        val nameIndex = getBestMatchingStringField(worldSafeLocations, { true }, 0)
        if (nameIndex < 0) {
            logger.error("Unable to find world safe locations name in WorldSafeLocs storage")
            throw IllegalArgumentException("Unable to parse WorldSafeLocs storage file")
        }

        val mapIdIndex = getBestMatchingIntField(worldSafeLocations, {
            mapStorage.getRowById(it) != null
        }, 0, nameIndex)

        if (mapIdIndex < 0) {
            logger.error("Unable to find map id in WorldSafeLocs storage")
            throw IllegalArgumentException("Unable to parse WorldSafeLocs storage file")
        }

        val positionIndex = getBestMatchingVector3(worldSafeLocations, 0, nameIndex, mapIdIndex)
        if (positionIndex < 0) {
            logger.error("Unable to find position in WorldSafeLocs stoarge")
            throw IllegalArgumentException("Unable to parse WorldSafeLocs storage file")
        }

        worldSafeLocationsDescriptor.mapIdIndex = mapIdIndex
        worldSafeLocationsDescriptor.nameIndex = nameIndex
        worldSafeLocationsDescriptor.positionIndex = positionIndex
    }

    private fun findTaxiNodeIndices() {
        val nameIndex = getBestMatchingStringField(taxiNode, { true }, 0)
        if (nameIndex < 0) {
            logger.error("Unable to find tax node name in TaxiNodes storage")
            throw IllegalArgumentException("Unable to parse TaxiNodes storage file")
        }

        val mapIdIndex = getBestMatchingIntFieldWithVariance(taxiNode, {
            mapStorage.getRowById(it) != null
        }, 0, nameIndex)

        if (mapIdIndex < 0) {
            logger.error("Unable to find map id in TaxiNodes storage")
            throw IllegalArgumentException("Unable to parse TaxiNodes storage file")
        }

        val positionIndex = getBestMatchingVector3(taxiNode, 0, nameIndex, mapIdIndex)
        if (positionIndex < 0) {
            logger.error("Unable to find position in TaxiNodes stoarge")
            throw IllegalArgumentException("Unable to parse TaxiNodes storage file")
        }

        taxiNodeDescriptor.mapIdIndex = mapIdIndex
        taxiNodeDescriptor.nameIndex = nameIndex
        taxiNodeDescriptor.positionIndex = positionIndex
    }

    private fun findAreaTable() {
        val mapIdIndex = getBestMatchingIntFieldWithVariance(areaStorage, { mapStorage.getRowById(it) != null })
        if (mapIdIndex < 0) {
            logger.error("Unable to find map id link in AreaTable storage")
            throw IllegalArgumentException("Unable to parse AreaTable storage file")
        }

        areaTableDescriptor.mapIdIndex = mapIdIndex
        val nameIndex = getBestMatchingStringField(areaStorage, { it.isNotEmpty() })
        if (nameIndex < 0) {
            logger.error("Unable to find area name index in AreaTable storage")
            throw IllegalArgumentException("Unable to parse AreaTable storage file")
        }

        areaTableDescriptor.nameIndex = nameIndex
    }
}