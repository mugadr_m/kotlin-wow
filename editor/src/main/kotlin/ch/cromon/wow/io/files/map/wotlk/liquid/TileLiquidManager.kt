package ch.cromon.wow.io.files.map.wotlk.liquid

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.files.storage.DataStorage
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.utils.LIQUID_VERTEX_SIZE
import ch.cromon.wow.utils.set
import java.nio.ByteBuffer
import java.nio.FloatBuffer

class TileLiquidManager(private val dataStorage: DataStorage) {
    private val renderCache = HashMap<Int, LiquidTypeRender>()
    private val legacyCache = HashMap<Int, LiquidTypeRender>()

    var renderers: List<LiquidTypeRender> = ArrayList()
        private set

    val hasAnyLiquid get() = renderCache.isNotEmpty() || legacyCache.isNotEmpty()

    fun updateRenderers() {
        renderers = renderCache.values.union(legacyCache.values).toList()
    }

    fun handleLiquidInstance(chunkData: LittleEndianStream, chunkPos: Vector3, instance: LiquidChunkInstance) {
        val vertices = ArrayList<Vector3>()
        val texCoords = ArrayList<Vector2>()
        val depths = ArrayList<Float>()
        val indices = ArrayList<Int>()
        val renderer = getOrCreateLiquidRenderer(instance.liquidType.toInt())

        chunkData.position = instance.offsetBitmap
        val bitmap = if (instance.offsetBitmap == 0) {
            LiquidBitmap()
        } else {
            LiquidBitmap(chunkData.readBytes(((instance.width * instance.height + 7) / 8)))
        }

        chunkData.position = instance.offsetVertices
        val rowPitch = instance.width + 1
        val rows = instance.height + 1
        val heightMap = FloatBuffer.allocate(rowPitch * rows)
        val depthMap = ByteBuffer.allocate(rowPitch * rows)
        if (instance.liquidType.toInt() != 2) {
            for (i in 0 until (rowPitch * rows)) {
                heightMap[i] = chunkData.readFloat()
            }
        } else {
            for (i in 0 until (rowPitch * rows)) {
                heightMap[i] = instance.minHeight
            }
        }

        if (instance.offsetVertices != 0) {
            for (i in 0 until (rowPitch * rows)) {
                depthMap[i] = chunkData.readByte()
            }
        } else {
            for (i in 0 until (rowPitch * rows)) {
                depthMap[i] = 255.toByte()
            }
        }

        var baseIndex = renderer.baseIndex
        val bx = instance.offsetX * LIQUID_VERTEX_SIZE + chunkPos.x
        val by = instance.offsetY * LIQUID_VERTEX_SIZE + chunkPos.y
        for (x in 0 until instance.width) {
            for (y in 0 until instance.height) {
                if (!bitmap.isSet(x, y, instance.width.toInt(), instance.height.toInt())) {
                    continue
                }

                handleInstanceQuad(bx, by, x, y, rowPitch, heightMap, depthMap, vertices, texCoords, depths, indices, baseIndex)
                baseIndex += 4
            }
        }

        renderer.pushChunk(vertices, texCoords, depths, indices)
    }

    private fun handleInstanceQuad(
            bx: Float, by: Float,
            x: Int, y: Int,
            rowPitch: Int,
            heightMap: FloatBuffer, depthMap: ByteBuffer,
            vertices: ArrayList<Vector3>,
            texCoords: ArrayList<Vector2>,
            depths: ArrayList<Float>,
            indices: ArrayList<Int>,
            baseIndex: Int) {
        val v0 = Vector3(bx + x * LIQUID_VERTEX_SIZE, by + y * LIQUID_VERTEX_SIZE, heightMap[y * rowPitch + x])
        val v1 = Vector3(bx + (x + 1) * LIQUID_VERTEX_SIZE, by + y * LIQUID_VERTEX_SIZE, heightMap[y * rowPitch + x + 1])
        val v2 = Vector3(bx + (x + 1) * LIQUID_VERTEX_SIZE, by + (y + 1) * LIQUID_VERTEX_SIZE, heightMap[(y + 1) * rowPitch + x + 1])
        val v3 = Vector3(bx + x * LIQUID_VERTEX_SIZE, by + (y + 1) * LIQUID_VERTEX_SIZE, heightMap[(y + 1) * rowPitch + x])
        val t0 = Vector2(x, y)
        val t1 = Vector2(x + 1, y)
        val t2 = Vector2(x + 1, y + 1)
        val t3 = Vector2(x, y + 1)
        val d0 = (depthMap[y * rowPitch + x].toInt() and 0xFF) / 255.0f
        val d1 = (depthMap[y * rowPitch + x + 1].toInt() and 0xFF) / 255.0f
        val d2 = (depthMap[(y + 1) * rowPitch + x + 1].toInt() and 0xFF) / 255.0f
        val d3 = (depthMap[(y + 1) * rowPitch + x].toInt() and 0xFF) / 255.0f
        vertices.add(v0)
        vertices.add(v1)
        vertices.add(v2)
        vertices.add(v3)

        texCoords.add(t0)
        texCoords.add(t1)
        texCoords.add(t2)
        texCoords.add(t3)

        depths.add(d0)
        depths.add(d1)
        depths.add(d2)
        depths.add(d3)

        indices.add(baseIndex + 0)
        indices.add(baseIndex + 1)
        indices.add(baseIndex + 2)
        indices.add(baseIndex + 3)
    }

    fun handleChunkLiquidOld(chunkPos: Vector3, liquidType: Int, chunk: LittleEndianStream) {
        val renderer = getOrCreateLegacyRenderer(liquidType)

        chunk.position += 8 // minHeight, maxHeight

        val vertices = ArrayList<Vector3>()
        val texCoords = ArrayList<Vector2>()
        val depths = ArrayList<Float>()
        for (y in 0 until 9) {
            for (x in 0 until 9) {
                val d = chunk.readByte().toInt() and 0xFF
                val depth =
                        if (!renderer.isOcean)
                            Math.min(1.0f, d * 0.111111f * 0.21428572f)
                        else
                            Math.min(1.0f, d * 0.0375f * 21.0f * 0.0277777f)
                depths.add(depth)
                chunk.position += 3
                var h = chunk.readFloat()
                if (h >= 1.0e10f) {
                    h = 0.0f
                }

                vertices.add(Vector3(chunkPos.x + x * LIQUID_VERTEX_SIZE, chunkPos.y + y * LIQUID_VERTEX_SIZE, h))
                texCoords.add(Vector2(x, y))
            }
        }

        val baseIndex = renderer.baseIndex
        val indices = ArrayList<Int>(64 * 64)

        for (y in 0 until 8) {
            for (x in 0 until 8) {
                val flag = chunk.readByte().toInt()
                if (flag == 0x0F || flag == 0x08) {
                    continue
                }

                val tl = y * 9 + x
                val tr = y * 9 + x + 1
                val br = (y + 1) * 9 + x + 1
                val bl = (y + 1) * 9 + x

                indices.add(baseIndex + tl)
                indices.add(baseIndex + tr)
                indices.add(baseIndex + br)
                indices.add(baseIndex + bl)
            }
        }

        renderer.pushChunk(vertices, texCoords, depths, indices)
    }

    private fun getOrCreateLiquidRenderer(type: Int): LiquidTypeRender {
        val current = renderCache[type]
        if (current != null) {
            return current
        }

        val liquidInfo = LiquidTypeRender(dataStorage.getLiquidType(type) ?: FALLBACK_LIQUID)
        renderCache[type] = liquidInfo
        return liquidInfo
    }

    private fun getOrCreateLegacyRenderer(type: Int): LiquidTypeRender {
        val current = legacyCache[type]
        if (current != null) {
            return current
        }

        val liquid = when (type) {
            1 -> RIVER_LIQUID
            2 -> OCEAN_LIQUID
            3 -> MAGMA_LIQUID
            4 -> SLIME_LIQUID
            else -> FALLBACK_LIQUID
        }

        val liquidInfo = LiquidTypeRender(liquid)
        legacyCache[type] = liquidInfo
        return liquidInfo
    }
}