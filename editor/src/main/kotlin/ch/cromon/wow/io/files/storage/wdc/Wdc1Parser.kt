package ch.cromon.wow.io.files.storage.wdc

import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.files.storage.Db2FormatParser
import ch.cromon.wow.io.files.storage.Db2Row
import ch.cromon.wow.io.files.storage.wdb.FieldDescriptor
import ch.cromon.wow.io.files.storage.wdb.HeaderFlags
import ch.cromon.wow.io.files.storage.wdb.OffsetDuplicate
import com.google.common.io.LittleEndianDataOutputStream
import org.apache.commons.compress.utils.BitInputStream
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*


class Wdc1Parser(content: LittleEndianStream) : Db2FormatParser() {
    private val recordCount = content.readInt()
    private var fieldCount = content.readInt()
    private val recordSize = content.readInt()
    private val stringBlockSize = content.readInt()
    private val offsetTableStart = stringBlockSize
    private val tableHash = content.readInt()
    private val layoutHash = content.readInt()
    private val minId = content.readInt()
    private val maxId = content.readInt()
    private val locale = content.readInt()
    private val copyTableSize = content.readInt()
    private val flags = content.readShort().toInt()
    private val idIndex = content.readShort().toInt()
    private val totalFieldSize = content.readInt()
    private val packedDataOffset = content.readInt()
    private val relationShipCount = content.readInt()
    private val offsetTablePos = content.readInt()
    private val indexSize = content.readInt()
    private val columnMetaDataSize = content.readInt()
    private val sparseDataSize = content.readInt()
    private val palletDataSize = content.readInt()
    private val relationShipDataSize = content.readInt()

    private val fieldStructure = ArrayList<FieldDescriptor>()
    private val recordData = ByteArray(recordCount * recordSize + 8)

    private val hasIndexTable = HeaderFlags.HAS_INDEX_MAP setIn flags
    private val hasOffsetTable = HeaderFlags.HAS_OFFSET_MAP setIn flags

    private var commonDataTablePos = 0
    private val stringBlock = HashMap<Int, String>()
    private val offsetDuplicates = HashMap<Int, Int>()
    private val idCopies = HashMap<Int, ArrayList<Int>>()
    private val columnMeta = ArrayList<ColumnDescriptor>()

    private val recordMap = HashMap<Int, ByteArray>()
    private val recordList = ArrayList<ByteArray>()

    init {
        for (i in 0 until fieldCount) {
            fieldStructure.add(FieldDescriptor(content.readShort().toInt(), content.readShort().toInt()))
        }

        content.readBytes(recordData, 0, recordCount * recordSize)

        parseStringBlock(stringBlock, content.readBytes(stringBlockSize))

        val offsetMap = ArrayList<Pair<Int, Int>>()
        val firstIndex = HashMap<Int, OffsetDuplicate>()
        if (hasOffsetTable && offsetTablePos > 0) {
            content.position = offsetTablePos
            parseOffsetTable(content, offsetMap, firstIndex)
        }

        val indexMap = HashMap<Int, Int>()
        if (hasIndexTable) {
            for (i in 0 until recordCount) {
                indexMap[i] = content.readInt()
            }
        }

        if (copyTableSize > 0) {
            val end = content.position + copyTableSize
            while (content.position < end) {
                val id = content.readInt()
                val idCopy = content.readInt()
                idCopies.computeIfAbsent(id, { ArrayList() })
                idCopies[id]?.add(idCopy)
            }
        }

        for (i in 0 until fieldCount) {
            val column = ColumnDescriptor(
                    recordOffset = content.readShort().toInt(),
                    size = content.readShort().toInt(),
                    additionalDataSize = content.readInt(),
                    compressionType = CompressionType.values()[content.readInt()],
                    bitOffset = content.readInt(),
                    bitWidth = content.readInt(),
                    cardinality = content.readInt()
            )

            if (column.compressionType == CompressionType.NONE) {
                column.arraySize = Math.max(column.size / fieldStructure[i].bitCount, 1)
            } else if (column.compressionType == CompressionType.PALLET_ARRAY) {
                column.arraySize = Math.max(column.cardinality, 1)
            }

            columnMeta.add(column)
        }

        for (i in 0 until columnMeta.size) {
            val col = columnMeta[i]
            if (col.compressionType == CompressionType.PALLET_ARRAY || col.compressionType == CompressionType.PALLET) {
                val numElements = col.additionalDataSize / 4
                val cardinality = Math.max(1, col.cardinality)
                for (j in 0 until (numElements / cardinality)) {
                    col.palletValues.add(content.readBytes(cardinality * 4))
                }
            }
        }

        for (i in 0 until columnMeta.size) {
            val col = columnMeta[i]
            if (col.compressionType == CompressionType.SPARSE) {
                for (j in 0 until (col.additionalDataSize / 8)) {
                    col.sparseValues[content.readInt()] = content.readBytes(4)
                }
            }
        }

        if (relationShipDataSize > 0) {
            val records = content.readInt()
            content.position += 8 + 8 * records
        }

        val copyTable = HashMap<Int, ByteArray>()
        for (i in 0 until recordCount) {
            var id = 0
            var idOffset = 0
            val rowContent = if (hasOffsetTable && hasIndexTable) {
                id = indexMap[copyTable.size]!!
                val map = offsetMap[i]
                if (copyTableSize == 0 && firstIndex[map.first]?.hidden != i) {
                    continue
                }

                content.position = map.first
                val data = content.readBytes(map.second)
                val recordBytes = ByteArray(4 + data.size + if (relationShipDataSize > 0) 4 else 0)
                val bb = ByteBuffer.wrap(recordBytes).order(ByteOrder.LITTLE_ENDIAN).putInt(id).put(data)
                if (relationShipDataSize > 0) {
                    bb.putInt(0)
                }

                recordBytes
            } else {
                val rowData = ByteArrayInputStream(recordData, i * recordSize, recordSize)
                BitInputStream(rowData, ByteOrder.LITTLE_ENDIAN).use {
                    val outputData = ByteArrayOutputStream()
                    val wrapper = LittleEndianDataOutputStream(outputData)
                    if(hasIndexTable) {
                        id = indexMap[i]!!
                        wrapper.writeInt(id)
                    }

                    for(field in 0 until fieldCount) {
                        val col = columnMeta[field]
                        val bitWidth = col.bitWidth

                        when(col.compressionType) {
                            CompressionType.NONE -> {
                                val bitSize = fieldStructure[field].bitCount
                                if(!hasIndexTable && field == idIndex) {
                                    idOffset = outputData.size()
                                    id = it.readBits(bitSize).toInt()
                                    wrapper.writeInt(id)
                                } else {
                                    for(x in 0 until col.arraySize) {
                                        val tmp = it.readBits(bitSize)
                                        var numBytes = (bitSize + 7) / 8
                                        numBytes = when {
                                            numBytes <= 4 -> 4
                                            numBytes < 8 -> 8
                                            else -> throw IllegalArgumentException("Cannot read $numBytes")
                                        }

                                        for(l in 0 until numBytes) {
                                            wrapper.write(((tmp shr (l * 8)) and 0xFF).toInt())
                                        }
                                    }
                                }
                            }
                            CompressionType.IMMEDIATE -> {
                                if(!hasIndexTable && field == idIndex) {
                                    idOffset = outputData.size()
                                    id = it.readBits(bitWidth).toInt()
                                    wrapper.writeInt(id)
                                } else {
                                    var numBytes = (bitWidth + 7) / 8
                                    numBytes = when {
                                        numBytes <= 4 -> 4
                                        numBytes < 8 -> 8
                                        else -> throw IllegalArgumentException("Cannot read $numBytes")
                                    }
                                    val tmp = it.readBits(bitWidth)
                                    for(l in 0 until numBytes) {
                                        wrapper.write(((tmp shr (l * 8)) and 0xFF).toInt())
                                    }
                                }
                            }
                            CompressionType.SPARSE -> {
                                val sparse = col.sparseValues[id]
                                var numBytes = (bitWidth + 7) / 8
                                numBytes = when {
                                    numBytes <= 4 -> 4
                                    numBytes < 8 -> 8
                                    else -> throw IllegalArgumentException("Cannot read $numBytes")
                                }
                                if(sparse != null) {
                                    for(l in 0 until numBytes) {
                                        if(l < sparse.size) {
                                            wrapper.writeByte(sparse[l].toInt())
                                        } else {
                                            wrapper.writeByte(0)
                                        }
                                    }
                                } else {
                                    wrapper.writeInt(col.bitOffset)
                                    for(l in 4 until numBytes) {
                                        wrapper.writeByte(0)
                                    }
                                }
                            }
                            CompressionType.PALLET, CompressionType.PALLET_ARRAY -> {
                                val index = it.readBits(bitWidth).toInt()
                                wrapper.write(col.palletValues[index])
                            }
                        }
                    }
                    outputData.toByteArray()
                }
            }

            copyTable[id] = rowContent
            val copies = idCopies[id]
            if (copies != null) {
                for (copy in copies) {
                    val copyData = Arrays.copyOf(rowContent, rowContent.size)
                    ByteBuffer.wrap(copyData).order(ByteOrder.LITTLE_ENDIAN).putInt(idOffset, copy)
                    copyTable[copy] = copyData
                }
            }
        }

        recordMap.putAll(copyTable)
        recordList.addAll(copyTable.values)
    }

    private fun parseOffsetTable(content: LittleEndianStream, offsetMap: ArrayList<Pair<Int, Int>>, firstIndex: HashMap<Int, OffsetDuplicate>) {
        for (i in 0 until (maxId - minId + 1)) {
            val offset = content.readInt()
            val length = content.readShort().toInt()
            if (offset == 0 || length == 0) {
                continue
            }

            if (copyTableSize == 0) {
                if (!firstIndex.containsKey(offset)) {
                    firstIndex[offset] = OffsetDuplicate(offsetMap.size, firstIndex.size)
                } else {
                    offsetDuplicates[minId + i] = firstIndex[offset]!!.visible
                    continue
                }
            }

            offsetMap.add(offset to length)
        }
    }

    override fun getRowByIndex(row: Int): Db2Row {
        return Db2Row(LittleEndianStream(recordList[row]), stringBlock)
    }

    override fun getRowById(row: Int): Db2Row? {
        val data = recordMap[row] ?: return null
        return Db2Row(LittleEndianStream(data), stringBlock)
    }

    override fun getRecordCount(): Int {
        return recordCount
    }

    override fun getFieldCount(): Int {
        return if(hasIndexTable) fieldCount + 1 else fieldCount
    }
}