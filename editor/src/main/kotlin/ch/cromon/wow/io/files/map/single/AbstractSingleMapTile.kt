package ch.cromon.wow.io.files.map.single

import ch.cromon.wow.io.files.map.Tile
import ch.cromon.wow.io.files.map.wotlk.liquid.TileLiquidManager
import java.nio.FloatBuffer


abstract class AbstractSingleMapTile(override val indexX: Int, override val indexY: Int) : Tile() {
    abstract val chunks: List<AbstractSingleMapChunk>

    abstract val fullVertices: FloatBuffer
    val textures = ArrayList<String>()
    abstract val liquidData: TileLiquidManager
}