package ch.cromon.wow.io.files.map

import ch.cromon.wow.io.data.DataLoadEvent
import ch.cromon.wow.utils.BuildBeanEntry
import ch.cromon.wow.utils.CdiUtils
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.DependsOn
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
@Scope("singleton")
@DependsOn("CdiUtils")
class MapLoader {
    private val loaders = ArrayList<BuildBeanEntry<MapTileLoader>>()

    @Autowired
    private lateinit var logger: Logger

    private lateinit var loader: MapTileLoader

    @PostConstruct
    fun initialize() {
        this.loaders.addAll(CdiUtils.getAllVersionBeans(MapTileLoader::class.java))
    }

    @EventListener
    fun dataLoaded(event: DataLoadEvent) {
        if(!event.success) {
            return
        }

        logger.info("Finding tile load function for build ${event.build}")
        loadLoaderByBuild(event.build)
    }

    fun loadTile(mapName: String, indexX: Int, indexY: Int) = loader.load(mapName, indexX, indexY)

    private fun loadLoaderByBuild(build: Int) {
        val maybeLoader = loaders.firstOrNull {
            it.minBuild <= build && it.maxBuild >= build
        }

        if(maybeLoader == null) {
            logger.error("No map loader found for build: $build")
            throw IllegalArgumentException("No loader for build found")
        }

        loader = maybeLoader.bean
        logger.info("Using map tile loader: ${loader.javaClass}")
        loader.activate()
    }
}