package ch.cromon.wow.io.files.wmo


interface WmoProvider {
    fun load(file: String): AbstractMapObject?
}