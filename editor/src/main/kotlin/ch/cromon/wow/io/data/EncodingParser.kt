package ch.cromon.wow.io.data

import ch.cromon.wow.io.LittleEndianStream
import java.util.*

data class EncodingEntry(val size: Int, val key: FileKeyMD5)


class EncodingParser(content: LittleEndianStream) {
    val encodingData = TreeMap<FileKeyMD5, EncodingEntry>()

    init {
        content.position += 9
        val numEntriesA = content.readIntBE()
        content.position += 5
        val stringBlock = content.readIntBE()
        content.position += stringBlock + numEntriesA * 32

        var curPos = content.position
        for(i in 0 until numEntriesA) {
            var keyCount = content.readShort()
            while(keyCount != 0.toShort()) {
                val fileSize = content.readIntBE()
                val md5 = content.readMd5()
                if(keyCount <= 0) {
                    keyCount = content.readShort()
                    continue
                }

                val key = FileKeyMD5(content.readMd5())
                encodingData[FileKeyMD5(md5)] = EncodingEntry(fileSize, key)
                if(keyCount > 1) {
                    content.position += (keyCount - 1) * 16
                }
                keyCount = content.readShort()
            }

            content.position = curPos + 0x1000
            curPos = content.position
        }
    }
}