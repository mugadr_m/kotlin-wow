package ch.cromon.wow.io.files.wmo.wotlk

import ch.cromon.wow.math.Vector3

data class WmoRootHeader(
        val nTextures: Int,
        val nGroups: Int,
        val nPortals: Int,
        val nLights: Int,
        val nDoodadNames: Int,
        val nDoodadDefs: Int,
        val nDoodadSets: Int,
        val ambientColor: Int,
        val wmoAreaTable: Int,
        val bboxMin: Vector3,
        val bboxMax: Vector3,
        val flags: Int,
        val numLod: Int
)

data class WmoMaterial(val flags: Int, val shader: Int, val blendMode: Int, val texture: String,
                       val emissiveColor: Int, val runtimeEmissive: Int, val envNameIndex: Int,
                       val diffColor: Int, val groundType: Int, val texture2: String, val color2: Int,
                       val flags2: Int)


data class WmoGroupHeader(
        val groupName: Int,
        val longName: Int,
        val flags: Int,
        val bboxMin: Vector3,
        val bboxMax: Vector3,
        val portalStart: Int,
        val portalEnd: Int,
        val numTransparentBatches: Int,
        val numInteriorBatches: Int,
        val numExteriorBatches: Int,
        val unkBatchOrPadding: Int,
        val fogId1: Int,
        val fogId2: Int,
        val fogId3: Int,
        val fogId4: Int,
        val liquid: Int,
        val wmoAreaTable: Int,
        val flags2: Int,
        val unk2: Int
)

data class WmoBatch(
        val bboxMin: Vector3,
        val bboxMax: Vector3,
        val startIndex: Int,
        val indexCount: Int,
        val firstVertex: Int,
        val lastVertex: Int,
        val flags: Int,
        val materialId: Int
)