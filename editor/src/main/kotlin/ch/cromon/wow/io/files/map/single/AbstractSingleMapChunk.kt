package ch.cromon.wow.io.files.map.single

import ch.cromon.wow.io.files.map.Chunk
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.math.Vector3


data class MapVertex(var position: Vector3 = Vector3(), var texCoord: Vector2 = Vector2(), var texCoordAlpha: Vector2 = Vector2(), var normal: Vector3 = Vector3(), var vertexColor: Int = 0x7F7F7F7F)
data class TextureLayer(val textureId: Int, val flags: Int, val alphaOffset: Int, val groundEffect: Int)


abstract class AbstractSingleMapChunk : Chunk() {
    abstract val textureLayers: List<TextureLayer>
    abstract val alphaData: IntArray
}