package ch.cromon.wow.io.data.mpq

import ch.cromon.wow.io.LittleEndianFileStream
import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.data.FileProvider
import ch.cromon.wow.io.data.GAME_FOLDER_PROPERTY
import ch.cromon.wow.utils.I18N
import com.sun.jna.Pointer
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.regex.Pattern
import javax.xml.parsers.DocumentBuilderFactory
import kotlin.reflect.full.declaredMemberProperties

@Component
@Qualifier("MPQ")
@Scope("prototype")
class MpqFileHandler : FileProvider() {
    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var stormlibLoader: StormlibLoader

    private val archiveList = ArrayList<Pointer>()

    private lateinit var locale: String

    override fun initialize(properties: Properties): CompletableFuture<Boolean> {
        try {
            stormlibLoader.extract()
        } catch (e: Exception) {
            logger.error("Failed to load stormlib", e)
            return CompletableFuture.completedFuture(false)
        }

        val folder = properties.getProperty(GAME_FOLDER_PROPERTY)
        if (StringUtils.isBlank(folder)) {
            logger.error("Missing property $GAME_FOLDER_PROPERTY")
            return CompletableFuture.completedFuture(false)
        }

        val baseFolder = File(folder)
        if (!baseFolder.exists()) {
            logger.error("Folder not found: $folder")
            return CompletableFuture.completedFuture(false)
        }

        return CompletableFuture.supplyAsync {
            val ret = if (isAlphaBuild(baseFolder)) {
                loadAlphaFiles(baseFolder) &&
                        tryGetBuild(baseFolder)
            } else {
                loadBaseFiles(baseFolder) &&
                        loadLocalizedFiles(baseFolder) &&
                        tryGetBuild(baseFolder)
            }

            archiveList.reverse()
            ret
        }
    }

    override fun canOpenByFileDataId() = false
    override fun containsFileDataId(id: Int) = false

    override fun openFromFileDataId(fileDataId: Int): LittleEndianStream? {
        TODO("not implemented")
    }

    override fun openFile(name: String): LittleEndianStream? {
        synchronized(this) {
            archiveList.forEach {
                val ret = Stormlib.SFileOpenFileEx(it, name)
                if (ret != null) {
                    return readFileToStream(ret)
                }
            }
        }

        return null
    }

    override fun openFileAsStream(name: String): LittleEndianFileStream? {
        synchronized(this) {
            archiveList.forEach {
                val ret = Stormlib.SFileOpenFileEx(it, name)
                if (ret != null) {
                    return MpqFileStream(ret)
                }
            }
        }

        return null
    }

    override fun hasFile(name: String): Boolean {
        synchronized(this) {
            return archiveList.any {
                Stormlib.SFileHasFile(it, name)
            }
        }
    }

    private fun readFileToStream(file: Pointer): LittleEndianStream {
        val size = Stormlib.SFileGetFileSize(file).toInt()
        val buffer = ByteArray(size)
        Stormlib.SFileReadFile(file, buffer)
        Stormlib.SFileCloseFile(file)
        return LittleEndianStream(buffer)
    }

    private fun loadBaseFiles(folder: File): Boolean {
        val dataFolder = File(folder, "Data")
        if (!dataFolder.exists()) {
            return false
        }

        loadMpqsByPattern(dataFolder, Patterns.COMMON, Patterns.PATCH, Patterns.EXPANSION, Patterns.LICHKING,
                Patterns.CUSTOM_PATCH)
        return true
    }

    private fun loadLocalizedFiles(folder: File): Boolean {
        val dataFolder = File(folder, "Data")
        if (!dataFolder.exists()) {
            return false
        }

        val maybeLocale = getLocale(dataFolder)
        if (maybeLocale == null) {
            logger.error("No locale directory found")
            return false
        }

        locale = maybeLocale
        logger.info("Using locale $locale")
        val localeFolder = File(dataFolder, locale)
        val pattern = LocalePatterns(locale)
        loadMpqsByPattern(localeFolder, pattern.backup, pattern.base, pattern.speech, pattern.expansionSpeech,
                pattern.lichkingSpeech, pattern.locale, pattern.patch, pattern.expansionLocale, pattern.lichkingLocale,
                pattern.customPatches)
        return true
    }

    private fun getMpqsByPattern(folder: File, pattern: Pattern): List<File> {
        val children = folder.listFiles() ?: return emptyList()
        return children.filter { it.isFile && it.extension.toUpperCase() == "MPQ" && pattern.matcher(it.name).find() }.sortedBy { it.nameWithoutExtension }
    }

    private fun loadMpq(mpq: File) {
        handleProgress(I18N.MPQ_LOAD_ARCHIVE, mpq.name)
        val archive = Stormlib.SFileOpenArchive(mpq.absolutePath, 0, 0x100)
        logger.info("Archive ${mpq.name} was loaded ${if (archive != null) "successfully" else "unsuccessfully"}")
        if (archive == null) {
            return
        }

        archiveList.add(archive)
    }

    private fun getLocale(dataFolder: File): String? {
        return (dataFolder.listFiles() ?: emptyArray()).firstOrNull({
            if (it.isDirectory && it.name.length == 4) {
                val subFile = File(it, "locale-${it.name}.MPQ")
                subFile.exists()
            } else {
                false
            }
        })?.name
    }

    private fun loadMpqsByPattern(folder: File, vararg patterns: Pattern) {
        patterns.forEach {
            getMpqsByPattern(folder, it).forEach {
                loadMpq(it)
            }
        }
    }

    private fun tryGetBuild(basePath: File) = getBuildFromExecutable(basePath) || getBuildFromPackedExecutable() || getFromComponentInfo()

    private fun getFromComponentInfo(): Boolean {
        val stream = openFile("component.wow-$locale.txt") ?: openFile("component.wow-data.txt") ?: return false
        try {
            val xmlDocument = DocumentBuilderFactory
                    .newInstance()
                    .newDocumentBuilder()
                    .parse(stream)
            val componentNode = xmlDocument.getElementsByTagName("component")
            if (componentNode.length == 0) {
                logger.warn("No <component> element found in component info")
                return false
            }

            val component = componentNode.item(0)
            val versionAttribute = component.attributes.getNamedItem("version")
            if (versionAttribute == null) {
                logger.warn("<component> has no version attribute")
                return false
            }

            val version = versionAttribute.textContent
            if (!StringUtils.isNumeric(version)) {
                logger.warn("version attribute on <component> is not numeric: $version")
                return false
            }

            buildVersion = version.toInt()
            logger.info("Found build $buildVersion in component info")
            return true
        } catch (e: Exception) {
            logger.warn("Error parsing component info", e)
            return false
        }
    }

    private fun getBuildFromPackedExecutable(): Boolean {
        val stream = openFile("Wow.exe") ?: return false
        val ret = getVersionFromWoWStream(ByteArrayInputStream(stream.data))
        if (ret) {
            logger.info("Found build $buildVersion in Wow.exe in MPQ")
        }

        return ret
    }

    private fun getBuildFromExecutable(basePath: File): Boolean {
        val wowFile = tryGetWowFile(basePath) ?: return false
        FileInputStream(wowFile).use {
            val ret = getVersionFromWoWStream(it)
            if (ret) {
                logger.info("Found build $buildVersion in ${wowFile.absolutePath}")
            }

            return ret
        }
    }

    private fun tryGetWowFile(basePath: File): File? {
        return arrayOf("WoWClient.exe", "WowClient.exe", "WOWClient.exe", "Wow.exe", "WoW.exe", "WOW.exe", "wow.exe")
                .map {
                    File(basePath, it)
                }.firstOrNull {
                    it.exists()
                }
    }

    private fun getVersionFromWoWStream(stream: InputStream): Boolean {
        val input = IOUtils.toString(stream, StandardCharsets.ISO_8859_1)
        val searchPattern = "World of WarCraft (build "
        val index = input.indexOf(searchPattern)
        if (index < 0) {
            return getVersionFromAlphaWoW(input)
        }

        val endIndex = input.indexOf(")", index)
        val buildString = input.substring(index + searchPattern.length, endIndex)
        if (StringUtils.isNumeric(buildString)) {
            buildVersion = buildString.toInt()
            return true
        }

        return false
    }

    private fun getVersionFromAlphaWoW(content: String): Boolean {
        val pattern = "SET_GLUE_SCREEN"
        val index = content.indexOf(pattern)
        if(index < 0) {
            return false
        }

        val endIndex = content.indexOf('\u0000', index + pattern.length + 1)
        if(endIndex < 0) {
            return false
        }

        val buildString = content.substring(index + pattern.length + 1, endIndex)
        if(StringUtils.isNumeric(buildString)) {
            buildVersion = buildString.toInt()
            return true
        }

        return false
    }

    private fun isAlphaBuild(base: File): Boolean {
        val dataFolder = File(base, "Data")
        val childFiles = dataFolder.listFiles() ?: return false
        val patternClass = AlphaPatterns::class
        val matchCount = patternClass.declaredMemberProperties
                .mapNotNull {
                    it.get(AlphaPatterns) as? Pattern
                }.count {
                    val pattern = it
                    childFiles.any {
                        pattern.matcher(it.name).find()
                    }
                }
        return matchCount > 4
    }

    private fun loadAlphaFiles(base: File): Boolean {
        val dataFolder = File(base, "Data")
        FileUtils.iterateFiles(dataFolder, arrayOf("mpq", "mPq", "mpQ", "mPQ", "Mpq", "MPq", "MpQ", "MPQ"), true).forEach {
            loadMpq(it.absoluteFile)
        }
        return true
    }
}