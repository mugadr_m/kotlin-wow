package ch.cromon.wow.io.files.map.wotlk.liquid

import ch.cromon.wow.io.LittleEndianStream


data class LiquidChunkHeader(val offsetInstances: Int, val numLayers: Int, val offsetAttributes: Int)

data class LiquidChunkInstance(
        val liquidType: Short,
        val vertexFormat: Short,
        val minHeight: Float,
        val maxHeight: Float,
        val offsetX: Byte,
        val offsetY: Byte,
        val width: Byte,
        val height: Byte,
        val offsetBitmap: Int,
        val offsetVertices: Int
) {
    companion object {
        fun read(stream: LittleEndianStream): LiquidChunkInstance {
            return LiquidChunkInstance(
                    stream.readShort(),
                    stream.readShort(),
                    stream.readFloat(),
                    stream.readFloat(),
                    stream.readByte(),
                    stream.readByte(),
                    stream.readByte(),
                    stream.readByte(),
                    stream.readInt(),
                    stream.readInt()
            )
        }
    }
}

class LiquidBitmap(private val bytes: ByteArray) {
    companion object {
        private val ALL_TRUE_DATA = (0 until 8).map{ 255.toByte() }.toByteArray()
    }

    constructor() : this(ALL_TRUE_DATA)

    fun isSet(x: Int, y: Int, w: Int, h: Int): Boolean {
        if(x < 0 || x >= w || y < 0 || y >= h) {
            return false
        }

        val offset = y * w + x
        val byteOffset = offset / 8
        val bitOffset = offset % 8
        return (bytes[byteOffset].toInt() and (1 shl bitOffset)) != 0
    }
}