package ch.cromon.wow.memory

import org.lwjgl.BufferUtils
import java.nio.FloatBuffer
import java.util.*


object MapLowVertexBufferCache {
    private val data = LinkedList<FloatBuffer>()

    fun get(): FloatBuffer = data.poll() ?: BufferUtils.createFloatBuffer((17 * 17 + 16 * 16) * 3)

    fun release(buffer: FloatBuffer) {
        data.add(buffer)
    }
}