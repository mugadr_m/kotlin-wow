package ch.cromon.wow.memory

import ch.cromon.wow.io.files.wmo.MapObjVertex
import java.util.*


object MapObjectDataCache {
    private val vertexCache = LinkedList<Collection<MapObjVertex>>()
    private val indexCache = LinkedList<Collection<Short>>()

    fun freeIndices(indices: ArrayList<Short>) {
        synchronized(indexCache) {
            indexCache.push(indices)
        }
    }

    fun getIndices(count: Int): ArrayList<Short> {
        val ret = ArrayList<Short>()
        if(indexCache.size < count) {
            for(i in 0 until count) {
                ret.add(0)
            }

            return ret
        }

        synchronized(indexCache) {
            while(ret.size < count && indexCache.isNotEmpty()) {
                val part = indexCache.pop()
                val remaining = count - ret.size
                if(part.size <= remaining) {
                    ret.addAll(part)
                    continue
                }

                ret.addAll(part.take(remaining))
                indexCache.push(part.drop(remaining))
                return ret
            }
        }

        if(ret.size < count) {
            val remaining = count - ret.size
            for(i in 0 until remaining) {
                ret.add(0)
            }
        }

        return ret
    }

    fun freeVertices(vertices: ArrayList<MapObjVertex>) {
        synchronized(vertexCache) {
            vertexCache.push(vertices)
        }
    }

    fun getVertices(count: Int): ArrayList<MapObjVertex> {
        val ret = ArrayList<MapObjVertex>()
        if(vertexCache.size < count) {
            for(i in 0 until count) {
                ret.add(MapObjVertex())
            }

            return ret
        }

        synchronized(vertexCache) {
            while(ret.size < count && vertexCache.isNotEmpty()) {
                val part = vertexCache.pop()
                val remaining = count - ret.size
                if(part.size <= remaining) {
                    ret.addAll(part)
                    continue
                }

                ret.addAll(part.take(remaining))
                vertexCache.push(part.drop(remaining))
                return ret
            }
        }

        if(ret.size < count) {
            val remaining = count - ret.size
            for(i in 0 until remaining) {
                ret.add(MapObjVertex())
            }
        }

        return ret
    }
}