package ch.cromon.wow.memory

import ch.cromon.wow.io.files.map.single.MapVertex
import org.lwjgl.BufferUtils
import java.nio.FloatBuffer
import java.util.*


object MapVertexBufferCache {
    private val mapVertexBufferCache = LinkedList<FloatBuffer>()

    fun getBuffer(): FloatBuffer {
        return mapVertexBufferCache.poll() ?: BufferUtils.createFloatBuffer(256 * 145 * 11)
    }

    fun release(buffer: FloatBuffer) {
        mapVertexBufferCache.add(buffer)
    }
}

object HeightDataCache {
    private val heightDataCache = LinkedList<FloatArray>()

    fun getBuffer(): FloatArray{
        return heightDataCache.poll() ?: FloatArray(145)
    }

    fun release(buffer: FloatArray) {
        heightDataCache.add(buffer)
    }
}

object MapVertexCache {
    private val mapVertexCache = LinkedList<Array<MapVertex>>()

    fun getVertices(): Array<MapVertex> {
        return mapVertexCache.poll() ?: Array(145, { MapVertex() })
    }

    fun release(data: Array<MapVertex>) {
        mapVertexCache.add(data)
    }
}

object MapAlphaCache {
    private val alphaDataCache = LinkedList<IntArray>()

    fun getData(): IntArray {
        return alphaDataCache.poll() ?: IntArray(4096)
    }

    fun release(data: IntArray) {
        alphaDataCache.add(data)
    }
}