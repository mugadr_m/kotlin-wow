package ch.cromon.wow.ui.controller

import ch.cromon.wow.AppStateManager
import ch.cromon.wow.ApplicationState
import ch.cromon.wow.io.data.DataLoadEvent
import ch.cromon.wow.io.data.DataManager
import ch.cromon.wow.io.files.storage.DataStorage
import ch.cromon.wow.utils.LAST_LOCAL_DIRECTORY
import ch.cromon.wow.utils.Settings
import javafx.stage.DirectoryChooser
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.io.File


@Component
@Scope("singleton")
class DataController {
    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var stateManager: AppStateManager

    @Autowired
    private lateinit var dataManager: DataManager

    @Autowired
    private lateinit var dataStorage: DataStorage

    @Autowired
    private lateinit var mapController: MapController

    @Autowired
    private lateinit var settings: Settings

    @EventListener
    fun onDataLoad(event: DataLoadEvent) {
        if(!event.success) {
            stateManager.currentState = ApplicationState.DATA_INIT
            return
        }

        try {
            dataStorage.loadStorageFiles()
        } catch(e: Exception) {
            logger.error("Error initializing data", e)
            stateManager.currentState = ApplicationState.DATA_INIT
            return
        }

        mapController.onInitMapButtons()
        stateManager.currentState = ApplicationState.MAP_SELECT
    }

    fun onSelectRemote(server: String) {
        stateManager.currentState = ApplicationState.DATA_LOAD
        dataManager.initFromRemote(server)
    }

    fun onSelectCDN() {
        stateManager.currentState = ApplicationState.DATA_LOAD
        dataManager.initFromCdn(determineRegion())
    }

    fun onSelectLocalFolder() {
        val lastDir = settings[LAST_LOCAL_DIRECTORY]
        val chooser = DirectoryChooser()
        if(lastDir.isNotBlank()) {
            chooser.initialDirectory = File(lastDir)
        }
        val file = chooser.showDialog(null)
        if(file == null || !file.exists()) {
            return
        }

        settings[LAST_LOCAL_DIRECTORY] = file.absolutePath
        stateManager.currentState = ApplicationState.DATA_LOAD
        logger.info("Loading from local folder: ${file.absolutePath}")
        dataManager.initFromLocal(file.absolutePath)
    }

    private fun determineRegion(): String {
        return "us"
    }
}