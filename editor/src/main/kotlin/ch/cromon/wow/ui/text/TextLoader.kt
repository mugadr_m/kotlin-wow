package ch.cromon.wow.ui.text

import ch.cromon.wow.math.Vector2
import java.awt.Color
import java.awt.Font
import java.awt.RenderingHints
import java.awt.font.LineBreakMeasurer
import java.awt.font.TextAttribute
import java.awt.image.BufferedImage
import java.text.AttributedString
import javax.swing.JLabel

class LoadResult(val bufferedImage: BufferedImage, val bounds: Vector2)

class TextLoader(private val font: Font) {
    companion object {
        private val TRANSPARENT = Color(0, true)
    }
    private val label = JLabel()
    private val fontMetrics = label.getFontMetrics(font)
    private val graphics = BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB).createGraphics()
    private var currentImage = BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)
    private var currentGraphics = currentImage.createGraphics()

    fun drawText(text: String): LoadResult {
        val metrics = fontMetrics.getLineMetrics(text, graphics)
        val bounds = fontMetrics.getStringBounds(text, graphics)

        val width = Math.max(1, bounds.width.toInt())
        val height = Math.max(1, bounds.height.toInt())
        if(currentImage.width < width || currentImage.height < height) {
            currentImage = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
            currentGraphics = currentImage.createGraphics()
        }

        val g = currentGraphics
        g.color = TRANSPARENT
        g.background = TRANSPARENT
        g.clearRect(0, 0, currentImage.width, currentImage.height)
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)
        g.font = font
        g.color = Color.WHITE
        g.drawString(text, 0, metrics.ascent.toInt())
        return LoadResult(currentImage, Vector2(width, height))
    }

    fun drawTextMultiline(text: String, width: Int): LoadResult {
        val actualWidth = Math.max(1, width)
        val attributedString = AttributedString(text)
        attributedString.addAttribute(TextAttribute.FONT, font)
        val iterator = attributedString.iterator
        val start = iterator.beginIndex
        val end = iterator.endIndex

        val measurer = LineBreakMeasurer(iterator, graphics.fontRenderContext)
        measurer.position = start
        var sizeY = 0.0f
        while(measurer.position < end) {
            val layout = measurer.nextLayout(actualWidth.toFloat())
            sizeY += layout.ascent + layout.descent + layout.leading
        }

        val img = BufferedImage(actualWidth, Math.max(sizeY.toInt(), 1), BufferedImage.TYPE_INT_ARGB)
        measurer.position = start

        val g = img.createGraphics()
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)
        g.font = font
        g.color = Color.WHITE
        val x = 0.0f
        var y = 0.0f

        while(measurer.position < end) {
            val layout = measurer.nextLayout(actualWidth.toFloat())
            y += layout.ascent
            val dx = if(layout.isLeftToRight) 0.0f else actualWidth - layout.advance
            layout.draw(g, x + dx, y)
            y += layout.descent + layout.leading
        }

        return LoadResult(img, Vector2(img.width, img.height))
    }
}