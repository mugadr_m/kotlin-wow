package ch.cromon.wow.ui.utils

import ch.cromon.wow.math.Matrix4
import org.slf4j.LoggerFactory
import java.util.*


class TransformStack(private val callback: (Matrix4) -> Unit) {
    companion object {
        private val LOG = LoggerFactory.getLogger(TransformStack::class.java)
    }

    private val matrixStack = Stack<Matrix4>()

    val currentMatrix: Matrix4
        get() = matrixStack.peek()

    init {
        matrixStack.push(Matrix4())
    }

    fun popMatrix() {
        if(matrixStack.size <= 1) {
            LOG.error("Attempted to pop from TransformStack with only the identity matrix left")
            throw IllegalStateException("Attempted to pop from empty matrix stack")
        }

        matrixStack.pop()
        callback(matrixStack.peek())
    }

    fun pushMatrix(matrix: Matrix4) {
        val top = matrixStack.peek()
        val newMat = Matrix4()
        top.times(matrix, newMat)
        matrixStack.push(newMat)
        callback(newMat)
    }
}