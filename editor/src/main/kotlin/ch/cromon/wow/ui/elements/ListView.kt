package ch.cromon.wow.ui.elements

import ch.cromon.wow.gx.BlendMode
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.messaging.MouseClickMessage
import ch.cromon.wow.ui.messaging.MouseMoveMessage
import ch.cromon.wow.ui.messaging.WindowMessage
import ch.cromon.wow.ui.utils.Alignment
import ch.cromon.wow.utils.pointInQuad
import java.awt.Color

private class ListViewItem<out T>(val item: T, val element: Element, var isHovered: Boolean = false)

class ListView<T> : ItemsElement() {
    private lateinit var hoverBackground: Quad
    private lateinit var background: Quad
    private lateinit var border: Border
    private val itemList = ArrayList<ListViewItem<T>>()
    private var hoveredItem: ListViewItem<T>? = null
    private var clickedItem: ListViewItem<T>? = null

    private var visibleItemsStart = 0
    private var visibleItemsEnd = 0
    private var scrollOffset = 0.0f

    private var clipStart = Vector2()
    private var clipEnd = Vector2()

    var itemClicked: (T) -> Unit = { }

    var itemTemplate: (T) -> Element = {
        Label().run {
            text = it.toString()
            fontSize = 16
            this
        }
    }

    fun addItem(item: T) {
        val element = itemTemplate(item)
        appendChild(element)
        element.run {
            verticalAlignment = Alignment.START
            horizontalAlignment = Alignment.STRETCH
            margin = Margin(5.0f, 0.0f, 0.0f, 0.0f)
            pack()
        }
        itemList.add(ListViewItem(item, element))
        onItemsChanged()
    }

    override fun doMessage(message: WindowMessage) {
        when(message) {
            is MouseMoveMessage -> onMouseMove(message)
            is MouseClickMessage -> onMouseClick(message)
        }
    }

    override fun onInitialize() {
        super.onInitialize()

        background = Quad()
        hoverBackground = Quad()
        border = Border()

        border.color = Color.WHITE
        background.blendMode = BlendMode.ALPHA
        background.color = Color(0xDD222222.toInt(), true)

        hoverBackground.blendMode = BlendMode.ALPHA
        hoverBackground.color = Color(0xDD666666.toInt(), true)
    }

    override fun onFrame(context: UiContext) {
        background.onFrame()
        border.onFrame()

        context.clipStack.push(clipStart, clipEnd)

        val hoverItem = hoveredItem
        if(hoverItem != null) {
            hoverBackground.position = Vector2(hoverItem.element.calculatedPosition.x + margin.left, hoverItem.element.calculatedPosition.y + margin.top)
            hoverBackground.size = hoverItem.element.calculatedSize
            hoverBackground.onFrame()
        }

        super.onFrame(context)

        context.clipStack.pop()
    }

    override fun positionChanged() {
        super.positionChanged()
        positionSizeChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        positionSizeChanged()
    }

    private fun positionSizeChanged() {
        background.position = calculatedPosition
        background.size = calculatedSize

        border.topLeft = calculatedPosition
        border.bottomRight = calculatedPosition + calculatedSize

        clipStart = calculatedPosition + Vector2(5, 5)
        clipEnd = calculatedPosition + calculatedSize - Vector2(5, 5)
    }

    private fun onItemsChanged() {
        var index = 0
        var currentPos = -scrollOffset
        visibleItemsStart = 0
        visibleItemsEnd = 0

        var hasStart = false
        itemList.forEach {
            val size = it.element.calculatedSize.y
            it.element.position = Vector2(0.0f, currentPos + 5)
            if (currentPos + size >= -25 && !hasStart) {
                visibleItemsStart = index
                hasStart = true
            }

            if (currentPos < calculatedSize.y + 10) {
                visibleItemsEnd = index
            }

            ++index
            currentPos += size
        }
    }

    private fun onMouseClick(message: MouseClickMessage) {
        when(message.pressed) {
            true -> onMouseDown(message)
            false -> onMouseUp(message)
        }
    }

    private fun onMouseDown(message: MouseClickMessage) {
        if(message.isHandled) {
            clickedItem = null
            return
        }

        clickedItem = hoveredItem
    }

    private fun onMouseUp(message: MouseClickMessage) {
        if(message.isHandled) {
            clickedItem = null
            return
        }

        if(hoveredItem == clickedItem) {
            val item = clickedItem
            if(item != null) {
                itemClicked(item.item)
            }
        }

        clickedItem = null
    }

    private fun onMouseMove(message: MouseMoveMessage) {
        if(message.isHandled) {
            return
        }

        val actualPosition = Vector2(message.position.x - margin.left, message.position.y - margin.top)

        var hasHover = false
        val hoverItem = hoveredItem
        for(item in itemList) {
            val isHovered = pointInQuad(actualPosition, item.element.calculatedPosition, item.element.calculatedPosition + item.element.calculatedSize)
            if(isHovered) {
                if(hoverItem != null && hoverItem != item) {
                    hoverItem.isHovered = false
                }

                hoveredItem = item
                item.isHovered = true
                hasHover = true
                break
            }
        }

        if(!hasHover && hoverItem != null) {
            hoverItem.isHovered = false
            hoveredItem = null
        }
    }
}