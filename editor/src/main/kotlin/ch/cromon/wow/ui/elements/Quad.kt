package ch.cromon.wow.ui.elements

import ch.cromon.wow.gx.*
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.math.Vector4
import ch.cromon.wow.utils.toVector4
import org.lwjgl.BufferUtils
import java.awt.Color
import kotlin.properties.Delegates

class Quad {
    private var matTransform = Matrix4()
    private var colorTransformed = Vector4(1, 1, 1, 1)

    var position: Vector2 by Delegates.observable(Vector2(), { _, _, _ -> updateSizePosition() })
    var size: Vector2 by Delegates.observable(Vector2(), { _, _, _ -> updateSizePosition() })
    var color: Color by Delegates.observable(Color.WHITE, { _, _, _ -> colorTransformed = color.toVector4() })

    var blendMode = BlendMode.NONE

    fun onFrame() {
        mesh.program.setMatrix(uniformMatTransform, matTransform)
        mesh.program.setVec4(uniformColor, colorTransformed)
        mesh.blendMode = blendMode
        mesh.render()
    }

    fun updateSizePosition() {
        Matrix4.translation(matTransform, position.x, position.y, 0.0f)
        matTransform.scaleInplace(size.x, size.y, 1.0f)
    }

    companion object {
        private lateinit var mesh: Mesh
        private var uniformMatTransform = -1
        private var uniformGlobalTransform = -1
        private var uniformMatProjection = -1
        private var uniformColor = -1

        fun updateProjection(matrix: Matrix4) {
            mesh.program.setMatrix(uniformMatProjection, matrix)
        }

        fun updateGlobalTransform(matrix: Matrix4) {
            mesh.program.setMatrix(uniformGlobalTransform, matrix)
        }

        fun initialize() {
            mesh = Mesh()
            val program = Program()
            program.compileVertexShaderFromResource(Shader.QUAD.vertexShader)
            program.compileFragmentShaderFromResource(Shader.QUAD.fragmentShader)
            program.link()

            uniformMatProjection = program.getUniform("matProjection")
            uniformMatTransform = program.getUniform("matTransform")
            uniformGlobalTransform = program.getUniform("globalTransform")
            uniformColor = program.getUniform("quadColor")

            mesh.program = program
            mesh.blendMode = BlendMode.NONE

            mesh.addElement(VertexElement("position", 0, 2))

            mesh.finalize()

            val vb = VertexBuffer()
            val vbData = BufferUtils.createFloatBuffer(4 * 2)
            vbData.put(0.0f).put(1.0f)
            vbData.put(1.0f).put(1.0f)
            vbData.put(1.0f).put(0.0f)
            vbData.put(0.0f).put(0.0f)
            vbData.flip()
            vb.data(vbData)

            val ib = IndexBuffer(IndexType.UINT8)
            val ibData = BufferUtils.createByteBuffer(6)
            ibData.put(0).put(1).put(2)
            ibData.put(0).put(2).put(3)
            ibData.flip()
            ib.data(ibData)

            mesh.vertexBuffer = vb
            mesh.indexBuffer = ib
            mesh.indexCount = 6
            mesh.depthCheck = DepthCheck.DISABLED
        }
    }
}