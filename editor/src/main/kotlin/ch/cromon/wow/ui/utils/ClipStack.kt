package ch.cromon.wow.ui.utils

import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector2
import org.lwjgl.opengl.GL11.*
import org.slf4j.LoggerFactory
import java.util.*

private class StackEntry(val topLeft: Vector2, val bottomRight: Vector2)

class ClipStack {
    companion object {
        private val LOG = LoggerFactory.getLogger(ClipStack::class.java)
    }

    var windowHeight = 0

    private val clipStack = Stack<StackEntry>()

    var transformStack : TransformStack? = null

    fun pop() {
        if(clipStack.empty()) {
            LOG.error("Attempted to pop an entry from an empty ClipStack")
            throw IllegalStateException("Attempted to pop from an empty stack")
        }

        clipStack.pop()
        if(clipStack.empty()) {
            glDisable(GL_SCISSOR_TEST)
        } else {
            val top = clipStack.peek()
            updateScissorRect(top.topLeft, top.bottomRight)
        }
    }

    fun push(topLeft: Vector2, bottomRight: Vector2, ignoreTransform: Boolean = false) {
        val transform = transformStack?.currentMatrix?.transposed() ?: Matrix4()
        val ttl = if(ignoreTransform) topLeft else transform * topLeft
        val tbr = if(ignoreTransform) bottomRight else transform * bottomRight

        if(clipStack.empty()) {
            pushEntry(ttl, tbr)
        } else {
            val curTop = clipStack.peek()
            val tl = curTop.topLeft
            val br = curTop.bottomRight
            val startX = maxOf(tl.x, ttl.x)
            val startY = maxOf(tl.y, ttl.y)
            val endX = minOf(br.x, tbr.x)
            val endY = minOf(br.y, tbr.y)
            pushEntry(Vector2(startX, startY), Vector2(endX, endY))
        }
    }

    private fun pushEntry(topLeft: Vector2, bottomRight: Vector2) {
        val entry = StackEntry(topLeft, bottomRight)
        if(clipStack.empty()) {
            glEnable(GL_SCISSOR_TEST)
        }

        clipStack.push(entry)
        updateScissorRect(topLeft, bottomRight)
    }

    private fun updateScissorRect(topLeft: Vector2, bottomRight: Vector2) {
        val w = (bottomRight.x - topLeft.x).toInt()
        val h = (bottomRight.y - topLeft.y).toInt()

        glScissor(topLeft.x.toInt(), (windowHeight - topLeft.y - h).toInt(), w, h)
    }
}