package ch.cromon.wow.ui.views

import ch.cromon.wow.AppStateChangedEvent
import ch.cromon.wow.AppStateManager
import ch.cromon.wow.ApplicationState
import ch.cromon.wow.ui.elements.ItemsElement
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct


@Component
@Scope("singleton")
class ViewManager {
    private var uiRoot: ItemsElement? = null

    @Autowired
    private lateinit var appStateManager: AppStateManager

    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var context: ApplicationContext

    private var activeView: View? = null

    private val viewMap = HashMap<ApplicationState, View>()

    @PostConstruct
    fun initialize() {
        val viewBeans = context.getBeansWithAnnotation(AppStateView::class.java)
        for ((_, view) in viewBeans) {
            if (view !is View) {
                logger.warn("Ignoring bean with @AppStateView that is not of type View: ${view.javaClass}")
                continue
            }

            val appStateAnnotation = view.javaClass.getAnnotation(AppStateView::class.java)
            if (appStateAnnotation == null) {
                logger.warn("Inconsistent reflection information for class: ${view.javaClass}. Unable to get @AppStateView annotation")
                continue
            }

            val state = appStateAnnotation.state
            if (viewMap.containsKey(state)) {
                logger.warn("Multiple beans with the same @AppStateView state have been found: ${view.javaClass}, ${viewMap[state]!!.javaClass}")
                continue
            }

            viewMap[state] = view
        }
    }

    fun attach(root: ItemsElement) {
        uiRoot = root
        viewMap.forEach { _, value ->
            value.onInitialize()
        }
        switchToState(appStateManager.currentState)
    }

    @EventListener
    fun onStateChanged(event: AppStateChangedEvent) {
        switchToState(event.newState)
    }

    private fun switchToState(state: ApplicationState) {
        val root = uiRoot ?: return

        activeView?.let {
            root.removeChild(it)
        }

        activeView = viewMap[state]

        activeView?.let {
            root.appendChild(it)
        }
    }
}