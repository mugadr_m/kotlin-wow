package ch.cromon.wow.ui.text

import ch.cromon.wow.gx.*
import ch.cromon.wow.io.ImageSource
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.utils.toVector4
import org.lwjgl.BufferUtils
import java.awt.Color
import java.awt.Font
import kotlin.properties.Delegates


class StaticText(font: Font) {
    private val loader = TextLoader(font)
    var isMultiLine by Delegates.observable(false, { _, _, _ -> onPropertyUpdated() })
    var text by Delegates.observable(null as String?, { _, _, _ -> onPropertyUpdated() })
    private var isEmpty = true
    var maxWidth by Delegates.observable(-1.0f, { _, _, _ -> onPropertyUpdated() })
    var textWidth = 0.0f
    var textHeight = 0.0f
    private val texture = Texture()
    private var matTransform = Matrix4()
    private var matTranslate = Matrix4()
    private var textColor = Color.WHITE.toVector4()
    private var awtColor = Color.WHITE
    var position: Vector2 by Delegates.observable(Vector2(), { _, _, _ -> Matrix4.translation(matTranslate, position.x, position.y, 0.0f) })
    private var lastLoadResult: LoadResult? = null
    private var dimension = Vector2(1, 1)
    private var imageFactors = Vector2(1, 1)

    var color: Color
        get() = awtColor
        set(value) {
            textColor = value.toVector4()
            awtColor = value
        }

    fun onFrame() {
        if (isEmpty) {
            return
        }

        lastLoadResult?.let {
            texture.loadArgb(ImageSource.fromBufferedImage(it.bufferedImage))
            lastLoadResult = null
        }

        mesh.program.setMatrix(uniformMatTranslate, matTranslate)
        mesh.program.setMatrix(uniformMatTransform, matTransform)
        mesh.textureInput.add(uniformTextTexture, texture)
        mesh.program.setVec4(uniformColor, textColor)
        mesh.program.setVec2(uniformDimension, dimension)
        mesh.program.setVec2(uniformFactors, imageFactors)
        mesh.render()
    }

    private fun onPropertyUpdated() {
        val actualText = text ?: ""
        if (actualText.isBlank()) {
            textWidth = 0.0f
            isEmpty = true
            return
        }

        isEmpty = false

        val result =
                if (!isMultiLine || maxWidth < 0)
                    loader.drawText(actualText)
                else
                    loader.drawTextMultiline(actualText, maxWidth.toInt())

        lastLoadResult = result
        Matrix4.scale(matTransform, result.bounds.x, result.bounds.y, 1.0f)
        textWidth = result.bounds.x
        textHeight = result.bounds.y
        dimension.x = textWidth
        dimension.y = textHeight
        imageFactors.x = result.bounds.x / result.bufferedImage.width
        imageFactors.y = result.bounds.y / result.bufferedImage.height
    }

    companion object {
        private lateinit var mesh: Mesh
        private var uniformMatTranslate = -1
        private var uniformMatTransform = -1
        private var uniformProjection = -1
        private var uniformTextTexture = -1
        private var uniformColor = -1
        private var uniformGlobalTransform = -1
        private var uniformDimension = -1
        private var uniformFactors = -1

        fun updateProjection(matrix: Matrix4) {
            mesh.program.setMatrix(uniformProjection, matrix)
        }

        fun updateGlobalTransform(matrix: Matrix4) {
            mesh.program.setMatrix(uniformGlobalTransform, matrix)
        }

        fun initialize() {
            mesh = Mesh()
            mesh.addElement(VertexElement("position", 0, 2))
            mesh.addElement(VertexElement("texCoord", 0, 2))

            val program = Program()
            program.compileFragmentShaderFromResource(Shader.TEXT.fragmentShader)
            program.compileVertexShaderFromResource(Shader.TEXT.vertexShader)
            program.link()

            uniformMatTranslate = program.getUniform("matTranslate")
            uniformMatTransform = program.getUniform("matTransform")
            uniformProjection = program.getUniform("matProjection")
            uniformTextTexture = program.getUniform("textTexture")
            uniformColor = program.getUniform("color")
            uniformGlobalTransform = program.getUniform("globalTransform")
            uniformDimension = program.getUniform("dimension")
            uniformFactors = program.getUniform("imageFactors")

            mesh.program = program
            mesh.blendMode = BlendMode.ALPHA
            mesh.depthCheck = DepthCheck.DISABLED

            val vb = VertexBuffer()
            val vbData = BufferUtils.createFloatBuffer(4 * 4)
            vbData.put(0.0f).put(1.0f).put(0.0f).put(1.0f)
            vbData.put(1.0f).put(1.0f).put(1.0f).put(1.0f)
            vbData.put(1.0f).put(0.0f).put(1.0f).put(0.0f)
            vbData.put(0.0f).put(0.0f).put(0.0f).put(0.0f)
            vbData.flip()
            vb.data(vbData)

            val ib = IndexBuffer(IndexType.UINT8)
            val ibData = BufferUtils.createByteBuffer(6)
            ibData.put(0).put(1).put(2)
            ibData.put(0).put(2).put(3)
            ibData.flip()
            ib.data(ibData)

            mesh.vertexBuffer = vb
            mesh.indexBuffer = ib
            mesh.indexCount = 6

            mesh.finalize()
        }
    }
}