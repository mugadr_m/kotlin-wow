package ch.cromon.wow.ui.views.init.elements

import ch.cromon.wow.io.AsyncImageLoader
import ch.cromon.wow.io.ImageSource
import ch.cromon.wow.io.data.remote.ServerDefinition
import ch.cromon.wow.io.data.remote.ServerType
import ch.cromon.wow.io.data.remote.ServerType.*
import ch.cromon.wow.io.data.remote.Servers
import ch.cromon.wow.ui.controller.DataController
import ch.cromon.wow.utils.REMOTE_SERVER_CACHE
import ch.cromon.wow.utils.Settings
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct


@Component
@Scope("singleton")
class ServerBrowserController {
    companion object {
        private val mapper = jacksonObjectMapper()
    }

    private lateinit var control: ServerBrowserElement

    @Autowired
    private lateinit var imageLoader: AsyncImageLoader

    @Autowired
    private lateinit var settings: Settings

    @Autowired
    private lateinit var dataController: DataController

    private lateinit var imageClassic: ImageSource
    private lateinit var imageBC: ImageSource
    private lateinit var imageWotlk: ImageSource
    private lateinit var imageCata: ImageSource
    private lateinit var imageMists: ImageSource
    private lateinit var imageWarlords: ImageSource
    private lateinit var imageLegion: ImageSource
    private lateinit var imageBfa: ImageSource

    private lateinit var imageUnknown: ImageSource

    @PostConstruct
    fun initialize() {
        imageLoader.loadFromImageResource("Classic-Logo-Small.png").thenAccept { imageClassic = it }
        imageLoader.loadFromImageResource("BC-Logo-Small.png").thenAccept { imageBC = it }
        imageLoader.loadFromImageResource("Wrath-Logo-Small.png").thenAccept { imageWotlk = it }
        imageLoader.loadFromImageResource("Cata-Logo-Small.png").thenAccept { imageCata = it }
        imageLoader.loadFromImageResource("Mists-Logo-Small.png").thenAccept { imageMists = it }
        imageLoader.loadFromImageResource("Wod-Logo-Small.png").thenAccept { imageWarlords = it }
        imageLoader.loadFromImageResource("Legion-Logo-Small.png").thenAccept { imageLegion = it }
        imageLoader.loadFromImageResource("Bfa-Logo-Small.png").thenAccept { imageBfa = it }
        imageLoader.loadFromImageResource("Unknown-Logo-Small.png").thenAccept { imageUnknown = it }
    }

    fun openFromServer(server: ServerDefinition) {
        dataController.onSelectRemote(server.url.toString())
    }

    fun loadServers(): Servers {
        val cache = settings[REMOTE_SERVER_CACHE, "[]"]
        return mapper.readValue(cache)
    }

    fun attach(element: ServerBrowserElement) {
        control = element
    }

    fun getImage(type: ServerType): ImageSource {
        return when(type) {
            CLASSIC -> imageClassic
            BC -> imageBC
            WOTLK -> imageWotlk
            CATA -> imageCata
            MOP -> imageMists
            WOD -> imageWarlords
            LEGION -> imageLegion
            BFA -> imageBfa
        }
    }
}