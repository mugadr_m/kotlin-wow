package ch.cromon.wow.ui

import ch.cromon.wow.ui.elements.Element
import ch.cromon.wow.ui.utils.ClipStack
import ch.cromon.wow.ui.utils.TransformStack


class UiContext(val clipStack: ClipStack, val transformStack: TransformStack) {
    init {
        clipStack.transformStack = transformStack
    }

    var inputFocus: Element? = null
}