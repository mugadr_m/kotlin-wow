package ch.cromon.wow.ui.views.init

import ch.cromon.wow.ApplicationState
import ch.cromon.wow.ui.views.AppStateView
import ch.cromon.wow.ui.views.View
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component
@Scope("singleton")
@AppStateView(ApplicationState.EDITOR_MAIN)
class EditorMain : View()