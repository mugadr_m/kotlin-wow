package ch.cromon.wow.ui.elements

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.messaging.MouseButton
import ch.cromon.wow.ui.messaging.MouseClickMessage
import ch.cromon.wow.ui.messaging.MouseMoveMessage
import ch.cromon.wow.ui.messaging.WindowMessage
import ch.cromon.wow.ui.text.StaticText
import ch.cromon.wow.utils.pointInQuad
import java.awt.Color
import java.awt.Font
import kotlin.properties.Delegates


class CheckBox : Element() {
    companion object {
        private val BOX_SIZE = Vector2(30, 30)
    }

    private val checkBorder = Border()
    private val checkText = StaticText(Font("Arial", Font.BOLD, 35))
    private val captionRender = StaticText(Font("Arial", Font.BOLD, 30))
    private var isHovered = false
    private var isClicked = false

    private var caption by Delegates.observable("", { _, _, _ -> onTextChanged() })

    private var isChecked = false

    init {
        checkBorder.thickness = 3
        checkBorder.color = Color.WHITE
        checkText.text = "×"
    }

    override fun doMessage(message: WindowMessage) {
        if(message is MouseMoveMessage) {
            handleMouseMove(message)
        } else if(message is MouseClickMessage) {
            handleMouseClick(message)
        }
    }

    private fun handleMouseClick(message: MouseClickMessage) {
        if(isHovered) {
            message.isHandled = true
        }

        if(message.button != MouseButton.LEFT) {
            return
        }

        if(!message.pressed) {
            if(isHovered && isClicked) {
                isChecked = !isChecked
            }

            isClicked = false
        } else {
            if(isHovered) {
                isClicked = true
            }
        }
    }

    private fun handleMouseMove(message: MouseMoveMessage) {
        if(!message.isHandled) {
            val isHovered = pointInQuad(message.position, calculatedPosition, calculatedPosition + BOX_SIZE)
            if(isHovered && !this.isHovered) {
                this.isHovered = true
                checkText.color = Color.ORANGE
                checkBorder.color = Color.ORANGE
            } else if(!isHovered && this.isHovered) {
                this.isHovered = false
                checkText.color = Color.WHITE
                checkBorder.color = Color.WHITE
            }

            if(isHovered) {
                message.isHandled = true
            }
        }
    }

    override fun onFrame(context: UiContext) {
        checkBorder.onFrame()
        if(isChecked) {
            checkText.onFrame()
        }
        captionRender.onFrame()
    }

    override fun positionChanged() {
        super.positionChanged()
        
        checkBorder.topLeft = calculatedPosition
        checkBorder.bottomRight = calculatedPosition + BOX_SIZE
        checkText.position = calculatedPosition + Vector2(5, -5)
        val textHeight = captionRender.textHeight
        captionRender.position = calculatedPosition + Vector2(BOX_SIZE.x + 5, (BOX_SIZE.y - textHeight) / 2)
    }

    private fun onTextChanged() {
        captionRender.text = caption
        val textHeight = captionRender.textHeight
        captionRender.position = calculatedPosition + Vector2(BOX_SIZE.x + 5, (BOX_SIZE.y - textHeight) / 2)
    }

}