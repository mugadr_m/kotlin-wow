package ch.cromon.wow.ui.elements

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.messaging.WindowMessage
import ch.cromon.wow.ui.utils.Alignment
import ch.cromon.wow.ui.utils.AlignmentManager
import kotlin.properties.Delegates


abstract class Element {
    private var skipAlignmentCalculation = false

    internal var calculatedPosition: Vector2 = Vector2()
    internal var calculatedSize: Vector2 = Vector2()

    internal var isInitialized = false

    var margin: Margin by Delegates.observable(Margin(0.0f), { _, _, _ -> onMarginChanged() })
    var parent: ItemsElement? by Delegates.observable(null as ItemsElement?, { _, _, _ -> parentChanged() })
    var position: Vector2 by Delegates.observable(Vector2(), { _, _, _ -> positionChanged() })
    var size: Vector2 by Delegates.observable(Vector2(), { _, _, _ -> sizeChanged() })
    var horizontalAlignment: Alignment by Delegates.observable(Alignment.START, { _, old, new -> alignmentChanged(old, new) })
    var verticalAlignment: Alignment by Delegates.observable(Alignment.START, { _, old, new -> alignmentChanged(old, new) })

    var visible = true
    var disabled = false

    private fun updateMessagePosition(message: WindowMessage): WindowMessage {
        return message
    }

    abstract fun onFrame(context: UiContext)

    protected open fun doMessage(message: WindowMessage) {

    }

    fun handleMessage(message: WindowMessage) {
        val realMessage = updateMessagePosition(message)
        doMessage(realMessage)
        if(realMessage.isHandled && !message.isHandled) {
            message.isHandled = true
        }
    }

    open fun pack() {

    }

    open fun onInitialize() {
        isInitialized = true
    }

    internal open fun onParentResize() {
        positionChanged()
        sizeChanged()
    }

    fun removeFromParent() {
        parent?.removeChild(this)
        parentChanged()
    }

    internal open fun positionChanged() {
        if(skipAlignmentCalculation) {
            return
        }

        val result = AlignmentManager.calculate(parent, this, horizontalAlignment, verticalAlignment, margin)
        skipAlignmentCalculation = true
        this.calculatedPosition = result.position
        this.calculatedSize = result.size
        skipAlignmentCalculation = false
    }

    internal open fun sizeChanged() {
        if(skipAlignmentCalculation) {
            return
        }

        val result = AlignmentManager.calculate(parent, this, horizontalAlignment, verticalAlignment, margin)
        skipAlignmentCalculation = true
        this.calculatedPosition = result.position
        this.calculatedSize = result.size
        skipAlignmentCalculation = false
    }

    private fun parentChanged() {
        val result = AlignmentManager.calculate(parent, this, horizontalAlignment, verticalAlignment, margin)
        this.calculatedPosition = result.position
        this.calculatedSize = result.size
    }

    private fun alignmentChanged(old: Alignment, new: Alignment) {
        if(old == new) {
            return
        }

        val result = AlignmentManager.calculate(parent, this, horizontalAlignment, verticalAlignment, margin)
        this.calculatedPosition = result.position
        this.calculatedSize = result.size
    }

    private fun onMarginChanged() {
        val result = AlignmentManager.calculate(parent, this, horizontalAlignment, verticalAlignment, margin)
        this.calculatedPosition = result.position
        this.calculatedSize = result.size
    }
}