package ch.cromon.wow.ui.quickmenu

import ch.cromon.wow.ui.elements.ItemsElement
import ch.cromon.wow.ui.quickmenu.model.Definition
import ch.cromon.wow.ui.quickmenu.model.readDefinition
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component


@Component
@Scope("singleton")
class QuickMenuManager {
    private lateinit var uiRoot: ItemsElement
    private lateinit var definition: Definition
    private lateinit var menuRoot: MenuRoot

    fun attach(uiRoot: ItemsElement) {
        this.uiRoot = uiRoot
        definition = readDefinition()
        menuRoot = MenuRoot()
        uiRoot.appendChild(menuRoot)
        menuRoot.loadFromDefinition(definition)
    }
}