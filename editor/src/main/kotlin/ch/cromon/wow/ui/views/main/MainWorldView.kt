package ch.cromon.wow.ui.views.main

import ch.cromon.wow.ApplicationState
import ch.cromon.wow.gx.BlendMode
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.scene.Camera
import ch.cromon.wow.scene.WorldFrame
import ch.cromon.wow.scene.terrain.MapLoadFinishedEvent
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.controller.ZoneChangedEvent
import ch.cromon.wow.ui.elements.Border
import ch.cromon.wow.ui.elements.IconButton
import ch.cromon.wow.ui.elements.Quad
import ch.cromon.wow.ui.text.StaticText
import ch.cromon.wow.ui.utils.FpsCounter
import ch.cromon.wow.ui.views.AppStateView
import ch.cromon.wow.ui.views.View
import ch.cromon.wow.utils.Settings
import ch.cromon.wow.utils.TIME_OF_DAY_DELAY_FACTOR
import ch.cromon.wow.utils.Time
import ch.cromon.wow.utils.format
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.awt.Color
import java.awt.Font
import javax.annotation.PostConstruct


@AppStateView(state = ApplicationState.MAP_EDIT)
@Component
@Scope("singleton")
class MainWorldView : View() {
    private lateinit var leftBackground: Quad
    private lateinit var rightBackground: Quad
    private lateinit var topBackground: Quad
    private lateinit var bottomBackground: Quad

    private lateinit var border: Border

    private lateinit var positionLabel: StaticText
    private lateinit var positionXText: StaticText
    private lateinit var positionYText: StaticText
    private lateinit var positionZText: StaticText

    private lateinit var mapNameLabel: StaticText
    private lateinit var mapName: StaticText

    private lateinit var zoneNameLabel: StaticText
    private lateinit var zoneName: StaticText

    private lateinit var timeOfDayLabel: StaticText
    private lateinit var timeOfDay: StaticText

    private lateinit var fpsLabel: StaticText
    private lateinit var fpsText: StaticText

    @Autowired
    private lateinit var time: Time

    @Autowired
    private lateinit var settings: Settings

    @Autowired
    private lateinit var worldFrame: WorldFrame
    private lateinit var camera: Camera

    private var timeFactor: Float = 10.0f

    @PostConstruct
    fun afterInit() {
        timeFactor = settings[TIME_OF_DAY_DELAY_FACTOR, "10"].toLong().toFloat()
    }

    override fun onInitialize() {
        super.onInitialize()

        leftBackground = Quad()
        rightBackground = Quad()
        topBackground = Quad()
        bottomBackground = Quad()

        border = Border()

        leftBackground.blendMode = BlendMode.ALPHA
        rightBackground.blendMode = BlendMode.ALPHA
        topBackground.blendMode = BlendMode.ALPHA
        bottomBackground.blendMode = BlendMode.ALPHA

        leftBackground.color = Color(0xB0333333.toInt(), true)
        rightBackground.color = Color(0xB0333333.toInt(), true)
        topBackground.color = Color(0xB0333333.toInt(), true)
        bottomBackground.color = Color(0xB0333333.toInt(), true)

        camera = worldFrame.activeCamera

        initBottomBar()
        initLeftBar()
    }

    override fun onFrame(context: UiContext) {
        updatePerFrameData()

        leftBackground.onFrame()
        rightBackground.onFrame()
        bottomBackground.onFrame()
        topBackground.onFrame()

        border.onFrame()

        super.onFrame(context)

        renderBottomBar()
    }

    private fun renderBottomBar() {
        positionLabel.onFrame()
        positionXText.onFrame()
        positionYText.onFrame()
        positionZText.onFrame()

        mapNameLabel.onFrame()
        mapName.onFrame()

        zoneNameLabel.onFrame()
        zoneName.onFrame()

        timeOfDayLabel.onFrame()
        timeOfDay.onFrame()

        fpsLabel.onFrame()
        fpsText.onFrame()
    }

    override fun positionChanged() {
        super.positionChanged()
        sizePositionChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        sizePositionChanged()
    }

    private fun sizePositionChanged() {
        updateBackgrounds()
        updateBottomBar()
    }

    private fun updateBackgrounds() {
        leftBackground.position = Vector2(0, 0)
        leftBackground.size = Vector2(50.0f, calculatedSize.y)

        topBackground.position = Vector2(50, 0)
        topBackground.size = Vector2(calculatedSize.x - 100, 50.0f)

        rightBackground.position = Vector2(calculatedSize.x - 50, 0.0f)
        rightBackground.size = Vector2(50.0f, calculatedSize.y)

        bottomBackground.position = Vector2(50.0f, calculatedSize.y - 50)
        bottomBackground.size = Vector2(calculatedSize.x - 100, 50.0f)

        border.topLeft = Vector2(50, 50)
        border.bottomRight = Vector2(calculatedSize.x - 50, calculatedSize.y - 50)
    }

    private fun updateBottomBar() {
        val textPosY = calculatedSize.y - 37
        positionLabel.position = Vector2(55.0f, textPosY)
        positionXText.position = Vector2(55 + positionLabel.textWidth, textPosY)
        positionYText.position = Vector2(positionXText.position.x + positionXText.textWidth + 10, textPosY)
        positionZText.position = Vector2(positionYText.position.x + positionYText.textWidth + 10, textPosY)

        mapNameLabel.position = Vector2(positionZText.position.x + positionZText.textWidth + 25, textPosY)
        mapName.position = Vector2(mapNameLabel.position.x + mapNameLabel.textWidth + 5, textPosY)

        zoneNameLabel.position = Vector2(mapName.position.x + mapName.textWidth + 25, textPosY)
        zoneName.position = Vector2(zoneNameLabel.position.x + zoneNameLabel.textWidth + 5, textPosY)

        timeOfDayLabel.position = Vector2(zoneName.position.x + zoneName.textWidth + 25, textPosY)
        timeOfDay.position = Vector2(timeOfDayLabel.position.x + timeOfDayLabel.textWidth + 5, textPosY)

        fpsLabel.position = Vector2(timeOfDay.position.x + timeOfDay.textWidth + 25, textPosY)
        fpsText.position = Vector2(fpsLabel.position.x + fpsLabel.textWidth + 5, textPosY)
    }

    private fun updatePerFrameData() {
        val position = camera.position
        positionXText.text = "x: " + position.x.format(3)
        positionYText.text = "y: " + position.y.format(3)
        positionZText.text = "z: " + position.z.format(3)

        fpsText.text = FpsCounter.fps.toInt().toString()

        updateBottomBar()
    }

    @EventListener
    fun onEnterWorld(event: MapLoadFinishedEvent) {
        mapName.text = "${event.displayName} (id: ${event.mapId})"
    }

    @EventListener
    fun onZoneChanged(event: ZoneChangedEvent) {
        zoneName.text = "${event.zoneName} (id: ${event.zoneId})"
    }

    @Scheduled(fixedDelay = 20)
    private fun updateTimeScheduled() {
        val runTime = ((time.runTime - time.worldStart) / timeFactor).toLong() % 2880
        val totalMinutes = (runTime * 0.5f).toInt()
        val minutes = totalMinutes % 60
        val hours = totalMinutes / 60
        timeOfDay.text = "$hours:${minutes.toString().padStart(2, '0')}"
    }

    private fun initBottomBar() {
        val font = Font("Arial", Font.BOLD, 20)

        positionLabel = StaticText(font)
        positionLabel.text = "Position: "

        positionXText = StaticText(font)
        positionXText.color = Color(0xff5050)

        positionYText = StaticText(font)
        positionYText.color = Color(0x33cc33)

        positionZText = StaticText(font)
        positionZText.color = Color(0x3366ff)

        border.color = Color(0x777777)

        mapNameLabel = StaticText(font)
        mapName = StaticText(font)

        zoneNameLabel = StaticText(font)
        zoneName = StaticText(font)

        timeOfDayLabel = StaticText(font)
        timeOfDay = StaticText(font)

        fpsLabel = StaticText(font)
        fpsText = StaticText(font)

        mapNameLabel.text = "Map:"
        zoneNameLabel.text = "Zone:"
        timeOfDayLabel.text = "Time:"
        fpsLabel.text = "FPS:"
    }

    private fun initLeftBar() {
        var index = 0
        addLeftBarButton(index++, '\uf1fc')
        addLeftBarButton(index++, '\uf279')
        addLeftBarButton(index++, '\uf19c')
        addLeftBarButton(index++, '\uf185')
    }

    private fun addLeftBarButton(index: Int, char: Char) {
        val button = IconButton()
        appendChild(button)

        button.position = Vector2(5, 55 + index * 50)
        button.fontSize = 50.0f
        button.character = char
        button.size = Vector2(40, 40)
    }
}