package ch.cromon.wow.ui.dialogs

import ch.cromon.wow.scene.sky.SkyManager
import javafx.fxml.FXML
import javafx.scene.control.ListView
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.net.URL
import java.util.*

@ViewPath("/dialogs/light_editor.fxml")
@Component
@Scope("singleton")
class LightEditor : AbstractDialogController() {
    @Autowired
    private lateinit var skyManager: SkyManager

    @FXML
    private lateinit var lightListView: ListView<Any>

    override val title: String
        get() = "Light Editor"

    override fun initialize(location: URL, resources: ResourceBundle?) {
        context.autowireCapableBeanFactory.autowireBean(this)

        skyManager.getActiveLights().forEach {
            lightListView.items.add(it)
        }
    }
}