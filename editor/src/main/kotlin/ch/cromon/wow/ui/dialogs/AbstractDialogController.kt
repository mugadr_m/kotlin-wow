package ch.cromon.wow.ui.dialogs

import javafx.fxml.Initializable
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware


abstract class AbstractDialogController : Initializable, ApplicationContextAware, Dialog {
    companion object {
        internal lateinit var context: ApplicationContext
    }

    override fun setApplicationContext(applicationContext: ApplicationContext?) {
        applicationContext ?: return
        context = applicationContext
    }
}