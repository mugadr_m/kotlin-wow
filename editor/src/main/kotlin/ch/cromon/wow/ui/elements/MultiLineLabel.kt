package ch.cromon.wow.ui.elements

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.text.StaticText
import java.awt.Color
import java.awt.Font
import kotlin.properties.Delegates


class MultiLineLabel : Element() {
    private lateinit var textRender: StaticText

    private var width by Delegates.observable(1.0f, { _, _, _ -> widthChanged() })

    var fontSize by Delegates.observable(20, { _, _, _ -> fontSizeChanged() })
    var text by Delegates.observable("", { _, _, _ -> textChanged() })

    var color: Color by Delegates.observable(Color.WHITE, {_, _, _ -> textRender.color = color })

    val textWidth get() = textRender.textWidth
    val textHeight get() = textRender.textHeight

    private var clipStart = Vector2()
    private var clipEnd = Vector2()

    override fun sizeChanged() {
        super.sizeChanged()
        width = calculatedSize.x
        sizePositionChanged()
    }

    override fun positionChanged() {
        super.positionChanged()
        sizePositionChanged()
    }

    override fun onFrame(context: UiContext) {
        context.clipStack.push(clipStart, clipEnd)

        textRender.onFrame()

        context.clipStack.pop()
    }

    override fun onInitialize() {
        textRender = StaticText(Font("Arial", Font.BOLD, 20))
        textRender.isMultiLine = true
        textRender.maxWidth = width
    }

    private fun fontSizeChanged() {
        textRender = StaticText(Font("Arial", Font.BOLD, fontSize))
        textRender.text = text
        textRender.position = calculatedPosition
        textRender.isMultiLine = true
        textRender.maxWidth = width
    }

    private fun textChanged() {
        textRender.text = text
    }

    private fun widthChanged() {
        textRender.maxWidth = width
    }

    private fun sizePositionChanged() {
        clipStart = calculatedPosition
        clipEnd = calculatedPosition + calculatedSize
        textRender.position = calculatedPosition
    }
}