package ch.cromon.wow.ui.controller

import ch.cromon.wow.AppStateManager
import ch.cromon.wow.ApplicationState
import ch.cromon.wow.io.AsyncImageLoader
import ch.cromon.wow.io.ImageSource
import ch.cromon.wow.io.ImageSourceFormat
import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.io.data.DataManager
import ch.cromon.wow.io.files.map.WdlFile
import ch.cromon.wow.io.files.storage.DataStorage
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.scene.terrain.MapLoadFailedEvent
import ch.cromon.wow.scene.terrain.MapManager
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.util.concurrent.CompletableFuture

data class PointOfInterest(val name: String, val position: Vector3)

data class MapSelectEntry(val mapId: Int, val displayName: String, val mapName: String, val loadScreen: () -> CompletableFuture<LittleEndianStream?>)

class MapSelectLoadEvent(val entries: List<MapSelectEntry>, source: Any) : ApplicationEvent(source)

class MapSelectedEvent(val entry: MapSelectEntry, val pointOfInterests: List<PointOfInterest>, source: Any) : ApplicationEvent(source)

@Component
@Scope("singleton")
class MapController {
    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var dataStorage: DataStorage

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    @Autowired
    private lateinit var appStateManager: AppStateManager

    @Autowired
    private lateinit var dataManager: DataManager

    @Autowired
    private lateinit var asyncImageLoader: AsyncImageLoader

    @Autowired
    private lateinit var mapManager: MapManager

    private lateinit var selectedMap: MapSelectEntry

    val currentMap get() = selectedMap

    val poiImageSource: ImageSource by lazy {
        asyncImageLoader.loadOneOf("INTERFACE\\WORLDSTATEFRAME\\COLUMNICON-FLAGRETURN0.BLP", "INTERFACE\\WorldMap\\WorldMapPlayerIcon.blp").get()
    }

    val poiHighlightImage: ImageSource by lazy {
        asyncImageLoader.loadOneOf("INTERFACE\\WORLDSTATEFRAME\\ALLIANCEFLAGFLASH.BLP", "INTERFACE\\WorldMap\\WorldMapPlayerIcon.blp").get()
    }

    @EventListener
    fun onMapLoadFailed(event: MapLoadFailedEvent) {
        appStateManager.currentState = ApplicationState.MAP_ENTRY_SELECT
    }

    fun getLoadingScreenInfo(mapId: Int): Pair<LittleEndianStream?, LittleEndianStream?> {
        val loadingScreenId = dataStorage.getLoadingScreenIdForMap(mapId)
        val fallbackLoadingScreen = dataStorage.getFallbackLoadingScreen()
        if (loadingScreenId < 0) {
            return fallbackLoadingScreen to null
        }

        val widePath = dataStorage.getWideLoadingScreen(loadingScreenId)
        val path = dataStorage.getLoadingScreen(loadingScreenId) ?: return fallbackLoadingScreen to null
        return path to widePath
    }

    fun onEnterWorld(mapSelectEntry: MapSelectEntry, position: Vector2) {
        selectedMap = mapSelectEntry
        logger.info("Entering ${mapSelectEntry.displayName} @ $position")
        mapManager.onEnterWorld(mapSelectEntry.mapId, mapSelectEntry.mapName, position)
        appStateManager.currentState = ApplicationState.MAP_LOAD
    }

    fun switchToMapSelect() {
        appStateManager.currentState = ApplicationState.MAP_SELECT
    }

    fun onInitMapButtons() {
        val entries = ArrayList<MapSelectEntry>()

        dataStorage.listMapRows { row, descriptor ->
            val mapId = row.getInt(0)
            val mapName = row.getString(descriptor.continentIndex * 4)
            val displayName = row.getString(descriptor.displayNameIndex * 4)

            entries.add(MapSelectEntry(mapId, displayName, mapName, {
                CompletableFuture.supplyAsync({
                    var loadingScreen = if (descriptor.loadScreenIndex < 0) {
                        dataStorage.getFallbackLoadingScreen()
                    } else {
                        dataStorage.getLoadingScreen(row.getInt(descriptor.loadScreenIndex * 4))
                    }
                    if (loadingScreen == null) {
                        loadingScreen = dataStorage.getFallbackLoadingScreen()
                    }
                    loadingScreen
                })
            }))
        }

        publisher.publishEvent(MapSelectLoadEvent(entries.sortedBy { it.mapId }, this))
    }

    fun onMapSelected(entry: MapSelectEntry) {
        logger.info("Entering map: ${entry.displayName}")
        appStateManager.currentState = ApplicationState.MAP_ENTRY_SELECT
        val safeLocs = dataStorage.getSafeLocationsForMap(entry.mapId)
        logger.info(safeLocs.joinToString("\n"))
        val taxiNodes = dataStorage.getTaxiNodesForMap(entry.mapId)
        logger.info(taxiNodes.joinToString("\n"))
        val pois = safeLocs.map { PointOfInterest(it.name, it.position) }.union(taxiNodes.map { PointOfInterest(it.name, it.position) }).toList()

        publisher.publishEvent(MapSelectedEvent(entry, pois, this))
    }

    fun loadWdlImage(entry: MapSelectEntry): ImageSource {
        val edge = 64 * 17
        val buffer = ByteArray(edge * edge * 4)
        val wdlFile = "World\\Maps\\${entry.mapName}\\${entry.mapName}.wdl"
        val wdlFileShort = "${entry.mapName}.wdl" // from alpha
        val file = dataManager.openFile(wdlFile) ?: dataManager.openFile(wdlFileShort)
        ?: return ImageSource(buffer, 1024, 1024, ImageSourceFormat.ARGB8)
        val wdl = WdlFile(file)
        wdl.parse()

        for (y in 0 until 64) {
            for (x in 0 until 64) {
                val mare = wdl.getMareChunk(x, y)
                for (k in 0 until 17) {
                    for (l in 0 until 17) {
                        var r: Byte
                        var g: Byte
                        var b: Byte
                        var a = 0xFF.toByte()
                        if (mare == null) {
                            r = 0
                            g = 0
                            b = 0
                            a = 0
                        } else {
                            val h = mare.outer[k * 17 + l]
                            when {
                                h > 2000 -> {
                                    r = 255.toByte()
                                    g = 255.toByte()
                                    b = 255.toByte()
                                }
                                h > 1000 -> {
                                    val am = (h - 1000.0f) / 1000.0f
                                    r = (0.75f + am * 0.25f * 255.0f).toByte()
                                    g = (0.5f * am * 255.0f).toByte()
                                    b = (0.5f + am * 0.5f * 255.0f).toByte()
                                }
                                h > 600 -> {
                                    val am = (h - 600.0f) / 400.0f
                                    r = (0.75f + am * 0.25f * 255.0f).toByte()
                                    g = (0.5f * am * 255.0f).toByte()
                                    b = (am * 255.0f).toByte()
                                }
                                h > 300 -> {
                                    val am = (h - 300.0f) / 300.0f
                                    r = (255.0f - am * 255.0f).toByte()
                                    g = 1
                                    b = 0
                                }
                                h > 0 -> {
                                    val am = h / 300.0f
                                    r = (0.75f * am * 255.0f).toByte()
                                    g = (255 - 0.5f * am * 255.0f).toByte()
                                    b = 0
                                }
                                h > -100 -> {
                                    val am = (h + 100.0f) / 100.0f
                                    r = 0
                                    g = (am * 127.0f).toByte()
                                    b = 200.toByte()
                                }
                                else -> {
                                    r = 0
                                    g = 0
                                    b = 0x2F
                                }
                            }

                            if (k == 0 || l == 0) {
                                r = 0
                                g = 0
                                b = 0
                            }
                        }

                        val baseIndex = (y * 17 + k) * edge + x * 17 + l
                        buffer[baseIndex * 4] = r
                        buffer[baseIndex * 4 + 1] = g
                        buffer[baseIndex * 4 + 2] = b
                        buffer[baseIndex * 4 + 3] = a
                    }
                }
            }
        }

        return ImageSource(buffer, edge, edge, ImageSourceFormat.ARGB8)
    }
}