package ch.cromon.wow.ui.controller

import ch.cromon.wow.io.files.storage.DataStorage
import ch.cromon.wow.math.Vector3
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

data class ZoneChangedEvent(val zoneId: Int, val zoneName: String)

@Component
@Scope("singleton")
class EditController {
    @Autowired
    private lateinit var dataStorage: DataStorage

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    private var lastZoneId = -2

    fun onPositionChanged(position: Vector3, zone: Int) {
        if(zone == lastZoneId) {
            return
        }

        lastZoneId = zone
        if(zone > 0) {
            val zoneName = dataStorage.getAreaName(zone) ?: ""
            publisher.publishEvent(ZoneChangedEvent(zone, zoneName))
        } else {
            publisher.publishEvent(ZoneChangedEvent(-1, ""))
        }
    }
}