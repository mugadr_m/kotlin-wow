package ch.cromon.wow.ui.elements

import ch.cromon.wow.gx.BlendMode
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.messaging.*
import ch.cromon.wow.ui.text.StaticText
import ch.cromon.wow.utils.pointInQuad
import java.awt.Color
import java.awt.Font
import kotlin.properties.Delegates


class Frame : ItemsElement() {
    private val border = Border()
    private val systemBar = Quad()
    private val background = Quad()
    private val captionRender = StaticText(Font("Arial", Font.BOLD, 18))
    private var lastDragPoint = Vector2()
    private var isDragging = false

    private var caption by Delegates.observable("", { _, _, _ -> handleTextChanged() })

    init {
        childOffset = Vector2(2, 30)

        border.thickness = 2
        border.color = Color(0x3F, 0x7F, 0xFF)

        systemBar.color = Color(0x3F, 0x7F, 0xFF)

        background.blendMode = BlendMode.ALPHA
        background.color = Color(0xA0606060.toInt(), true)

        captionRender.isMultiLine = false
    }

    override fun sizeChanged() {
        super.sizeChanged()
        positionChanged()
    }

    override fun doMessage(message: WindowMessage) {
        if(message is MouseMoveMessage) {
            if(isDragging) {
                message.isHandled = true
                val diff = message.position - lastDragPoint
                position += diff
                lastDragPoint = message.position
                return
            }
        } else if(message is MouseClickMessage) {
            if(message.button == MouseButton.LEFT) {
                if(!message.pressed) {
                    isDragging = false
                } else {
                    if(!message.isHandled) {
                        isDragging = pointInQuad(message.position, calculatedPosition, calculatedPosition + Vector2(calculatedSize.x, 30.0f))
                        message.isHandled = isDragging
                        if(isDragging) {
                            lastDragPoint = message.position
                        }
                    }
                }
            }
        }

        super.doMessage(message)

        if(message is MouseMessage) {
            if(pointInQuad(message.position, calculatedPosition, calculatedPosition + calculatedSize)) {
                message.isHandled = true
            }
        }
    }

    override fun onFrame(context: UiContext) {
        background.onFrame()

        context.clipStack.push(calculatedPosition + childOffset, calculatedPosition + calculatedSize - Vector2(2, 2))
        super.onFrame(context)
        context.clipStack.pop()

        border.onFrame()
        systemBar.onFrame()
        captionRender.onFrame()
    }

    override fun positionChanged() {
        super.positionChanged()

        background.position = calculatedPosition
        background.size = calculatedSize
        background.position

        border.topLeft = calculatedPosition
        border.bottomRight = calculatedPosition + calculatedSize

        systemBar.position = calculatedPosition
        systemBar.size = Vector2(size.x, 30.0f)

        updateTextPlacement()
    }

    private fun handleTextChanged() {
        captionRender.text = caption
        updateTextPlacement()
    }

    private fun updateTextPlacement() {
        val textHeight = captionRender.textHeight
        captionRender.position = Vector2(calculatedPosition.x + 5, calculatedPosition.y + (30 - textHeight) / 2.0f)
    }

}