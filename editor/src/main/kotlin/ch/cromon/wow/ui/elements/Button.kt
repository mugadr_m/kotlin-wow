package ch.cromon.wow.ui.elements

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.messaging.MouseButton
import ch.cromon.wow.ui.messaging.MouseClickMessage
import ch.cromon.wow.ui.messaging.MouseMoveMessage
import ch.cromon.wow.ui.messaging.WindowMessage
import ch.cromon.wow.ui.text.StaticText
import ch.cromon.wow.utils.Event
import ch.cromon.wow.utils.EventHandler
import ch.cromon.wow.utils.pointInQuad
import java.awt.Color
import java.awt.Font
import kotlin.properties.Delegates


class Button : Element() {
    companion object {
        val SIZE = Vector2(150.0f, 35.0f)
    }

    private val captionDraw = StaticText(Font("Arial", Font.BOLD, 20))
    private val background = Quad()
    private val border = Border()

    private var isHovered = false
    private var isClicked = false

    private val clickEventDelegate = EventHandler<Button>()

    var onClick = Event(clickEventDelegate)

    var caption: String by Delegates.observable("", { _, _, _ -> textChanged() })

    init {
        this.size = SIZE
        background.color = Color.DARK_GRAY
        background.size = SIZE
        border.color = Color.WHITE

        captionDraw.isMultiLine = false
        captionDraw.color = Color.WHITE
        position = Vector2(0, 0)
    }

    override fun doMessage(message: WindowMessage) {
        if(!visible) {
            return
        }

        if (message is MouseMoveMessage) {
            handleMouseMove(message)
        } else if (message is MouseClickMessage) {
            handleMouseClick(message)
        }
    }

    override fun onFrame(context: UiContext) {
        if(!visible) {
            return
        }

        background.onFrame()

        context.clipStack.push(calculatedPosition, calculatedPosition + calculatedSize)
        captionDraw.onFrame()
        context.clipStack.pop()

        border.onFrame()
    }

    private fun handleMouseMove(message: MouseMoveMessage) {
        val pos = calculatedPosition
        val hasHover = pointInQuad(message.position, pos, pos + calculatedSize) and !message.isHandled
        if (hasHover && !isHovered) {
            background.color = Color.GRAY
        } else if (!hasHover && isHovered) {
            background.color = Color.DARK_GRAY
        }

        isHovered = hasHover
    }

    private fun handleMouseClick(message: MouseClickMessage) {
        if (message.button == MouseButton.LEFT) {
            if (message.pressed) {
                if (isHovered) {
                    isClicked = true
                }
            } else {
                if (isClicked) {
                    if (isHovered) {
                        clickEventDelegate(this)
                    }
                    isClicked = false
                }
            }
        }

        message.isHandled = message.isHandled || isHovered
    }

    override fun positionChanged() {
        super.positionChanged()
        updateSizePosition()
    }

    private fun updateSizePosition() {
        border.topLeft = calculatedPosition
        border.bottomRight = calculatedPosition + calculatedSize
        background.position = calculatedPosition
        updateCaptionPosition()
    }

    private fun updateCaptionPosition() {
        val textWidth = captionDraw.textWidth
        val textHeight = captionDraw.textHeight
        if (textWidth < 0.1 || textHeight < 0.1) {
            return
        }

        captionDraw.position = Vector2(calculatedPosition.x + (calculatedSize.x - textWidth) / 2.0f,
                calculatedPosition.y + (calculatedSize.y - textHeight) / 2.0f)
    }

    private fun textChanged() {
        captionDraw.text = caption
        updateCaptionPosition()
    }
}