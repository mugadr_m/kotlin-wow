package ch.cromon.wow.ui.dialogs

import ch.cromon.wow.utils.I18N
import ch.cromon.wow.utils.I18N.SERVER_DIALOG_TITLE
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.net.URL
import java.util.*

@ViewPath("/dialogs/server_browser.fxml")
@Component
@Scope("singleton")
class ServerBrowser : AbstractDialogController() {
    private val titleField = I18N.getString(SERVER_DIALOG_TITLE)

    override val title: String
        get() = titleField

    override fun initialize(location: URL?, resources: ResourceBundle?) {

    }
}