package ch.cromon.wow.ui.quickmenu.model

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue


data class Node(val description: String, val children: List<Node>)

data class Definition(val children: List<Node>)

val mapper = jacksonObjectMapper()

fun readDefinition(): Definition {
    return Definition::class.java.getResourceAsStream("/quickmenu/definition.json")?.use {
        mapper.readValue<Definition>(it)
    } ?: throw IllegalStateException("Unable to find resource /quickmenu/definition.json")
}