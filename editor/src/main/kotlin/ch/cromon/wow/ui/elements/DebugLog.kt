package ch.cromon.wow.ui.elements

import ch.cromon.wow.gx.BlendMode
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.text.StaticText
import org.springframework.stereotype.Component
import java.awt.Color
import java.awt.Font

private class DebugLogMessage(val content: String, var drawer: StaticText? = null)

@Component
class DebugLog : Element() {
    companion object {
        private val FONT = Font("Arial", Font.BOLD, 16)
    }

    private val messageList = ArrayList<DebugLogMessage>()
    private lateinit var background: Quad
    private lateinit var border: Border

    private var clipStart = Vector2()
    private var clipEnd = Vector2()

    override fun onFrame(context: UiContext) {
        background.onFrame()
        border.onFrame()

        context.clipStack.push(clipStart, clipEnd)

        var currentPosition = calculatedPosition.y + calculatedSize.y - 20
        for(i in (messageList.size - 1) downTo 0) {
            val entry = messageList[i]
            val drawer = entry.drawer ?: StaticText(FONT)
            if(entry.drawer == null) {
                drawer.text = entry.content
                entry.drawer = drawer
            }

            drawer.position = Vector2(5.0f, currentPosition)
            drawer.onFrame()
            currentPosition -= 20
            if(currentPosition < calculatedPosition.y - 30) {
                break
            }
        }

        context.clipStack.pop()
    }

    override fun onInitialize() {
        super.onInitialize()
        background = Quad()
        border = Border()

        border.drawBottom = false
        border.drawLeft = false
        border.drawRight = false

        background.color = Color(0xCC222222.toInt(), true)
        background.blendMode = BlendMode.ALPHA

        //DebugLogAppender.INSTANCE.callback = { onDebugMessage(it) }
    }

    override fun positionChanged() {
        super.positionChanged()
        positionSizeChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        positionSizeChanged()
    }

    /*private fun onDebugMessage(event: DebugLogEvent) {
        val message = event.message
        messageList.add(DebugLogMessage(message))
    }*/

    private fun positionSizeChanged() {
        background.position = calculatedPosition
        background.size = calculatedSize

        border.topLeft = calculatedPosition
        border.bottomRight = calculatedPosition + calculatedSize

        clipStart = calculatedPosition + Vector2(5, 5)
        clipEnd = calculatedPosition + calculatedSize - Vector2(5, 5)
    }
}