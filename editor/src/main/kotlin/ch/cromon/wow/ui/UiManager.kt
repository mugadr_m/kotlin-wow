package ch.cromon.wow.ui

import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.elements.*
import ch.cromon.wow.ui.messaging.WindowMessage
import ch.cromon.wow.ui.quickmenu.QuickMenuManager
import ch.cromon.wow.ui.text.StaticText
import ch.cromon.wow.ui.utils.ClipStack
import ch.cromon.wow.ui.utils.TransformStack
import ch.cromon.wow.ui.views.ViewManager
import org.lwjgl.opengl.GL11.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component


@Component
class UiManager {
    private lateinit var uiRoot: ItemsElement
    private val clipStack = ClipStack()
    private val transformStack = TransformStack({ globalTransformChanged(it) })
    val context = UiContext(clipStack, transformStack)

    @Autowired
    private lateinit var debugLog: DebugLog

    @Autowired
    private lateinit var viewManager: ViewManager

    @Autowired
    private lateinit var quickMenuManager: QuickMenuManager

    fun onInitialize() {
        StaticText.initialize()
        Quad.initialize()
        MemoryControl.initialize()
        Image.initialize()

        Quad.updateGlobalTransform(Matrix4())
        StaticText.updateGlobalTransform(Matrix4())

        uiRoot = ItemsElement()

        viewManager.attach(uiRoot)
        quickMenuManager.attach(uiRoot)

        debugLog.onInitialize()
    }

    @EventListener
    fun onResize(event: WindowResizeEvent) {
        val x = event.dimension.x
        val y = event.dimension.y
        val projection = Matrix4()
        Matrix4.ortho(projection, 0.0f, x, y, 0.0f)
        StaticText.updateProjection(projection)
        Quad.updateProjection(projection)
        MemoryControl.updateProjection(projection)
        Image.updateProjection(projection)
        clipStack.windowHeight = y.toInt()
        uiRoot.size = Vector2(x, y)
        debugLog.position = Vector2(0.0f, y - 400)
        debugLog.size = Vector2(x, 400.0f)
    }

    fun onFrame() {
        glDisable(GL_DEPTH_TEST)

        uiRoot.onFrame(context)
        //debugLog.onFrame(context)

        glEnable(GL_DEPTH_TEST)
    }

    fun handleMessage(message: WindowMessage) {
        uiRoot.handleMessage(message)
    }

    private fun globalTransformChanged(matrix: Matrix4) {
        Quad.updateGlobalTransform(matrix)
        StaticText.updateGlobalTransform(matrix)
        Image.updateGlobalTransform(matrix)
    }
}