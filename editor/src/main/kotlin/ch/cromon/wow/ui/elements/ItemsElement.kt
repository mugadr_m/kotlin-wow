package ch.cromon.wow.ui.elements

import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.messaging.WindowMessage
import kotlin.properties.Delegates


open class ItemsElement : Element() {
    protected var childOffset: Vector2 by Delegates.observable(Vector2(), { _, _, _ -> positionChanged() })

    private var matrixTransform = Matrix4()
    private var children = ArrayList<Element>()
    private var messageRelativePos = Vector2()
    private val childrenToRemove = ArrayList<Element>()
    private val childrenToAdd = ArrayList<Element>()
    private var isLooping = false

    internal fun getChildOffset(): Vector2 = childOffset

    fun appendChild(child: Element): ItemsElement {
        if (children.contains(child)) {
            return this
        }

        child.parent = this
        if(!isLooping) {
            children.add(child)
        } else {
            childrenToAdd.add(child)
        }

        if (!child.isInitialized) {
            child.onInitialize()
        }

        child.onParentResize()
        return this
    }

    fun removeChild(child: Element) {
        if (!children.contains(child)) {
            return
        }

        child.parent = null
        if (!isLooping) {
            children.remove(child)
        } else {
            childrenToRemove.add(child)
        }
    }

    override fun pack() {
        super.pack()
        var minY = Float.MAX_VALUE
        var maxY = Float.MIN_VALUE
        applyToChildren({
            val sy = it.calculatedPosition.y
            val ey = it.calculatedPosition.y + it.calculatedSize.y
            minY = Math.min(minY, sy)
            maxY = Math.max(maxY, ey)
        })

        size = Vector2(calculatedSize.x, maxY - minY)
    }

    override fun onFrame(context: UiContext) {
        if(!visible) {
            return
        }

        context.transformStack.pushMatrix(matrixTransform)

        applyToChildren({
            it.onFrame(context)
        })

        context.transformStack.popMatrix()
    }

    override fun doMessage(message: WindowMessage) {
        val transformedMessage = message.forwardMessage(messageRelativePos)
        applyToChildren({ it.handleMessage(transformedMessage) }, true)

        message.isHandled = transformedMessage.isHandled
    }

    override fun positionChanged() {
        super.positionChanged()
        Matrix4.translation(matrixTransform, calculatedPosition.x + childOffset.x, calculatedPosition.y + childOffset.y, 0.0f)
        messageRelativePos = calculatedPosition + childOffset
        applyToChildren({ it.onParentResize() })
    }

    override fun sizeChanged() {
        super.sizeChanged()
        Matrix4.translation(matrixTransform, calculatedPosition.x + childOffset.x, calculatedPosition.y + childOffset.y, 0.0f)
        messageRelativePos = calculatedPosition + childOffset

        applyToChildren({ it.onParentResize() })

    }

    override fun onInitialize() {
        super.onInitialize()
        applyToChildren({
            if (!it.isInitialized) {
                it.onInitialize()
            }
        })
    }

    protected fun applyToChildren(callback: (Element) -> Unit, reversed: Boolean = false) {
        val childList = children
        isLooping = true
        if (!reversed) {
            childList.forEach { callback(it) }
        } else {
            childList.reversed().forEach { callback(it) }
        }
        isLooping = false

        if(childrenToRemove.size > 0 || childrenToAdd.size > 0) {
            val childrenCopy = ArrayList(children)
            childrenCopy.removeAll(childrenToRemove)
            childrenCopy.addAll(childrenToAdd)
            children = childrenCopy
            childrenToRemove.clear()
            childrenToAdd.clear()
        }
    }
}