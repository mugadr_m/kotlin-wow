package ch.cromon.wow.ui.views.init

import ch.cromon.wow.ApplicationState.DATA_INIT
import ch.cromon.wow.gx.BlendMode
import ch.cromon.wow.io.AsyncImageLoader
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.controller.DataController
import ch.cromon.wow.ui.elements.*
import ch.cromon.wow.ui.utils.Alignment
import ch.cromon.wow.ui.views.AppStateView
import ch.cromon.wow.ui.views.View
import ch.cromon.wow.ui.views.init.elements.ServerBrowserElement
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.awt.Color

@Component
@AppStateView(DATA_INIT)
class DataSelectView : View() {
    @Autowired
    private lateinit var controller: DataController

    @Autowired
    private lateinit var asyncImageLoader: AsyncImageLoader

    @Autowired
    private lateinit var serverBrowser: ServerBrowserElement

    private lateinit var background: Quad

    private lateinit var splitBorder: Border
    private lateinit var splashIcon: Image
    private lateinit var applicationHeader: Label
    private lateinit var actionHeader: Label
    private lateinit var openFromServer: LinkLabel

    override fun onFrame(context: UiContext) {
        background.onFrame()

        splitBorder.onFrame()
        super.onFrame(context)
    }

    override fun onInitialize() {
        super.onInitialize()

        splitBorder = Border()
        splitBorder.color = Color.WHITE

        horizontalAlignment = Alignment.STRETCH
        verticalAlignment = Alignment.STRETCH

        splashIcon = Image()
        applicationHeader = Label()
        actionHeader = Label()
        openFromServer = LinkLabel()
        val openLocal = LinkLabel()
        val openCdn = LinkLabel()
        appendChild(splashIcon)
        appendChild(applicationHeader)
        appendChild(actionHeader)
        appendChild(openFromServer)
        appendChild(openLocal)
        appendChild(openCdn)
        appendChild(serverBrowser)

        splashIcon.run {
            position = Vector2(10, 10)
            size = Vector2(128, 128)
            asyncImageLoader.loadFromImageResource("application_icon.png").thenAccept({
                imageSource = it
            })
        }

        applicationHeader.run {
            fontSize = 40
            text = "WoW Editor"
            position = Vector2(140.0f, 10.0f + (128.0f - textHeight) / 2.0f)
        }

        actionHeader.run {
            fontSize = 25
            text = "Actions"
            position = Vector2(10, 180)
        }

        openFromServer.run {
            fontSize = 20
            text = "Open from server"
            position = Vector2(10, 215)
        }

        serverBrowser.run {
            horizontalAlignment = Alignment.STRETCH
            verticalAlignment = Alignment.STRETCH
            visible = false
        }

        openLocal.run {
            fontSize = 20
            text = "Open From Local Folder"
            position = Vector2(10, 240)
        }

        openCdn.run {
            fontSize = 20
            text = "Open From CDN"
            position = Vector2(10, 265)
        }

        openFromServer.callback = this::handleOpenFromServer
        openLocal.callback = this::handleOpenFromLocal
        openCdn.callback = this::handleOpenFromCdn


        background = Quad()
        background.blendMode = BlendMode.NONE
        background.color = Color(0x333333)
    }

    override fun sizeChanged() {
        super.sizeChanged()

        val splitOffset = Math.max(400.0f, calculatedSize.x * 0.3f)
        splitBorder.topLeft = Vector2(splitOffset, 0.0f)
        splitBorder.bottomRight = Vector2(splitOffset + 2, calculatedSize.y)

        background.position = calculatedPosition
        background.size = calculatedSize
        serverBrowser.margin = Margin(splitOffset + 1.0f, 0.0f, 0.0f, 0.0f)
    }

    private fun handleOpenFromServer() {
        serverBrowser.visible = true
    }

    private fun handleOpenFromLocal() {
        controller.onSelectLocalFolder()
    }

    private fun handleOpenFromCdn() {
        controller.onSelectCDN()
    }
}