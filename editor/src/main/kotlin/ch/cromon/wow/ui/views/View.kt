package ch.cromon.wow.ui.views

import ch.cromon.wow.ApplicationState
import ch.cromon.wow.ui.elements.ItemsElement
import ch.cromon.wow.ui.utils.Alignment

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class AppStateView(val state: ApplicationState)

abstract class View : ItemsElement() {
    override fun onInitialize() {
        super.onInitialize()

        horizontalAlignment = Alignment.STRETCH
        verticalAlignment = Alignment.STRETCH
    }
}