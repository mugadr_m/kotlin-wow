package ch.cromon.wow.ui.elements

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.messaging.MouseClickMessage
import ch.cromon.wow.ui.messaging.MouseMoveMessage
import ch.cromon.wow.ui.messaging.WindowMessage
import ch.cromon.wow.ui.text.StaticText
import ch.cromon.wow.utils.pointInQuad
import java.awt.Color
import java.awt.Font
import kotlin.properties.Delegates


class IconButton : Element() {
    companion object {
        private lateinit var FONT: Font
        private var fontLoaded = false
    }

    private lateinit var iconText: StaticText

    private var isHovered = false

    var fontSize by Delegates.observable(18.0f, { _, _, _ -> onFontSizeChanged() })
    var character by Delegates.observable('\u0000', {_, _, _ -> onTextChanged() })

    val textWidth get() = iconText.textWidth
    val textHeight get() = iconText.textHeight

    override fun onInitialize() {
        super.onInitialize()

        if (!fontLoaded) {
            fontLoaded = true
            FONT = IconButton::class.java.getResourceAsStream("/fonts/FA5.otf")?.use {
                Font.createFont(Font.PLAIN, it)
            } ?: throw IllegalStateException("Unable to find FontAwesome in '/fonts/FA5.otf'")
        }

        iconText = StaticText(FONT)
        onFontSizeChanged()
    }

    override fun onFrame(context: UiContext) {
        iconText.onFrame()
    }

    override fun doMessage(message: WindowMessage) {
        when(message) {
            is MouseMoveMessage -> handleMouseMove(message)
            is MouseClickMessage -> handleMouseClick(message)
        }
    }

    override fun positionChanged() {
        super.positionChanged()
        onReposition()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        onReposition()
    }

    private fun onReposition() {
        val offsetX = (calculatedSize.x - iconText.textWidth) / 2.0f
        val offsetY = (calculatedSize.y - iconText.textHeight) / 2.0f
        iconText.position = calculatedPosition + Vector2(offsetX, offsetY)
    }

    private fun onFontSizeChanged() {
        val realFont = FONT.deriveFont(fontSize)
        val color = iconText.color
        iconText = StaticText(realFont)
        iconText.color = color
        onTextChanged()
    }

    private fun onTextChanged() {
        iconText.text = character.toString()
    }

    private fun handleMouseMove(message: MouseMoveMessage) {
        val wasHovered = isHovered
        isHovered = !message.isHandled && pointInQuad(message.position, calculatedPosition, calculatedPosition + calculatedSize)
        if(isHovered) {
            message.isHandled = true
            if(!wasHovered) {
                iconText.color = Color(0xff7f3f)
            }
        } else if(wasHovered) {
            iconText.color = Color.WHITE
        }
    }

    private fun handleMouseClick(message: MouseClickMessage) {

    }
}