package ch.cromon.wow.ui.utils

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.elements.Element
import ch.cromon.wow.ui.elements.ItemsElement
import ch.cromon.wow.ui.elements.Margin


enum class Alignment {
    START,
    END,
    CENTER,
    STRETCH
}

class AlignmentResult(var position: Vector2 = Vector2(), var size: Vector2 = Vector2())

object AlignmentManager {
    fun calculate(parent: ItemsElement?, element: Element, horizontalAlignment: Alignment, verticalAlignment: Alignment, margin: Margin): AlignmentResult {
        if(parent == null) {
            return AlignmentResult(element.position, element.size)
        }

        val offset = parent.getChildOffset()
        val px = 0
        val py = 0
        val ex = element.position.x
        val ey = element.position.y
        val psx = parent.calculatedSize.x - 2 * offset.x
        val psy = parent.calculatedSize.y - offset.y
        val esx = element.size.x
        val esy = element.size.y

        return when(horizontalAlignment) {
            Alignment.START -> {
                when(verticalAlignment) {
                    Alignment.START -> AlignmentResult(
                            element.position + Vector2(margin.left, margin.top),
                            element.size
                    )
                    Alignment.CENTER -> {
                        val posY = py + (psy - esy) / 2.0f + ey + margin.top
                        return AlignmentResult(Vector2(ex + margin.left, posY), element.size)
                    }
                    Alignment.STRETCH -> {
                        return AlignmentResult(Vector2(ex + margin.left, py + margin.top), Vector2(esx, psy - margin.top - margin.bottom))
                    }
                    Alignment.END -> {
                        val posY = psy - ey - margin.bottom
                        return AlignmentResult(Vector2(ex + margin.left, posY), element.size)
                    }
                }
            }
            Alignment.STRETCH -> {
                when(verticalAlignment) {
                    Alignment.START -> {
                        return AlignmentResult(Vector2(px + margin.left, ey + margin.top), Vector2(psx - margin.left - margin.right, esy))
                    }
                    Alignment.CENTER -> {
                        val posY = py + (psy - esy) / 2.0f + margin.top
                        return AlignmentResult(Vector2(px + margin.left, posY), Vector2(psx - margin.left - margin.right, esy))
                    }
                    Alignment.STRETCH -> {
                        return AlignmentResult(Vector2(px + margin.left, py + margin.top), Vector2(psx - margin.left - margin.right, psy - margin.top - margin.bottom))
                    }
                    Alignment.END -> {
                        val posY = psy - esy - margin.bottom
                        return AlignmentResult(Vector2(px + margin.left, posY), Vector2(psx - margin.left - margin.right, esy))
                    }
                }
            }
            Alignment.END -> {
                when(verticalAlignment) {
                    Alignment.START -> {
                        val posX = parent.size.x - element.size.x - margin.right
                        return AlignmentResult(Vector2(posX, element.position.y + margin.top), element.size)
                    }
                    Alignment.CENTER -> {
                        val posX = parent.size.x - element.size.x - margin.right
                        val posY = parent.position.y + (parent.size.y - element.size.y) / 2.0f + margin.left
                        return AlignmentResult(Vector2(posX, posY), element.size)
                    }
                    Alignment.STRETCH -> {
                        val posX = parent.size.x - element.size.x - margin.right
                        return AlignmentResult(Vector2(posX, margin.top), Vector2(element.size.x, parent.size.y - margin.top - margin.bottom))
                    }
                    Alignment.END -> {
                        val posX = parent.size.x - element.size.x - margin.right
                        val posY = parent.size.y - element.size.y - margin.bottom
                        return AlignmentResult(Vector2(posX, posY), element.size)
                    }
                }
            }
            Alignment.CENTER -> {
                when(verticalAlignment) {
                    Alignment.START -> {
                        val posX = (parent.size.x - element.size.x) / 2.0f + element.position.x + margin.left
                        val posY = element.position.y + margin.top
                        return AlignmentResult(Vector2(posX, posY), element.size)
                    }
                    Alignment.CENTER -> {
                        val posX = (parent.size.x - element.size.x) / 2.0f + element.position.x + margin.left
                        val posY = (parent.size.y - element.size.y) / 2.0f + element.position.y + margin.top
                        return AlignmentResult(Vector2(posX, posY), element.size)
                    }
                    Alignment.STRETCH -> {
                        val posX = (parent.size.x - element.size.x) / 2.0f + element.position.x + margin.left
                        return AlignmentResult(Vector2(posX, margin.top), Vector2(element.size.x, parent.size.y - margin.top - margin.bottom))
                    }
                    Alignment.END -> {
                        val posX = (parent.size.x - element.size.x) / 2.0f + element.position.x  + margin.left
                        val posY = parent.size.y - element.size.y - margin.bottom
                        return AlignmentResult(Vector2(posX, posY), element.size)
                    }
                }
            }
        }
    }
}