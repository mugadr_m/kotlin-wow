package ch.cromon.wow.ui.dialogs

import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

interface Dialog {
    val title: String
}

@Component
@Scope("singleton")
class DialogManager {
    fun <T : Dialog> showDialog(dialogClass: Class<T>, modal: Boolean = true): T {
        val viewAnnotation = dialogClass.getAnnotation(ViewPath::class.java)
                ?: throw IllegalArgumentException("Unable to find @ViewPath on class $dialogClass")

        val loader = FXMLLoader(javaClass.getResource(viewAnnotation.resource))
        val element = loader.load<Parent>()
        val controller = loader.getController<T>()
        val scene = Scene(element)
        val stage = Stage()
        stage.scene = scene
        stage.title = controller.title
        if (modal) {
            stage.showAndWait()
        } else {
            stage.show()
        }
        return controller
    }
}