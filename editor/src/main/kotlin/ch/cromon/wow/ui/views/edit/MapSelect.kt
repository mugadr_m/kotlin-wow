package ch.cromon.wow.ui.views.edit

import ch.cromon.wow.ApplicationState
import ch.cromon.wow.gx.BlendMode
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.controller.MapSelectEntry
import ch.cromon.wow.ui.controller.MapSelectLoadEvent
import ch.cromon.wow.ui.elements.ItemsElement
import ch.cromon.wow.ui.elements.MapSelectButton
import ch.cromon.wow.ui.elements.Margin
import ch.cromon.wow.ui.elements.Quad
import ch.cromon.wow.ui.messaging.MouseClickMessage
import ch.cromon.wow.ui.messaging.MouseMoveMessage
import ch.cromon.wow.ui.messaging.MouseWheelMessage
import ch.cromon.wow.ui.messaging.WindowMessage
import ch.cromon.wow.ui.utils.Alignment
import ch.cromon.wow.ui.views.AppStateView
import ch.cromon.wow.ui.views.View
import ch.cromon.wow.utils.pointInQuad
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.awt.Color

data class MapSelectButtonEntry(val button: MapSelectButton, val entry: MapSelectEntry, var isLoaded: Boolean = false)

@Component
@Scope("singleton")
@AppStateView(ApplicationState.MAP_SELECT)
class MapSelectView : View() {
    private lateinit var mapEntries: List<MapSelectEntry>
    private var entriesReady = false

    private val buttons = ArrayList<MapSelectButtonEntry>()

    private lateinit var background: Quad

    private lateinit var scrollBarBackground: Quad

    private lateinit var buttonContainer: ItemsElement

    private var scrollOffset = 0.0f
    private var maxScrollOffset = 0.0f
    private var totalVisibleHeight = 0.0f
    private var clickOffset = 0.0f

    private var isScrollbarSelected = false
    private var isScrollbarHovered = false

    @Autowired
    private lateinit var context: ApplicationContext

    override fun onFrame(context: UiContext) {
        if (entriesReady) {
            onLoadEntries()
            entriesReady = false
        }

        background.onFrame()
        super.onFrame(context)
        scrollBarBackground.onFrame()
    }

    override fun onInitialize() {
        super.onInitialize()

        background = Quad()
        background.blendMode = BlendMode.NONE
        background.color = Color(0x333333)

        buttonContainer = ItemsElement()
        buttonContainer.horizontalAlignment = Alignment.STRETCH
        buttonContainer.verticalAlignment = Alignment.STRETCH
        buttonContainer.margin = Margin(15.0f, 35.0f, 150.0f, 0.0f)

        verticalAlignment = Alignment.STRETCH
        horizontalAlignment = Alignment.STRETCH

        appendChild(buttonContainer)

        scrollBarBackground = Quad()
        scrollBarBackground.blendMode = BlendMode.NONE
        scrollBarBackground.color = Color(0x888888)
    }

    override fun doMessage(message: WindowMessage) {
        super.doMessage(message)

        if (message is MouseClickMessage && !message.pressed) {
            isScrollbarSelected = false
            updateScrollBarColor()
        }

        if (message.isHandled) {
            return
        }

        if (message is MouseClickMessage && message.pressed) {
            isScrollbarSelected = pointInQuad(message.position, scrollBarBackground.position, scrollBarBackground.position + scrollBarBackground.size)
            if (isScrollbarSelected) {
                clickOffset = message.position.y - scrollBarBackground.position.y
                updateScrollBarColor()
            }
        } else if (message is MouseMoveMessage) {
            if (isScrollbarSelected) {
                handleScroll(message)
            }

            val hovered = pointInQuad(message.position, scrollBarBackground.position, scrollBarBackground.position + scrollBarBackground.size)
            if (!isScrollbarHovered && hovered) {
                isScrollbarHovered = true
                updateScrollBarColor()
            } else if (isScrollbarHovered && !hovered) {
                isScrollbarHovered = false
                updateScrollBarColor()
            }
        } else if(message is MouseWheelMessage) {
            scrollOffset -= 150.0f * message.dy
            updateScroll()
        }
    }

    private fun updateScrollBarColor() {
        if(isScrollbarSelected) {
            if(isScrollbarHovered) {
                scrollBarBackground.color = Color(0xEEEEEE)
            } else {
                scrollBarBackground.color = Color(0xCCCCCC)
            }
        } else {
            if(isScrollbarHovered) {
                scrollBarBackground.color = Color(0xAAAAAA)
            } else {
                scrollBarBackground.color = Color(0x888888)
            }
        }
    }

    @EventListener
    fun onMapSelectReady(event: MapSelectLoadEvent) {
        mapEntries = event.entries
        entriesReady = true
    }

    override fun positionChanged() {
        super.positionChanged()
        sizePositionChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        sizePositionChanged()
    }

    private fun onLoadEntries() {
        for (entry in mapEntries) {
            val button = context.getBean(MapSelectButton::class.java)
            buttonContainer.appendChild(button)
            button.size = Vector2(200, 200)
            buttons.add(MapSelectButtonEntry(button, entry))
        }

        sizePositionChanged()
    }

    private fun handleScroll(message: MouseMoveMessage) {
        val scrollPixelOffset = message.position.y - clickOffset - buttonContainer.calculatedPosition.y

        scrollOffset = scrollPixelOffset * (totalVisibleHeight / buttonContainer.calculatedSize.y)
        updateScroll()
    }

    private fun updateScroll() {
        scrollOffset = Math.max(Math.min(scrollOffset, maxScrollOffset), 0.0f)
        scrollBarBackground.position.y = buttonContainer.calculatedPosition.y + (Math.max(Math.min(scrollOffset, maxScrollOffset), 0.0f) / totalVisibleHeight) * (buttonContainer.calculatedSize.y)
        scrollBarBackground.updateSizePosition()
        updateButtons()
    }

    private fun sizePositionChanged() {
        val totalHeight = updateButtons()
        totalVisibleHeight = totalHeight

        val containerHeight = buttonContainer.calculatedSize.y
        val visiblePct = containerHeight / (totalHeight + 200)
        maxScrollOffset = (1.0f - visiblePct) * (totalHeight + 200)

        scrollBarBackground.size = Vector2(15.0f, (visiblePct * containerHeight))
        scrollBarBackground.position = Vector2(calculatedPosition.x + calculatedSize.x - 20.0f,
                buttonContainer.calculatedPosition.y + (Math.max(Math.min(scrollOffset, maxScrollOffset), 0.0f) / totalVisibleHeight) * (buttonContainer.calculatedSize.y))
    }

    private fun updateButtons(): Float {
        val width = buttonContainer.calculatedSize.x
        val margin = 15.0f
        var curOffsetX = 0.0f
        var curOffsetY = 0.0f - Math.max(Math.min(scrollOffset, maxScrollOffset), 0.0f)
        var totalHeight = 0.0f

        val dimension = buttonContainer.calculatedSize
        val start = Vector2()
        for (buttonEntry in buttons) {
            val buttonStart = Vector2(curOffsetX, curOffsetY)
            if (!buttonEntry.isLoaded) {
                val buttonEnd = buttonStart + Vector2(200, 200)
                if (pointInQuad(buttonStart, start, dimension) || pointInQuad(buttonEnd, start, dimension)) {
                    buttonEntry.button.load(buttonEntry.entry)
                    buttonEntry.isLoaded = true
                }
            }
            buttonEntry.button.position = buttonStart
            curOffsetX += 200 + margin
            if (curOffsetX + 200 >= width - margin) {
                curOffsetX = 0.0f
                curOffsetY += 200 + margin
                totalHeight += 200 + margin
            }
        }

        background.position = calculatedPosition
        background.size = calculatedSize

        return totalHeight
    }
}