package ch.cromon.wow.ui.elements

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.messaging.MouseClickMessage
import ch.cromon.wow.ui.messaging.MouseMoveMessage
import ch.cromon.wow.ui.messaging.WindowMessage
import ch.cromon.wow.utils.pointInQuad
import java.awt.Color


class LinkLabel : Label() {
    companion object {
        private val COLOR_REGULAR = Color(0x3F7FFF)
        private val COLOR_HOVER = Color(0x60A0FF)
        private val COLOR_CLICK = Color(0x4080FF)
        private val COLOR_CLICK_HOVER = Color(0x5090FF)
    }

    private var isHovered = false
    private var isClicked = false

    var callback: (() -> Unit)? = null

    override fun onInitialize() {
        super.onInitialize()

        textRender.color = COLOR_REGULAR
    }

    override fun doMessage(message: WindowMessage) {
        when(message) {
            is MouseMoveMessage -> handleMouseMove(message)
            is MouseClickMessage -> if(message.pressed) handleMouseDown(message) else handleMouseUp(message)
        }
    }

    private fun handleMouseMove(message: MouseMoveMessage) {
        if(message.isHandled) {
            isHovered = false
            isClicked = false
            return
        }

        val hasHover = pointInQuad(message.position, calculatedPosition, calculatedPosition + Vector2(textWidth, textHeight))
        if(hasHover && !isHovered) {
            isHovered = true
            textRender.color = if(isClicked) COLOR_CLICK_HOVER else COLOR_HOVER
        } else if(!hasHover && isHovered) {
            isHovered = false
            textRender.color = if(isClicked) COLOR_CLICK else COLOR_REGULAR
        }
    }

    private fun handleMouseDown(message: MouseClickMessage) {
        if(message.isHandled) {
            return
        }

        if(isHovered && !isClicked) {
            isClicked = true
            textRender.color = COLOR_CLICK_HOVER
        }
    }

    private fun handleMouseUp(message: MouseClickMessage) {
        if(message.isHandled) {
            return
        }

        if(isHovered && isClicked) {
            callback?.invoke()
        }

        if(isClicked) {
            isClicked = false
            textRender.color = if(isHovered) COLOR_HOVER else COLOR_REGULAR
        }
    }
}