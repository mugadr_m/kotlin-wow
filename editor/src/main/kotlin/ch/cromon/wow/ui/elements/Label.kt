package ch.cromon.wow.ui.elements

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.text.StaticText
import java.awt.Font
import kotlin.properties.Delegates


open class Label : Element() {
    protected var textRender = StaticText(Font("Arial", Font.BOLD, 20))

    private var width = -1

    var fontSize by Delegates.observable(20, { _, _, _ -> fontSizeChanged() })
    var text by Delegates.observable("", { _, _, _ -> textChanged() })


    init {
        textRender.isMultiLine = false
    }

    val textHeight get() = textRender.textHeight
    val textWidth get() = textRender.textWidth

    override fun onFrame(context: UiContext) {
        var clipped = false
        if(width > 0) {
            context.clipStack.push(calculatedPosition, calculatedPosition + Vector2(width.toFloat(), textRender.textHeight))
            clipped = true
        }

        textRender.onFrame()

        if(clipped) {
            context.clipStack.pop()
        }
    }

    private fun fontSizeChanged() {
        val color = textRender.color
        textRender = StaticText(Font("Arial", Font.BOLD, fontSize))
        textRender.text = text
        textRender.position = calculatedPosition
        textRender.color = color
        size = Vector2(calculatedSize.x, textHeight)
    }

    private fun textChanged() {
        textRender.text = text
        size = Vector2(calculatedSize.x, textHeight)
    }

    override fun positionChanged() {
        super.positionChanged()
        textRender.position = calculatedPosition
    }

}