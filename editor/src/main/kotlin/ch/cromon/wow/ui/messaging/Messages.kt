package ch.cromon.wow.ui.messaging

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext


abstract class WindowMessage(val context: UiContext) {
    open fun forwardMessage(position: Vector2): WindowMessage = this

    var isHandled = false
}

enum class MouseButton {
    LEFT,
    RIGHT,
    MIDDLE,
    UNKNOWN
}

open class MouseMessage(val position: Vector2, context: UiContext) : WindowMessage(context)

class MouseClickMessage(position: Vector2, val button: MouseButton, val pressed: Boolean, context: UiContext) : MouseMessage(position, context) {
    override fun forwardMessage(position: Vector2): WindowMessage {
        val ret = MouseClickMessage(this.position - position, button, pressed, context)
        ret.isHandled = this.isHandled
        return ret
    }
}

class MouseMoveMessage(position: Vector2, context: UiContext) : MouseMessage(position, context) {
    override fun forwardMessage(position: Vector2): WindowMessage {
        val ret = MouseMoveMessage(this.position - position, context)
        ret.isHandled = this.isHandled
        return ret
    }
}

open class KeyboardMessage(val character: Char, val charCode: Short, context: UiContext) : WindowMessage(context)

class KeyPressMessage(chr: Char, chrCode: Short, val isRepeat: Boolean, context: UiContext): KeyboardMessage(chr, chrCode, context)

class KeyReleaseMessage(chr: Char, chrCode: Short, context: UiContext): KeyboardMessage(chr, chrCode, context)

class MouseWheelMessage(position: Vector2, val dx: Float, val dy: Float, context: UiContext) : MouseMessage(position, context)