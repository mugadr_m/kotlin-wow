package ch.cromon.wow.ui.views.world

import ch.cromon.wow.AppStateChangedEvent
import ch.cromon.wow.ApplicationState
import ch.cromon.wow.gx.BlendMode
import ch.cromon.wow.io.AsyncImageLoader
import ch.cromon.wow.io.ImageSource
import ch.cromon.wow.io.data.DataLoadEvent
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.scene.terrain.MapLoadProgressEvent
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.controller.MapController
import ch.cromon.wow.ui.elements.Image
import ch.cromon.wow.ui.elements.Quad
import ch.cromon.wow.ui.utils.Alignment
import ch.cromon.wow.ui.views.AppStateView
import ch.cromon.wow.ui.views.View
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.awt.Color
import java.util.concurrent.CompletableFuture


@Component
@Scope("singleton")
@AppStateView(ApplicationState.MAP_LOAD)
class MapLoadView : View() {
    @Autowired
    private lateinit var controller: MapController

    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var imageLoader: AsyncImageLoader

    private lateinit var background: Quad

    private lateinit var loadImage: Image
    private lateinit var loadBarImage: Image
    private lateinit var loadBarBackgroundImage: Image
    private lateinit var loadBarFill: Image

    private var normalScreen: ImageSource? = null
    private var wideScreen: ImageSource? = null

    private var activeImageSource: ImageSource? = null

    private var loadBarPct = 0.0f
    private var hasPctChanged = false

    override fun onFrame(context: UiContext) {
        if(hasPctChanged) {
            hasPctChanged = false
            updateLoadBar()
        }

        background.onFrame()
        super.onFrame(context)
    }

    override fun onInitialize() {
        super.onInitialize()

        verticalAlignment = Alignment.STRETCH
        horizontalAlignment = Alignment.STRETCH

        loadImage = Image()
        loadBarImage = Image()
        loadBarBackgroundImage = Image()
        loadBarFill = Image()

        appendChild(loadImage)
        appendChild(loadBarBackgroundImage)
        appendChild(loadBarFill)
        appendChild(loadBarImage)

        background = Quad()
        background.blendMode = BlendMode.NONE
        background.color = Color(0x333333)
        background.position = Vector2(0, 0)
    }

    @EventListener
    fun onLoadProgress(event: MapLoadProgressEvent) {
        loadBarPct = event.loaded.toFloat() / event.total.toFloat()
        loadBarPct = Math.max(Math.min(loadBarPct, 1.0f), 0.0f)
        hasPctChanged = true
    }

    @EventListener
    fun onDataLoaded(event: DataLoadEvent) {
        if(!event.success) {
            return
        }

        imageLoader.loadImage("Interface\\Glues\\LoadingBar\\Loading-BarBorder.blp").thenAccept {
            loadBarImage.imageSource = it
        }

        imageLoader.loadImage("Interface\\Glues\\COMMON\\Glue-Tooltip-Background.blp").thenAccept( {
            loadBarBackgroundImage.imageSource = it
        })

        imageLoader.loadImage("Interface\\Glues\\LoadingBar\\Loading-BarFill.blp").thenAccept( {
            loadBarFill.imageSource = it
        })
    }

    @EventListener
    fun stateChanged(event: AppStateChangedEvent) {
        if (event.newState != ApplicationState.MAP_LOAD) {
            return
        }

        val selectedMap = controller.currentMap
        val (normal, wide) = controller.getLoadingScreenInfo(selectedMap.mapId)
        val normalFuture = imageLoader.loadImage(normal)
        val wideFuture = if (wide != null) imageLoader.loadImage(wide) else CompletableFuture.completedFuture(null as ImageSource?)
        CompletableFuture.allOf(normalFuture, wideFuture).handle { _, error ->
            if (error != null) {
                logger.warn("Failed to load loading screens", error)
                wideScreen = null
                normalScreen = null
            } else {
                normalScreen = normalFuture.get()
                wideScreen = wideFuture.get()
            }

            onLoadImages()
        }
    }

    override fun sizeChanged() {
        super.sizeChanged()
        updateImageSize()
        background.size = calculatedSize
    }

    private fun onLoadImages() {
        if (normalScreen == null && wideScreen == null) {
            loadImage.imageSource = null
            return
        }

        val ratio = calculatedSize.x / calculatedSize.y
        val nonWideRatio = 4.0f / 3.0f
        val wideRatio = 16.0f / 9.0f
        val diffWide = Math.abs(ratio - wideRatio)
        val diffNormal = Math.abs(ratio - nonWideRatio)
        loadImage.imageSource = if (diffWide < diffNormal) {
            wideScreen ?: normalScreen
        } else {
            normalScreen ?: wideScreen
        }

        activeImageSource = loadImage.imageSource
        updateImageSize()
    }

    private fun updateImageSize() {
        val source = activeImageSource ?: return

        val ratio = calculatedSize.x / calculatedSize.y
        val nonWideRatio = 4.0f / 3.0f
        val wideRatio = 16.0f / 9.0f
        val diffWide = Math.abs(ratio - wideRatio)
        val diffNormal = Math.abs(ratio - nonWideRatio)
        val useWide = diffWide < diffNormal
        if(activeImageSource != wideScreen && wideScreen != null && useWide) {
            loadImage.imageSource = wideScreen
            activeImageSource = wideScreen
        } else if(activeImageSource != normalScreen && normalScreen != null && !useWide) {
            loadImage.imageSource = normalScreen
            activeImageSource = normalScreen
        }

        val aspect = if (source == wideScreen) 16.0f / 9.0f else 4.0f / 3.0f

        val w = source.width.toFloat() * aspect
        val h = source.height.toFloat()

        val maxW = calculatedSize.x
        val maxH = calculatedSize.y

        val facX = maxW / w
        val facY = maxH / h

        val scale = Math.min(facX, facY)
        val scaledW = w * scale
        val scaledH = h * scale
        loadImage.size = Vector2(scaledW, scaledH)
        loadImage.position = Vector2((calculatedSize.x - scaledW) / 2.0f, (calculatedSize.y - scaledH) / 2.0f)

        updateLoadBar()
    }

    private fun updateLoadBar() {
        val source = activeImageSource ?: return
        val aspect = if (source == wideScreen) 16.0f / 9.0f else 4.0f / 3.0f

        loadBarImage.imageSource?.let {
            val loadBarWidth = it.width * aspect
            val loadBarHeight = it.height.toFloat()
            val posX = (calculatedSize.x - loadBarWidth) / 2.0f
            val posY = 0.8f * calculatedSize.y - loadBarHeight / 2.0f
            loadBarImage.position = Vector2(posX, posY)
            loadBarImage.size = Vector2(loadBarWidth, loadBarHeight)
            loadBarBackgroundImage.position = Vector2(posX + 30 * aspect, posY + 15)
            loadBarBackgroundImage.size = Vector2(loadBarWidth - 60 * aspect, loadBarHeight - 30)

            loadBarFill.position = Vector2(posX + 30 * aspect, posY + 10)
            loadBarFill.size = Vector2((loadBarWidth - 60 * aspect) * loadBarPct, loadBarHeight - 20)
        }
    }
}