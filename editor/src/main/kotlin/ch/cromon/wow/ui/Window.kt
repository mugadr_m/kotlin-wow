package ch.cromon.wow.ui

import ch.cromon.wow.gx.GraphicsLoadedEvent
import ch.cromon.wow.gx.GxContext
import ch.cromon.wow.io.ImageSource
import ch.cromon.wow.io.files.texture.TextureManager
import ch.cromon.wow.io.input.Keyboard
import ch.cromon.wow.io.input.Mouse
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.scene.WorldFrame
import ch.cromon.wow.ui.messaging.*
import ch.cromon.wow.ui.utils.FpsCounter
import org.lwjgl.BufferUtils
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.glfw.GLFWImage
import org.lwjgl.opengl.GL
import org.lwjgl.system.MemoryUtil.NULL
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component
import java.util.*
import javax.annotation.PostConstruct

data class WindowResizeEvent(val dimension: Vector2, val window: Window) : ApplicationEvent(window)

@Component
class Window @Autowired constructor(private val context: GxContext) {
    companion object {
        private val buildProperties = Properties()
        private const val BUILD_VERSION_KEY = "gradle.build.version"
        private const val BUILD_TIME_KEY = "gradle.build.time"
        private const val GIT_HASH_KEY = "git.hash"

        init {
            buildProperties.load(Window::class.java.getResourceAsStream("/build.properties"))
        }
    }

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var uiManager: UiManager

    @Autowired
    private lateinit var worldFrame: WorldFrame

    @Autowired
    private lateinit var mouse: Mouse

    @Autowired
    private lateinit var keyboard: Keyboard

    @Autowired
    private lateinit var textureManager: TextureManager

    private var window: Long = 0

    @PostConstruct
    fun initialize() {
        logger.info("Initializing window subsystem...")

        glfwInit()
        glfwWindowHint(GLFW_SAMPLES, 8)
        window = glfwCreateWindow(1024, 768, getWindowTitle(), NULL, NULL)
        glfwSetWindowIcon(window, getWindowIcons())

        glfwSetWindowSizeCallback(window, { _, w, h ->
            reshape(w, h)
        })

        init()
    }

    fun runLoop() {
        publisher.publishEvent(GraphicsLoadedEvent(context, this))
        worldFrame.initialize()
        triggerResize()
        while (!glfwWindowShouldClose(window)) {
            glfwPollEvents()
            FpsCounter.onFrame()
            textureManager.onFrame()
            context.beginFrame()
            worldFrame.onFrame()
            uiManager.onFrame()
            context.endFrame()
            glfwSwapBuffers(window)
        }

        glfwHideWindow(window)
    }

    fun show() {
        glfwShowWindow(window)
    }

    private fun reshape(width: Int, height: Int) {
        publisher.publishEvent(WindowResizeEvent(Vector2(width, height), this))
    }

    private fun triggerResize() {
        val w = intArrayOf(0)
        val h = intArrayOf(0)
        glfwGetWindowSize(window, w, h)
        reshape(w[0], h[0])
    }

    private fun init() {
        glfwMakeContextCurrent(window)
        GL.createCapabilities()
        glfwSwapInterval(1)
        context.onInitialize()
        uiManager.onInitialize()
        loadEventHandlers()
    }

    private fun loadEventHandlers() {
        glfwSetCursorPosCallback(window, { _, x, y ->
            onMouseMove(x, y)
        })

        glfwSetMouseButtonCallback(window, { _, button, action, _ ->
            onMouseClickEvent(button, action == GLFW_PRESS)
        })

        glfwSetCharCallback(window, { _, codePoint ->
            onKeyEvent(codePoint.toChar(), true, false)
        })

        glfwSetKeyCallback(window, { _, key, _, action, _ ->
            onKeyEvent(key, action == GLFW_PRESS || action == GLFW_REPEAT, action == GLFW_REPEAT)
        })

        glfwSetScrollCallback(window, { _, dx, dy ->
            onScroll(dx.toFloat(), dy.toFloat())
        })
    }

    private fun onMouseMove(x: Double, y: Double) {
        mouse.position = Vector2(x.toFloat(), y.toFloat())
        val message = MouseMoveMessage(Vector2(x, y), uiManager.context)
        uiManager.handleMessage(message)
    }

    private fun onMouseClickEvent(buttonRaw: Int, isPressed: Boolean) {
        val button = when (buttonRaw) {
            GLFW_MOUSE_BUTTON_1 -> MouseButton.LEFT
            GLFW_MOUSE_BUTTON_2 -> MouseButton.RIGHT
            GLFW_MOUSE_BUTTON_3 -> MouseButton.MIDDLE
            else -> MouseButton.UNKNOWN
        }

        val x = doubleArrayOf(0.0)
        val y = doubleArrayOf(0.0)
        glfwGetCursorPos(window, x, y)
        mouse.setButton(button, isPressed)

        val message = MouseClickMessage(Vector2(x[0], y[0]), button, isPressed, uiManager.context)
        uiManager.handleMessage(message)
    }

    private fun onKeyEvent(scanCode: Int, pressed: Boolean, repeated: Boolean) {
        if (uiManager.context.inputFocus == null) {
            keyboard.setKeyState(scanCode.toShort(), pressed)
        }

        if (pressed) {
            uiManager.handleMessage(KeyPressMessage('\u0000', scanCode.toShort(), repeated, uiManager.context))
        } else {
            uiManager.handleMessage(KeyReleaseMessage('\u0000', scanCode.toShort(), uiManager.context))
        }
    }

    private fun onKeyEvent(keyChar: Char, pressed: Boolean, repeated: Boolean) {
        if (pressed) {
            uiManager.handleMessage(KeyPressMessage(keyChar, 0, repeated, uiManager.context))
        } else {
            uiManager.handleMessage(KeyReleaseMessage(keyChar, 0, uiManager.context))
        }
    }

    private fun onScroll(dx: Float, dy: Float) {
        val x = doubleArrayOf(0.0)
        val y = doubleArrayOf(0.0)
        glfwGetCursorPos(window, x, y)
        uiManager.handleMessage(MouseWheelMessage(Vector2(x[0], y[0]), dx, dy, uiManager.context))
    }

    private fun getWindowTitle(): String {
        val version = buildProperties.getProperty(BUILD_VERSION_KEY)
        val buildTime = buildProperties.getProperty(BUILD_TIME_KEY)
        val gitHash = buildProperties.getProperty(GIT_HASH_KEY)

        return "WoW-Editor -- Version: $version, Build: $buildTime, Commit: $gitHash"
    }

    private fun getWindowIcons(): GLFWImage.Buffer {
        val ret = GLFWImage.calloc(1)
        val image = GLFWImage.calloc()
        ImageSource.fromImageResource("application_icon.png").run {
            val nativeBuffer = BufferUtils.createByteBuffer(buffer.size)
            nativeBuffer.put(buffer).flip()
            image.set(width, height, nativeBuffer)
        }
        ret.put(0, image)
        return ret
    }
}