package ch.cromon.wow.ui.quickmenu

import ch.cromon.wow.gx.BlendMode
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.elements.Border
import ch.cromon.wow.ui.elements.ItemsElement
import ch.cromon.wow.ui.elements.Quad
import ch.cromon.wow.ui.quickmenu.model.Node
import ch.cromon.wow.ui.text.StaticText
import java.awt.Color
import java.awt.Font


class MenuNode(val shortcut: Int, private val description: String) : ItemsElement() {
    companion object {
        private val FONT = Font("Arial", Font.BOLD, 20)
    }

    private lateinit var textRender: StaticText
    private lateinit var background: Quad
    private lateinit var border: Border

    fun loadChildren(parentAngle: Float, children: List<Node>) {
        if(children.isEmpty()) {
            return
        }

        val perChildAngle = Math.PI / (children.size + 1)
        val baseOffset = Math.PI / 2.0f
        var index = 1
        for((childIndex, child) in children.withIndex()) {
            val entry = MenuNode(index, child.description)
            ++index
            appendChild(entry)
            val angle = -baseOffset + (childIndex + 1) * perChildAngle + parentAngle
            val x = Math.cos(angle)
            val y = Math.sin(angle)
            entry.position = Vector2(x * 200, y * 200)
            entry.loadChildren(parentAngle, child.children)
        }
    }

    override fun onInitialize() {
        super.onInitialize()
        textRender = StaticText(FONT)
        textRender.text = description

        border = Border()
        background = Quad()
        background.blendMode = BlendMode.ALPHA
        background.color = Color(0xAA333333.toInt(), true)

        visible = false
    }

    override fun onFrame(context: UiContext) {
        if(!visible) {
            return
        }

        background.onFrame()
        border.onFrame()
        textRender.onFrame()

        super.onFrame(context)
    }

    override fun positionChanged() {
        super.positionChanged()
        handleReposition()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        handleReposition()
    }

    private fun handleReposition() {
        background.position = calculatedPosition
        background.size = Vector2(20 + textRender.textWidth, 35.0f)
        border.topLeft = calculatedPosition
        border.bottomRight = calculatedPosition + Vector2(20 + textRender.textWidth, 35.0f)
        textRender.position = calculatedPosition + Vector2(10.0f, 5.0f)
    }
}