package ch.cromon.wow.ui.views.init

import ch.cromon.wow.ApplicationState
import ch.cromon.wow.gx.BlendMode
import ch.cromon.wow.io.data.DataLoadProgressEvent
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.elements.Label
import ch.cromon.wow.ui.elements.Quad
import ch.cromon.wow.ui.utils.Alignment
import ch.cromon.wow.ui.views.AppStateView
import ch.cromon.wow.ui.views.View
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.awt.Color


@Component
@Scope("singleton")
@AppStateView(ApplicationState.DATA_LOAD)
class DataSelectLoad : View() {
    private lateinit var statusText: Label
    private lateinit var background: Quad

    override fun onInitialize() {
        super.onInitialize()

        horizontalAlignment = Alignment.STRETCH
        verticalAlignment = Alignment.STRETCH

        statusText = Label()
        appendChild(statusText)

        statusText.fontSize = 26
        statusText.verticalAlignment = Alignment.CENTER
        statusText.horizontalAlignment = Alignment.CENTER

        background = Quad()
        background.blendMode = BlendMode.NONE
        background.color = Color(0x333333)
    }

    override fun onFrame(context: UiContext) {
        background.onFrame()

        super.onFrame(context)
    }

    @EventListener
    fun loadEventListener(event: DataLoadProgressEvent) {
        statusText.text = event.message
        handleSizeTextChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        handleSizeTextChanged()
        background.position = calculatedPosition
        background.size = calculatedSize
    }

    private fun handleSizeTextChanged() {
        val textWidth = statusText.textWidth
        val offsetX = calculatedPosition.x + (calculatedSize.x - textWidth) / 2.0f
        val offsetY = calculatedPosition.y + (calculatedSize.y - statusText.textHeight) / 2.0f
        statusText.position = Vector2(offsetX, offsetY)
    }
}