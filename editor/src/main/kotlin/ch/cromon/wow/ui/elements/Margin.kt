package ch.cromon.wow.ui.elements


class Margin(val left: Float, val right: Float, val top: Float, val bottom: Float) {
    constructor(margin: Float) : this(margin, margin, margin, margin)
    constructor(x: Float, y: Float): this(x, x, y, y)
}