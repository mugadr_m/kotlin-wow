package ch.cromon.wow.ui.elements

import ch.cromon.wow.gx.BlendMode
import ch.cromon.wow.io.AsyncImageLoader
import ch.cromon.wow.io.LittleEndianStream
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.controller.MapController
import ch.cromon.wow.ui.controller.MapSelectEntry
import ch.cromon.wow.ui.messaging.MouseButton
import ch.cromon.wow.ui.messaging.MouseClickMessage
import ch.cromon.wow.ui.messaging.MouseMoveMessage
import ch.cromon.wow.ui.messaging.WindowMessage
import ch.cromon.wow.utils.pointInQuad
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.awt.Color

@Component
@Scope("prototype")
class MapSelectButton : Element() {
    private lateinit var caption: MultiLineLabel
    private lateinit var background: Image

    private lateinit var mapName: String
    private var screenImage: LittleEndianStream? = null
    private lateinit var entry: MapSelectEntry
    private var isLoaded = false

    private lateinit var backdrop: Quad

    private var isExpanded = false
    private var topLeft = Vector2()
    private var bottomRight = Vector2()

    private var textTopLeft = Vector2()
    private var textBottomRight = Vector2()

    @Autowired
    private lateinit var imageLoader: AsyncImageLoader

    @Autowired
    private lateinit var controller: MapController

    fun load(entry: MapSelectEntry) {
        mapName = entry.displayName
        caption.text = mapName
        entry.loadScreen().thenAccept({
            screenImage = it
        })
        this.entry = entry
    }

    override fun doMessage(message: WindowMessage) {
        if (message is MouseMoveMessage) {
            if (message.isHandled) {
                if (isExpanded) {
                    isExpanded = false
                    sizePositionChanged()
                }
            } else {
                val hovered = pointInQuad(message.position, topLeft, bottomRight)
                if (hovered && !isExpanded) {
                    isExpanded = true
                    sizePositionChanged()
                } else if (!hovered && isExpanded) {
                    isExpanded = false
                    sizePositionChanged()
                }
                if (hovered) {
                    message.isHandled = true
                }
            }
        } else if (message is MouseClickMessage) {
            if (!message.isHandled && message.pressed && message.button == MouseButton.LEFT) {
                val hovered = pointInQuad(message.position, topLeft, bottomRight)
                if (hovered) {
                    controller.onMapSelected(entry)
                }
            }
        }
    }

    override fun onFrame(context: UiContext) {
        if(screenImage != null && !isLoaded) {
            loadImage()
        }

        background.onFrame(context)
        backdrop.onFrame()
        context.clipStack.push(textTopLeft, textBottomRight)
        caption.onFrame(context)
        context.clipStack.pop()
    }

    override fun onInitialize() {
        super.onInitialize()

        caption = MultiLineLabel()
        caption.onInitialize()
        caption.text = "<Unknown Map>"
        caption.size = Vector2(190, 190)

        background = Image()
        background.onInitialize()

        background.size = Vector2(200, 200)
        backdrop = Quad()
        backdrop.size = Vector2(200, 70)
        backdrop.blendMode = BlendMode.ALPHA
        backdrop.color = Color(0xDD333333.toInt(), true)
    }

    override fun positionChanged() {
        super.positionChanged()
        sizePositionChanged()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        sizePositionChanged()
    }

    private fun sizePositionChanged() {
        background.position = calculatedPosition
        if (!isExpanded) {
            backdrop.position = calculatedPosition + Vector2(0, 130)
            backdrop.size = Vector2(calculatedSize.x, 70.0f)
            val height = Math.min(caption.textHeight, 60.0f)
            caption.position = calculatedPosition + Vector2(5.0f, 130 + (70 - height) / 2.0f)
            textTopLeft = caption.position
            textBottomRight = caption.position + Vector2(calculatedSize.x - 10, 60.0f)
        } else {
            backdrop.position = calculatedPosition
            backdrop.size = calculatedSize
            caption.position = calculatedPosition + Vector2(5, 5)
            textTopLeft = caption.position
            textBottomRight = caption.position + caption.size
        }

        topLeft = calculatedPosition
        bottomRight = calculatedPosition + calculatedSize
    }

    private fun loadImage() {
        isLoaded = true
        imageLoader.loadImageToSize(screenImage, 200, 200).thenAccept({
            background.imageSource = it
        })
    }
}