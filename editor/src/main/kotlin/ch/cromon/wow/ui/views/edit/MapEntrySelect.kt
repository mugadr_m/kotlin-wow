package ch.cromon.wow.ui.views.edit

import ch.cromon.wow.AppStateManager
import ch.cromon.wow.ApplicationState
import ch.cromon.wow.gx.BlendMode
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.UiContext
import ch.cromon.wow.ui.controller.MapController
import ch.cromon.wow.ui.controller.MapSelectEntry
import ch.cromon.wow.ui.controller.MapSelectedEvent
import ch.cromon.wow.ui.controller.PointOfInterest
import ch.cromon.wow.ui.elements.Button
import ch.cromon.wow.ui.elements.Image
import ch.cromon.wow.ui.elements.Quad
import ch.cromon.wow.ui.messaging.MouseButton
import ch.cromon.wow.ui.messaging.MouseClickMessage
import ch.cromon.wow.ui.messaging.MouseMoveMessage
import ch.cromon.wow.ui.messaging.WindowMessage
import ch.cromon.wow.ui.text.StaticText
import ch.cromon.wow.ui.utils.Alignment
import ch.cromon.wow.ui.views.AppStateView
import ch.cromon.wow.ui.views.View
import ch.cromon.wow.utils.MAP_MID_POINT
import ch.cromon.wow.utils.TILE_SIZE
import ch.cromon.wow.utils.format
import ch.cromon.wow.utils.pointInQuad
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.awt.Color
import java.awt.Font
import java.util.concurrent.CompletableFuture

data class PoiImage(val image: Image, val highlight: Image, val poi: PointOfInterest)

@Component
@Scope("singleton")
@AppStateView(ApplicationState.MAP_ENTRY_SELECT)
class MapEntrySelect : View() {
    @Autowired
    private lateinit var controller: MapController

    @Autowired
    private lateinit var appStateManager: AppStateManager

    private lateinit var wdlImage: Image

    private lateinit var background: Quad

    private val poiImages = ArrayList<PoiImage>()
    private val poiLocations = ArrayList<PointOfInterest>()
    private var loadPoiImages = false

    private lateinit var poiCaption: StaticText
    private lateinit var poiBackground: Quad
    private var selectedPoi: PointOfInterest? = null

    private lateinit var selectedAdtLabel: StaticText
    private lateinit var selectedAdt: StaticText

    private lateinit var selectedPositionLabel: StaticText
    private lateinit var selectedPosition: StaticText

    private lateinit var backButton: Button

    private lateinit var mapSelectEntry: MapSelectEntry

    override fun doMessage(message: WindowMessage) {
        super.doMessage(message)
        if (message.isHandled || appStateManager.currentState != ApplicationState.MAP_ENTRY_SELECT) {
            return
        }

        if (message is MouseMoveMessage) {
            val pos = message.position
            updateSelectedAdt(pos)
            val selection = selectedPoi
            selectedPoi = null
            for (poi in poiImages) {
                val hovered = pointInQuad(pos, poi.image.calculatedPosition, poi.image.calculatedPosition + poi.image.calculatedSize)
                if (hovered) {
                    selectedPoi = poi.poi
                    if (poi.poi != selection) {
                        poiCaption.text = poi.poi.name
                    }

                    poiCaption.position = Vector2(pos.x - poiCaption.textWidth / 2.0f, pos.y + 25)
                    poiBackground.position = Vector2(pos.x - poiCaption.textWidth / 2.0f - 5, pos.y + 20)
                    poiBackground.size = Vector2(poiCaption.textWidth + 10, poiCaption.textHeight + 10)
                    break
                }
            }
        } else if(message is MouseClickMessage) {
            if(message.pressed && message.button == MouseButton.LEFT) {
                val pos = message.position
                val dx = (pos.x - wdlImage.calculatedPosition.x) / wdlImage.calculatedSize.x
                val dy = (pos.y - wdlImage.calculatedPosition.y) / wdlImage.calculatedSize.y
                if (dx >= 0 && dy >= 0 && dx < 1.0 && dy < 1.0f) {
                    val entryPos = Vector2(dx * 64 * TILE_SIZE, dy * 64 * TILE_SIZE)
                    controller.onEnterWorld(mapSelectEntry, entryPos)
                }
            }
        }
    }

    override fun onInitialize() {
        super.onInitialize()

        background = Quad()
        background.blendMode = BlendMode.NONE
        background.color = Color(0x333333)

        poiCaption = StaticText(Font("Arial", Font.BOLD, 20))
        poiBackground = Quad()
        poiBackground.blendMode = BlendMode.ALPHA
        poiBackground.color = Color(0xDD333333.toInt(), true)

        wdlImage = Image()
        appendChild(wdlImage)
        wdlImage.size = Vector2(64 * 17, 64 * 17)

        verticalAlignment = Alignment.STRETCH
        horizontalAlignment = Alignment.STRETCH

        selectedAdt = StaticText(Font("Arial", Font.BOLD, 20))
        selectedAdt.position = Vector2(15, 150)

        selectedAdtLabel = StaticText(Font("Arial", Font.BOLD, 20))
        selectedAdtLabel.position = Vector2(15, 120)
        selectedAdtLabel.text = "Selected ADT:"

        selectedPosition = StaticText(Font("Arial", Font.BOLD, 20))
        selectedPosition.position = Vector2(15, 250)

        selectedPositionLabel = StaticText(Font("Arial", Font.BOLD, 20))
        selectedPositionLabel.position = Vector2(15, 220)
        selectedPositionLabel.text = "Selected Position:"

        backButton = Button()
        backButton.caption = "Back"
        backButton.position = Vector2(15, 350)

        backButton.onClick += {
            wdlImage.imageSource = null
            controller.switchToMapSelect()
        }

        appendChild(backButton)
    }

    override fun onFrame(context: UiContext) {
        if (loadPoiImages) {
            loadPoiImages()
            loadPoiImages = false
        }

        background.onFrame()

        super.onFrame(context)

        if (selectedPoi != null) {
            poiBackground.onFrame()
            poiCaption.onFrame()
        }

        selectedAdtLabel.onFrame()
        selectedAdt.onFrame()

        selectedPositionLabel.onFrame()
        selectedPosition.onFrame()
    }

    @EventListener
    fun onMapSelected(event: MapSelectedEvent) {
        mapSelectEntry = event.entry
        poiLocations.clear()
        poiLocations.addAll(event.pointOfInterests)
        loadPoiImages = true
        CompletableFuture.runAsync {
            wdlImage.imageSource = controller.loadWdlImage(event.entry)
        }
    }

    override fun positionChanged() {
        super.positionChanged()
        sizePositionChange()
    }

    override fun sizeChanged() {
        super.sizeChanged()
        sizePositionChange()
    }

    private fun sizePositionChange() {
        val maxW = calculatedSize.x - 300
        val maxH = calculatedSize.y

        val dimension = Math.min(maxW, maxH)
        wdlImage.size = Vector2(dimension, dimension)
        wdlImage.position = Vector2(300, 0)

        background.position = calculatedPosition
        background.size = calculatedSize

        positionImages()
    }

    private fun positionImages() {
        for (poi in poiImages) {
            val pctX = (-poi.poi.position.y + MAP_MID_POINT) / (64.0f * TILE_SIZE)
            val pctY = (-poi.poi.position.x + MAP_MID_POINT) / (64.0f * TILE_SIZE)
            val posX = pctX * wdlImage.size.x - 12
            val posY = pctY * wdlImage.size.y - 12
            poi.image.position = Vector2(calculatedPosition.x + posX + 300.0f, calculatedPosition.y + posY)
            poi.highlight.position = Vector2(calculatedPosition.x + posX + 300.0f, calculatedPosition.y + posY)
        }
    }

    private fun loadPoiImages() {
        poiImages.forEach {
            removeChild(it.image)
            removeChild(it.highlight)
        }

        poiImages.clear()
        for (poi in poiLocations) {
            val image = Image()
            image.size = Vector2(24, 24)
            image.imageSource = controller.poiImageSource
            val highlight = Image()
            highlight.size = Vector2(24, 24)
            highlight.imageSource = controller.poiHighlightImage
            highlight.blendMode = BlendMode.COLOR
            poiImages.add(PoiImage(image, highlight, poi))
            appendChild(image)
            appendChild(highlight)
        }

        positionImages()
    }

    private fun updateSelectedAdt(pos: Vector2) {
        val dx = pos.x - wdlImage.calculatedPosition.x
        val dy = pos.y - wdlImage.calculatedPosition.y
        val pctX = dx / wdlImage.calculatedSize.x
        val pctY = dy / wdlImage.calculatedSize.y
        if(pctX < 0 || pctY < 0 || pctX > 1 || pctY > 1) {
            selectedAdt.text = ""
            selectedPosition.text = ""
        } else {
            val adtX = Math.floor(pctX * 64.0).toInt()
            val adtY = Math.floor(pctY * 64.0).toInt()
            val posX = pctX * 64.0f * TILE_SIZE
            val posY = pctY * 64.0f * TILE_SIZE
            selectedAdt.text = "$adtX/$adtY"
            selectedPosition.text = "X: ${posX.format(3)} / Y: ${posY.format(3)}"
        }
    }
}