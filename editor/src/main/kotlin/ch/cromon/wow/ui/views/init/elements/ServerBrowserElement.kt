package ch.cromon.wow.ui.views.init.elements

import ch.cromon.wow.io.data.remote.ServerDefinition
import ch.cromon.wow.io.data.remote.ServerType
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.elements.*
import ch.cromon.wow.ui.utils.Alignment
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.net.URI
import javax.annotation.PostConstruct

@Component
@Scope("prototype")
class ServerBrowserElement : ItemsElement() {
    @Autowired
    lateinit var controller: ServerBrowserController

    private lateinit var serverList: ListView<ServerDefinition>

    private lateinit var serverUrl: Label
    private lateinit var serverTypeSelect: Label
    private lateinit var serverName: Label
    private lateinit var openServerButton: Button

    @PostConstruct
    fun initialize() {
        controller.attach(this)
    }

    override fun onInitialize() {
        super.onInitialize()

        val addServer = IconButton()
        val removeServer = IconButton()
        val copyServer = IconButton()
        serverList = ListView()

        appendChild(addServer)
        appendChild(removeServer)
        appendChild(copyServer)
        appendChild(serverList)

        addServer.run {
            character = '\uf067'
            size = Vector2(40, 40)
            fontSize = 50.0f
        }

        removeServer.run {
            character = '\uf068'
            size = Vector2(40, 40)
            fontSize = 50.0f
            position = Vector2(40, 0)
        }

        copyServer.run {
            character = '\uf0c5'
            size = Vector2(40, 40)
            fontSize = 50.0f
            position = Vector2(80, 0)
        }

        serverList.run {
            verticalAlignment = Alignment.STRETCH
            margin = Margin(0.0f, 0.0f, 40.0f, 0.0f)
            size = Vector2(300.0f, 0.0f)
        }

        serverList.itemTemplate = this::serverListItemFactory
        serverList.itemClicked = this::onServerSelected

        initEditPane()
        updateServers()
    }

    private fun updateServers() {
        var servers = controller.loadServers()
        servers = servers.copy(servers = arrayListOf(ServerDefinition("Wotlk Server", ServerType.WOTLK, URI("http://localhost:8080"))))

        for (server in servers) {
            serverList.addItem(server)
        }
    }

    private fun serverListItemFactory(definition: ServerDefinition): Element {
        val parent = ItemsElement()
        val img = Image()
        val label = Label()
        parent.appendChild(img)
        parent.appendChild(label)
        val imageSource = controller.getImage(definition.type)
        val width = Math.max(30, imageSource.width)
        val height = 20
        img.run {
            this.imageSource = imageSource
            size = Vector2(imageSource.width, imageSource.height)
            position = Vector2(0.0f, (height - imageSource.height) / 2.0f)
        }

        label.run {
            text = "${definition.name} (${definition.url.authority})"
            fontSize = 16
            position = Vector2(width + 5.0f, (height - textHeight) / 2.0f)
        }

        return parent
    }

    private fun onServerSelected(definition: ServerDefinition) {
        serverTypeSelect.text = definition.type.displayName
        serverName.text = definition.name
        serverUrl.text = definition.url.toString()
        openServerButton.visible = true
        openServerButton.onClick.clear()
        openServerButton.onClick += { controller.openFromServer(definition) }
    }

    private fun initEditPane() {
        serverTypeSelect = Label()
        serverName = Label()
        serverUrl = Label()
        openServerButton = Button()
        appendChild(serverTypeSelect)
        appendChild(serverName)
        appendChild(serverUrl)
        appendChild(openServerButton)

        serverTypeSelect.run {
            margin = Margin(serverList.calculatedSize.x + 15, 0.0f, 45.0f, 0.0f)
            fontSize = 18
        }

        serverName.run {
            margin = Margin(serverList.calculatedSize.x + 15, 0.0f, 70.0f, 0.0f)
            fontSize = 18
        }

        serverUrl.run {
            margin = Margin(serverList.calculatedSize.x + 15, 0.0f, 95.0f, 0.0f)
            fontSize = 18
        }

        openServerButton.run {
            margin = Margin(serverList.calculatedSize.x + 15, 0.0f, 130.0f, 0.0f)
            caption = "Open"
            visible = false
        }
    }
}