package ch.cromon.wow.ui.quickmenu

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.elements.ItemsElement
import ch.cromon.wow.ui.messaging.KeyPressMessage
import ch.cromon.wow.ui.messaging.WindowMessage
import ch.cromon.wow.ui.quickmenu.model.Definition
import ch.cromon.wow.ui.utils.Alignment
import org.lwjgl.glfw.GLFW.GLFW_KEY_TAB


class MenuRoot : ItemsElement() {
    override fun doMessage(message: WindowMessage) {
        if (message.isHandled) {
            return
        }

        val context = message.context
        if (message is KeyPressMessage) {
            if (context.inputFocus == null && !message.isRepeat && message.charCode.toInt() == GLFW_KEY_TAB) {
                visible = !visible
                applyToChildren({
                    it.visible = !it.visible
                })
            }
        }

        super.doMessage(message)
    }

    override fun onInitialize() {
        super.onInitialize()

        verticalAlignment = Alignment.CENTER
        horizontalAlignment = Alignment.CENTER

        visible = false
    }

    fun loadFromDefinition(definition: Definition) {
        var shortcutIndex = 1
        val perChildAngle = (2 * Math.PI) / definition.children.size
        val angleOffset = Math.PI / definition.children.size
        for ((childIndex, child) in definition.children.withIndex()) {
            val entry = MenuNode(shortcutIndex++, child.description)
            val angle = -angleOffset + childIndex * perChildAngle - Math.PI / 2.0f
            val x = Math.cos(angle)
            val y = Math.sin(angle)

            appendChild(entry)
            entry.position = Vector2(x * 200, y * 200)
            entry.loadChildren(angle.toFloat(), child.children)
        }
    }
}