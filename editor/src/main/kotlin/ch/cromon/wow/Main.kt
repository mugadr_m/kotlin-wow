package ch.cromon.wow

import ch.cromon.wow.ui.Window
import javafx.application.Application
import javafx.application.Platform
import javafx.stage.Stage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.stereotype.Component

@Configuration
@ComponentScan
@EnableAsync
@EnableScheduling
open class Scanner

@Component
class ClientRunner {
    @Autowired
    private lateinit var window: Window

    fun run() {
        window.show()
        window.runLoop()
    }
}

class MainApplication : Application() {
    override fun start(primaryStage: Stage?) {
        Thread.currentThread().name = "main"
        AnnotationConfigApplicationContext().use {
            it.scan(Scanner::class.java.`package`.name)
            it.refresh()
            it.getBean(ClientRunner::class.java).run()
        }

        Platform.exit()
    }
}

fun main(args: Array<String>) {
    Application.launch(MainApplication::class.java, *args)
}