package ch.cromon.wow.scene.terrain

import ch.cromon.wow.io.files.map.MapAreaLow
import ch.cromon.wow.io.files.map.MapAreaLowLoader
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.scene.Scene
import ch.cromon.wow.scene.terrain.MapManager.Companion.getIndex
import ch.cromon.wow.utils.TILE_SIZE
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.*

@Component
@Scope("singleton")
class MapAreaLowManager {
    companion object {
        const val LOAD_RADIUS = MapManager.LOAD_RADIUS + 4
    }

    private var lastTileX = -1
    private var lastTileY = -1

    private var activeTiles = HashMap<Int, MapAreaLowRender>()
    private var activeTileList = ArrayList<MapAreaLowRender>()
    private var tilesToLoad = ArrayList<Int>()
    private var loadedTiles = LinkedList<MapAreaLow>()

    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var loader: MapAreaLowLoader

    fun onFrame(scene: Scene) {
        var tile = loadedTiles.poll()
        while (tile != null) {
            try {
                val renderer = MapAreaLowRender(tile.indexX, tile.indexY)
                renderer.asyncLoad(tile)
                activeTiles[getIndex(tile.indexX, tile.indexY)] = renderer
                activeTileList.add(renderer)
            } catch (e: Exception) {
                logger.warn("Error loading tile: ${tile.indexX}/${tile.indexY}", e)
            }

            tile = loadedTiles.poll()
        }

        for (renderTile in activeTileList) {
            renderTile.onFrame(scene)
        }
    }

    fun updatePosition(position: Vector3) {
        val tileX = (position.x / TILE_SIZE).toInt()
        val tileY = (position.y / TILE_SIZE).toInt()
        if (tileX == lastTileX && tileY == lastTileY) {
            return
        }

        val tiles = activeTiles
        val tilesToRemove = ArrayList<MapAreaLowRender>()
        for ((_, tile) in tiles) {
            val dx = Math.abs(tile.indexX - tileX)
            val dy = Math.abs(tile.indexY - tileY)
            if (dx >= LOAD_RADIUS || dy > LOAD_RADIUS) {
                tilesToRemove.add(tile)
            }
        }

        val loadTiles = ArrayList<Int>()
        for (x in (tileX - LOAD_RADIUS)..(tileX + LOAD_RADIUS)) {
            for (y in (tileY - LOAD_RADIUS)..(tileY + LOAD_RADIUS)) {
                val index = MapManager.getIndex(x, y)
                if (tiles.containsKey(index)) {
                    continue
                }

                loadTiles.add(index)
            }
        }

        synchronized(this) {
            tilesToLoad = loadTiles
        }
    }

    @Scheduled(fixedRate = 100)
    private fun onUpdateLoadedTiles() {
        val loadTiles = synchronized(this) {
            val ret = tilesToLoad
            tilesToLoad = ArrayList()
            ret
        }

        if (loadTiles.isEmpty()) {
            return
        }

        for (index in loadTiles) {
            val (x, y) = MapManager.inverseIndex(index)
            val tile = loader.loadTile(x, y) ?: continue
            loadedTiles.add(tile)
        }
    }

}