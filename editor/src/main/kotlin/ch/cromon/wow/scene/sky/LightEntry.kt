package ch.cromon.wow.scene.sky

import ch.cromon.wow.math.Vector3
import ch.cromon.wow.math.Vector4

enum class LightColor {
    Diffuse,
    Ambient,
    Top,
    Middle,
    MiddleLower,
    Lower,
    Horizon,
    Fog,
    Unk1,
    Sun,
    Halo,
    Unk2,
    Cloud,
    Unk3,
    OceanLight,
    OceanDark,
    RiverLight,
    RiverDark
}

enum class LightFloat {
    FogEnd,
    FogDensity
}

val MAX_LIGHT_COLOR = LightColor.values().maxBy { it.ordinal }?.ordinal ?: 0
val MAX_LIGHT_FLOAT = LightFloat.values().maxBy { it.ordinal }?.ordinal ?: 0

data class LightEntry(
        val id: Int,
        val mapId: Int,
        val position: Vector3,
        val innerRadius: Float,
        val outerRadius: Float,
        private val colors: List<InterpolatedColor>,
        private val floats: List<InterpolatedFloat>,
        private val isGlobal: Boolean = false) {
    val global by lazy {
        isGlobal || (Math.abs(position.x) < 0.001f && Math.abs(position.y) < 0.001f && Math.abs(position.z) < 0.001f)
    }

    fun getColorForTime(table: LightColor, timePoint: Long): Vector4 {
        val index = table.ordinal
        if (index < 0 || index > MAX_LIGHT_COLOR) {
            return Vector4(0, 0, 0, 0)
        }

        val time = timePoint % 2880
        val interpolator = colors[index]
        return interpolator.getColorForTime(time)
    }

    fun getFloatForTime(table: LightFloat, timePoint: Long): Float {
        val index = table.ordinal
        if(index < 0 || index > MAX_LIGHT_FLOAT) {
            return 0.0f
        }

        val time = timePoint % 2880
        val interpolator = floats[index]
        return interpolator.getFloatForTime(time)
    }
}