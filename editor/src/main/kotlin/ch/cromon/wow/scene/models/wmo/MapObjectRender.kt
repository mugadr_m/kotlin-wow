package ch.cromon.wow.scene.models.wmo

import ch.cromon.wow.gx.IndexBuffer
import ch.cromon.wow.gx.IndexType
import ch.cromon.wow.gx.VertexBuffer
import ch.cromon.wow.io.files.texture.TextureManager
import ch.cromon.wow.io.files.wmo.AbstractMapObject
import ch.cromon.wow.io.files.wmo.MapObjectBatch
import ch.cromon.wow.math.BoundingBox
import java.nio.FloatBuffer
import java.nio.ShortBuffer


class MapObjectRender(private val textureManager: TextureManager) {
    private var isAsyncLoaded = false
    private var isSyncLoaded = false

    private val groups = ArrayList<MapObjectGroupRender>()

    private lateinit var vertexBuffer: VertexBuffer
    private lateinit var indexBuffer: IndexBuffer

    private val materials = ArrayList<MapObjectMaterial>()

    private lateinit var vertexData: FloatBuffer
    private lateinit var indexData: ShortBuffer

    lateinit var boundingBox: BoundingBox
        private set

    fun getMaterial(batch: MapObjectBatch): MapObjectMaterial = materials[batch.materialId]

    fun onFrame(instances: List<MapObjectRenderInstance>) {
        if (!isAsyncLoaded) {
            return
        }

        if (!isSyncLoaded) {
            syncLoad()
            return
        }

        val mesh = MapObjectMesh.mesh
        mesh.vertexBuffer = vertexBuffer
        mesh.indexBuffer = indexBuffer
        mesh.setup()

        for (group in groups) {
            mesh.startVertex = group.startVertex
            group.onFrame(mesh, instances)
        }
    }

    fun asyncLoad(root: AbstractMapObject) {
        root.materials.mapTo(materials, {
            MapObjectMaterial(
                    it.textureName,
                    textureManager.loadTexture(it.textureName),
                    it.blendMode,
                    (it.flags and 0x04) != 0
            )
        })

        boundingBox = root.boundingBox
        vertexData = root.vertexData
        indexData = root.indexData

        asyncLoadGroups(root)

        root.unload()
        isAsyncLoaded = true
    }

    private fun syncLoad() {
        vertexBuffer = VertexBuffer()
        indexBuffer = IndexBuffer(IndexType.UINT16)

        vertexBuffer.data(vertexData)
        indexBuffer.data(indexData)

        isSyncLoaded = true
    }

    private fun asyncLoadGroups(root: AbstractMapObject) {
        root.groups.mapTo(groups, {
            val ret = MapObjectGroupRender(this)
            ret.asyncLoad(it)
            ret
        })
    }
}