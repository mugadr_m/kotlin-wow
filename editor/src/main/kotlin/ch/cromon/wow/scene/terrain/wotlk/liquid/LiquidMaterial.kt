package ch.cromon.wow.scene.terrain.wotlk.liquid

import ch.cromon.wow.gx.*
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.math.Vector4


object LiquidMaterial {
    private lateinit var mesh: Mesh

    private var uniformTransform = -1
    private var uniformView = -1
    private var uniformProjection = -1
    private var uniformTexture = -1

    private var cameraPosition = -1
    private var clipParameters = -1
    private var fogColor = -1
    private var lightColor = -1
    private var darkColor = -1

    var oceanColorLight = Vector4(1, 1, 1, 1)
    var oceanColorDark = Vector4(0, 0, 0, 0)

    var riverColorLight = Vector4(1, 1, 1, 1)
    var riverColorDark = Vector4(0, 0, 0, 0)

    fun setColors(isOcean: Boolean) {
        val program = mesh.program
        if(isOcean) {
            program.setVec4(lightColor, oceanColorLight)
            program.setVec4(darkColor, oceanColorDark)
        } else {
            program.setVec4(lightColor, riverColorLight)
            program.setVec4(darkColor, riverColorDark)
        }
    }

    fun onFrame(vertexBuffer: VertexBuffer, indexBuffer: IndexBuffer, numIndices: Int, transform: Matrix4, texture: Texture) {
        CullState.CW.apply()
        mesh.vertexBuffer = vertexBuffer
        mesh.indexBuffer = indexBuffer
        mesh.indexCount = numIndices

        mesh.textureInput.add(uniformTexture, texture)
        mesh.program.setMatrix(uniformTransform, transform)

        mesh.render()
        CullState.CCW.apply()
    }

    fun updateView(matrix: Matrix4) {
        mesh.program.setMatrix(uniformView, matrix)
    }

    fun updateProjection(matrix: Matrix4) {
        mesh.program.setMatrix(uniformProjection, matrix)
    }

    fun updateCameraPosition(position: Vector3) {
        mesh.program.setVec3(cameraPosition, position)
    }

    fun updateClipParameters(parameters: Vector3) {
        mesh.program.setVec3(clipParameters, parameters)
    }

    fun updateFogColor(color: Vector4) {
        mesh.program.setVec4(fogColor, color)
    }

    fun initialize() {
        mesh = Mesh()

        val program = Program()
        program.compileFromResource(Shader.LIQUID)
        program.link()

        uniformProjection = program.getUniform("matProjection")
        uniformView = program.getUniform("matView")
        uniformTransform = program.getUniform("matTransform")
        uniformTexture = program.getUniform("waterTexture")
        clipParameters = program.getUniform("clipParameters")
        cameraPosition = program.getUniform("cameraPosition")
        lightColor = program.getUniform("waterColorLight")
        darkColor = program.getUniform("waterColorDark")
        fogColor = program.getUniform("fogColor")

        mesh.program = program
        mesh.topology = Topology.QUADS
        mesh.depthCheck = DepthCheck.LESS

        mesh.addElement(VertexElement("position", 0, 3))
        mesh.addElement(VertexElement("texCoord", 0, 2))
        mesh.addElement(VertexElement("waterDepth", 0, 1))

        mesh.finalize()
    }
}