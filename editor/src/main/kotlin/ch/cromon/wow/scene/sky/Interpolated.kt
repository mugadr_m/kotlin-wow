package ch.cromon.wow.scene.sky

import ch.cromon.wow.math.Vector4


data class InterpolatedColor(private val timeValues: List<Int>, private val colorValues: List<Vector4>) {
    fun getColorForTime(timePoint: Long): Vector4 {
        if (timeValues.isEmpty()) {
            return Vector4(0, 0, 0, 0)
        }

        if (timeValues.size == 1) {
            return colorValues[0]
        }

        var time = timePoint
        if (timeValues[0] > time) {
            time = timeValues[0].toLong()
        }

        var v1 = Vector4(0, 0, 0, 0)
        var v2 = Vector4(0, 0, 0, 0)
        var t1 = 0
        var t2 = 0

        for (i in 0 until timeValues.size) {
            if ((i + 1) >= timeValues.size) {
                v1 = colorValues[i]
                v2 = colorValues[0]
                t1 = timeValues[i]
                t2 = timeValues[0] + 2880
                break
            }

            val ts = timeValues[i]
            val te = timeValues[i + 1]
            if (time in ts..te) {
                t1 = ts
                t2 = te
                v1 = colorValues[i]
                v2 = colorValues[i + 1]
                break
            }
        }

        val diff = t2 - t1
        if (diff == 0) {
            return v1
        }

        val sat = (time - t1) / diff.toFloat()
        return v1 * (1.0f - sat) + v2 * sat
    }
}

data class InterpolatedFloat(private val timeValues: List<Int>, private val colorValues: List<Float>) {
    fun getFloatForTime(timePoint: Long): Float {
        if (timeValues.isEmpty()) {
            return 0.0f
        }

        if (timeValues.size == 1) {
            return colorValues[0]
        }

        var time = timePoint
        if (timeValues[0] > time) {
            time = timeValues[0].toLong()
        }

        var v1 = 0.0f
        var v2 = 0.0f
        var t1 = 0
        var t2 = 0

        for (i in 0 until timeValues.size) {
            if ((i + 1) >= timeValues.size) {
                v1 = colorValues[i]
                v2 = colorValues[0]
                t1 = timeValues[i]
                t2 = timeValues[0] + 2880
                break
            }

            val ts = timeValues[i]
            val te = timeValues[i + 1]
            if (time in ts..te) {
                t1 = ts
                t2 = te
                v1 = colorValues[i]
                v2 = colorValues[i + 1]
                break
            }
        }

        val diff = t2 - t1
        if (diff == 0) {
            return v1
        }

        val sat = (time - t1) / diff.toFloat()
        return v1 * (1.0f - sat) + v2 * sat
    }
}