package ch.cromon.wow.scene.sky

import ch.cromon.wow.gx.CullState
import ch.cromon.wow.io.data.DataLoadEvent
import ch.cromon.wow.io.files.storage.DataStorage
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.math.Vector4
import ch.cromon.wow.scene.GraphicsLoader
import ch.cromon.wow.scene.terrain.MapLoadFinishedEvent
import ch.cromon.wow.utils.*
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.DependsOn
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.concurrent.atomic.AtomicBoolean
import javax.annotation.PostConstruct


interface SkyProvider {
    fun initialize(storage: DataStorage)
    fun getLightsForMap(mapId: Int, mapName: String): List<LightEntry>
}

@Component
@Scope("singleton")
@DependsOn("CdiUtils")
class SkyManager {
    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var storage: DataStorage

    @Autowired
    private lateinit var time: Time

    @Autowired
    private lateinit var graphicsLoader: GraphicsLoader

    @Autowired
    private lateinit var skySphere: SkySphere

    @Autowired
    private lateinit var settings: Settings

    private val providers = ArrayList<BuildBeanEntry<SkyProvider>>()

    private var mapLights: List<LightEntry> = ArrayList()

    private lateinit var provider: SkyProvider

    private var activeMap = -1
    private lateinit var curPosition: Vector3

    private var positionChanged = AtomicBoolean(false)
    private var colorsChanged: AtomicBoolean = AtomicBoolean(false)

    private val loadLock = Any()

    private val currentColors = Array(MAX_LIGHT_COLOR + 1, { Vector4(0, 0, 0, 0) })
    private val currentFloats = FloatArray(MAX_LIGHT_FLOAT + 1)

    private var workingLights: List<LightEntry> = ArrayList()
    private var workingWeights: MutableList<Float> = ArrayList()
    private var globalLights: MutableList<Int> = ArrayList()

    private var timeFactor: Float = 10.0f

    @PostConstruct
    fun initialize() {
        providers.addAll(CdiUtils.getAllVersionBeans(SkyProvider::class.java))
        timeFactor = settings[TIME_OF_DAY_DELAY_FACTOR, "10"].toLong().toFloat()
    }

    fun getActiveLights(): Collection<LightEntry> = mapLights

    fun onUpdatePosition(position: Vector3) {
        curPosition = position
        positionChanged.set(true)
    }

    fun onFrame() {
        CullState.DISABLED.apply()
        skySphere.onFrame()

        if (!colorsChanged.compareAndSet(true, false)) {
            return
        }

        val ambient = currentColors[LightColor.Ambient.ordinal]
        val diffuse = currentColors[LightColor.Diffuse.ordinal]
        val riverLight = currentColors[LightColor.RiverLight.ordinal]
        val riverDark = currentColors[LightColor.RiverDark.ordinal]
        val oceanLight = currentColors[LightColor.OceanLight.ordinal]
        val oceanDark = currentColors[LightColor.OceanDark.ordinal]
        val fog = currentColors[LightColor.Fog.ordinal]
        val fogEnd = currentFloats[LightFloat.FogEnd.ordinal]

        graphicsLoader.onUpdateAmbientLight(ambient)
        graphicsLoader.onUpdateDiffuseLight(diffuse)
        graphicsLoader.onUpdateFogParams(fog, fogEnd)
        graphicsLoader.onUpdateWaterColors(riverLight, riverDark, oceanLight, oceanDark)
    }

    @EventListener
    fun onDataLoaded(event: DataLoadEvent) {
        if (!event.success) {
            return
        }

        val maybeProvider = providers.find { it.maxBuild >= event.build && it.minBuild <= event.build }
        if (maybeProvider == null) {
            logger.error("No implementation found for ${SkyProvider::class.java} that can use build ${event.build}")
            throw IllegalStateException("No SkyProvider found for build ${event.build}")
        }

        logger.info("Using sky loader: ${maybeProvider.bean.javaClass}")
        provider = maybeProvider.bean
        provider.initialize(storage)
    }

    @EventListener
    fun onEnterWorld(event: MapLoadFinishedEvent) {
        val mapId = event.mapId
        val position = event.position
        val mapName = event.continent

        time.worldStart = time.runTime
        positionChanged.set(true)
        curPosition = position

        synchronized(loadLock) {
            mapLights = provider.getLightsForMap(mapId, mapName).sortedWith(Comparator { o1, o2 ->
                if (o1.global && o2.global) {
                    0
                } else if (o1.global) {
                    -1
                } else if (o2.global) {
                    1
                } else if (o1.outerRadius > o2.outerRadius) {
                    1
                } else if (o2.outerRadius > o1.outerRadius) {
                    -1
                } else {
                    0
                }
            })

            workingWeights = mapLights.map { 0.0f }.toMutableList()
            globalLights = mapLights.withIndex().filter { it.value.global }.map { it.index }.toMutableList()
        }

        activeMap = mapId
    }

    @Scheduled(fixedRate = 100)
    fun onUpdateLights() {
        if (activeMap < 0) {
            return
        }

        if (positionChanged.compareAndSet(true, false)) {
            workingLights = synchronized(loadLock) {
                mapLights
            }


            for (i in 0 until workingWeights.size) {
                workingWeights[i] = 0.0f
            }
            calculateWeights(workingLights)
        }

        for (i in 0..MAX_LIGHT_COLOR) {
            val cur = currentColors[i]
            cur.x = 0.0f
            cur.y = 0.0f
            cur.z = 0.0f
        }

        for (i in 0..MAX_LIGHT_FLOAT) {
            currentFloats[i] = 0.0f
        }

        var sx: Float
        var sy: Float
        var sz: Float
        var sw: Float
        val diffTime = ((time.runTime - time.worldStart) / timeFactor).toLong()
        for (i in 0 until workingLights.size) {
            if (workingWeights[i] >= 1e-3) {
                for (k in 0..MAX_LIGHT_COLOR) {
                    sx = currentColors[k].x
                    sy = currentColors[k].y
                    sz = currentColors[k].z
                    sw = currentColors[k].w
                    val tmp = workingLights[i].getColorForTime(LightColor.values()[k], diffTime)
                    val w = workingWeights[i]
                    sx += tmp.x * w
                    sy += tmp.y * w
                    sz += tmp.z * w
                    sw += tmp.w * w
                    currentColors[k].x = sx
                    currentColors[k].y = sy
                    currentColors[k].z = sz
                    currentColors[k].w = sw
                }
                for (k in 0..MAX_LIGHT_FLOAT) {
                    currentFloats[k] +=
                            workingLights[i].getFloatForTime(LightFloat.values()[k], diffTime) * workingWeights[i]
                }
            }
        }

        skySphere.asyncUpdate(currentColors)
        colorsChanged.set(true)
    }

    private fun calculateWeights(activeLights: List<LightEntry>) {
        for (index in (activeLights.size - 1) downTo 0) {
            val light = activeLights[index]
            if (light.global) {
                continue
            }

            val distance = (curPosition - light.position).length()
            when {
                distance < light.innerRadius -> {
                    workingWeights[index] = 1.0f
                    for (j in (index + 1) until workingWeights.size) {
                        workingWeights[j] = 0.0f
                    }
                }
                distance < light.outerRadius -> {
                    val sat = (distance - light.innerRadius) / (light.outerRadius - light.innerRadius)
                    workingWeights[index] = 1.0f - sat
                    for (j in (index + 1) until workingWeights.size) {
                        workingWeights[j] *= sat
                    }
                }
                else -> workingWeights[index] = 0.0f
            }
        }

        val totalWeight = workingWeights.sum()
        if (totalWeight >= 1 || globalLights.isEmpty()) {
            return
        }

        val remainingWeight = 1.0f - totalWeight
        val perGlobalWeight = remainingWeight / globalLights.size
        for (global in globalLights) {
            workingWeights[global] = perGlobalWeight
        }
    }
}