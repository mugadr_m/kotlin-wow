package ch.cromon.wow.scene.terrain.wotlk

import ch.cromon.wow.gx.*
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.math.Vector4
import org.lwjgl.BufferUtils
import java.nio.ByteBuffer


class TerrainMaterial private constructor(private val fragmentName: String) {
    companion object {
        val NEW_BLEND = TerrainMaterial("MapFragmentNew.glsl")
        val OLD_BLEND = TerrainMaterial("MapFragment.glsl")
    }

    private var texture0: Int = -1
    private var texture1: Int = -1
    private var texture2: Int = -1
    private var texture3: Int = -1
    private var texture0Specular: Int = -1
    private var texture1Specular: Int = -1
    private var texture2Specular: Int = -1
    private var texture3Specular: Int = -1
    private var baseTexture: Int = -1
    private var ambientColor: Int = -1
    private var diffuseColor: Int = -1
    private var cameraPosition = -1
    private var clipParameters = -1
    private var fogColor = -1
    private var specularFactors = -1

    private lateinit var textureUniforms: IntArray
    private lateinit var specularTextureUniforms: IntArray

    private var matView: Int = -1
    private var matProjection: Int = -1

    private lateinit var glMesh: Mesh
    private lateinit var program: Program

    val mesh get() = glMesh

    fun updateView(matrix: Matrix4) {
        program.setMatrix(matView, matrix)
    }

    fun updateProjection(matrix: Matrix4) {
        program.setMatrix(matProjection, matrix)
    }

    fun updateCameraPosition(position: Vector3) {
        program.setVec3(cameraPosition, position)
    }

    fun updateClipParameters(parameters: Vector3) {
        program.setVec3(clipParameters, parameters)
    }

    fun updateFogColor(color: Vector4) {
        program.setVec4(fogColor, color)
    }

    fun setTextures(textures: ArrayList<Texture>, specularTextures: ArrayList<Texture>, baseTexture: Texture) {
        for(i in 0 until textures.size) {
            glMesh.textureInput.add(textureUniforms[i], textures[i])
            glMesh.textureInput.add(specularTextureUniforms[i], specularTextures[i])
        }

        glMesh.textureInput.add(this.baseTexture, baseTexture)
    }

    fun updateAmbient(color: Vector4) {
        program.setVec4(ambientColor, color)
    }

    fun updateDiffuse(color: Vector4) {
        program.setVec4(diffuseColor, color)
    }

    fun initialize() {
        glMesh = Mesh()
        glMesh.addElement(VertexElement("position", 0, 3))
        glMesh.addElement(VertexElement("texCoord", 0, 2))
        glMesh.addElement(VertexElement("texCoord", 1, 2))
        glMesh.addElement(VertexElement("normal", 0, 3))
        glMesh.addElement(VertexElement("color", 0, 4, DataType.BYTE, true))

        program = Program()
        program.compileVertexShaderFromResource("shaders/wotlk/MapVertex.glsl")
        program.compileFragmentShaderFromResource("shaders/wotlk/$fragmentName")
        program.link()

        matView = program.getUniform("matView")
        matProjection = program.getUniform("matProjection")
        texture0 = program.getUniform("texture0")
        texture1 = program.getUniform("texture1")
        texture2 = program.getUniform("texture2")
        texture3 = program.getUniform("texture3")
        texture0Specular = program.getUniform("texture0_s")
        texture1Specular = program.getUniform("texture1_s")
        texture2Specular = program.getUniform("texture2_s")
        texture3Specular = program.getUniform("texture3_s")
        clipParameters = program.getUniform("clipParameters")
        cameraPosition = program.getUniform("cameraPosition")
        fogColor = program.getUniform("fogColor")
        specularFactors = program.getUniform("specularFactors")

        textureUniforms = intArrayOf(texture0, texture1, texture2, texture3)
        specularTextureUniforms = intArrayOf(texture0Specular, texture1Specular, texture2Specular, texture3Specular)

        baseTexture = program.getUniform("baseTexture")

        ambientColor = program.getUniform("ambientColor")
        diffuseColor = program.getUniform("diffuseColor")

        program.setVec4(specularFactors, Vector4(1, 1, 1, 1))

        glMesh.program = program
        glMesh.indexCount = 768

        glMesh.finalize()
        initIndices()
    }

    private fun initIndices() {
        val indices = ByteArray(768)
        for (y in 0 until 8) {
            for (x in 0 until 8) {
                val i = y * 8 * 12 + x * 12
                indices[i + 0] = (y * 17 + x).toByte()
                indices[i + 2] = (y * 17 + x + 1).toByte()
                indices[i + 1] = (y * 17 + x + 9).toByte()

                indices[i + 3] = (y * 17 + x + 1).toByte()
                indices[i + 5] = (y * 17 + x + 18).toByte()
                indices[i + 4] = (y * 17 + x + 9).toByte()

                indices[i + 6] = (y * 17 + x + 18).toByte()
                indices[i + 8] = (y * 17 + x + 17).toByte()
                indices[i + 7] = (y * 17 + x + 9).toByte()

                indices[i + 9] = (y * 17 + x + 17).toByte()
                indices[i + 11] = (y * 17 + x).toByte()
                indices[i + 10] = (y * 17 + x + 9).toByte()
            }
        }

        val indexBuffer = IndexBuffer(IndexType.UINT8)
        indexBuffer.data(BufferUtils.createByteBuffer(768).put(indices).flip() as ByteBuffer)
        glMesh.indexBuffer = indexBuffer
    }
}