package ch.cromon.wow.scene

import ch.cromon.wow.AppStateManager
import ch.cromon.wow.io.input.InputAction
import ch.cromon.wow.io.input.InputController
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.ui.WindowResizeEvent
import ch.cromon.wow.utils.Time
import ch.cromon.wow.utils.UNIT_Z
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import kotlin.properties.Delegates

@Component
@Scope("singleton")
class Camera {
    private var matView = Matrix4()
    private var matProjection = Matrix4()

    private var skipViewEvent = false

    private var aspect = 1.0f
    var position by Delegates.observable(Vector3(), { _, _, _ -> updateViewVectors() })
    var target by Delegates.observable(Vector3(), { _, _, _ -> updateViewVectors() })
    var up by Delegates.observable(Vector3(), { _, _, _ -> updateViewVectors() })

    private var right = Vector3()
    private var forward = Vector3()

    private var fov by Delegates.observable(60.0f, { _, _, _ -> updateProjection() })
    private var zNear by Delegates.observable(3.0f, { _, _, _ -> updateProjection() })
    var zFar by Delegates.observable(2000.0f, {_, _, _ -> updateProjection() })

    @Autowired
    private lateinit var inputController: InputController

    @Autowired
    private lateinit var time: Time

    @Autowired
    private lateinit var stateManager: AppStateManager

    @Autowired
    private lateinit var graphicsLoader: GraphicsLoader

    @Autowired
    private lateinit var worldFrame: WorldFrame

    private var lastUpdate: Long = 0

    fun changePosition(newPos: Vector3) {
        skipViewEvent = true
        val oldForward = forward
        position = newPos
        target = newPos + oldForward
        skipViewEvent = false
        worldFrame.onPositionChanged(position)
        graphicsLoader.onCameraPositionChanged(position)
        updateViewVectors()
    }

    private fun yaw(amount: Float) {
        val rotation = createRotation(amount, UNIT_Z)
        val newForward = rotation * forward
        target = position + newForward.normalized()
        up = (rotation * up).normalized()
    }

    private fun pitch(amount: Float) {
        val rotation = createRotation(amount, right)
        val newForward = rotation * forward
        target = position + newForward
        up = (rotation * up).normalized()
    }

    private fun moveForward(amount: Float) {
        val mx = forward.x * amount
        val my = forward.y * amount
        val mz = forward.z * amount
        position.x += mx
        position.y += my
        position.z += mz
        target.x += mx
        target.y += my
        target.z += mz
    }

    private fun moveRight(amount: Float) {
        val mx = right.x * amount
        val my = right.y * amount
        val mz = right.z * amount
        position.x += mx
        position.y += my
        position.z += mz
        target.x += mx
        target.y += my
        target.z += mz
    }

    private fun moveUp(amount: Float) {
        val mx = UNIT_Z.x * amount
        val my = UNIT_Z.y * amount
        val mz = UNIT_Z.z * amount
        position.x += mx
        position.y += my
        position.z += mz
        target.x += mx
        target.y += my
        target.z += mz
    }

    @EventListener
    fun onResize(event: WindowResizeEvent) {
        aspect = event.dimension.x / event.dimension.y
        updateProjection()
    }

    fun onUpdate(): Boolean {
        val now = time.runTime
        if(!stateManager.currentState.renderWorld) {
            lastUpdate = now
            return false
        }

        skipViewEvent = true
        var hasPositionChanged = false
        var hasViewUpdate = false
        val diff = now - lastUpdate

        val forward = inputController.getInputActionIntensity(InputAction.CAMERA_FRONT)
        val back = inputController.getInputActionIntensity(InputAction.CAMERA_BACK)
        val left = inputController.getInputActionIntensity(InputAction.CAMERA_LEFT)
        val right = inputController.getInputActionIntensity(InputAction.CAMERA_RIGHT)
        val top = inputController.getInputActionIntensity(InputAction.CAMERA_UP)
        val bottom = inputController.getInputActionIntensity(InputAction.CAMERA_DOWN)

        if(Math.abs(forward) >= Double.MIN_VALUE) {
            moveForward(forward * diff / 10.0f)
            hasPositionChanged = true
        }

        if(Math.abs(back) >= Double.MIN_VALUE) {
            moveForward(-back * diff / 10.0f)
            hasPositionChanged = true
        }

        if(Math.abs(right) >= Double.MIN_VALUE) {
            moveRight(right * diff / 10.0f)
            hasPositionChanged = true
        }

        if(Math.abs(left) >= Double.MIN_VALUE) {
            moveRight(-left * diff / 10.0f)
            hasPositionChanged = true
        }

        if(Math.abs(top) >= Double.MIN_VALUE) {
            moveUp(top * diff / 10.0f)
            hasPositionChanged = true
        }

        if(Math.abs(bottom) >= Double.MIN_VALUE) {
            moveUp(-bottom * diff / 10.0f)
            hasPositionChanged = true
        }

        val rotX = inputController.getInputActionIntensity(InputAction.CAMERA_ROTATE_H)
        val rotY = inputController.getInputActionIntensity(InputAction.CAMERA_ROTATE_V)
        if(Math.abs(rotX) >= Double.MIN_VALUE) {
            yaw(rotX * diff * 0.01f)
            hasViewUpdate = true
        }

        if(Math.abs(rotY) >= Double.MIN_VALUE) {
            pitch(rotY * diff * 0.01f)
            hasViewUpdate = true
        }

        if(hasPositionChanged) {
            hasViewUpdate = true
        }

        skipViewEvent = false
        if(hasViewUpdate) {
            updateViewVectors()
        }

        if(hasPositionChanged) {
            worldFrame.onPositionChanged(position)
            graphicsLoader.onCameraPositionChanged(position)
        }

        lastUpdate = now

        return hasViewUpdate || hasPositionChanged
    }

    private fun updateViewVectors() {
        Matrix4.lookAt(matView, position, target, up)
        (target - position).normalize(forward)
        up.cross(forward, right)

        if(!skipViewEvent) {
            graphicsLoader.onMatrixUpdated(true, matView)
            worldFrame.onMatrixChanged(true, matView)
        }
    }

    private fun updateProjection() {
        Matrix4.perspective(matProjection, Math.toRadians(fov.toDouble()).toFloat(), aspect, zNear, zFar)
        graphicsLoader.onMatrixUpdated(false, matProjection)
        worldFrame.onMatrixChanged(false, matProjection)
    }

    private fun createRotation(angle: Float, axis: Vector3) = Matrix4.rotation(Math.toRadians(angle.toDouble()).toFloat(), axis)
}