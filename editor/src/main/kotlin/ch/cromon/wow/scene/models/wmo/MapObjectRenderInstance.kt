package ch.cromon.wow.scene.models.wmo

import ch.cromon.wow.math.BoundingBox
import ch.cromon.wow.math.Matrix4


data class MapObjectRenderInstance(
        val uuid: Int,
        val boundingBox: BoundingBox,
        val transform: Matrix4,
        val inverseTransform: Matrix4
)