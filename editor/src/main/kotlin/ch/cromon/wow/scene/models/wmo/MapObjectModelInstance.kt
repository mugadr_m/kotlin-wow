package ch.cromon.wow.scene.models.wmo

import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.scene.Scene
import java.util.*
import java.util.concurrent.atomic.AtomicInteger


data class MapObjectModelInstance(
        val modelName: String,
        val model: MapObjectRender,
        private val instances: LinkedList<MapObjectRenderInstance> = LinkedList()
) {
    private val referenceMap = HashMap<Int, AtomicInteger>()
    private val visibleInstances = ArrayList<MapObjectRenderInstance>()
    private val pendingInstances = LinkedList<MapObjectRenderInstance>()
    private val uuidToRemove = LinkedList<Int>()

    fun addInstance(uuid: Int, transform: Matrix4) {
        uuidToRemove.remove(uuid)
        val references = referenceMap[uuid]
        if(references != null) {
            references.incrementAndGet()
            return
        }

        val boundingBox = model.boundingBox
        val transformTransposedInverted = transform.transposed().inverted()
        pendingInstances.add(MapObjectRenderInstance(uuid, boundingBox.translate(transform.transposed()), transform, transformTransposedInverted))
        referenceMap[uuid] = AtomicInteger(1)
    }

    fun removeInstance(uuid: Int) {
        val reference = referenceMap[uuid] ?: return
        val newCount = reference.decrementAndGet()
        if(newCount > 0) {
            return
        }

        referenceMap.remove(uuid)
        uuidToRemove.add(uuid)
    }

    fun onFrame() {
        var numLoaded = 0
        do {
            val elem = pendingInstances.pollFirst() ?: break
            if(uuidToRemove.contains(elem.uuid)) {
                continue
            }
            instances.add(elem)
        } while(++numLoaded < 5)

        numLoaded = 0
        do {
            val elem = uuidToRemove.pollFirst() ?: break
            visibleInstances.removeAll { it.uuid == elem }
            instances.removeAll { it.uuid == elem }
        } while(++numLoaded < 5)

        model.onFrame(visibleInstances)
    }

    fun updateVisibleInstances(scene: Scene) {
        visibleInstances.clear()

        for(instance in instances) {
            if(scene.frustum.intersect(instance.boundingBox)) {
               visibleInstances.add(instance)
            }
        }
    }
}