package ch.cromon.wow.scene

import ch.cromon.wow.io.input.InputController
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.scene.models.ModelManager
import ch.cromon.wow.scene.sky.SkySphere
import ch.cromon.wow.scene.terrain.MapManager
import ch.cromon.wow.utils.Time
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component


@Component
@Scope("singleton")
class WorldFrame {
    @Autowired
    private lateinit var mapManager: MapManager

    @Autowired
    private lateinit var modelManager: ModelManager

    @Autowired
    private lateinit var camera: Camera

    @Autowired
    private lateinit var time: Time

    @Autowired
    private lateinit var skySphere: SkySphere

    @Autowired
    private lateinit var inputController: InputController

    val activeCamera get() = camera

    private val scene = Scene()

    fun initialize() {
        camera.position = Vector3(0, 0, 0)
        camera.target = Vector3(1, 0, 0)
        camera.up = Vector3(0, 0, 1)

        time.reset()

        skySphere.initialize(999.0f)
    }

    fun onFrame() {
        scene.loadSlotsUsed = 0
        scene.maxLoadCount = 16
        scene.time = time.runTime

        inputController.onFrame()

        val hasViewChanged = camera.onUpdate()
        if(hasViewChanged) {
            modelManager.onViewChanged(scene)
        }

        mapManager.onFrame(scene)
        modelManager.onFrame()

        inputController.endFrame()
    }

    fun onPositionChanged(position: Vector3) {
        skySphere.updatePosition(position)
        mapManager.onPositionChanged(position)
    }

    fun onMatrixChanged(view: Boolean, matrix: Matrix4) {
        if(view) {
            scene.frustum.updateView(matrix)
        } else {
            scene.frustum.updateProjection(matrix)
        }
    }
}