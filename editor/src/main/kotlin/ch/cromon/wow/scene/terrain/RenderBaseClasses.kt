package ch.cromon.wow.scene.terrain

import ch.cromon.wow.io.files.map.Tile
import ch.cromon.wow.scene.Scene


abstract class TileRender {
    abstract val data: Tile

    abstract fun initialLoad()
    abstract fun asyncLoad(tile: Tile): Boolean
    abstract fun unload()
    abstract fun onFrame(scene: Scene)

    abstract fun getChunk(index: Int): ChunkRender?
}

abstract class ChunkRender