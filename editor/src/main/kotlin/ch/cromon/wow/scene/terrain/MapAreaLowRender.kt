package ch.cromon.wow.scene.terrain

import ch.cromon.wow.gx.*
import ch.cromon.wow.io.files.map.MapAreaLow
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.math.Vector4
import ch.cromon.wow.scene.Scene
import ch.cromon.wow.utils.set
import org.lwjgl.BufferUtils


class MapAreaLowRender(val indexX: Int, val indexY: Int) {
    private lateinit var vertexBuffer: VertexBuffer
    private lateinit var tileData: MapAreaLow
    private var isSyncLoaded = false
    private var isAsyncLoaded = false

    fun onFrame(scene: Scene) {
        if(!isAsyncLoaded) {
            return
        }

        if(!scene.frustum.intersect(tileData.boundingBox)) {
            return
        }

        if(!isSyncLoaded) {
            if(scene.loadSlotsUsed >= scene.maxLoadCount) {
                return
            }

            syncLoad()
            isSyncLoaded = true
            ++scene.loadSlotsUsed
        }

        mesh.vertexBuffer = vertexBuffer
        mesh.render()
    }

    private fun syncLoad() {
        vertexBuffer = VertexBuffer()
        vertexBuffer.data(tileData.vertices)
    }

    fun asyncLoad(data: MapAreaLow) {
        tileData = data
        isAsyncLoaded = true
    }

    companion object {
        private lateinit var mesh: Mesh
        private lateinit var program: Program

        private var matView = -1
        private var matProjection = -1
        private var clipParameters = -1
        private var fogColor = -1
        private var cameraPosition = -1

        fun updateView(view: Matrix4) {
            program.setMatrix(matView, view)
        }

        fun updateProjection(projection: Matrix4) {
            program.setMatrix(matProjection, projection)
        }

        fun updateFogColor(color: Vector4) {
            program.setVec4(fogColor, color)
        }

        fun updateClipParameters(clipParameters: Vector2) {
            program.setVec2(this.clipParameters, clipParameters)
        }

        fun updateCameraPosition(position: Vector3) {
            program.setVec3(cameraPosition, position)
        }

        fun initialize() {
            program = Program()
            program.compileFromResource(Shader.TERRAIN_LOW)
            program.link()

            matView = program.getUniform("matView")
            matProjection = program.getUniform("matProjection")
            clipParameters = program.getUniform("clipParameters")
            fogColor = program.getUniform("fogColor")
            cameraPosition = program.getUniform("cameraPosition")

            mesh = Mesh()
            mesh.program = program

            mesh.addElement(VertexElement("position", 0, 3))

            initIndices()
            mesh.indexCount = 16 * 16 * 4 * 3
            mesh.depthCheck = DepthCheck.DISABLED
            mesh.finalize()
        }

        private fun initIndices() {
            val indices = BufferUtils.createShortBuffer(16 * 16 * 12)
            for(y in 0 until 16) {
                for(x in 0 until 16) {
                    val i = y * 16 * 12 + x * 12

                    indices[i] = (y * 33 + x).toShort()
                    indices[i + 2] = (y * 33 + x + 1).toShort()
                    indices[i + 1] = (y * 33 + x + 17).toShort()

                    indices[i + 3] = (y * 33 + x + 1).toShort()
                    indices[i + 5] = (y * 33 + x + 34).toShort()
                    indices[i + 4] = (y * 33 + x + 17).toShort()

                    indices[i + 6] = (y * 33 + x + 34).toShort()
                    indices[i + 8] = (y * 33 + x + 33).toShort()
                    indices[i + 7] = (y * 33 + x + 17).toShort()

                    indices[i + 9] = (y * 33 + x + 33).toShort()
                    indices[i + 11] = (y * 33 + x).toShort()
                    indices[i + 10] = (y * 33 + x + 17).toShort()
                }
            }

            mesh.indexBuffer = IndexBuffer(IndexType.UINT16)
            mesh.indexBuffer.data(indices)
        }
    }
}