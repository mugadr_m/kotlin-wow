package ch.cromon.wow.scene.terrain.alpha

import ch.cromon.wow.io.files.texture.TextureManager
import ch.cromon.wow.scene.models.ModelManager
import ch.cromon.wow.scene.terrain.RenderTileProvider
import ch.cromon.wow.scene.terrain.wotlk.MapTileRender
import ch.cromon.wow.scene.terrain.wotlk.TerrainMaterial
import ch.cromon.wow.utils.BuildVersion
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component


@Component
@Scope("singleton")
@BuildVersion(minBuild = 0, maxBuild = 3494)
class AlphaTileProvider : RenderTileProvider {
    @Autowired
    private lateinit var textureManager: TextureManager

    @Autowired
    private lateinit var modelManager: ModelManager

    override fun createTile() = MapTileRender(textureManager, modelManager)

    override fun onEnterWorld(mapName: String) {
        MapTileRender.activeMaterial = TerrainMaterial.OLD_BLEND
    }

}