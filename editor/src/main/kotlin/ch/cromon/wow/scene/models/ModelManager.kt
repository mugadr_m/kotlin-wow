package ch.cromon.wow.scene.models

import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.scene.Scene
import ch.cromon.wow.scene.models.wmo.MapObjectManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component


@Component
@Scope("singleton")
class ModelManager {
    @Autowired
    private lateinit var wmoManager: MapObjectManager

    fun loadWmo(path: String, uuid: Int, transform: Matrix4) {
        wmoManager.load(path, uuid, transform)
    }

    fun unloadWmo(modelPath: String, uuid: Int) {
        wmoManager.unload(modelPath, uuid)
    }

    fun onFrame() {
        wmoManager.onFrame()
    }

    fun onViewChanged(scene: Scene) {
        wmoManager.onViewChanged(scene)
    }
}