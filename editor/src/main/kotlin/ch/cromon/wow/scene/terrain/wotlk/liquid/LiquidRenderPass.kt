package ch.cromon.wow.scene.terrain.wotlk.liquid

import ch.cromon.wow.gx.IndexBuffer
import ch.cromon.wow.gx.IndexType
import ch.cromon.wow.gx.Texture
import ch.cromon.wow.gx.VertexBuffer
import ch.cromon.wow.io.files.map.wotlk.liquid.LiquidTypeRender
import ch.cromon.wow.io.files.texture.TextureManager
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.scene.Scene


class LiquidRenderPass(private val renderer: LiquidTypeRender) {
    private lateinit var vertexBuffer: VertexBuffer
    private lateinit var indexBuffer: IndexBuffer

    private lateinit var texture: Texture
    private val textures = ArrayList<Texture>()

    fun syncLoad(textureManager: TextureManager) {
        vertexBuffer = VertexBuffer()
        indexBuffer = IndexBuffer(IndexType.UINT32)

        vertexBuffer.data(renderer.fillBuffer())
        indexBuffer.data(renderer.fillIndexBuffer())

        renderer.liquidTextureNames.mapTo(textures, { textureManager.loadTexture(it) })
    }

    fun onFrame(matTransform: Matrix4, scene: Scene) {
        val textureIndex = (scene.time / 60.0f).toInt() % textures.size
        LiquidMaterial.setColors(renderer.isOcean)
        LiquidMaterial.onFrame(vertexBuffer, indexBuffer, renderer.indexCount, matTransform, textures[textureIndex])
    }
}