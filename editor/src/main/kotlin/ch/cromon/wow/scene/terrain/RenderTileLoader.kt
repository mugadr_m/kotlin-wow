package ch.cromon.wow.scene.terrain

import ch.cromon.wow.io.data.DataLoadEvent
import ch.cromon.wow.utils.BuildBeanEntry
import ch.cromon.wow.utils.CdiUtils
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.DependsOn
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

interface RenderTileProvider {
    fun createTile(): TileRender
    fun onEnterWorld(mapName: String)
}

@Component
@Scope("singleton")
@DependsOn("CdiUtils")
class RenderTileLoader {
    private val loaders = ArrayList<BuildBeanEntry<RenderTileProvider>>()

    @Autowired
    private lateinit var logger: Logger

    private lateinit var loader: RenderTileProvider

    @PostConstruct
    fun initialize() {
        loaders.addAll(CdiUtils.getAllVersionBeans(RenderTileProvider::class.java))
    }

    @EventListener
    fun onDataLoaded(event: DataLoadEvent) {
        if(!event.success) {
            return
        }

        val build = event.build
        val maybeLoader = loaders.firstOrNull {
            it.minBuild <= build && it.maxBuild >= build
        }

        if(maybeLoader == null) {
            logger.error("Unable to find a render tile loader for build $build")
            throw IllegalStateException("Build not supported")
        }

        logger.info("Using render tile loader: ${maybeLoader.bean.javaClass}")
        loader = maybeLoader.bean
    }

    fun createTile() = loader.createTile()
    fun onEnterWorld(mapName: String) = loader.onEnterWorld(mapName)
}