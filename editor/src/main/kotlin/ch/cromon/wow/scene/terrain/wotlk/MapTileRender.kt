package ch.cromon.wow.scene.terrain.wotlk

import ch.cromon.wow.gx.Texture
import ch.cromon.wow.gx.VertexBuffer
import ch.cromon.wow.io.files.map.Tile
import ch.cromon.wow.io.files.map.single.AbstractSingleMapTile
import ch.cromon.wow.io.files.texture.TextureManager
import ch.cromon.wow.scene.Scene
import ch.cromon.wow.scene.models.ModelManager
import ch.cromon.wow.scene.terrain.ChunkRender
import ch.cromon.wow.scene.terrain.TileRender
import ch.cromon.wow.scene.terrain.wotlk.liquid.MapLiquidRender
import java.io.File


class MapTileRender(private val textureManager: TextureManager, private val modelManager: ModelManager) : TileRender() {
    private val mapChunks = ArrayList<MapChunkRender>()

    private var asyncLoaded = false
    private var isSyncLoaded = false
    private lateinit var vertexBuffer: VertexBuffer

    private val textures = ArrayList<Texture>()
    private val specularTextures = ArrayList<Texture>()

    private lateinit var liquidRender: MapLiquidRender

    private lateinit var tile: AbstractSingleMapTile
    override val data get() = tile

    override fun getChunk(index: Int): ChunkRender? {
        if(index >= mapChunks.size) {
            return null
        }

        return mapChunks[index]
    }

    override fun unload() {
        if(isSyncLoaded) {
            vertexBuffer.delete()
            mapChunks.forEach {
                it.unload(this)
            }
        }

        textures.forEach { textureManager.release(it) }
        tile.unload()
    }

    override fun initialLoad() {
        syncLoad()
        for(chunk in mapChunks) {
            chunk.initialLoad()
        }
    }

    override fun asyncLoad(tile: Tile): Boolean {
        if (tile !is AbstractSingleMapTile) {
            return false
        }

        this.tile = tile

        tile.textures.forEach {
            textures.add(textureManager.loadTexture(it))
            val path = File(it)
            specularTextures.add(textureManager.loadTexture("${path.parentFile?.path ?: ""}\\${path.nameWithoutExtension}_s.blp"))
        }

        tile.chunks.forEach {
            val mapChunk = MapChunkRender()
            if (!mapChunk.asyncLoad(it, this)) {
                return false
            }

            mapChunks.add(mapChunk)
        }

        liquidRender = MapLiquidRender(tile.indexX, tile.indexY, textureManager)
        liquidRender.asyncLoad(tile.liquidData)

        asyncLoaded = true
        return true
    }

    override fun onFrame(scene: Scene) {
        if(!scene.frustum.intersect(tile.boundingBox)) {
            return
        }

        if (!asyncLoaded) {
            return
        }

        if (!isSyncLoaded) {
            if(scene.loadSlotsUsed >= scene.maxLoadCount) {
                return
            }
            syncLoad()
            ++scene.loadSlotsUsed
        }

        liquidRender.onFrame(scene)

        val mesh = activeMaterial.mesh
        mesh.vertexBuffer = vertexBuffer
        var baseVertex = 0
        for (chunk in mapChunks) {
            chunk.onFrame(scene, baseVertex, activeMaterial)
            baseVertex += 145
        }
    }

    internal fun referenceMapObject(reference: Int) {
        if(reference >= tile.mapObjectDefinitions.size) {
            return
        }

        val mapObjDef = tile.mapObjectDefinitions[reference]
        modelManager.loadWmo(mapObjDef.modelName, mapObjDef.uniqueId, mapObjDef.transform)
    }

    internal fun unreferenceMapObject(reference: Int) {
        if(reference >= tile.mapObjectDefinitions.size) {
            return
        }

        val mapObjDef = tile.mapObjectDefinitions[reference]
        modelManager.unloadWmo(mapObjDef.modelName, mapObjDef.uniqueId)
    }

    internal fun getTextureByIndex(index: Int) = textures[index]
    internal fun getSpecularTextureByIndex(index: Int) = specularTextures[index]

    private fun syncLoad() {
        isSyncLoaded = true
        vertexBuffer = VertexBuffer()
        vertexBuffer.data(tile.fullVertices)
    }

    companion object {
        lateinit var activeMaterial: TerrainMaterial
    }
}