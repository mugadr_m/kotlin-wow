package ch.cromon.wow.scene

import ch.cromon.wow.math.Frustum


class Scene {
    var maxLoadCount = 0
    var loadSlotsUsed = 0
    var time: Long = 0
    val frustum = Frustum()
}