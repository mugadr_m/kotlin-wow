package ch.cromon.wow.scene.sky

import ch.cromon.wow.gx.*
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.math.Vector4
import org.lwjgl.BufferUtils
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component
@Scope("singleton")
class SkySphere {
    private val skyDataBuffer = BufferUtils.createByteBuffer(180 * 4)
    private var isDirty = false
    private lateinit var vertexBuffer: VertexBuffer
    private lateinit var indexBuffer: IndexBuffer
    private var indexCount = 0
    private lateinit var skyTexture: Texture
    private var matTransform = Matrix4()
    private var isValid = false
    private var positionChanged = false

    fun onFrame() {
        if(indexCount <= 0 || !isValid) {
            return
        }

        if(isDirty) {
            skyTexture.loadArgb(1, 180, skyDataBuffer)
            isDirty = false
        }

        mesh.textureInput.add(uniformTexture, skyTexture)
        mesh.indexCount = indexCount
        mesh.vertexBuffer = vertexBuffer
        mesh.indexBuffer = indexBuffer

        if(positionChanged) {
            positionChanged = false
            mesh.program.setMatrix(uniformTransform, matTransform)
        }

        mesh.render()
    }

    fun updatePosition(position: Vector3) {
        Matrix4.translation(matTransform, position)
        positionChanged = true
    }

    fun initialize(radius: Float) {
        initVertices(radius, 100, 100)
        isValid = true
    }

    fun asyncUpdate(colors: Array<Vector4>) {
        val colorTop = colors[LightColor.Top.ordinal]
        val colorMiddle = colors[LightColor.Middle.ordinal]
        val colorLower = colors[LightColor.MiddleLower.ordinal]
        val colorLow = colors[LightColor.Lower.ordinal]
        val colorHorizon = colors[LightColor.Horizon.ordinal]
        val fog = colors[LightColor.Fog.ordinal]
        val fogColor = fog.toXrgb()
        for(i in 0 until 80) {
            skyDataBuffer.putInt(i * 4, fogColor)
        }
        for(i in 80 until 90) {
            val sat = (i - 80.0f) / 10.0f
            skyDataBuffer.putInt(i * 4, (fog + (colorHorizon - fog) * sat).toXrgb())
        }
        for(i in 90 until 95) {
            val sat = (i - 90.0f) / 5.0f
            skyDataBuffer.putInt(i * 4, (colorHorizon + (colorLow - colorHorizon) * sat).toXrgb())
        }
        for(i in 95 until 105) {
            val sat = (i - 95.0f) / 10.0f
            skyDataBuffer.putInt(i * 4, (colorLow + (colorLower - colorLow) * sat).toXrgb())
        }
        for(i in 105 until 120) {
            val sat = (i - 105.0f) / 15.0f
            skyDataBuffer.putInt(i * 4, (colorLower + (colorMiddle - colorLower) * sat).toXrgb())
        }
        for(i in 120 until 180) {
            val sat = (i - 120.0f) / 60.0f
            skyDataBuffer.putInt(i * 4, (colorMiddle + (colorTop - colorMiddle) * sat).toXrgb())
        }
        isDirty = true
    }

    @Suppress("LocalVariableName")
    private fun initVertices(radius: Float, rings: Int, sectors: Int) {
        val R = 1.0f / (rings - 1)
        val S = 1.0f / (sectors - 1)

        val vertexData = BufferUtils.createFloatBuffer(5 * rings * sectors)

        for(r in 0 until rings) {
            for(s in 0 until sectors) {
                val z = Math.sin(-(Math.PI / 2.0f) + Math.PI * r * R).toFloat()
                val x = (Math.cos((2 * Math.PI * s * S)) * Math.sin(Math.PI * r * R)).toFloat()
                val y = (Math.sin((2 * Math.PI * s * S)) * Math.sin(Math.PI * r * R)).toFloat()

                vertexData.put(x * radius).put(y * radius).put(z * radius)
                vertexData.put(s * S).put(r * R)
            }
        }
        vertexData.flip()

        val indexData = BufferUtils.createIntBuffer(rings * sectors * 6)
        for(r in 0 until (rings - 1)) {
            for(s in 0 until (sectors - 1)) {
                indexData.put(r * sectors + s).put((r + 1) * sectors + s + 1).put(r * sectors + s + 1)
                indexData.put(r * sectors + s).put((r + 1) * sectors + s).put((r + 1) * sectors + s + 1)
            }
        }
        indexData.flip()

        vertexBuffer = VertexBuffer()
        vertexBuffer.data(vertexData)

        indexBuffer = IndexBuffer(IndexType.UINT32)
        indexBuffer.data(indexData)

        indexCount = rings * sectors * 6
        skyTexture = Texture()
        skyTexture.loadArgb(1, 180, skyDataBuffer)
        skyTexture.setFiltering(TextureFilter.LINEAR, TextureFilter.LINEAR)
    }

    companion object {
        private lateinit var mesh: Mesh
        private lateinit var program: Program

        private var uniformView = -1
        private var uniformProjection = -1
        private var uniformTransform = -1
        private var uniformTexture = -1

        fun updateView(matrix: Matrix4) {
            program.setMatrix(uniformView, matrix)
        }

        fun updateProjection(matrix: Matrix4) {
            program.setMatrix(uniformProjection, matrix)
        }

        fun initialize() {
            mesh = Mesh()
            mesh.addElement(VertexElement("position", 0, 3))
            mesh.addElement(VertexElement("texCoord", 0, 2))

            program = Program()

            program.compileFromResource(Shader.SKY)
            program.link()

            uniformView = program.getUniform("matView")
            uniformProjection = program.getUniform("matProjection")
            uniformTransform = program.getUniform("matTransform")
            uniformTexture = program.getUniform("skyTexture")

            mesh.program = program

            mesh.finalize()
        }
    }
}