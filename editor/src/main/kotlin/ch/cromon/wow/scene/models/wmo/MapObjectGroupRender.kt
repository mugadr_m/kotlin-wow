package ch.cromon.wow.scene.models.wmo

import ch.cromon.wow.gx.BlendMode
import ch.cromon.wow.gx.CullState
import ch.cromon.wow.gx.Mesh
import ch.cromon.wow.io.files.wmo.AbstractMapObjectGroup


class MapObjectGroupRender(val parent: MapObjectRender) {
    private var isAsyncLoaded = false

    private val batches = ArrayList<MapObjectBatchRender>()

    var startVertex: Int = 0
        private set

    fun onFrame(mesh: Mesh, instances: List<MapObjectRenderInstance>) {
        if (!isAsyncLoaded) {
            return
        }

        var activeCullState = CullState.DISABLED
        CullState.DISABLED.apply()
        for (batch in batches) {
            if (batch.hasBlend && mesh.blendMode != BlendMode.ALPHA) {
                mesh.blendMode = BlendMode.ALPHA
                BlendMode.ALPHA.apply()
            } else if (!batch.hasBlend && mesh.blendMode != BlendMode.NONE) {
                mesh.blendMode = BlendMode.NONE
                BlendMode.NONE.apply()
            }

            if (batch.program != mesh.program) {
                mesh.program = batch.program
                mesh.setup()
            }

            MapObjectMesh.setTexture(batch.texture, if(batch.hasBlend) (10.0f / 255.0f) else 0.0f)
            mesh.startIndex = batch.startIndex
            mesh.indexCount = batch.numIndices

            if(batch.cullState != activeCullState) {
                batch.cullState.apply()
                activeCullState = batch.cullState
            }

            for(instance in instances) {
                MapObjectMesh.setup(instance.transform, instance.inverseTransform)
                mesh.draw()
            }
        }
    }

    fun asyncLoad(group: AbstractMapObjectGroup) {
        startVertex = group.baseVertex

        group.batches.sortedBy { parent.getMaterial(it).blendMode }.mapTo(batches, {
            val material = parent.getMaterial(it)
            MapObjectBatchRender(
                    material.texture,
                    if(group.isIndoor) MapObjectMesh.indoorProgram else MapObjectMesh.outdoorProgram,
                    group.baseIndex + it.startIndex,
                    it.numIndices,
                    material.blendMode != 0,
                    if(material.cullDisabled) CullState.DISABLED else CullState.CCW
            )
        })

        isAsyncLoaded = true
    }
}