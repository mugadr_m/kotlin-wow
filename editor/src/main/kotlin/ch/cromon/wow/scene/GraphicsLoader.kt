package ch.cromon.wow.scene

import ch.cromon.wow.gx.GraphicsLoadedEvent
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.math.Vector4
import ch.cromon.wow.scene.models.wmo.MapObjectMesh
import ch.cromon.wow.scene.sky.SkySphere
import ch.cromon.wow.scene.terrain.MapAreaLowRender
import ch.cromon.wow.scene.terrain.wotlk.TerrainMaterial.Companion.NEW_BLEND
import ch.cromon.wow.scene.terrain.wotlk.TerrainMaterial.Companion.OLD_BLEND
import ch.cromon.wow.scene.terrain.wotlk.liquid.LiquidMaterial
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component


@Component
@Scope("singleton")
class GraphicsLoader {
    @Autowired
    private lateinit var worldFrame: WorldFrame

    @EventListener
    fun onGraphicsReady(event: GraphicsLoadedEvent) {
        NEW_BLEND.initialize()
        OLD_BLEND.initialize()
        SkySphere.initialize()
        MapAreaLowRender.initialize()
        LiquidMaterial.initialize()
        MapObjectMesh.initialize()
    }

    fun onCameraPositionChanged(position: Vector3) {
        MapAreaLowRender.updateCameraPosition(position)
        NEW_BLEND.updateCameraPosition(position)
        OLD_BLEND.updateCameraPosition(position)
        LiquidMaterial.updateCameraPosition(position)
        MapObjectMesh.updateCameraPosition(position)
    }

    fun onMatrixUpdated(isView: Boolean, matrix: Matrix4) {
        if(isView) {
            NEW_BLEND.updateView(matrix)
            OLD_BLEND.updateView(matrix)
            SkySphere.updateView(matrix)
            MapAreaLowRender.updateView(matrix)
            LiquidMaterial.updateView(matrix)
            MapObjectMesh.updateView(matrix)
        } else {
            NEW_BLEND.updateProjection(matrix)
            OLD_BLEND.updateProjection(matrix)
            SkySphere.updateProjection(matrix)
            MapAreaLowRender.updateProjection(matrix)
            LiquidMaterial.updateProjection(matrix)
            MapObjectMesh.updateProjection(matrix)
        }
    }

    fun onUpdateDiffuseLight(color: Vector4) {
        NEW_BLEND.updateDiffuse(color)
        OLD_BLEND.updateDiffuse(color)
        MapObjectMesh.updateDiffuseLight(color)
    }

    fun onUpdateAmbientLight(color: Vector4) {
        NEW_BLEND.updateAmbient(color)
        OLD_BLEND.updateAmbient(color)
        MapObjectMesh.updateAmbientLight(color)
    }

    fun onUpdateFogParams(color: Vector4, distance: Float) {
        val fogStart = Math.min(distance, 890.0f)
        val fogEnd = 900.0f
        val clipEnd = worldFrame.activeCamera.zFar

        MapAreaLowRender.updateFogColor(color)
        MapAreaLowRender.updateClipParameters(Vector2(fogEnd, clipEnd))

        val clipParameters = Vector3(fogStart, fogEnd, clipEnd)

        NEW_BLEND.updateFogColor(color)
        NEW_BLEND.updateClipParameters(clipParameters)

        OLD_BLEND.updateFogColor(color)
        OLD_BLEND.updateClipParameters(clipParameters)

        LiquidMaterial.updateFogColor(color)
        LiquidMaterial.updateClipParameters(clipParameters)

        MapObjectMesh.updateFogColor(color)
        MapObjectMesh.updateClipParameters(clipParameters)
    }

    fun onUpdateWaterColors(riverLight: Vector4, riverDark: Vector4, oceanLight: Vector4, oceanDark: Vector4) {
        LiquidMaterial.run {
            oceanColorDark = oceanDark
            oceanColorLight = oceanLight

            riverColorDark = riverDark
            riverColorLight = riverLight
        }
    }
}