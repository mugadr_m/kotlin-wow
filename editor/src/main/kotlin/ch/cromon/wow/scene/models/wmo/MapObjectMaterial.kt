package ch.cromon.wow.scene.models.wmo

import ch.cromon.wow.gx.Texture


data class MapObjectMaterial(
        val textureName: String,
        val texture: Texture,
        val blendMode: Int,
        val cullDisabled: Boolean
)