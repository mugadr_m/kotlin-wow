package ch.cromon.wow.scene.terrain.wotlk

import ch.cromon.wow.gx.Texture
import ch.cromon.wow.gx.TextureFilter
import ch.cromon.wow.io.files.map.single.AbstractSingleMapChunk
import ch.cromon.wow.scene.Scene
import ch.cromon.wow.scene.terrain.ChunkRender


class MapChunkRender : ChunkRender() {
    private val alphaTexture = Texture()
    private val textures = ArrayList<Texture>()
    private val specularTextures = ArrayList<Texture>()

    private lateinit var chunk: AbstractSingleMapChunk

    private var syncLoaded = false

    fun unload(tile: MapTileRender) {
        alphaTexture.delete()
        chunk.mapObjectReferences.forEach {
            tile.unreferenceMapObject(it)
        }
    }

    fun initialLoad() {
        syncLoad()
    }

    fun asyncLoad(chunk: AbstractSingleMapChunk, tile: MapTileRender): Boolean {
        this.chunk = chunk
        chunk.textureLayers.forEach {
            textures.add(tile.getTextureByIndex(it.textureId))
            specularTextures.add(tile.getSpecularTextureByIndex(it.textureId))
        }

        chunk.mapObjectReferences.forEach {
            tile.referenceMapObject(it)
        }
        return true
    }

    fun onFrame(scene: Scene, baseVertex: Int, material: TerrainMaterial) {
        if(!scene.frustum.intersect(chunk.boundingBox)) {
            return
        }

        if (!syncLoaded) {
            if(scene.loadSlotsUsed >= scene.maxLoadCount) {
                return
            }

            syncLoad()
            ++scene.loadSlotsUsed
        }

        val mesh = material.mesh
        material.setTextures(textures, specularTextures, alphaTexture)
        mesh.startVertex = baseVertex
        mesh.render()
    }

    private fun syncLoad() {
        syncLoaded = true
        alphaTexture.loadArgb(64, 64, chunk.alphaData)
        alphaTexture.setFiltering(TextureFilter.LINEAR, TextureFilter.LINEAR)
    }
}