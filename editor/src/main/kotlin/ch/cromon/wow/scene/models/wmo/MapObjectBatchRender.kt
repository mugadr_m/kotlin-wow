package ch.cromon.wow.scene.models.wmo

import ch.cromon.wow.gx.CullState
import ch.cromon.wow.gx.Program
import ch.cromon.wow.gx.Texture


data class MapObjectBatchRender(
        val texture: Texture,
        val program: Program,
        val startIndex: Int,
        val numIndices: Int,
        val hasBlend: Boolean,
        val cullState: CullState
)