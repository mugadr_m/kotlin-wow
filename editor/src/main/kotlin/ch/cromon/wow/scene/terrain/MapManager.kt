package ch.cromon.wow.scene.terrain

import ch.cromon.wow.AppStateManager
import ch.cromon.wow.ApplicationState
import ch.cromon.wow.gx.CullState
import ch.cromon.wow.io.files.map.MapLoader
import ch.cromon.wow.io.files.storage.DataStorage
import ch.cromon.wow.io.files.texture.TextureManager
import ch.cromon.wow.math.Vector2
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.scene.Scene
import ch.cromon.wow.scene.WorldFrame
import ch.cromon.wow.scene.sky.SkyManager
import ch.cromon.wow.ui.controller.EditController
import ch.cromon.wow.utils.CHUNK_SIZE
import ch.cromon.wow.utils.TILE_SIZE
import javafx.application.Platform
import org.lwjgl.opengl.GL11.glDepthRange
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Scope
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.CompletableFuture
import kotlin.properties.Delegates

data class MapLoadProgressEvent(val loaded: Int, val total: Int, private val eventSource: Any) : ApplicationEvent(eventSource)
data class MapLoadFinishedEvent(val mapId: Int, val continent: String, val displayName: String, val position: Vector3, private val eventSource: Any) : ApplicationEvent(eventSource)

data class MapLoadFailedEvent(private val eventSource: Any) : ApplicationEvent(eventSource)

@Component
@Scope("singleton")
class MapManager {
    companion object {
        fun getIndex(x: Int, y: Int) = y * 64 + x
        fun inverseIndex(index: Int) = (index % 64) to (Math.floor(index / 64.0)).toInt()
        const val LOAD_RADIUS = 2
    }

    @Autowired
    private lateinit var mapLoader: MapLoader

    @Autowired
    private lateinit var logger: Logger

    @Autowired
    private lateinit var renderLoader: RenderTileLoader

    @Autowired
    private lateinit var appStateManager: AppStateManager

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    @Autowired
    private lateinit var worldFrame: WorldFrame

    @Autowired
    private lateinit var skyManager: SkyManager

    @Autowired
    private lateinit var dataManager: DataStorage

    @Autowired
    private lateinit var textureManager: TextureManager

    @Autowired
    private lateinit var mapLowManager: MapAreaLowManager

    @Autowired
    private lateinit var editController: EditController

    private lateinit var mapName: String
    private var mapId = -1

    private var activeTiles = HashMap<Int, TileRender>()
    private val loadedTiles = LinkedList<TileRender>()
    private var tilesToLoad: List<Int> = ArrayList()

    private var hasOneLoadedTile = false
    private var isInitialLoad = false
    private var initialLoadStepsRequired = 0
    private var totalLoadSteps by Delegates.observable(0, { _, _, _ -> onLoadProgress() })
    private var fireInitialLoadDone = false

    private var unloadLock = Any()

    private var currentTileX = -1
    private var currentTileY = -1

    private var entryPosition = Vector3()

    fun onPositionChanged(position: Vector3) {
        skyManager.onUpdatePosition(position)
        onUpdateLoadedTiles(position)
    }

    fun onFrame(scene: Scene) {
        skyManager.onFrame()

        val state = appStateManager.currentState
        if (!state.loadAllowed && !state.renderWorld) {
            return
        }

        mapLowManager.onFrame(scene)
        while (loadedTiles.size > 0) {
            val tile = loadedTiles.pollLast()
            if (tile != null) {
                activeTiles[getIndex(tile.data.indexX, tile.data.indexY)] = tile
            }

            if (isInitialLoad) {
                tile.initialLoad()
                ++totalLoadSteps
            }
        }

        if (fireInitialLoadDone) {
            mapLowManager.updatePosition(entryPosition)
            onLoadDone()
            appStateManager.currentState = ApplicationState.MAP_EDIT
            fireInitialLoadDone = false
        }

        if (!state.renderWorld) {
            return
        }

        CullState.CCW.apply()
        for (entries in activeTiles) {
            entries.value.onFrame(scene)
        }

        glDepthRange(0.0, 1.0)
    }

    fun onEnterWorld(mapId: Int, mapName: String, entryPoint: Vector2) {
        renderLoader.onEnterWorld(mapName)
        entryPosition = Vector3(entryPoint.x, entryPoint.y, 0.0f)
        this.mapId = mapId
        this.mapName = mapName
        initialLoad(entryPoint).handle { success, error ->
            if (error != null) {
                logger.error("Error loading map: ", error)
                Platform.runLater {
                    publisher.publishEvent(MapLoadFailedEvent(this))
                }
            } else {
                when (success) {
                    false -> {
                        logger.error("Unable to load map, no tiles found")
                        Platform.runLater {
                            publisher.publishEvent(MapLoadFailedEvent(this))
                        }
                    }
                    true -> logger.info("World loaded")
                }
            }
        }
    }

    private fun initialLoad(entry: Vector2): CompletableFuture<Boolean> {
        val ix = Math.floor((entry.x / TILE_SIZE).toDouble()).toInt()
        val iy = Math.floor((entry.y / TILE_SIZE).toDouble()).toInt()

        return CompletableFuture.supplyAsync {
            var hasLoadedTile = false
            initialLoadStepsRequired = (LOAD_RADIUS * 2 + 1) * (LOAD_RADIUS * 2 + 1) * 2
            isInitialLoad = true
            hasOneLoadedTile = false
            for (x in (ix - LOAD_RADIUS)..(ix + LOAD_RADIUS)) {
                for (y in (iy - LOAD_RADIUS)..(iy + LOAD_RADIUS)) {
                    val tile = mapLoader.loadTile(mapName, x, y)
                    if(tile == null) {
                        totalLoadSteps += 2
                        continue
                    }

                    val renderTile = renderLoader.createTile()
                    if (!renderTile.asyncLoad(tile)) {
                        totalLoadSteps += 2
                        continue
                    }

                    hasLoadedTile = true
                    hasOneLoadedTile = true
                    ++totalLoadSteps
                    loadedTiles.add(renderTile)
                }
            }
            hasLoadedTile
        }
    }

    private fun onLoadProgress() {
        if (!isInitialLoad || initialLoadStepsRequired <= 0 || !hasOneLoadedTile) {
            return
        }

        val progressEvent = MapLoadProgressEvent(totalLoadSteps, initialLoadStepsRequired, this)
        publisher.publishEvent(progressEvent)

        if (totalLoadSteps >= initialLoadStepsRequired) {
            isInitialLoad = false
            fireInitialLoadDone = true
        }
    }

    private fun onLoadDone() {
        currentTileX = (entryPosition.x / TILE_SIZE).toInt()
        currentTileY = (entryPosition.y / TILE_SIZE).toInt()
        entryPosition.z = 400.0f
        worldFrame.activeCamera.changePosition(entryPosition)
        val displayName = dataManager.getMapName(mapId) ?: "<Unknown Map>"
        publisher.publishEvent(MapLoadFinishedEvent(mapId, mapName, displayName, entryPosition, this))
    }

    private fun updateZoneId(position: Vector3) {
        val tx = (position.x / TILE_SIZE).toInt()
        val ty = (position.y / TILE_SIZE).toInt()
        val curIndex = getIndex(tx, ty)
        val tile = activeTiles[curIndex]
        if(tile != null) {
            val cpx = position.x - tx * TILE_SIZE
            val cpy = position.y - ty * TILE_SIZE
            val cx = (cpx / CHUNK_SIZE).toInt()
            val cy = (cpy / CHUNK_SIZE).toInt()
            val chunk = tile.data.getChunk(cx, cy)
            editController.onPositionChanged(position, chunk.zoneId)
        }
    }

    private fun onUpdateLoadedTiles(position: Vector3) {
        updateZoneId(position)

        val tx = (position.x / TILE_SIZE).toInt()
        val ty = (position.y / TILE_SIZE).toInt()
        if (tx == currentTileX && ty == currentTileY) {
            return
        }

        val tilesToLoad = ArrayList<Int>()
        for (i in (tx - LOAD_RADIUS)..(tx + LOAD_RADIUS)) {
            for (j in (ty - LOAD_RADIUS)..(ty + LOAD_RADIUS)) {
                val index = getIndex(i, j)
                if (!activeTiles.containsKey(index)) {
                    tilesToLoad.add(index)
                }
            }
        }

        val tilesToUnload = ArrayList<Int>()
        for (i in (currentTileX - LOAD_RADIUS)..(currentTileX + LOAD_RADIUS)) {
            for (j in (currentTileY - LOAD_RADIUS)..(currentTileY + LOAD_RADIUS)) {
                val dx = Math.abs(tx - i)
                val dy = Math.abs(ty - j)
                if (dx > LOAD_RADIUS || dy > LOAD_RADIUS) {
                    tilesToUnload.add(getIndex(i, j))
                }
            }
        }

        currentTileX = tx
        currentTileY = ty

        unloadTiles(tilesToUnload)
        loadTiles(tilesToLoad)

        mapLowManager.updatePosition(position)
    }

    private fun unloadTiles(unload: Collection<Int>) {
        for (index in unload) {
            val tile = activeTiles[index] ?: continue
            tile.unload()
            activeTiles.remove(index)
        }
    }

    private fun loadTiles(load: Collection<Int>) {
        tilesToLoad = load.toList()
    }

    @Scheduled(fixedRate = 100)
    private fun mapLoadScheduler() {
        val loadTiles = synchronized(unloadLock) {
            val ret = tilesToLoad
            tilesToLoad = listOf()
            ret
        }

        if (loadTiles.isEmpty()) {
            return
        }

        loadTiles.forEach {
            val (x, y) = inverseIndex(it)
            val tile = mapLoader.loadTile(mapName, x, y) ?: return@forEach
            val renderTile = renderLoader.createTile()
            if (!renderTile.asyncLoad(tile)) {
                ++totalLoadSteps
                return@forEach
            }

            loadedTiles.add(renderTile)
        }

        textureManager.compact()
    }
}