package ch.cromon.wow.scene.models.wmo

import ch.cromon.wow.gx.CullState
import ch.cromon.wow.io.data.DataLoadEvent
import ch.cromon.wow.io.files.texture.TextureManager
import ch.cromon.wow.io.files.wmo.WmoProvider
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.scene.Scene
import ch.cromon.wow.utils.CdiUtils
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.DependsOn
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.util.*


@Component
@Scope("singleton")
@DependsOn("CdiUtils")
class MapObjectManager {
    private lateinit var wmoProvider: WmoProvider

    @Autowired
    private lateinit var textureManager: TextureManager

    @Autowired
    private lateinit var logger: Logger

    private val models = LinkedList<MapObjectModelInstance>()

    private val loadQueue = LinkedList<MapObjectModelInstance>()
    private val modelMap = HashMap<String, MapObjectModelInstance>()

    @EventListener
    fun onDataLoaded(event: DataLoadEvent) {
        if (!event.success) {
            return
        }

        wmoProvider = CdiUtils.getAllVersionBeans(WmoProvider::class.java).filter {
            it.minBuild <= event.build && it.maxBuild >= event.build
        }.map {
            it.bean
        }.firstOrNull() ?: throw IllegalStateException("No wmo provider found for version ${event.build}")

        logger.info("Using WMO loader: ${wmoProvider.javaClass}")
    }

    fun onFrame() {
        var numLoaded = 0
        do {
            val elem = loadQueue.pollFirst() ?: break
            models.add(elem)
        } while(++numLoaded < 5)

        CullState.DISABLED.apply()
        for (model in models) {
            model.onFrame()
        }
    }

    fun load(modelName: String, uuid: Int, transform: Matrix4) {
        val model = modelMap[modelName.toUpperCase()] ?: loadModel(modelName)
        if (model != null) {
            model.addInstance(uuid, transform)
        } else {
            logger.info("Unable to load model $modelName")
        }
    }

    fun unload(modelName: String, uuid: Int) {
        val model = modelMap[modelName.toUpperCase()] ?: return
        model.removeInstance(uuid)
    }

    private fun loadModel(modelName: String): MapObjectModelInstance? {
        val model = wmoProvider.load(modelName) ?: return null
        val renderer = MapObjectRender(textureManager)
        renderer.asyncLoad(model)
        val ret = MapObjectModelInstance(modelName, renderer)
        loadQueue.add(ret)
        modelMap[modelName.toUpperCase()] = ret
        return ret
    }

    fun onViewChanged(scene: Scene) {
        for (model in models) {
            model.updateVisibleInstances(scene)
        }
    }
}