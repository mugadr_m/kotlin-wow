package ch.cromon.wow.scene.models.wmo

import ch.cromon.wow.gx.*
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.math.Vector3
import ch.cromon.wow.math.Vector4


object MapObjectMesh {
    private data class ProgramData(
            var uniformProjection: Int = -1,
            var uniformView: Int = -1,
            var uniformTransform: Int = -1,
            var uniformTransformTransposedInverted: Int = -1,
            var uniformCameraPosition: Int = -1,
            var uniformTexture: Int = -1,
            var uniformAmbient: Int = -1,
            var uniformDiffuse: Int = -1,
            var uniformClipParameters: Int = -1,
            var uniformFogColor: Int = -1,
            var uniformMinAlpha: Int = -1
    )

    lateinit var indoorProgram: Program
    lateinit var outdoorProgram: Program

    lateinit var mesh: Mesh

    private val indoorParameters = ProgramData()
    private val outdoorParameters = ProgramData()

    fun initialize() {
        indoorProgram = Program()
        outdoorProgram = Program()

        indoorProgram.compileFromResource(Shader.WMO_INDOOR)
        outdoorProgram.compileFromResource(Shader.WMO_OUTDOOR)

        indoorProgram.link()
        outdoorProgram.link()

        mesh = Mesh()
        mesh.addElement(VertexElement("position", 0, 3))
        mesh.addElement(VertexElement("texCoord", 0, 2))
        mesh.addElement(VertexElement("normal", 0, 3))
        mesh.addElement(VertexElement("color", 0, 4, DataType.BYTE, true))

        mesh.program = indoorProgram
        mesh.blendMode = BlendMode.NONE

        mesh.finalize()

        loadParameters(indoorParameters, indoorProgram)
        loadParameters(outdoorParameters, outdoorProgram)
    }

    fun setTexture(texture: Texture, alphaMin: Float) {
        if(mesh.program == indoorProgram) {
            mesh.textureInput.add(indoorParameters.uniformTexture, texture)
            indoorProgram.setFloat(indoorParameters.uniformMinAlpha, alphaMin)
        } else {
            mesh.textureInput.add(outdoorParameters.uniformTexture, texture)
            outdoorProgram.setFloat(outdoorParameters.uniformMinAlpha, alphaMin)
        }

        mesh.textureInput.bind()
    }

    fun setup(instance: Matrix4, instanceTransposeInverse: Matrix4) {
        indoorProgram.setMatrix(indoorParameters.uniformTransform, instance)
        indoorProgram.setMatrix(indoorParameters.uniformTransformTransposedInverted, instanceTransposeInverse)
        outdoorProgram.setMatrix(outdoorParameters.uniformTransform, instance)
        outdoorProgram.setMatrix(outdoorParameters.uniformTransformTransposedInverted, instanceTransposeInverse)
    }

    fun updateDiffuseLight(color: Vector4) {
        indoorProgram.setVec4(indoorParameters.uniformDiffuse, color)
        outdoorProgram.setVec4(outdoorParameters.uniformDiffuse, color)
    }

    fun updateAmbientLight(color: Vector4) {
        indoorProgram.setVec4(indoorParameters.uniformAmbient, color)
        outdoorProgram.setVec4(outdoorParameters.uniformAmbient, color)
    }

    fun updateFogColor(color: Vector4) {
        indoorProgram.setVec4(indoorParameters.uniformFogColor, color)
        outdoorProgram.setVec4(outdoorParameters.uniformFogColor, color)
    }

    fun updateClipParameters(clipParameters: Vector3) {
        indoorProgram.setVec3(indoorParameters.uniformClipParameters, clipParameters)
        outdoorProgram.setVec3(outdoorParameters.uniformClipParameters, clipParameters)
    }

    fun updateProjection(projection: Matrix4) {
        indoorProgram.setMatrix(indoorParameters.uniformProjection, projection)
        outdoorProgram.setMatrix(outdoorParameters.uniformProjection, projection)
    }

    fun updateView(view: Matrix4) {
        indoorProgram.setMatrix(indoorParameters.uniformView, view)
        outdoorProgram.setMatrix(outdoorParameters.uniformView, view)
    }

    fun updateCameraPosition(position: Vector3) {
        indoorProgram.setVec3(indoorParameters.uniformCameraPosition, position)
        outdoorProgram.setVec3(outdoorParameters.uniformCameraPosition, position)
    }

    private fun loadParameters(parameter: ProgramData, program: Program) {
        parameter.uniformAmbient = program.getUniform("ambientColor")
        parameter.uniformCameraPosition = program.getUniform("cameraPosition")
        parameter.uniformClipParameters = program.getUniform("clipParameters")
        parameter.uniformDiffuse = program.getUniform("diffuseColor")
        parameter.uniformFogColor = program.getUniform("fogColor")
        parameter.uniformMinAlpha = program.getUniform("alphaMin")
        parameter.uniformProjection = program.getUniform("matProjection")
        parameter.uniformTexture = program.getUniform("batchTexture")
        parameter.uniformTransform = program.getUniform("matTransform")
        parameter.uniformTransformTransposedInverted = program.getUniform("matTransformTransposedInverted")
        parameter.uniformView = program.getUniform("matView")
    }
}