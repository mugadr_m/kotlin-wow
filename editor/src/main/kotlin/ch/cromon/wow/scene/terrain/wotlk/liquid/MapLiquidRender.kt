package ch.cromon.wow.scene.terrain.wotlk.liquid

import ch.cromon.wow.io.files.map.wotlk.liquid.TileLiquidManager
import ch.cromon.wow.io.files.texture.TextureManager
import ch.cromon.wow.math.Matrix4
import ch.cromon.wow.scene.Scene
import ch.cromon.wow.utils.TILE_SIZE


class MapLiquidRender(indexX: Int, indexY: Int, private val textureManager: TextureManager) {
    private var isSyncLoaded = false
    private var isAsyncLoaded = false
    private var isEmpty = true

    private val matTransform = Matrix4()
    private lateinit var passes: List<LiquidRenderPass>

    init {
        Matrix4.translation(matTransform, indexX * TILE_SIZE, indexY * TILE_SIZE, 0.0f)
    }

    fun onFrame(scene: Scene) {
        if(!isAsyncLoaded) {
            return
        }

        if(isEmpty) {
            return
        }

        if(!isSyncLoaded) {
            isSyncLoaded = true
            syncLoad()
        }

        for(pass in passes) {
            pass.onFrame(matTransform, scene)
        }
    }

    fun asyncLoad(liquidData: TileLiquidManager) {
        if(!liquidData.hasAnyLiquid) {
            isEmpty = true
            isAsyncLoaded = true
            return
        }

        isEmpty = false
        passes = liquidData.renderers.map { LiquidRenderPass(it) }
        isAsyncLoaded = true
    }

    private fun syncLoad() {
        passes.forEach { it.syncLoad(textureManager) }
    }
}