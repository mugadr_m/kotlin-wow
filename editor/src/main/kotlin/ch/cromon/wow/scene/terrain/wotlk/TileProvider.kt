package ch.cromon.wow.scene.terrain.wotlk

import ch.cromon.wow.io.data.DataManager
import ch.cromon.wow.io.files.map.wotlk.WdtFile
import ch.cromon.wow.io.files.texture.TextureManager
import ch.cromon.wow.scene.models.ModelManager
import ch.cromon.wow.scene.terrain.RenderTileProvider
import ch.cromon.wow.utils.BuildVersion
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component


@Component
@Scope("singleton")
@BuildVersion(minBuild = 8303, maxBuild = 26365)
class WotlkTileProvider : RenderTileProvider {
    @Autowired
    private lateinit var textureManager: TextureManager

    @Autowired
    private lateinit var dataManager: DataManager

    @Autowired
    private lateinit var modelManager: ModelManager

    override fun createTile() = MapTileRender(textureManager, modelManager)

    override fun onEnterWorld(mapName: String) {
        val path = "World\\Maps\\$mapName\\$mapName.wdt"
        val content = dataManager.openFile(path) ?: throw IllegalArgumentException("Unable to load map, missing WDT: $path")
        val wdt = WdtFile(content)
        if((wdt.flags and 0x84) != 0) {
            MapTileRender.activeMaterial = TerrainMaterial.NEW_BLEND
        } else {
            MapTileRender.activeMaterial = TerrainMaterial.OLD_BLEND
        }
    }
}