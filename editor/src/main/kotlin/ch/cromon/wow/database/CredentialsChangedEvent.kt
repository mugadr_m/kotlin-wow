package ch.cromon.wow.database

import org.springframework.context.ApplicationEvent


data class CredentialsChangedEvent(val jdbcUrl: String, val user: String, val password: String, val dbType: DatabaseType, private val obj: Any) :
        ApplicationEvent(obj)