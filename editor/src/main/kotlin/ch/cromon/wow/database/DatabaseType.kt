package ch.cromon.wow.database

import ch.cromon.wow.database.mangos.MangosDbPackage


enum class DatabaseType(val persistenceUnit: String, val entitiesPackage: Package) {
    MANGOS("db-mangos", MangosDbPackage.javaClass.`package`)
}