package ch.cromon.wow.database

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence

@Component
@Scope("singleton")
class EntityManagerProducer {
    private var credentials: CredentialsChangedEvent? = null
    private var factory: EntityManagerFactory? = null

    @Bean
    @Scope("prototype")
    fun entityManager(): EntityManager {
        val cred = credentials ?: throw IllegalStateException("Database is not yet initialized")
        val factory = this.factory ?: Persistence.createEntityManagerFactory(cred.dbType.persistenceUnit, createProperties(cred))
        this.factory = factory

        return factory.createEntityManager()
    }

    @EventListener
    fun onCredentialsChanged(event: CredentialsChangedEvent) {
        if(credentials != null) {
            onReset()
        }

        credentials = event
    }

    private fun onReset() {
        factory = null
    }

    private fun createProperties(credentials: CredentialsChangedEvent) = hashMapOf(
            "hibernate.connection.url" to credentials.jdbcUrl,
            "hibernate.connection.username" to credentials.user,
            "hibernate.connection.password" to credentials.password,
            "hibernate.dialect" to "org.hibernate.dialect.MySQLDialect",
            "hibernate.show-sql" to "true"
    )
}