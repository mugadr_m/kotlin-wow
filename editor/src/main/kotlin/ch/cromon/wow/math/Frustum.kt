package ch.cromon.wow.math


class Frustum {
    private val planes = Array(6, { Plane() })

    private var view = Matrix4()
    private var projection = Matrix4()
    private var tmpMatrix = Matrix4()

    fun intersect(box: BoundingBox): Boolean {
        for (i in 0 until 6) {
            if (!box.intersects(planes[i])) {
                return false
            }
        }

        return true
    }

    fun updateView(view: Matrix4) {
        this.view = view
        onMatrixUpdated()
    }

    fun updateProjection(projection: Matrix4) {
        this.projection = projection
        onMatrixUpdated()
    }

    private fun onMatrixUpdated() {
        projection.times(view, tmpMatrix)
        tmpMatrix.transpose()
        val m = tmpMatrix.m

        planes[0].set(
                m[0] + m[12],
                m[1] + m[13],
                m[2] + m[14],
                m[3] + m[15]
        )

        planes[1].set(
                -m[0] + m[12],
                -m[1] + m[13],
                -m[2] + m[14],
                -m[3] + m[15]
        )

        planes[2].set(
                m[4] + m[12],
                m[5] + m[13],
                m[6] + m[14],
                m[7] + m[15]
        )

        planes[3].set(
                -m[4] + m[12],
                -m[5] + m[13],
                -m[6] + m[14],
                -m[7] + m[15]
        )

        planes[4].set(
                m[8] + m[12],
                m[9] + m[13],
                m[10] + m[14],
                m[11] + m[15]
        )

        planes[5].set(
                -m[8] + m[12],
                -m[9] + m[13],
                -m[10] + m[14],
                -m[11] + m[15]
        )
    }
}