package ch.cromon.wow.math

import ch.cromon.wow.utils.d
import ch.cromon.wow.utils.f
import ch.cromon.wow.utils.set
import org.lwjgl.BufferUtils


class Matrix4 {
    companion object {
        fun scale(res: Matrix4, x: Float, y: Float, z: Float) {
            res.setIdentity()
            val m = res.m
            m[0] = x
            m[5] = y
            m[10] = z
        }

        fun translation(res: Matrix4, x: Float, y: Float, z: Float) {
            res.setIdentity()
            val m = res.m
            m[12] = x
            m[13] = y
            m[14] = z
        }

        fun translation(res: Matrix4, position: Vector3) {
            res.setIdentity()
            val m = res.m
            m[12] = position.x
            m[13] = position.y
            m[14] = position.z
        }

        fun rotationYawPitchRoll(res: Matrix4, yaw: Float, pitch: Float, roll: Float) {
            val yd = Math.toRadians(yaw.toDouble())
            val pd = Math.toRadians(pitch.toDouble())
            val rd = Math.toRadians(roll.toDouble())
            res.setIdentity()
            val m = res.m
            m[0] = (Math.cos(yd) * Math.sin(pd)).toFloat()
            m[1] = (Math.cos(yd) * Math.sin(pd) * Math.sin(rd)).toFloat() - (Math.sin(yd) * Math.cos(rd)).toFloat()
            m[2] = (Math.cos(yd) * Math.sin(pd) * Math.cos(rd)).toFloat() + (Math.sin(yd) * Math.sin(rd)).toFloat()
            m[4] = (Math.sin(yd) * Math.cos(pd)).toFloat()
            m[5] = (Math.sin(yd) * Math.sin(pd) * Math.sin(rd)).toFloat() + (Math.cos(yd) * Math.cos(rd)).toFloat()
            m[6] = (Math.sin(yd) * Math.sin(pd) * Math.cos(rd)).toFloat() - (Math.cos(yd) * Math.sin(rd)).toFloat()
            m[8] = -Math.sin(pd).toFloat()
            m[9] = (Math.cos(pd) * Math.sin(rd)).toFloat()
            m[10] = (Math.cos(pd) * Math.cos(rd)).toFloat()
        }

        fun rotation(angle: Float, axis: Vector3): Matrix4 {
            val ret = Matrix4()
            rotation(ret, angle, axis)
            return ret
        }

        fun rotation(res: Matrix4, angle: Float, axis: Vector3) {
            val c = Math.cos(angle.toDouble()).toFloat()
            val s = Math.sin(angle.toDouble()).toFloat()

            val inv = 1.0f / Math.sqrt(axis.dot().toDouble()).toFloat()

            val aX = axis.x * inv
            val aY = axis.y * inv
            val aZ = axis.z * inv

            val tmpX = (1.0f - c) * aX
            val tmpY = (1.0f - c) * aY
            val tmpZ = (1.0f - c) * aZ

            val rotate00 = c + tmpX * aX
            val rotate01 = tmpX * aY + s * aZ
            val rotate02 = tmpX * aZ - s * aY

            val rotate10 = tmpY * aX - s * aZ
            val rotate11 = c + tmpY * aY
            val rotate12 = tmpY * aZ + s * aX

            val rotate20 = tmpZ * aX + s * aY
            val rotate21 = tmpZ * aY - s * aX
            val rotate22 = c + tmpZ * aZ

            res.setIdentity()
            val m = res.m
            m[0] = rotate00
            m[1] = rotate01
            m[2] = rotate02
            m[4] = rotate10
            m[5] = rotate11
            m[6] = rotate12
            m[8] = rotate20
            m[9] = rotate21
            m[10] = rotate22
        }

        fun ortho(res: Matrix4, left: Float, right: Float, bottom: Float, top: Float) {
            res.setIdentity()
            val m = res.m
            m[0] = 2.0f / (right - left)
            m[5] = 2.0f / (top - bottom)
            m[10] = -1.0f
            m[12] = -(right + left) / (right - left)
            m[13] = -(top + bottom) / (top - bottom)
        }

        fun perspective(res: Matrix4, fovY: Float, aspect: Float, zNear: Float, zFar: Float) {
            val tanHalf = Math.tan(fovY / 2.0).toFloat()
            res.setIdentity()
            val m = res.m
            m[10] = 0.0f
            m[15] = 0.0f

            m[0] = 1.0f / (aspect * tanHalf)
            m[5] = 1.0f / tanHalf
            m[11] = 1.0f
            m[10] = (zFar + zNear) / (zFar - zNear)
            m[14] = -(2.0f * zFar * zNear) / (zFar - zNear)
        }

        fun lookAt(res: Matrix4, eye: Vector3, center: Vector3, up: Vector3) {
            val rm = res.m
            val ceX = center.x - eye.x
            val ceY = center.y - eye.y
            val ceZ = center.z - eye.z

            var dot = ceX * ceX + ceY * ceY + ceZ * ceZ
            var invSqr = 1.0f / Math.sqrt(dot.d).f
            val fX = ceX * invSqr
            val fY = ceY * invSqr
            val fZ = ceZ * invSqr

            val ufX = up.y * fZ - fY * up.z
            val ufY = up.z * fX - fZ * up.x
            val ufZ = up.x * fY - fX * up.y
            dot = ufX * ufX + ufY * ufY + ufZ * ufZ
            invSqr = 1.0f / Math.sqrt(dot.d).f
            val sX = ufX * invSqr
            val sY = ufY * invSqr
            val sZ = ufZ * invSqr

            val uX = fY * sZ - sY * fZ
            val uY = fZ * sX - sZ * fX
            val uZ = fX * sY - sX * fY

            res.setIdentity()
            rm[0] = sX
            rm[4] = sY
            rm[8] = sZ
            rm[1] = uX
            rm[5] = uY
            rm[9] = uZ
            rm[2] = fX
            rm[6] = fY
            rm[10] = fZ
            rm[12] = -(sX * eye.x + sY * eye.y + sZ * eye.z)
            rm[13] = -(uX * eye.x + uY * eye.y + uZ * eye.z)
            rm[14] = -(fX * eye.x + fY * eye.y + fZ * eye.z)
        }
    }

    val m = BufferUtils.createFloatBuffer(16)!!

    init {
        m[0] = 1.0f
        m[5] = 1.0f
        m[10] = 1.0f
        m[15] = 1.0f
    }

    fun transpose() {
        val v01 = m[1]
        val v02 = m[2]
        val v03 = m[3]
        val v10 = m[4]
        val v12 = m[6]
        val v13 = m[7]
        val v20 = m[8]
        val v21 = m[9]
        val v23 = m[11]
        val v30 = m[12]
        val v31 = m[13]
        val v32 = m[14]

        m[1] = v10
        m[2] = v20
        m[3] = v30
        m[4] = v01
        m[6] = v21
        m[7] = v31
        m[8] = v02
        m[9] = v12
        m[11] = v32
        m[12] = v03
        m[13] = v13
        m[14] = v23
    }

    fun transposed(): Matrix4 {
        val v01 = m[1]
        val v02 = m[2]
        val v03 = m[3]
        val v10 = m[4]
        val v12 = m[6]
        val v13 = m[7]
        val v20 = m[8]
        val v21 = m[9]
        val v23 = m[11]
        val v30 = m[12]
        val v31 = m[13]
        val v32 = m[14]

        val ret = Matrix4()
        val m = ret.m

        m[0] = this.m[0]
        m[1] = v10
        m[2] = v20
        m[3] = v30
        m[4] = v01
        m[5] = this.m[5]
        m[6] = v21
        m[7] = v31
        m[8] = v02
        m[9] = v12
        m[10] = this.m[10]
        m[11] = v32
        m[12] = v03
        m[13] = v13
        m[14] = v23
        m[15] = this.m[15]
        return ret
    }

    fun setIdentity() {
        m[0] = 1.0f
        m[1] = 0.0f
        m[2] = 0.0f
        m[3] = 0.0f
        m[4] = 0.0f
        m[5] = 1.0f
        m[6] = 0.0f
        m[7] = 0.0f
        m[8] = 0.0f
        m[9] = 0.0f
        m[10] = 1.0f
        m[11] = 0.0f
        m[12] = 0.0f
        m[13] = 0.0f
        m[14] = 0.0f
        m[15] = 1.0f
    }

    fun scaleInplace(vX: Float, vY: Float, vZ: Float): Matrix4 {
        m[0] = m[0] * vX
        m[1] = m[1] * vX
        m[2] = m[2] * vX
        m[3] = m[3] * vX

        m[4] = m[4] * vY
        m[5] = m[5] * vY
        m[6] = m[6] * vY
        m[7] = m[7] * vY

        m[8] = m[8] * vZ
        m[9] = m[9] * vZ
        m[10] = m[10] * vZ
        m[11] = m[11] * vZ

        return this
    }

    fun times(v: Matrix4, res: Matrix4) {
        val a = m
        val b = v.m
        val r = res.m
        r[0] = a[0] * b[0] + a[4] * b[1] + a[8] * b[2] + a[12] * b[3]
        r[1] = a[1] * b[0] + a[5] * b[1] + a[9] * b[2] + a[13] * b[3]
        r[2] = a[2] * b[0] + a[6] * b[1] + a[10] * b[2] + a[14] * b[3]
        r[3] = a[3] * b[0] + a[7] * b[1] + a[11] * b[2] + a[15] * b[3]

        r[4] = a[0] * b[4] + a[4] * b[5] + a[8] * b[6] + a[12] * b[7]
        r[5] = a[1] * b[4] + a[5] * b[5] + a[9] * b[6] + a[13] * b[7]
        r[6] = a[2] * b[4] + a[6] * b[5] + a[10] * b[6] + a[14] * b[7]
        r[7] = a[3] * b[4] + a[7] * b[5] + a[11] * b[6] + a[15] * b[7]

        r[8] = a[0] * b[8] + a[4] * b[9] + a[8] * b[10] + a[12] * b[11]
        r[9] = a[1] * b[8] + a[5] * b[9] + a[9] * b[10] + a[13] * b[11]
        r[10] = a[2] * b[8] + a[6] * b[9] + a[10] * b[10] + a[14] * b[11]
        r[11] = a[3] * b[8] + a[7] * b[9] + a[11] * b[10] + a[15] * b[11]

        r[12] = a[0] * b[12] + a[4] * b[13] + a[8] * b[14] + a[12] * b[15]
        r[13] = a[1] * b[12] + a[5] * b[13] + a[9] * b[14] + a[13] * b[15]
        r[14] = a[2] * b[12] + a[6] * b[13] + a[10] * b[14] + a[14] * b[15]
        r[15] = a[3] * b[12] + a[7] * b[13] + a[11] * b[14] + a[15] * b[15]
    }

    fun inverted(): Matrix4 {
        val b0 = m[5]  * m[10] * m[15] -
                m[5]  * m[11] * m[14] -
                m[9]  * m[6]  * m[15] +
                m[9]  * m[7]  * m[14] +
                m[13] * m[6]  * m[11] -
                m[13] * m[7]  * m[10]

        val b4 = -m[4]  * m[10] * m[15] +
                m[4]  * m[11] * m[14] +
                m[8]  * m[6]  * m[15] -
                m[8]  * m[7]  * m[14] -
                m[12] * m[6]  * m[11] +
                m[12] * m[7]  * m[10]

        val b8 = m[4]  * m[9] * m[15] -
                m[4]  * m[11] * m[13] -
                m[8]  * m[5] * m[15] +
                m[8]  * m[7] * m[13] +
                m[12] * m[5] * m[11] -
                m[12] * m[7] * m[9]

        val b12 = -m[4]  * m[9] * m[14] +
                m[4]  * m[10] * m[13] +
                m[8]  * m[5] * m[14] -
                m[8]  * m[6] * m[13] -
                m[12] * m[5] * m[10] +
                m[12] * m[6] * m[9]

        val det = m[0] * b0 + m[1] * b4 + m[2] * b8 + m[3] * b12
        if(Math.abs(det) <= Double.MIN_VALUE) {
            throw IllegalStateException("Matrix cannot be inverted")
        }

        val b1 = -m[1]  * m[10] * m[15] +
                m[1]  * m[11] * m[14] +
                m[9]  * m[2] * m[15] -
                m[9]  * m[3] * m[14] -
                m[13] * m[2] * m[11] +
                m[13] * m[3] * m[10]

        val b5 = m[0]  * m[10] * m[15] -
                m[0]  * m[11] * m[14] -
                m[8]  * m[2] * m[15] +
                m[8]  * m[3] * m[14] +
                m[12] * m[2] * m[11] -
                m[12] * m[3] * m[10]

        val b9 = -m[0]  * m[9] * m[15] +
                m[0]  * m[11] * m[13] +
                m[8]  * m[1] * m[15] -
                m[8]  * m[3] * m[13] -
                m[12] * m[1] * m[11] +
                m[12] * m[3] * m[9]

        val b13 = m[0]  * m[9] * m[14] -
                m[0]  * m[10] * m[13] -
                m[8]  * m[1] * m[14] +
                m[8]  * m[2] * m[13] +
                m[12] * m[1] * m[10] -
                m[12] * m[2] * m[9]

        val b2 = m[1]  * m[6] * m[15] -
                m[1]  * m[7] * m[14] -
                m[5]  * m[2] * m[15] +
                m[5]  * m[3] * m[14] +
                m[13] * m[2] * m[7] -
                m[13] * m[3] * m[6]

        val b6 = -m[0]  * m[6] * m[15] +
                m[0]  * m[7] * m[14] +
                m[4]  * m[2] * m[15] -
                m[4]  * m[3] * m[14] -
                m[12] * m[2] * m[7] +
                m[12] * m[3] * m[6]

        val b10 = m[0]  * m[5] * m[15] -
                m[0]  * m[7] * m[13] -
                m[4]  * m[1] * m[15] +
                m[4]  * m[3] * m[13] +
                m[12] * m[1] * m[7] -
                m[12] * m[3] * m[5]

        val b14 = -m[0]  * m[5] * m[14] +
                m[0]  * m[6] * m[13] +
                m[4]  * m[1] * m[14] -
                m[4]  * m[2] * m[13] -
                m[12] * m[1] * m[6] +
                m[12] * m[2] * m[5]

        val b3 = -m[1] * m[6] * m[11] +
                m[1] * m[7] * m[10] +
                m[5] * m[2] * m[11] -
                m[5] * m[3] * m[10] -
                m[9] * m[2] * m[7] +
                m[9] * m[3] * m[6]

        val b7 = m[0] * m[6] * m[11] -
                m[0] * m[7] * m[10] -
                m[4] * m[2] * m[11] +
                m[4] * m[3] * m[10] +
                m[8] * m[2] * m[7] -
                m[8] * m[3] * m[6]

        val b11 = -m[0] * m[5] * m[11] +
                m[0] * m[7] * m[9] +
                m[4] * m[1] * m[11] -
                m[4] * m[3] * m[9] -
                m[8] * m[1] * m[7] +
                m[8] * m[3] * m[5]

        val b15 = m[0] * m[5] * m[10] -
                m[0] * m[6] * m[9] -
                m[4] * m[1] * m[10] +
                m[4] * m[2] * m[9] +
                m[8] * m[1] * m[6] -
                m[8] * m[2] * m[5]

        val invDet = 1.0f / det
        val ret = Matrix4()
        val r = ret.m
        r[0] = b0 * invDet
        r[1] = b1 * invDet
        r[2] = b2 * invDet
        r[3] = b3 * invDet
        r[4] = b4 * invDet
        r[5] = b5 * invDet
        r[6] = b6 * invDet
        r[7] = b7 * invDet
        r[8] = b8 * invDet
        r[9] = b9 * invDet
        r[10] = b10 * invDet
        r[11] = b11 * invDet
        r[12] = b12 * invDet
        r[13] = b13 * invDet
        r[14] = b14 * invDet
        r[15] = b15 * invDet
        return ret
    }

    operator fun times(v: Matrix4): Matrix4 {
        val ret = Matrix4()
        times(v, ret)
        return ret
    }

    operator fun times(v: Vector3): Vector3 {
        val x = v.x
        val y = v.y
        val z = v.z
        val w = 1.0f

        val rx = m[0] * x + m[1] * y + m[2] * z + m[3] * w
        val ry = m[4] * x + m[5] * y + m[6] * z + m[7] * w
        val rz = m[8] * x + m[9] * y + m[10] * z + m[11] * w
        val rw = m[12] * x + m[13] * y + m[14] * z + m[15] * w

        return Vector3(rx / rw, ry / rw, rz / rw)
    }

    operator fun times(v: Vector2): Vector2 {
        val x = v.x
        val y = v.y
        val z = 0.0f
        val w = 1.0f

        val rx = m[0] * x + m[1] * y + m[2] * z + m[3] * w
        val ry = m[4] * x + m[5] * y + m[6] * z + m[7] * w
        val rw = m[12] * x + m[13] * y + m[14] * z + m[15] * w

        return Vector2(rx / rw, ry / rw)
    }
}