package ch.cromon.wow.math


class BoundingBox(minValue: Vector3 = Vector3(), maxValue: Vector3 = Vector3()) {
    var min = minValue
    var max = maxValue

    fun intersects(plane: Plane): Boolean {
        val x = if (plane.normal.x > 0) max.x else min.x
        val y = if (plane.normal.y > 0) max.y else min.y
        val z = if (plane.normal.z > 0) max.z else min.z
        val dp = plane.normal.x * x + plane.normal.y * y + plane.normal.z * z
        return dp >= -plane.distance
    }

    fun translate(matrix: Matrix4): BoundingBox {
        val c0 = matrix * min
        val c1 = matrix * max

        return BoundingBox(
                Vector3(Math.min(c0.x, c1.x), Math.min(c0.y, c1.y), Math.min(c0.z, c1.z)),
                Vector3(Math.max(c0.x, c1.x), Math.max(c0.y, c1.y), Math.min(c0.z, c1.z))
        )
    }
}
