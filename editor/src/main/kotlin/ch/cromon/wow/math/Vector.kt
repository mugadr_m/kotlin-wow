package ch.cromon.wow.math

import ch.cromon.wow.utils.format

class Vector4(var x: Float, var y: Float, var z: Float, var w: Float) {
    constructor(x: Int, y: Int, z: Int, w: Int) : this(x.toFloat(), y.toFloat(), z.toFloat(), w.toFloat())

    operator fun times(v: Float) = Vector4(x * v, y * v, z * v, w * v)
    operator fun plus(v: Vector4) = Vector4(x + v.x, y + v.y, z + v.z, w + v.w)
    operator fun minus(v: Vector4) = Vector4(x - v.x, y - v.y, z - v.z, w - v.w)
    operator fun div(v: Float) = Vector4(x / v, y / v, z / v, w / v)

    fun toXrgb(): Int {
        val r = (this.x * 255).toInt()
        val g = (this.y * 255).toInt()
        val b = (this.z * 255).toInt()

        return 0xFF000000.toInt() or ((r and 0xFF) shl 16) or ((g and 0xFF) shl 8) or (b and 0xFF)
    }
}

class Vector3(var x: Float = 0.0f, var y: Float = 0.0f, var z: Float = 0.0f) {
    constructor(x: Int, y: Int, z: Int) : this(x.toFloat(), y.toFloat(), z.toFloat())

    override fun toString() = "<Vector3 x=${x.format(3)}, y=${y.format(3)}, z=${z.format(3)}>"

    operator fun times(v: Float) = Vector3(x * v, y * v, z * v)
    operator fun plus(v: Vector3) = Vector3(x + v.x, y + v.y, z + v.z)
    operator fun minus(v: Vector3) = Vector3(x - v.x, y - v.y, z - v.z)
    operator fun div(v: Float) = Vector3(x / v, y / v, z / v)

    fun set(v: Vector3) {
        x = v.x
        y = v.y
        z = v.z
    }

    fun set(x: Float, y: Float, z: Float) {
        this.x = x
        this.y = y
        this.z = z
    }

    fun distanceSquared(other: Vector3): Float {
        val dx = x - other.x
        val dy = y - other.y
        val dz = z - other.z
        return dx * dx + dy * dy + dz * dz
    }

    fun distance(other: Vector3) = Math.sqrt(distanceSquared(other).toDouble()).toFloat()

    fun distanceProjected(other: Vector3, direction: Vector3): Float {
        val dx = x - other.x
        val dy = y - other.y
        val dz = z - other.z
        return dx * direction.x + dy * direction.y + dz * direction.z
    }

    fun length() = Math.sqrt((x * x + y * y + z * z).toDouble()).toFloat()
    fun length2D() = Math.sqrt((x * x + y * y).toDouble()).toFloat()

    fun dot() = x * x + y * y + z * z

    fun normalize(res: Vector3) {
        val length = length()
        if(Math.abs(length.toDouble()) < 1e-6) {
            res.x = x
            res.y = y
            res.z = z
        } else {
            res.x = x / length
            res.y = y / length
            res.z = z / length
        }
    }

    fun normalized(): Vector3 {
        val ret = Vector3()
        normalize(ret)
        return ret
    }

    fun cross(v: Vector3, res: Vector3) {
        res.x = y * v.z - z * v.y
        res.y = z * v.x - x * v.z
        res.z = x * v.y - y * v.x
    }

    infix fun cross(v: Vector3) = Vector3(
            y * v.z - z * v.y,
            z * v.x - x * v.z,
            x * v.y - y * v.x
    )

    fun toXrgb(): Int {
        val r = (this.x * 255).toInt()
        val g = (this.y * 255).toInt()
        val b = (this.z * 255).toInt()

        return 0xFF000000.toInt() or ((r and 0xFF) shl 16) or ((g and 0xFF) shl 8) or (b and 0xFF)
    }

    fun takeMax(other: Vector3): Vector3 {
        x = Math.max(x, other.x)
        y = Math.max(y, other.y)
        z = Math.max(z, other.z)
        return this
    }

    fun takeMin(other: Vector3): Vector3 {
        x = Math.min(x, other.x)
        y = Math.min(y, other.y)
        z = Math.min(z, other.z)
        return this
    }
}

class Vector2(var x: Float = 0.0f, var y: Float = 0.0f) {
    constructor(x: Int, y: Int) : this(x.toFloat(), y.toFloat())
    constructor(x: Double, y: Double): this(x.toFloat(), y.toFloat())

    override fun toString() = "<Vector2 x=${x.format(3)}, y=${y.format(3)}>"

    operator fun plus(v: Vector2) = Vector2(x + v.x, y + v.y)
    operator fun minus(v: Vector2) = Vector2(x - v.x, y - v.y)

    fun set(x: Float, y: Float) {
        this.x = x
        this.y = y
    }
}