package ch.cromon.wow.math


class Plane(var normal: Vector3 = Vector3(), var distance: Float = 0.0f) {
    constructor(a: Float, b: Float, c: Float, d: Float) : this(Vector3(a, b, c), d)

    fun set(a: Float, b: Float, c: Float, d: Float) {
        normal.x = a
        normal.y = b
        normal.z = c
        distance = d
    }
}