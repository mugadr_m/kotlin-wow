package ch.cromon.wow.utils

import ch.cromon.wow.math.Vector4
import java.awt.Color
import java.nio.ByteBuffer
import java.nio.FloatBuffer
import java.nio.ShortBuffer

fun String.toFourCC(): Int {
    if(this.length != 4) {
        throw IllegalArgumentException("String to four CC must be 4 characters")
    }

    val chars = this.toCharArray()
    val b0 = chars[3].toInt()
    val b1 = chars[2].toInt()
    val b2 = chars[1].toInt()
    val b3 = chars[0].toInt()
    return (b0 or (b1 shl 8) or (b2 shl 16) or (b3 shl 24))
}

fun Float.format(digits: Int) = String.format("%.${digits}f", this)
fun Int.format(digits: Int) = String.format("%0${digits}d", this)

val Float.d get() = this.toDouble()
val Double.f get() = this.toFloat()

fun Byte.toUInt() = toInt() and 0xFF
fun Int.toUInt() = this
fun Long.toUInt() = toInt()
fun Int.toULong() = toLong() and 0xFFFFFFFFL

operator fun FloatBuffer.set(index: Int, float: Float): FloatBuffer = put(index, float)
operator fun ShortBuffer.set(index: Int, short: Short): ShortBuffer = put(index, short)
operator fun ByteBuffer.set(index: Int, byte: Byte): ByteBuffer = put(index, byte)

fun Color.toVector4() = Vector4(this.red / 255.0f, this.green / 255.0f, this.blue / 255.0f, this.alpha / 255.0f)

fun ByteArray.isAllZero(): Boolean {
    for(i in 0 until size) {
        if(this[i].toInt() != 0) {
            return false
        }
    }

    return true
}