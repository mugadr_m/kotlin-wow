package ch.cromon.wow.utils

import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.nio.charset.StandardCharsets


class NativeLoader(private val libraryName: String, private val resourceBase: String, private val loadFunction: (String) -> Unit) {
    companion object {
        private val logger = LoggerFactory.getLogger(NativeLoader::class.java)
    }

    private lateinit var tempDir: File
    private lateinit var nativeDir: File
    private lateinit var resourceFolder: String
    private var isLoaded = false

    private var isWindows = false
    private var isMac = false
    private var isLinux = false

    private var is64Bit = false

    init {
        var tmpFolder = System.getProperty("java.io.tmpdir")
        if(StringUtils.isBlank(tmpFolder) || !File(tmpFolder).exists()) {
            tmpFolder = System.getProperty("user.home")
            if(StringUtils.isBlank(tmpFolder)) {
                logger.error("Unable to find property java.io.tmpdir or user.home")
                throw IllegalStateException("Unable to find either java.io.tmpdir nor user.home")
            }
        }

        tempDir = File(tmpFolder, "wow_editor_user_${System.getProperty("user.name") ?: "unknown"}")
        if(!tempDir.exists() && !tempDir.mkdirs()) {
            logger.error("Failed to create directory '${tempDir.absolutePath}'")
            throw IllegalStateException("Unable to create directory")
        }

        checkSystem()
        generateNativeFolder()
    }

    fun extract() {
        if(isLoaded) {
            return
        }

        val targetFile = File(nativeDir, "$libraryName.dll")
        if(!targetFile.exists() || !isExistingMatching(targetFile)) {
            extractFile(targetFile)
        }

        loadFunction(targetFile.absolutePath)
        isLoaded = true
    }

    private fun checkSystem() {
        val arch = System.getProperty("sun.arch.data.model")
        is64Bit = !StringUtils.isBlank(arch) && arch == "64"

        val system = System.getProperty("os.name").toLowerCase()
        if(system.contains("win")) {
            isWindows = true
            isLinux = false
            isMac = false
        } else if(system.contains("mac")) {
            isMac = true
            isWindows = false
            isLinux = false
        } else if(system.contains("nix") || system.contains("nux") || system.contains("aix")) {
            isLinux = true
            isWindows = false
            isMac = false
        } else {
            logger.error("Unable to figure out running system '$system'")
            throw IllegalStateException("Unknown system $system")
        }
    }

    private fun generateNativeFolder() {
        val systemName = when {
            isWindows -> "win"
            isLinux -> "unix"
            isMac -> "mac"
            else -> "unknown"
        }

        val archName = if(is64Bit) "x64" else "x86"

        val folderName = "${systemName}_$archName"
        nativeDir = File(tempDir, folderName)
        if(!nativeDir.exists() && !nativeDir.mkdirs()) {
            logger.error("Unable to create folder ${nativeDir.absolutePath}")
            throw IllegalStateException("Unable to create folder")
        }

        resourceFolder = "$resourceBase/$systemName/$archName"
    }

    private fun extractFile(target: File) {
        val symExtension = when {
            isWindows -> ".dll"
            isLinux -> ".so"
            isMac -> ".dynlib"
            else -> ""
        }

        val resource = "$resourceFolder/$libraryName$symExtension"
        val stream = javaClass.getResourceAsStream(resource) ?: throw IllegalStateException("Failed to find resource $resource")
        stream.use {
            val data = it
            FileOutputStream(target).use {
                IOUtils.copy(data, it)
            }
        }
    }

    private fun isExistingMatching(existing: File): Boolean {
        FileInputStream(existing).use {
            val content = IOUtils.toByteArray(it)
            val hash = sha1(content)
            val resourcePath = "$resourceFolder/$libraryName.sha"
            val shaStream = javaClass.getResourceAsStream(resourcePath) ?: return false
            shaStream.use {
                val expected = IOUtils.toString(shaStream, StandardCharsets.UTF_8)
                val current = HexBin.encode(hash).toUpperCase()
                return current == expected
            }
        }
    }
}