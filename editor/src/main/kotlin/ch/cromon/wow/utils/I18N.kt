package ch.cromon.wow.utils

import org.slf4j.LoggerFactory
import java.util.*


object I18N {
    private val logger = LoggerFactory.getLogger(I18N::class.java)

    const val SPLASH_INTRODUCTION = "splash.introduction"
    const val CDN_LOAD_ADDRESSES = "messages.cdn.load.addresses"
    const val CDN_LOAD_ADDRESSES_DONE = "messages.cdn.load.addresses.loaded"
    const val CDN_LOAD_ADDRESSES_ERROR = "messages.cdn.load.addresses.error"
    const val CDN_DOWNLOAD_PROGRESS = "messages.cdn.download.progress"
    const val CDN_GET_SERVER = "messages.cdn.load.get.server"
    const val CDN_GET_SERVER_ERROR_REGION = "messages.cdn.load.get.server.error.region"
    const val CDN_LOAD_VERSION = "messages.cdn.load.version"
    const val CDN_LOAD_INDEX = "messages.cdn.load.index"
    const val ENCODING_DECOMPRESS = "messages.encoding.decompress"
    const val ENCODING_PARSE = "messages.encoding.parse"
    const val ENCODING_DONE = "messages.encoding.done"
    const val ROOT_DECOMPRESS = "messages.root.decompress"
    const val ROOT_PARSE = "messages.root.parse"
    const val ROOT_DONE = "messages.root.done"

    const val LOCAL_LOAD_BUILD_INFO = "messages.local.load.build.info"
    const val LOCAL_BUILD_INFO_MISSING = "messages.local.load.build.info.missing"
    const val LOCAL_LOAD_BUILD = "messages.local.load.build"
    const val LOCAL_LOAD_BUILD_ERROR = "messages.local.load.build.error"
    const val LOCAL_LOAD_BUILD_DONE = "messages.local.load.build.done"
    const val LOCAL_LOAD_BUILD_CONFIG = "messages.local.load.build.config"
    const val LOCAL_LOAD_BUILD_CONFIG_LOCATION = "messages.local.load.build.config.location"
    const val LOCAL_LOAD_INDEX = "messages.local.load.index"
    const val LOCAL_LOAD_ENCODING_FILE = "messages.local.encoding.load"
    const val LOCAL_LOAD_ROOT_FILE = "messages.local.root.load"

    const val MPQ_LOAD_ARCHIVE = "messages.mpq.load.archive"

    const val SERVER_DIALOG_TITLE = "dialog.server_browser.title"

    private val stringBundle = ResourceBundle.getBundle("i18n.strings")

    init {
        logger.info("Loading localization from ${Locale.getDefault()}")
    }

    fun getString(id: String): String {
        return try {
            stringBundle.getString(id)
        } catch (e: MissingResourceException) {
            logger.warn("Missing resource $id")
            "<missing resource $id>"
        }
    }

}