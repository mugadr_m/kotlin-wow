package ch.cromon.wow.utils

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.math.Vector3


fun pointInQuad(point: Vector2, topLeft: Vector2, bottomRight: Vector2): Boolean {
    return point.x >= topLeft.x && point.x <= bottomRight.x &&
            point.y >= topLeft.y && point.y <= bottomRight.y
}

val UNIT_Z = Vector3(0, 0, 1)
val UNIT_Y = Vector3(0, 1, 0)
val UNIT_X = Vector3(1, 0, 0)