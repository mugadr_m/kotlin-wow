package ch.cromon.wow.utils

import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Component

data class BuildBeanEntry<out T>(val minBuild: Int, val maxBuild: Int, val bean: T)

@Component("CdiUtils")
object CdiUtils : ApplicationContextAware {
    private val logger = LoggerFactory.getLogger(CdiUtils.javaClass)

    private lateinit var context: ApplicationContext

    override fun setApplicationContext(applicationContext: ApplicationContext?) {
        context = applicationContext ?: return
    }

    fun <T : Any> getAllVersionBeans(clazz: Class<T>): ArrayList<BuildBeanEntry<T>> {
        val beans = context.getBeansOfType(clazz).map { it.value }
        val ret = ArrayList<BuildBeanEntry<T>>()
        for (bean in beans) {
            val rangeAnnotation = bean.javaClass.getAnnotation(BuildVersion::class.java)
            if (rangeAnnotation == null) {
                logger.warn("Found implementation of ${clazz.name} without @BuildVersion annotation: ${bean.javaClass}")
                continue
            }

            ret.add(BuildBeanEntry(rangeAnnotation.minBuild, rangeAnnotation.maxBuild, bean))
        }

        return ret
    }
}