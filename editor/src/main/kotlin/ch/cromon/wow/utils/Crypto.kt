package ch.cromon.wow.utils

import java.security.MessageDigest


fun sha1(data: ByteArray): ByteArray {
    val provider = MessageDigest.getInstance("SHA1")
    provider.update(data)
    return provider.digest()
}