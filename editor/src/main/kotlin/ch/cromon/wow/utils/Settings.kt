package ch.cromon.wow.utils

import ch.cromon.wow.MainApplication
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.util.prefs.Preferences

const val LAST_LOCAL_DIRECTORY = "last.local.directory"
const val TIME_OF_DAY_DELAY_FACTOR = "time.delay.factor"
const val REMOTE_SERVER_CACHE = "remote.servers.cache"


@Component
@Scope("singleton")
class Settings {
    private val prefs = Preferences.userNodeForPackage(MainApplication::class.java)

    operator fun get(key: String) = prefs[key, ""]!!
    operator fun get(key: String, def: String) = prefs[key, def]!!

    operator fun set(key: String, value: String) = prefs.put(key, value)
}