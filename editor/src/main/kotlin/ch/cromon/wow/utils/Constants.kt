package ch.cromon.wow.utils


const val TILE_SIZE = 533.0f + 1.0f / 3.0f
const val CHUNK_SIZE = TILE_SIZE / 16.0f
const val UNIT_SIZE = CHUNK_SIZE / 8.0f
const val MAP_MID_POINT = 32.0f * TILE_SIZE
const val CHUNK_RADIUS = 1.1442135f * CHUNK_SIZE
const val LIQUID_VERTEX_SIZE = CHUNK_SIZE / 8.0f