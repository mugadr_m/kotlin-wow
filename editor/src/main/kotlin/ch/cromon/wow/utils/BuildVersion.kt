package ch.cromon.wow.utils

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class BuildVersion(val minBuild: Int, val maxBuild: Int)