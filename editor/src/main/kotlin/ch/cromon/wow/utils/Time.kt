package ch.cromon.wow.utils

import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.util.*

@Component
@Scope("singleton")
class Time {
    private var startTime = Date()

    var worldStart = runTime

    val runTime get() = Date().time - startTime.time

    fun reset() {
        startTime = Date()
    }
}