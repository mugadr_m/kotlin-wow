package ch.cromon.wow.utils

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InjectionPoint
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component
class Producers {
    @Bean
    @Scope("prototype")
    fun logger(ip: InjectionPoint): Logger = LoggerFactory.getLogger(ip.member.declaringClass)
}