package ch.cromon.wow

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component


enum class ApplicationState(val renderWorld: Boolean, val loadAllowed: Boolean) {
    DATA_INIT(false, false),
    DATA_LOAD(false, false),
    EDITOR_MAIN(false, false),
    MAP_SELECT(false, false),
    MAP_ENTRY_SELECT(false, false),
    MAP_LOAD(false, true),
    MAP_EDIT(true, true)
}

class AppStateChangedEvent(val newState: ApplicationState, source: AppStateManager): ApplicationEvent(source)

@Component
@Scope("singleton")
class AppStateManager {
    private var activeState = ApplicationState.DATA_INIT

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    var currentState
        get() = activeState
        set(value) {
            val event = AppStateChangedEvent(value, this)
            activeState = value
            publisher.publishEvent(event)
        }
}