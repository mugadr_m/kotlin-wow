package ch.cromon.wow.gx

import org.lwjgl.opengl.GL11.glDrawElements
import org.lwjgl.opengl.GL32.glDrawElementsBaseVertex


class Mesh {
    private val elements = ArrayList<VertexElement>()
    var vertexBuffer = VertexBuffer()
    var indexBuffer = IndexBuffer(IndexType.UINT16)

    val textureInput = TextureInput()

    var blendMode = BlendMode.NONE

    lateinit var program: Program
    var topology = Topology.TRIANGLES
    var indexCount = 0
    var startVertex = 0
    var startIndex = 0
    var depthCheck = DepthCheck.LESS_EQUAL

    fun addElement(element: VertexElement) {
        elements.add(element)
    }

    fun finalize() {
        var totalSize = 0
        elements.forEach {
            it.bindToProgram(program, totalSize)
            totalSize += it.byteWidth
        }

        elements.forEach {
            it.stride = totalSize
        }

        textureInput.program = program
    }

    fun setup() {
        depthCheck.apply()

        program.use()

        vertexBuffer.bind()
        indexBuffer.bind()

        for(i in 0 until elements.size) {
            elements[i].apply()
        }

        textureInput.bind()
        blendMode.apply()
    }

    fun draw() {
        if(startVertex == 0) {
            glDrawElements(topology.native, indexCount, indexBuffer.indexType.native, startIndex.toLong() * indexBuffer.indexType.size)
        } else {
            glDrawElementsBaseVertex(topology.native, indexCount, indexBuffer.indexType.native, startIndex.toLong() * indexBuffer.indexType.size, startVertex)
        }
    }

    fun render() {
        setup()
        draw()
    }
}