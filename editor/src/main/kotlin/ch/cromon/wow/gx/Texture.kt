package ch.cromon.wow.gx

import ch.cromon.wow.io.ImageSource
import ch.cromon.wow.io.files.texture.LayeredTextureInfo
import ch.cromon.wow.io.files.texture.TextureFormat
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE
import org.lwjgl.opengl.GL13.glCompressedTexImage2D
import java.nio.ByteBuffer
import java.nio.IntBuffer
import java.util.concurrent.atomic.AtomicInteger

enum class TextureFilter(val native: Int) {
    NEAREST(GL_NEAREST),
    LINEAR(GL_LINEAR)
}

class Texture {
    private var texture = defaultTexture
    private val references = AtomicInteger()

    var tag: Any? = null

    fun addRef() {
        references.incrementAndGet()
    }

    fun release(): Boolean {
        if(references.decrementAndGet() <= 0) {
            return true
        }

        return false
    }

    fun isDestroyed() = references.get() <= 0

    fun delete() {
        if(texture == defaultTexture) {
            return
        }

        glDeleteTextures(texture)
    }

    fun loadFromBlp(layerData: LayeredTextureInfo) {
        if(layerData.format == TextureFormat.UNKNOWN) {
            return
        }

        if(texture == defaultTexture) {
            loadTexture(true, true)
        }

        bind()
        for(i in 0 until layerData.data.size) {
            val w = Math.max(1, layerData.width shr i)
            val h = Math.max(1, layerData.height shr i)
            val layer = layerData.data[i]
            val compressed = layerData.format != TextureFormat.ARGB8
            val buffer = BufferUtils.createByteBuffer(layer.size).put(layer).flip() as ByteBuffer
            if(compressed) {
                glCompressedTexImage2D(GL_TEXTURE_2D, i, layerData.format.native, w, h, 0, buffer)
            } else {
                glTexImage2D(GL_TEXTURE_2D, i, GL_RGBA, w, h, 0, layerData.format.native, GL_UNSIGNED_BYTE, buffer)
            }
        }
    }

    fun loadArgb(width: Int, height: Int, data: ByteBuffer) {
        if(texture == defaultTexture) {
            loadTexture(false)
        }

        bind()
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data)
    }

    fun loadArgb(width: Int, height: Int, data: IntArray) {
        if(texture == defaultTexture) {
            loadTexture(false)
        }

        val buffer = BufferUtils.createIntBuffer(data.size).put(data).flip() as IntBuffer
        bind()
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer)
    }

    fun loadArgb(source: ImageSource) {
        val data = source.buffer
        if(data.isEmpty()) {
            return
        }

        if(texture == defaultTexture) {
            loadTexture(false)
        }

        bind()
        val buffer = BufferUtils.createByteBuffer(data.size)
        buffer.put(data)
        buffer.flip()
        if(!source.isCompressed) {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, source.width, source.height, 0, source.format.textureFormat.native, GL_UNSIGNED_BYTE, buffer)
        } else {
            glCompressedTexImage2D(GL_TEXTURE_2D, 0, source.format.textureFormat.native, source.width, source.height, 0, buffer)
        }
    }

    fun bind() {
        glBindTexture(GL_TEXTURE_2D, texture)
    }

    fun setFiltering(min: TextureFilter, mag: TextureFilter) {
        bind()
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min.native)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag.native)
    }

    private fun loadTexture(mipMap: Boolean, repeat: Boolean = false) {
        val textures = intArrayOf(0)
        glGenTextures(textures)
        texture = textures[0]
        bind()
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, if(!repeat) GL_CLAMP_TO_EDGE else GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, if(!repeat) GL_CLAMP_TO_EDGE else GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, if(mipMap) GL_LINEAR_MIPMAP_LINEAR else GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, if(mipMap) GL_LINEAR else GL_NEAREST)
    }

    companion object {
        private var defaultTexture = 0

        fun initDefaultTexture() {
            val textures = intArrayOf(0)
            glGenTextures(textures)
            defaultTexture = textures[0]
            glBindTexture(GL_TEXTURE_2D, defaultTexture)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)

            val colors = intArrayOf(
                    0xFFFF0000.toInt(), 0xFF00FF00.toInt(),
                    0xFF0000FF.toInt(), 0xFFFF7F3F.toInt()
            )
            val buffer = BufferUtils.createIntBuffer(colors.size)
            buffer.put(colors)
            buffer.flip()
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer)
        }
    }
}