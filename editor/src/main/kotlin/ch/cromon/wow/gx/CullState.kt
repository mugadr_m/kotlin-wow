package ch.cromon.wow.gx

import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL11.*


enum class CullState(private val enabled: Boolean, private val frontFace: Int) {
    DISABLED(false, -1),
    CCW(true, GL11.GL_CCW),
    CW(true, GL11.GL_CW);

    fun apply() {
        if(!enabled) {
            glDisable(GL_CULL_FACE)
        } else {
            glEnable(GL_CULL_FACE)
            glFrontFace(frontFace)
        }
    }
}