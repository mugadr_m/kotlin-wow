package ch.cromon.wow.gx

import org.lwjgl.opengl.GL13.GL_TEXTURE0
import org.lwjgl.opengl.GL13.glActiveTexture

class TextureInput {
    lateinit var program: Program
    private val indices = ArrayList<Int>()
    private val textureMap = HashMap<Int, Texture>()

    fun add(index: Int, texture: Texture) {
        val alreadyAdded = textureMap.containsKey(index)
        textureMap[index] = texture
        if(alreadyAdded) {
            return
        }

        indices.add(index)
    }

    fun bind() {
        var index = 0
        while(index < indices.size) {
            val uniform = indices[index]
            val texture = textureMap[uniform]
            glActiveTexture(GL_TEXTURE0 + index)
            texture?.bind()
            program.setInt(uniform, index)
            ++index
        }
    }
}