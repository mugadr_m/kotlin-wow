package ch.cromon.wow.gx

import ch.cromon.wow.math.Vector2
import ch.cromon.wow.ui.WindowResizeEvent
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL20.GL_SHADING_LANGUAGE_VERSION
import org.lwjgl.opengl.GL30.*
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEvent
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

class GraphicsLoadedEvent(val context: GxContext, source: Any) : ApplicationEvent(source)

@Component
@Scope("singleton")
class GxContext {
    companion object {
        private val LOG = LoggerFactory.getLogger(GxContext::class.java)
    }

    private var frameBuffer = -1
    private var colorTexture = -1
    private var depthTexture = -1
    private var dimension = Vector2()

    fun beginFrame() {
        glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer)
        glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
    }

    fun endFrame() {
        glBindFramebuffer(GL_FRAMEBUFFER, 0)
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0)
        glBindFramebuffer(GL_READ_FRAMEBUFFER, frameBuffer)
        glBlitFramebuffer(0, 0, dimension.x.toInt(), dimension.y.toInt(), 0, 0, dimension.x.toInt(),
                dimension.y.toInt(), GL_COLOR_BUFFER_BIT, GL_LINEAR)
        glBindFramebuffer(GL_FRAMEBUFFER, 0)
    }

    fun onInitialize() {
        LOG.debug("Initializing GL context")

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f)
        glEnable(GL_CULL_FACE)
        glFrontFace(GL_CCW)
        glEnable(GL_POLYGON_OFFSET_FILL)

        val renderer = glGetString(GL_RENDERER)
        val vendor = glGetString(GL_VENDOR)
        val version = glGetString(GL_VERSION)
        val shaderVersion = glGetString(GL_SHADING_LANGUAGE_VERSION)

        LOG.info("OpenGL<renderer=$renderer, version=$version, vendor=$vendor, shaderLevel=$shaderVersion>")

        Texture.initDefaultTexture()
    }

    @EventListener
    fun onResize(event: WindowResizeEvent) {
        this.dimension = event.dimension
        glViewport(0, 0, event.dimension.x.toInt(), event.dimension.y.toInt())
        loadDepthBuffer()
    }

    private fun loadDepthBuffer() {
        val needCreate = frameBuffer < 0
        if(needCreate) {
            frameBuffer = glGenFramebuffers()
            colorTexture = glGenTextures()
            depthTexture = glGenRenderbuffers()
        }

        glBindTexture(GL_TEXTURE_2D, colorTexture)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, dimension.x.toInt(), dimension.y.toInt(), 0, GL_RGBA, GL_UNSIGNED_BYTE, 0)

        glBindRenderbuffer(GL_RENDERBUFFER, depthTexture)
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, dimension.x.toInt(), dimension.y.toInt())
        glBindRenderbuffer(GL_RENDERBUFFER, 0)

        if(needCreate) {
            glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer)
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorTexture, 0)
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthTexture)

            val status = glCheckFramebufferStatus(GL_FRAMEBUFFER)
            if (status != GL_FRAMEBUFFER_COMPLETE) {
                throw IllegalStateException("Unable to create frame buffer")
            }

            glBindFramebuffer(GL_FRAMEBUFFER, 0)
        }
    }
}