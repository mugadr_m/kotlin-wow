package ch.cromon.wow.gx

import org.lwjgl.opengl.GL11.*


enum class DepthCheck(private val enabled: Boolean, private val mode: Int) {
    DISABLED(false, -1),
    LESS(true, GL_LESS),
    LESS_EQUAL(true, GL_LEQUAL);

    fun apply() {
        if(enabled) {
            glEnable(GL_DEPTH_TEST)
            glDepthFunc(mode)
        } else {
            glDisable(GL_DEPTH_TEST)
        }
    }
}