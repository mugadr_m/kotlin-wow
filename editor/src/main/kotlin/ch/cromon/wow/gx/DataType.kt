package ch.cromon.wow.gx

import org.lwjgl.opengl.GL11.GL_FLOAT
import org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE


enum class DataType(val native: Int) {
    FLOAT(GL_FLOAT),
    BYTE(GL_UNSIGNED_BYTE)
}