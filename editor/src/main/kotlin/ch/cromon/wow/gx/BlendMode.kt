package ch.cromon.wow.gx

import org.lwjgl.opengl.GL11.*


enum class BlendMode {
    NONE { override fun apply() = glDisable(GL_BLEND) },
    ALPHA {
        override fun apply() {
            glEnable(GL_BLEND)
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        }
    },
    COLOR {
        override fun apply() {
            glEnable(GL_BLEND)
            glBlendFunc(GL_ONE, GL_ONE)
        }
    };

    abstract fun apply()
}