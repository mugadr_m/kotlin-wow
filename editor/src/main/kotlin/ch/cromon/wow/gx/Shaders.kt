package ch.cromon.wow.gx

class Shader private constructor(private val fs: String, private val vs: String) {
    val fragmentShader: String
        get() = fs

    val vertexShader: String
        get() = vs

    companion object {
        val TEXT = Shader("shaders/TextFragment.glsl", "shaders/TextVertex.glsl")
        val QUAD = Shader("shaders/QuadFragment.glsl", "shaders/QuadVertex.glsl")
        val LINE = Shader("shaders/LineFragment.glsl", "shaders/LineVertex.glsl")
        val MEMORY_CONTROL = Shader("shaders/MemoryControlFragment.glsl", "shaders/MemoryControlVertex.glsl")
        val IMAGE = Shader("shaders/ImageFragment.glsl", "shaders/ImageVertex.glsl")
        val SKY = Shader("shaders/SkyFragment.glsl", "shaders/SkyVertex.glsl")
        val TERRAIN_LOW = Shader("shaders/TerrainLowFragment.glsl", "shaders/TerrainLowVertex.glsl")
        val LIQUID = Shader("shaders/LiquidFragment.glsl", "shaders/LiquidVertex.glsl")
        val WMO_INDOOR = Shader("shaders/wmo/MapObjectFragmentIndoor.glsl", "shaders/wmo/MapObjectVertex.glsl")
        val WMO_OUTDOOR = Shader("shaders/wmo/MapObjectFragmentOutdoor.glsl", "shaders/wmo/MapObjectVertex.glsl")

        fun map(prefix: String): Shader = Shader("shaders/$prefix/MapFragment.glsl", "shaders/$prefix/MapVertex.glsl")
    }
}