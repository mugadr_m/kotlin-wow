package ch.cromon.wow.server

import ch.cromon.wow.server.web.mainClass
import org.springframework.boot.SpringApplication
import org.springframework.boot.SpringApplicationRunListener
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.core.env.ConfigurableEnvironment


@Suppress("UNUSED_PARAMETER")
class ApplicationListener(application: SpringApplication, args: Array<String>) : SpringApplicationRunListener {
    override fun running(context: ConfigurableApplicationContext?) {
        val stackTrace = Thread.currentThread().stackTrace
        mainClass = stackTrace[stackTrace.size - 1].className
    }

    override fun environmentPrepared(environment: ConfigurableEnvironment?) {

    }

    override fun started(context: ConfigurableApplicationContext?) {

    }

    override fun contextPrepared(context: ConfigurableApplicationContext?) {

    }

    override fun contextLoaded(context: ConfigurableApplicationContext?) {

    }

    override fun failed(context: ConfigurableApplicationContext?, exception: Throwable?) {

    }

    override fun starting() {

    }
}