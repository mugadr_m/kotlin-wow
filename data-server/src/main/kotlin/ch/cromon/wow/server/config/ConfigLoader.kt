package ch.cromon.wow.server.config

import org.apache.commons.lang3.ObjectUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import java.io.File
import java.nio.file.Path


@Component
@Scope("singleton")
class ConfigLoader {
    companion object {
        private val logger = LoggerFactory.getLogger(ConfigLoader::class.java)
    }

    @Value("\${server.name:#{null}}")
    private var serverNameProp: String? = null

    @Value("\${server.type:#{null}}")
    private var serverTypeProp: ServerType? = null

    @Value("\${server.data.location:#{null}}")
    private var dataLocationProp: String? = null

    val serverName: String get() = serverNameProp ?: throw IllegalStateException("No Server name defined")
    val serverType: ServerType get() = serverTypeProp ?: throw IllegalStateException("No server type defined")
    val dataLocation: String get() = dataLocationProp ?: throw IllegalStateException("No data location defined")

    private lateinit var dataPath: Path
    private lateinit var dataFolder: File

    private var isLoaded = false

    fun getFile(path: String) = File(dataFolder, path).toPath().toRealPath().toFile()

    fun isValidFile(path: String): Boolean {
        val file = File(dataFolder, path)
        val filePath = file.toPath().toRealPath()
        return filePath.startsWith(dataPath) && file.exists() && file.isFile
    }

    fun tryLoadConfig(): Boolean {
        if(isLoaded) {
            return true
        }

        if(!ObjectUtils.allNotNull(serverNameProp, serverTypeProp, dataLocationProp)) {
            return false
        }

        dataFolder = File(dataLocation)
        if(!dataFolder.exists() || !dataFolder.isDirectory) {
            logger.warn("Invalid path for data specified: $dataLocation. Must be an existing folder")
            return false
        }

        dataPath = dataFolder.toPath().toRealPath()
        isLoaded = true

        return true
    }
}