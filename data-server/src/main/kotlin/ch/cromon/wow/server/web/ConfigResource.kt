package ch.cromon.wow.server.web

import ch.cromon.wow.server.config.ConfigLoader
import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import java.io.File
import java.lang.management.ManagementFactory
import java.nio.charset.StandardCharsets
import javax.annotation.PostConstruct

lateinit var arguments: Array<out String>
lateinit var mainClass: String

@Controller
@RequestMapping("/config")
class ConfigResource {
    private val runtime = ManagementFactory.getRuntimeMXBean()

    @Autowired
    private lateinit var configLoader: ConfigLoader

    private var isConfigComplete = false

    @PostConstruct
    fun initialize() {
        isConfigComplete = configLoader.tryLoadConfig()
    }

    @GetMapping("config.html", produces = ["text/html"])
    fun getConfigPage(): ResponseEntity<String> {
        val viewName = if(isConfigComplete) "/web/complete.html" else "/web/config.html"
        return ConfigResource::class.java.getResourceAsStream(viewName)?.use {
            ResponseEntity.status(200).body(IOUtils.toString(it, StandardCharsets.UTF_8))
        } ?: ResponseEntity.status(500).build()
    }

    @GetMapping("api/folderCheck")
    fun checkFolder(@RequestParam("folder") folder: String): ResponseEntity<Any> {
        return when (File(folder).exists()) {
            true -> ResponseEntity.status(200).build()
            false -> ResponseEntity.status(400).build()
        }
    }

    @GetMapping("api/build")
    fun buildCmdLine(@RequestParam("name") name: String, @RequestParam("type") type: String, @RequestParam("folder") folder: String): ResponseEntity<String> {
        val inputArgs = runtime.inputArguments.filter {
            !it.contains("-Dserver.name") && !it.contains("-Dserver.type") && !it.contains("-Dserver.data.location")
        }.joinToString(" ")
        val classPath = runtime.classPath
        val programArgs = arguments.joinToString(" ")
        val javaPath = "${System.getProperty("java.home")}${File.separator}bin${File.separator}java"
        return ResponseEntity.status(200).body(
                "\"$javaPath\" $inputArgs -Dserver.name=\"$name\" -Dserver.type=$type -Dserver.data.location=\"$folder\" -classpath \"$classPath\" $mainClass $programArgs"
        )
    }

    @GetMapping("api/config", produces = [ "application/json" ])
    fun getConfig(): ResponseEntity<Any> {
        if(!isConfigComplete) {
            return ResponseEntity.status(400).build()
        }

        return ResponseEntity.ok(object {
            val name = configLoader.serverName
            val type = configLoader.serverType
            val location = configLoader.dataLocation
        })
    }
}