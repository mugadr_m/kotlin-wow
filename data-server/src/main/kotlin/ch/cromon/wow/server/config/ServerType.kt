package ch.cromon.wow.server.config


enum class ServerType {
    CLASSIC,
    BC,
    WOTLK,
    CATA,
    MOP,
    WOD,
    LEGION,
    BFA
}