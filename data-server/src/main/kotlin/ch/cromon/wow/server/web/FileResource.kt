package ch.cromon.wow.server.web

import ch.cromon.wow.server.config.ConfigLoader
import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import java.io.FileInputStream
import javax.servlet.http.HttpServletResponse


@Controller
class FileResource {
    @Autowired
    private lateinit var configLoader: ConfigLoader

    @GetMapping("/files/raw")
    fun getFile(@RequestParam file: String, response: HttpServletResponse) {
        if(!configLoader.isValidFile(file)) {
            response.status = 400
            return
        }

        FileInputStream(configLoader.getFile(file)).use {
            IOUtils.copy(it, response.outputStream)
        }
    }
}