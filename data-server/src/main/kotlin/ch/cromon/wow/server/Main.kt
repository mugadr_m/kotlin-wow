package ch.cromon.wow.server

import ch.cromon.wow.server.web.arguments
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication


@SpringBootApplication
class Application

fun main(vararg args: String) {
    arguments = args
    SpringApplication.run(Application::class.java, *args)
}