package ch.cromon.wow.server

import ch.cromon.wow.server.config.ConfigLoader
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.annotation.Scope
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component


@Component
@Scope("singleton")
class Server {
    companion object {
        private val logger = LoggerFactory.getLogger(Server::class.java)
    }

    @Autowired
    private lateinit var configLoader: ConfigLoader

    @EventListener
    fun afterRun(event: ApplicationReadyEvent) {
        logger.info("Initializing server")

        val hasValidConfig = configLoader.tryLoadConfig()
    }
}