#### Attributions
- DB2 implementations like WDB5 are 
converted to Kotlin from the great implementation of [WDBXEditor](https://github.com/WowDevTools/WDBXEditor) so all the research has been done
there and it has allowed implementation of Legion+ modding here.